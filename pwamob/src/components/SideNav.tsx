import React, { Fragment, SyntheticEvent, useState } from "react";
import Hamburger from "./Hamburger";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Toolbar,
  Typography,
  styled,
} from "@mui/material";
import Logo from "./Logo";
import { Close, XButton3, XButton4, XDarkTextButton } from "./StyledUI";
import { MdExpandMore } from "react-icons/md";
import { AiOutlineLogout } from "react-icons/ai";
import { BiUserCircle } from "react-icons/bi";
import SwitchTheme from "./SwitchTheme";
import { Link } from "react-router-dom";
import { useGetMenuDataQuery } from "../lib/menuApi";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { reset } from "../lib/formSlice";
import { logout } from "../lib/authSlice";

const SidenavWrapper = styled("div")`
  width: 400px;
  max-width: 100%;
  padding: 24px;
  @media (max-width: 400px) {
    padding: 0;
  }
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #fff;
  span: {
    font-weight: 700 !important;
  }
`;

export default function SideNav() {
  const { data, error, isLoading } = useGetMenuDataQuery(null);
  const { name, avatar, category } = useAppSelector(
    (state: ReduxState) => state.auth,
  );
  const dispatch = useAppDispatch();
  const [open, setOpen] = useState(false);
  const [expanded, setExpanded] = useState<string | false>(false);

  const handleChange =
    (panel: string) => (event: SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };

  const handleClick = () => {
    setOpen(!open);
  };

  if (data) {
    return (
      <Fragment>
        <div onClick={handleClick}>
          <Hamburger />
        </div>
        <Drawer
          anchor="left"
          open={open}
          onClose={handleClick}
          sx={{ maxWidth: "100%" }}
          PaperProps={{ sx: { backgroundColor: "#000", maxWidth: "100%" } }}
        >
          <SidenavWrapper>
            <Toolbar sx={{ display: "flex", justifyContent: "center" }}>
              <StyledLink to="/" onClick={() => setOpen(false)}>
                <Logo />
              </StyledLink>
            </Toolbar>
            <Box sx={{ position: "absolute", top: 0, right: 0 }}>
              <Close onClick={() => setOpen(false)} />
            </Box>

            <List sx={{ mt: 2 }}>
              {data.map((x) => (
                <div key={x.title}>
                  {x.children ? (
                    <Accordion
                      expanded={expanded === "panel1"}
                      onChange={handleChange("panel1")}
                      sx={{ background: "transparent", maxWidth: "100%" }}
                    >
                      <AccordionSummary
                        expandIcon={
                          <Box
                            sx={{
                              color: "#fff !important",
                              fontSize: "25px",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <MdExpandMore />
                          </Box>
                        }
                        aria-controls="panel1bh-content"
                        id="panel1bh-header"
                      >
                        <Typography
                          sx={{
                            width: "33%",
                            flexShrink: 0,
                            color: "#fff",
                            fontWeight: 700,
                          }}
                        >
                          {x.title}
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails sx={{ paddingBlock: 0 }}>
                        <List sx={{ paddingBlock: 0 }}>
                          {x.children.map((y: any) => (
                            <ListItemButton key={y.title}>
                              <StyledLink
                                to={`/${y.link}`}
                                onClick={() => setOpen(false)}
                              >
                                <ListItemText primary={y.title} />
                              </StyledLink>
                            </ListItemButton>
                          ))}
                        </List>
                      </AccordionDetails>
                    </Accordion>
                  ) : (
                    <ListItemButton>
                      <StyledLink
                        to={`/${x.link}`}
                        onClick={() => setOpen(false)}
                      >
                        <ListItemText primary={x.title} />
                      </StyledLink>
                    </ListItemButton>
                  )}
                </div>
              ))}
              <Divider />
              {name && avatar ? (
                <Fragment>
                  <ListItem>
                    <Link to="/cabinet" onClick={() => setOpen(false)}>
                      <XButton4>Кабинет</XButton4>
                    </Link>
                  </ListItem>
                  <ListItem>
                    <XDarkTextButton
                      sx={{ color: "#777", bordercolor: "#777" }}
                      startIcon={<AiOutlineLogout />}
                      variant="outlined"
                      onClick={async () => {
                        await dispatch(reset());
                        await dispatch(logout());
                        setOpen(false);
                      }}
                    >
                      Выйти
                    </XDarkTextButton>
                  </ListItem>
                </Fragment>
              ) : (
                <Fragment>
                  <ListItem>
                    <StyledLink to="/register" onClick={() => setOpen(false)}>
                      <XButton3 variant="outlined">Регистрация</XButton3>
                    </StyledLink>
                  </ListItem>
                  <ListItem>
                    <StyledLink to="/login" onClick={() => setOpen(false)}>
                      <XButton4
                        sx={{ color: "#777" }}
                        startIcon={<BiUserCircle />}
                        variant="text"
                      >
                        Вход
                      </XButton4>
                    </StyledLink>
                  </ListItem>
                </Fragment>
              )}
              <ListItem>
                <SwitchTheme />
              </ListItem>
            </List>
          </SidenavWrapper>
        </Drawer>
      </Fragment>
    );
  } else return null;
}
