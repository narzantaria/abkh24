import React from "react";
import {
  BachgroundContain,
  StyledBorder,
  StyledLink,
  Title3,
} from "./StyledUI";
import { Box, Grid, Stack } from "@mui/material";
import { IFuneral } from "../misc/types";

export default function PrevFuneral({
  id,
  title,
  text,
  createdAt,
  dir,
  photo,
}: IFuneral) {
  return (
    <div>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={5}>
          <StyledLink to={`/funerals/${id}`}>
            <StyledBorder>
              <BachgroundContain url={`${dir}/${photo}`} />
            </StyledBorder>
          </StyledLink>
        </Grid>
        <Grid item xs={12} sm={7}>
          <Stack spacing={2}>
            <Title3 variant="h5">
              <StyledLink to={`/funerals/${id}`}>{title}</StyledLink>
            </Title3>
            <Box sx={{ fontSize: "13px" }}>
              <time>{new Date(createdAt).toISOString().slice(0, 10)}</time>
            </Box>
          </Stack>
        </Grid>
      </Grid>
    </div>
  );
}
