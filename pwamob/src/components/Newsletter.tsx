import { Title5, XButtonFW } from "../components/StyledUI";
import { Box, Snackbar, Stack, TextField, Typography } from "@mui/material";
import axios from "axios";
import React, { ChangeEvent, Fragment, useState } from "react";
import ThemeWrapper from "./ThemeWrapper";
import { ReduxState, useAppSelector } from "../lib";

const VITE_API = import.meta.env.VITE_API;

export default function Newsletter() {
  const [data, setData] = useState<string>("");
  const [message, setMessage] = useState<string>("");
  const { color } = useAppSelector((state: ReduxState) => state?.theme);

  return (
    <Fragment>
      <ThemeWrapper>
        <Box
          sx={{
            p: 4,
            border: "1px solid #eee",
            backgroundColor: color === "dark" ? "#000" : "initial",
          }}
        >
          <Stack spacing={3} sx={{ textAlign: "center" }}>
            <Title5 variant="h5">Подписаться на новости</Title5>
            <Typography variant="caption">
              Будьте в курсе интересных собятий и важных обновлений нашего сайта
            </Typography>
            <TextField
              required
              id="outlined-required"
              label="Email"
              value={data}
              onChange={(event: ChangeEvent<HTMLInputElement>) => {
                setData(event.target.value);
              }}
              sx={{
                bordercolor: color === "dark" ? "#333" : "initial",
                backgroundColor: color === "dark" ? "#1a1a1a" : "initial",
              }}
            />
            <XButtonFW
              onClick={() => {
                axios
                  .post(`${VITE_API}/api/subscribers`, { email: data })
                  .then(async (res) => {
                    setMessage("Подписка оформлена");
                  })
                  .catch((err) => {
                    console.log(err);
                    setMessage("Whoops! Something went wrong...");
                  });
              }}
            >
              Отправить
            </XButtonFW>
          </Stack>
        </Box>
      </ThemeWrapper>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
