import { useState } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { Box, styled } from "@mui/material";
import { GREEN_CLR } from "../misc/tv";

const Indicator = styled("div")`
  margin-top: -30px;
  z-index: 1;
  position: absolute;
  display: flex;
  justify-content: center;
  gap: 20px;
  width: 650px;
  max-width: 100%;
`;

const SlideDirection = styled("div")`
  display: flex;
  justify-content: space-between;
`;

const CarouselImages = styled("div")`
  position: relative;
  width: 650px;
  max-width: 100%;
  overflow: hidden;
  img {
    width: 100%;
  }
`;

const controlStyles: { [key: string]: any } = {
  backgroundColor: GREEN_CLR,
  color: "#fff",
  padding: "10px 8px 8px 13px",
  margin: "auto 10px",
  borderRadius: "50%",
  position: "absolute",
  top: "0",
  bottom: "0",
  height: "25px",
  width: "25px",
  cursor: "pointer",
};

const dotStyles: { [key: string]: any } = {
  width: "15px",
  height: "15px",
  borderRadius: "50%",
  cursor: "pointer",
};

const PhotoSlider = ({ images }: { images: string[] }) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const [direction, setDirection] = useState<string | null>(null);

  const slideVariants = {
    hiddenRight: {
      x: "100%",
      opacity: 0,
    },
    hiddenLeft: {
      x: "-100%",
      opacity: 0,
    },
    visible: {
      x: "0",
      opacity: 1,
      transition: {
        duration: 1,
      },
    },
    exit: {},
  };
  const dotsVariants = {
    initial: {
      y: 0,
    },
    animate: {
      transition: { type: "spring", stiffness: 1000, damping: "10" },
    },
    hover: {
      scale: 1.1,
      transition: { duration: 0.2 },
    },
  };

  const handleNext = () => {
    setDirection("right");
    setCurrentIndex((prevIndex) =>
      prevIndex + 1 === images.length ? 0 : prevIndex + 1,
    );
  };

  const handlePrevious = () => {
    setDirection("left");

    setCurrentIndex((prevIndex) =>
      prevIndex - 1 < 0 ? images.length - 1 : prevIndex - 1,
    );
  };

  const handleDotClick = (index: number) => {
    setDirection(index > currentIndex ? "right" : "left");
    setCurrentIndex(index);
  };

  return (
    <Box sx={{ position: "relative" }}>
      <CarouselImages>
        <AnimatePresence>
          <motion.img
            key={currentIndex}
            src={images[currentIndex]}
            initial={direction === "right" ? "hiddenRight" : "hiddenLeft"}
            animate="visible"
            exit="exit"
            variants={slideVariants}
          />
        </AnimatePresence>
        <SlideDirection>
          <motion.div
            whileHover="hover"
            style={{ ...controlStyles, left: 0 }}
            onClick={handlePrevious}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="20"
              viewBox="0 96 960 960"
              width="20"
            >
              <path d="M400 976 0 576l400-400 56 57-343 343 343 343-56 57Z" />
            </svg>
          </motion.div>
          <motion.div
            whileHover="hover"
            style={{ ...controlStyles, right: 0 }}
            onClick={handleNext}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="20"
              viewBox="0 96 960 960"
              width="20"
            >
              <path d="m304 974-56-57 343-343-343-343 56-57 400 400-400 400Z" />
            </svg>
          </motion.div>
        </SlideDirection>
      </CarouselImages>
      <Indicator>
        {images.map((_, index) => (
          <motion.div
            key={index}
            style={{
              ...dotStyles,
              backgroundColor: currentIndex === index ? GREEN_CLR : "#333",
            }}
            onClick={() => handleDotClick(index)}
            initial="initial"
            animate={currentIndex === index ? "animate" : ""}
            whileHover="hover"
            variants={dotsVariants}
          ></motion.div>
        ))}
      </Indicator>
    </Box>
  );
};
export default PhotoSlider;
