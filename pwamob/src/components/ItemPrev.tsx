import React from "react";
import {
  BachgroundContain,
  Caption,
  StyledBorder,
  TagButton,
} from "./StyledUI";
import { Card, CardContent, Typography } from "@mui/material";
import { Item } from "../misc/types";
import { GREEN_CLR } from "../misc/tv";

export default function ItemPrev({
  id,
  dir,
  district,
  photos,
  price,
  title,
  source,
}: Item) {
  return (
    <Card sx={{ width: "100%" }}>
      <StyledBorder>
        <BachgroundContain
          url={dir && photos ? `${dir}/${photos[0]}` : "common/picture.png"}
        />
        <Caption>
          <TagButton background={GREEN_CLR}>{district}</TagButton>
        </Caption>
      </StyledBorder>
      <CardContent sx={{ p: "16px !important" }}>
        <Typography
          gutterBottom
          variant="h6"
          component="div"
          sx={{
            overflow: "hidden",
            display: "-webkit-box",
            WebkitLineClamp: 1,
            lineClamp: 1,
            WebkitBoxOrient: "vertical",
            textOverflow: "ellipsis",
          }}
        >
          {title}
        </Typography>
        <Typography sx={{ fontSize: 16 }} color="text.secondary">
          {`${price} ₽`}
        </Typography>
      </CardContent>
    </Card>
  );
}
