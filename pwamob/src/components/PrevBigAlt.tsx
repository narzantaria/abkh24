import React, { Fragment, useState } from "react";
import { styled, Box } from "@mui/material";
import {
  BackgroundImage,
  Border,
  Caption,
  Overlay,
  StyledLink,
  TagButton,
  Title3,
} from "./StyledUI";
import { IPost } from "../misc/types";
import LocalVideoPrev from "./LocalVideoPrev";

const StyledBorder = styled(Border)`
  height: 333px;
`;

interface IProps extends IPost {
  params: string[];
}

export default function PrevBigAlt({
  author,
  tags,
  createdAt,
  dir,
  id,
  photos,
  title,
  tagClr,
  video,
  params,
}: IProps) {
  const [hover, setHover] = useState<boolean>(false);

  return (
    <StyledLink to={`/news/${id}`}>
      <StyledBorder
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        {photos?.length ? (
          <BackgroundImage url={`${dir}/${photos[0]}`} hover={hover} />
        ) : video ? (
          <LocalVideoPrev dir={dir} video={video} />
        ) : (
          <BackgroundImage url="common/main.jpg" hover={hover} />
        )}
        <Overlay hover={hover} />
        <Caption spacing={2}>
          <TagButton
            background={tagClr?.background}
            bordercolor={tagClr?.bordercolor}
            txtcolor={tagClr?.color}
            size="small"
          >
            {tags[0]}
          </TagButton>
          <Title3 variant="h5">{title}</Title3>
          <Box sx={{ color: "#efefef", fontSize: "13px" }}>
            {author && (
              <Fragment>
                <span>{author[0].name}</span> -{" "}
              </Fragment>
            )}
            <span>{new Date(createdAt).toISOString().slice(0, 10)}</span>
          </Box>
        </Caption>
      </StyledBorder>
    </StyledLink>
  );
}
