import { LS } from "../misc/tv";
import { Box, Skeleton, Stack } from "@mui/material";
import React from "react";
import { v4 as uuidv4 } from "uuid";
import { XContainer } from "./StyledUI";

const data1 = Array.from({ length: 2 }).map(() => uuidv4());
const data2 = Array.from({ length: 2 }).map(() => uuidv4());

export default function NodataForm() {
  return (
    <Box sx={{ paddingBlock: LS }}>
      <XContainer>
        <Stack spacing={3}>
          {data1.map((x) => (
            <Skeleton
              variant="rectangular"
              height={20}
              sx={{ backgroundColor: "#777" }}
              key={x}
            />
          ))}
          <Skeleton variant="circular" width={200} height={200} />
          {data2.map((x) => (
            <Skeleton
              variant="rectangular"
              height={20}
              sx={{ backgroundColor: "#777" }}
              key={x}
            />
          ))}
        </Stack>
      </XContainer>
    </Box>
  );
}
