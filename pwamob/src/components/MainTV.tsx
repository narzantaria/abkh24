import React from "react";
import { XContainer } from "./StyledUI";
import { Box, Grid, useMediaQuery } from "@mui/material";
import { XLS } from "../misc/tv";
import SectionTitleMid from "./SectionTitleMid";
import { tagColor } from "../misc/tools";
import VPrev from "./VPrev";
import ThemeWrapper from "./ThemeWrapper";

interface ITV {
  id: string;
  title: string;
  videoId: string;
  source: string;
  createdAt: Date;
}

export default function MainTV({ data }: { data: ITV[] }) {
  const matches = useMediaQuery("(max-width:576px)");

  if (data) {
    return (
      <Box sx={{ pb: XLS }}>
        <XContainer>
          <SectionTitleMid first="Новости" second="ТВ" />
          <ThemeWrapper>
            <Grid container spacing="4px">
              {data.slice(0, 2).map((x) => (
                <Grid item xs={12} key={x.id}>
                  <VPrev tagClr={tagColor(x.source)} {...x} />
                </Grid>
              ))}
              {data.slice(2, 8).map((x) => (
                <Grid item xs={12} key={x.id}>
                  <VPrev tagClr={tagColor(x.source)} {...x} />
                </Grid>
              ))}
            </Grid>
          </ThemeWrapper>
        </XContainer>
      </Box>
    );
  } else return null;
}
