import { IPost } from "../misc/types";
import { Stack } from "@mui/material";
import React from "react";
import { v4 as uuidv4 } from "uuid";
import NodataLines from "./NodataLines";
import Newsletter from "./Newsletter";
import MiscApi from "./MiscApi";
import { useGetMainDataQuery } from "../lib/mainApi";

export default function MainSider() {
  const mainData = useGetMainDataQuery(null);
  const data: IPost[] | null =
    mainData.data?.length && mainData.data[0].popular?.length
      ? mainData.data[0].popular.map((x: IPost) => {
          return { ...x, key: uuidv4() };
        })
      : null;

  if (data) {
    return (
      <Stack spacing={3}>
        <MiscApi />
        <Newsletter />
      </Stack>
    );
  } else return <NodataLines />;
}
