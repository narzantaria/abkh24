import { Box, Grid, Skeleton, Stack, Typography, styled } from "@mui/material";
import React from "react";
import { XContainer } from "./StyledUI";
import Logo from "./Logo";
import PrevMinFull from "./PrevMinFull";
import PrevLi from "./PrevLi";
import { v4 as uuidv4 } from "uuid";
import { useGetMainDataQuery } from "../lib/mainApi";
import { useGetSettingsDataQuery } from "../lib/settingsApi";
import { IPost, ISettings } from "../misc/types";

const FooterTitle = styled(Typography)``;

const StyledUl = styled("ul")`
  padding-left: 0;
`;

export default function UpperFooter() {
  const mainData = useGetMainDataQuery(null);
  const settingsData = useGetSettingsDataQuery(null);

  const data: IPost[] | null =
    mainData.data?.length && mainData.data[0].upper?.length
      ? mainData.data[0].upper.map((x: IPost) => {
          return { ...x, key: uuidv4() };
        })
      : null;

  const popular: IPost[] | null =
    mainData.data?.length && mainData.data[0].popular?.length
      ? mainData.data[0].popular.map((x: IPost) => {
          return { ...x, key: uuidv4() };
        })
      : null;

  const settings: ISettings | null = settingsData.data || null;

  if (data && popular && settings) {
    return (
      <Box sx={{ paddingBlock: 5, backgroundColor: "#0f0f11" }}>
        <XContainer>
          <Grid container spacing={5}>
            <Grid item sm={12}>
              <Stack spacing={4}>
                <FooterTitle variant="h5">Общая информация</FooterTitle>
                <Logo />
                <p>{settings.title}</p>
                <p>{settings.description}</p>
              </Stack>
            </Grid>
            <Grid item sm={12}>
              <Stack spacing={4}>
                <FooterTitle
                  variant="h5"
                  onClick={() => {
                    console.log(data);
                  }}
                >
                  Последние новости
                </FooterTitle>
                {data.map((x: IPost, i: number) => (
                  <PrevMinFull {...x} key={x.key} />
                ))}
              </Stack>
            </Grid>
            <Grid item sm={12}>
              <Stack spacing={4}>
                <FooterTitle variant="h5">Популярное</FooterTitle>
                <StyledUl>
                  {popular.slice(0, 5).map((x: IPost, i: number) => (
                    <PrevLi {...x} key={x.key} />
                  ))}
                </StyledUl>
              </Stack>
            </Grid>
          </Grid>
        </XContainer>
      </Box>
    );
  } else return <MockData />;
}

function MockData() {
  const data = Array.from({ length: 3 }).map(() => {
    return {
      id: uuidv4(),
      children: Array.from({ length: 3 }).map(() => {
        return uuidv4();
      }),
    };
  });
  return (
    <Box sx={{ paddingBlock: 5, backgroundColor: "#0f0f11" }}>
      <XContainer>
        <Grid container spacing={5}>
          {data.map((x) => (
            <Grid item sm={12} key={x.id}>
              <Stack spacing={4}>
                {x.children.map((y) => (
                  <Skeleton
                    variant="rectangular"
                    height={85}
                    sx={{ backgroundColor: "#777" }}
                    key={y}
                  />
                ))}
              </Stack>
            </Grid>
          ))}
        </Grid>
      </XContainer>
    </Box>
  );
}
