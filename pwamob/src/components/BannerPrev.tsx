import React, { useEffect, useState } from "react";
import { BackgroundImage, Border, Overlay } from "./StyledUI";
import { styled } from "@mui/material";
import { IBanner } from "../misc/types";

const StyledBorder = styled(Border)`
  padding-bottom: 65%;
`;

export default function BannerPrev({ dir, img }: IBanner) {
  const [hover, setHover] = useState<boolean>(false);

  useEffect(() => {
    function delay(ms: number): Promise<void> {
      return new Promise((resolve) => {
        setTimeout(resolve, ms);
      });
    }
    delay(3000);
  }, []);

  return (
    <StyledBorder
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <BackgroundImage url={`${dir}/${img}`} hover={hover} />
      <Overlay hover={hover} />
    </StyledBorder>
  );
}
