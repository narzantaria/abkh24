import React from "react";
import { XDarkIconButton2 } from "./StyledUI";
import { FaRegMoon, FaRegSun } from "react-icons/fa";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { setTheme } from "../lib/themeSlice";
import { BsSun } from "react-icons/bs";

export default function SwitchTheme() {
  const { color } = useAppSelector((state: ReduxState) => state?.theme);
  const dispatch = useAppDispatch();

  return (
    <XDarkIconButton2
      aria-label="login"
      onClick={() => {
        if (color === "light") {
          dispatch(setTheme("dark"));
        } else {
          dispatch(setTheme("light"));
        }
      }}
    >
      {color === "light" ? <BsSun /> : <FaRegMoon />}
    </XDarkIconButton2>
  );
}
