import { Box, styled } from "@mui/material";
import React, { ReactNode } from "react";
import { ReduxState, useAppSelector } from "../lib";

interface IProps {
  children: ReactNode;
}

const StyledBox = styled(Box)(({ thx }: { thx: "light" | "dark" }) => {
  return `
  div, a, label, span, .__comment { color: ${thx === "light" ? "initial" : "#fff"}; }
  .MuiSelect-select { color: ${thx === "light" ? "initial" : "#888"}; }
  .w-e-text-container, .__comment { 
    background-color: ${thx === "light" ? "#fff" : "#151516"};
    border-color: ${thx === "light" ? "initial" : "#fff"};
  }
  `;
});

export default function NegativeWrapper({ children }: IProps) {
  const { color } = useAppSelector((state: ReduxState) => state?.theme);

  return <StyledBox thx={color}>{children}</StyledBox>;
}
