import React from "react";
import { Box, styled } from "@mui/material";
import { LS } from "../misc/tv";
import { IBanner } from "../misc/types";

const VITE_API = import.meta.env.VITE_API;

const StyledImg = styled("img")`
  max-width: 100%;
`;

export default function BigBanner({ url, img, dir, title }: IBanner) {
  return (
    <Box sx={{ paddingBlock: LS }}>
      <a href={url} target="_blank" rel="noopener noreferrer">
        <StyledImg src={`${VITE_API}/dist/${dir}/${img}`} alt={title} />
      </a>
    </Box>
  );
}
