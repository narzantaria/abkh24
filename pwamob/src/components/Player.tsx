import React from "react";
import ReactPlayer from "react-player";

export default function Player({ url }: { url: string }) {
  return <ReactPlayer style={{ maxWidth: "100%" }} controls url={url} />;
}
