import React, { useEffect, useState } from "react";
import { XButton4, XContainer } from "./StyledUI";
import {
  FormControl,
  FormControlLabel,
  FormControlLabelProps,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Radio,
  RadioGroup,
  Stack,
  styled,
  useRadioGroup,
} from "@mui/material";
import { ISparams, setSparams } from "../lib/searchSlice";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { useNavigate } from "react-router-dom";

interface StyledFormControlLabelProps extends FormControlLabelProps {
  checked: boolean;
}

const StyledFormControlLabel = styled((props: StyledFormControlLabelProps) => (
  <FormControlLabel {...props} />
))(({ theme, checked }) => ({
  ".MuiFormControlLabel-label": checked && {
    color: theme.palette.primary.main,
  },
}));

function MyFormControlLabel(props: FormControlLabelProps) {
  const radioGroup = useRadioGroup();

  let checked = false;

  if (radioGroup) {
    checked = radioGroup.value === props.value;
  }

  return <StyledFormControlLabel checked={checked} {...props} />;
}

const initialParams: ISparams = { path: "posts", word: "" };

export default function SearchBar() {
  const search = useAppSelector((state: ReduxState) => state.search);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useState<ISparams>(initialParams);

  useEffect(() => {
    setSearchParams(initialParams);
  }, [search]);

  return (
    <XContainer>
      <Stack spacing={3}>
        <FormControl sx={{ width: "100%" }} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-password">
            Поиск по сайту
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type="text"
            value={searchParams.word}
            onChange={(e) =>
              setSearchParams({ ...searchParams, word: e.target.value })
            }
            endAdornment={
              <InputAdornment position="end">
                <XButton4
                  onClick={async () => {
                    await dispatch(setSparams(searchParams));
                    navigate("/search");
                  }}
                >
                  Найти
                </XButton4>
              </InputAdornment>
            }
            label="Password"
          />
        </FormControl>
        <RadioGroup
          row
          name="use-radio-group"
          defaultValue="posts"
          value={searchParams.path}
          onChange={(e) =>
            setSearchParams({ ...searchParams, path: e.target.value })
          }
        >
          <MyFormControlLabel
            value="posts"
            label="Новости"
            control={<Radio />}
          />
          <MyFormControlLabel
            value="goods"
            label="Объявления"
            control={<Radio />}
          />
        </RadioGroup>
      </Stack>
    </XContainer>
  );
}
