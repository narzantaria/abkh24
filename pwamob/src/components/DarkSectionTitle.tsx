import { GREEN_CLR, MS } from "../misc/tv";
import { Typography, styled } from "@mui/material";
import React from "react";
import { Coloured } from "./StyledUI";

const MainWrapper = styled("div")`
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: space-between;
  margin-bottom: ${MS};
  &:after {
    content: 
    margin-top: 1px;
    height: 1px;
    background-color: #333;
    flex: 1 1 auto;
    margin-left: 20px;
  }
`;

const StyledTitle = styled(Typography)`
  color: ${GREEN_CLR};
`;

export default function DarkSectionTitle({
  first,
  second,
}: {
  first?: string;
  second: string;
}) {
  return (
    <MainWrapper>
      <StyledTitle variant="h5">
        {first ? first + " " : ""}
        <Coloured>{second}</Coloured>
      </StyledTitle>
    </MainWrapper>
  );
}
