import React, { Fragment, useState } from "react";
import {
  BackgroundImage,
  Border,
  StyledLink,
  TagButtonSharp,
  Title3,
  Turkish,
} from "./StyledUI";
import { Box, Stack, styled } from "@mui/material";
import { IPost } from "../misc/types";
import LocalVideoPrev from "./LocalVideoPrev";

const StyledBorder = styled(Border)`
  padding-bottom: 65%;
`;

const PrevTitle = styled(Title3)(
  ({ textclr }: { textclr?: string | null }) => `
overflow: hidden;
text-overflow: ellipsis;
display: -webkit-box;
-webkit-box-orient: vertical;
-webkit-line-clamp: 2;
color: ${textclr || "initial"}
`,
);

export default function Prev({
  author,
  createdAt,
  dir,
  id,
  photos,
  title,
  textclr,
  tagClr,
  video,
  tags,
}: IPost) {
  const [hover, setHover] = useState<boolean>(false);

  return (
    <Stack
      spacing={3}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <StyledLink to={`/news/${id}`}>
        <StyledBorder>
          {/* {photos?.length ? (
            <BackgroundImage url={`${dir}/${photos[0]}`} hover={hover} />
          ) : null}
          {video ? <LocalVideoPrev dir={dir} video={video} /> : null} */}
          {photos?.length ? (
            <BackgroundImage url={`${dir}/${photos[0]}`} hover={hover} />
          ) : video ? (
            <LocalVideoPrev dir={dir} video={video} />
          ) : (
            <BackgroundImage url="common/main.jpg" hover={hover} />
          )}
          <Turkish hover={hover} />
          <TagButtonSharp
            background={tagClr?.background}
            bordercolor={tagClr?.bordercolor}
            txtcolor={tagClr?.color}
            size="small"
          >
            {tags[0]}
          </TagButtonSharp>
        </StyledBorder>
      </StyledLink>
      <PrevTitle variant="h5" textclr={textclr}>
        <StyledLink to={`/news/${id}`}>{title}</StyledLink>
      </PrevTitle>
      <Box sx={{ fontSize: "13px" }}>
        {author && (
          <Fragment>
            <span className="__author">{author[0].name}</span> -{" "}
          </Fragment>
        )}
        <time onClick={() => console.log(tagClr)}>
          {new Date(createdAt).toISOString().slice(0, 10)}
        </time>
      </Box>
    </Stack>
  );
}
