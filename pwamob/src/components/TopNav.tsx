import React from "react";
import { AppBar, Button, Menu, MenuItem, styled } from "@mui/material";
import { CenteredToolbar, StyledLink } from "./StyledUI";
import ConditionalWrapper from "./ConditionalWrapper";
import { useGetMenuDataQuery } from "../lib/menuApi";

const StyledAppBar = styled(AppBar)`
  position: relative;
  background: inherit;
`;

export default function TopNav() {
  const { data, error, isLoading } = useGetMenuDataQuery(null);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  if (data) {
    return (
      <StyledAppBar>
        <CenteredToolbar>
          {data.map((x, i: number) => (
            <div key={i}>
              <Button
                color="inherit"
                aria-controls={open && x.children ? x.title : undefined}
                aria-haspopup="true"
                aria-expanded={open && x.children ? "true" : undefined}
                onClick={x.children && handleClick}
              >
                <ConditionalWrapper
                  condition={Boolean(x.link)}
                  render={(props) => (
                    <StyledLink to={`/${x.link}`}>{props}</StyledLink>
                  )}
                >
                  {x.title}
                </ConditionalWrapper>
              </Button>
              {x.children ? (
                <Menu
                  id={x.title}
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                >
                  {x.children.map((y: any) => (
                    <StyledLink to={`/${y.link}`} key={y.title}>
                      <MenuItem onClick={handleClose}>{y.title}</MenuItem>
                    </StyledLink>
                  ))}
                </Menu>
              ) : null}
            </div>
          ))}
        </CenteredToolbar>
      </StyledAppBar>
    );
  } else return null;
}
