import React from "react";
import { XContainer } from "./StyledUI";
import { Box, Grid } from "@mui/material";
import { LS } from "../misc/tv";
import { IBanner } from "../misc/types";
import BannerPrev from "./BannerPrev";

export default function Banners({ data }: { data: IBanner[] }) {
  return (
    <Box sx={{ paddingBlock: LS }}>
      <XContainer>
        <Grid container spacing={3}>
          {data.map((x) => (
            <Grid item sm={12} md={4}>
              <BannerPrev {...x} key={x.id} />
            </Grid>
          ))}
        </Grid>
      </XContainer>
    </Box>
  );
}
