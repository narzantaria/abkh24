import {
  Avatar,
  Box,
  Modal,
  Stack,
  styled,
  useMediaQuery,
} from "@mui/material";
import React, { Fragment, useState } from "react";
import {
  HeaderButtons,
  StyledLink,
  XButton4,
  XButton5,
  XContainer,
  XDarkIconButton,
} from "./StyledUI";
import Logo from "./Logo";
import { BiSearch, BiUserCircle } from "react-icons/bi";
import SideNav from "./SideNav";
import SearchBar from "./SearchBar";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";
import { useIsMount } from "../misc/tools";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";

const MainWrapper = styled("div")`
  background-color: #000;
  padding-block: 25px;
  @media (max-width: 576px) {
    padding-block: 20px;
  }
`;

const InnerContent = styled("div")`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
`;

const style = {
  position: "absolute" as const,
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "#121212",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  maxWidth: "95%",
};

const btnStyle = {
  width: "100%",
};

export default function SuperHeader() {
  const matches = useMediaQuery("(min-width:992px)");

  const { name, avatar, category } = useAppSelector(
    (state: ReduxState) => state.auth,
  );
  const dispatch = useAppDispatch();
  const isMount = useIsMount();

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [isVisible, setIsVisible] = useState(false);

  const handleButtonClick = () => {
    setIsVisible(!isVisible);
  };

  const containerVariants = {
    hidden: { height: 0 },
    visible: { height: "auto", transition: { duration: 0.3 } },
  };

  return (
    <Fragment>
      <MainWrapper>
        <XContainer>
          <InnerContent>
            {/* {matches ? (
              <Social facebook="fb" instagram="ins" vk="vk" />
            ) : (
              <Box sx={{ display: "flex", alignItems: "center" }}>
                <SideNav />
              </Box>
            )} */}
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <SideNav />
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Link to="/">
                <Logo />
              </Link>
            </Box>
            <HeaderButtons>
              {/* {matches ? (
                <Fragment>
                  {name && avatar ? (
                    <Stack direction="row" spacing={2}>
                      <Link to="/cabinet">
                        <Avatar alt={name} src={avatar} />
                      </Link>
                      <XDarkTextButton
                        sx={{ color: "#777", bordercolor: "#777" }}
                        startIcon={<AiOutlineLogout />}
                        variant="outlined"
                        onClick={async () => {
                          if (category === "google") signOut(auth);
                          await dispatch(reset());
                          await dispatch(logout());
                        }}
                      >
                        Выйти
                      </XDarkTextButton>
                    </Stack>
                  ) : (
                    <Fragment>
                      <StyledLink to="/register">
                        <XButton3 variant="outlined">Регистрация</XButton3>
                      </StyledLink>
                      <StyledLink to="/login">
                        <XDarkTextButton
                          sx={{ color: "#777" }}
                          startIcon={<BiUserCircle />}
                          variant="text"
                        >
                          Вход
                        </XDarkTextButton>
                      </StyledLink>
                    </Fragment>
                  )}
                </Fragment>
              ) : (
                <Fragment>
                  {name && avatar ? (
                    <Link to="/cabinet">
                      <Avatar alt={name} src={avatar} />
                    </Link>
                  ) : (
                    <XDarkIconButton aria-label="login" onClick={handleOpen}>
                      <BiUserCircle />
                    </XDarkIconButton>
                  )}
                  <XDarkIconButton
                    sx={{ transform: "translateY(3px)" }}
                    aria-label="login"
                    onClick={handleButtonClick}
                  >
                    <BiSearch />
                  </XDarkIconButton>
                </Fragment>
              )} */}
              <Fragment>
                {avatar ? (
                  <Link to="/cabinet">
                    <Avatar alt={name || "anon"} src={avatar} />
                  </Link>
                ) : (
                  <XDarkIconButton aria-label="login" onClick={handleOpen}>
                    <BiUserCircle />
                  </XDarkIconButton>
                )}
                <XDarkIconButton
                  sx={{ transform: "translateY(3px)" }}
                  aria-label="login"
                  onClick={handleButtonClick}
                >
                  <BiSearch />
                </XDarkIconButton>
              </Fragment>
            </HeaderButtons>
          </InnerContent>
        </XContainer>
      </MainWrapper>
      <motion.div
        style={{ width: "100%", overflow: "hidden" }}
        variants={containerVariants}
        initial={false}
        animate={isVisible ? "visible" : "hidden"}
      >
        <div
          style={{
            paddingBlock: "50px",
            width: "100%",
            borderBottom: "1px solid #ddd",
            backgroundColor: "#f0f0f0",
          }}
        >
          <SearchBar />
        </div>
      </motion.div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Stack spacing={4}>
            <StyledLink to="/login" onClick={handleClose}>
              <XButton4 sx={btnStyle}>Вход</XButton4>
            </StyledLink>
            <StyledLink to="/register" onClick={handleClose}>
              <XButton5 sx={btnStyle}>Регистрация</XButton5>
            </StyledLink>
          </Stack>
        </Box>
      </Modal>
    </Fragment>
  );
}
