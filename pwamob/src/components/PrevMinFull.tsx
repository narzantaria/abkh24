import React, { useState } from "react";
import {
  BackgroundImage,
  Border,
  StyledLink,
  Title5,
  Turkish,
} from "./StyledUI";
import { Stack, Typography, styled } from "@mui/material";
import LocalVideoPrev from "./LocalVideoPrev";
import { SS } from "../misc/tv";
import { IPost } from "../misc/types";

const StyledBorder = styled(Border)`
  padding-bottom: 65%;
`;

const PrevTitle = styled(Typography)`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
`;

const PrevContainer = styled("div")`
  display: grid;
  grid-template-columns: 5fr 7fr;
  grid-column-gap: ${SS};
`;

export default function PrevMinFull({
  author,
  createdAt,
  dir,
  id,
  photos,
  title,
  textclr,
  tagClr,
  video,
}: IPost) {
  const [hover, setHover] = useState<boolean>(false);

  return (
    <PrevContainer>
      <StyledLink to={`/news/${id}`}>
        <StyledBorder>
          {/* {photos?.length ? (
            <BackgroundImage url={`${dir}/${photos[0]}`} hover={hover} />
          ) : null}
          {video ? <LocalVideoPrev dir={dir} video={video} /> : null} */}
          {photos?.length ? (
            <BackgroundImage url={`${dir}/${photos[0]}`} hover={hover} />
          ) : video ? (
            <LocalVideoPrev dir={dir} video={video} />
          ) : (
            <BackgroundImage url="common/main.jpg" hover={hover} />
          )}
          <Turkish hover={hover} />
        </StyledBorder>
      </StyledLink>
      <Stack
        spacing={3}
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        <Title5 variant="h5">
          <StyledLink to={`/news/${id}`}>{title}</StyledLink>
        </Title5>
        <time>{new Date(createdAt).toISOString().slice(0, 10)}</time>
      </Stack>
    </PrevContainer>
  );
}
