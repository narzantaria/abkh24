import React, { useState } from "react";
import {
  BackgroundImage2,
  Border,
  StyledLink,
  TagButtonSharp,
  Title3,
  Turkish,
  VideoIcon,
} from "./StyledUI";
import { Box, Stack, styled } from "@mui/material";

interface IVideo {
  id: string;
  title: string;
  videoId: string;
  source: string;
  createdAt: Date;
  textclr?: string | null;
  tagClr?: { background: string; color: string; bordercolor: string } | null;
}

const StyledBorder = styled(Border)`
  padding-bottom: 65%;
`;

const PrevTitle = styled(Title3)(
  ({ textclr }: { textclr?: string }) => `
overflow: hidden;
text-overflow: ellipsis;
display: -webkit-box;
-webkit-box-orient: vertical;
-webkit-line-clamp: 2;
color: ${textclr || "initial"}
`,
);

export default function VPrev({
  createdAt,
  id,
  title,
  source,
  videoId,
  textclr,
  tagClr,
}: IVideo) {
  const [hover, setHover] = useState<boolean>(false);

  return (
    <Stack
      spacing={1}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <StyledLink to={`/videos/${id}`}>
        <StyledBorder>
          <VideoIcon fontSize={65} />
          <BackgroundImage2
            url={`https://i.ytimg.com/vi/${videoId}/hqdefault.jpg`}
            hover={hover}
          />
          <Turkish hover={hover} />
          <TagButtonSharp background={tagClr?.background} size="small">
            {source}
          </TagButtonSharp>
        </StyledBorder>
      </StyledLink>
      <StyledLink to={`/videos/${id}`}>
        <PrevTitle variant="h5" textclr={textclr || "initial"}>
          {title}
        </PrevTitle>
      </StyledLink>
      <Box sx={{ fontSize: "13px" }}>
        <time>{new Date(createdAt).toISOString().slice(0, 10)}</time>
      </Box>
    </Stack>
  );
}
