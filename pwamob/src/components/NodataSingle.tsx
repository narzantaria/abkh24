import { LS } from "../misc/tv";
import { Box, Skeleton, Stack } from "@mui/material";
import React from "react";
import { v4 as uuidv4 } from "uuid";
import { XContainer } from "./StyledUI";

const data = Array.from({ length: 5 }).map(() => uuidv4());

export default function NodataSingle() {
  return (
    <Box sx={{ paddingBlock: LS }}>
      <XContainer>
        <Stack spacing={3}>
          <Skeleton
            variant="rectangular"
            height={20}
            sx={{ backgroundColor: "#777" }}
          />
          <Skeleton
            variant="rectangular"
            height={250}
            width={500}
            sx={{ backgroundColor: "#777" }}
          />
          {data.map((x) => (
            <Skeleton
              variant="rectangular"
              height={20}
              sx={{ backgroundColor: "#777" }}
              key={x}
            />
          ))}
        </Stack>
      </XContainer>
    </Box>
  );
}
