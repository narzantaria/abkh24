import React, { useEffect } from "react";
import axios from "axios";
import { useAppDispatch } from "../lib";
import { setTheme } from "../lib/themeSlice";
import { logout, nnmAuth, setAuth } from "../lib/authSlice";
import { setField } from "../lib/formSlice";
import { authorizeFapi } from "../lib/miscSlice";

const VITE_API = import.meta.env.VITE_API;

export default function ThinkTank() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    const clientToken = localStorage.getItem("token");
    const clientRange = localStorage.getItem("category");
    const clientId = localStorage.getItem("id");
    const clientTheme = localStorage.getItem("theme");
    const clientVendor = localStorage.getItem("vendor");
    const clientCat = localStorage.getItem("category");

    if (clientTheme?.length) dispatch(setTheme(clientTheme));

    if (clientCat === "anon" && clientVendor?.length) {
      // anon
      dispatch(
        nnmAuth({
          vendor: clientVendor,
        }),
      );
    } else if (clientRange?.length && clientId?.length && clientToken?.length) {
      // user db
      axios
        .post(
          `${VITE_API}/api/pub/check/${clientId}`,
          {},
          {
            headers: {
              "Content-Type": "application/json",
              "auth-token": clientToken,
            },
          },
        )
        .then(async (res) => {
          await dispatch(
            setAuth({
              ...res.data,
              // avatar: `${VITE_API}/dist/${res.data.dir}/${res.data.photos[0]}`,
              avatar: res.data.photos
                ? `${VITE_API}/dist/${res.data.dir}/${res.data.photos[0]}`
                : `${VITE_API}/dist/common/avatar.png`,
              name: res.data.title,
              token: clientToken,
              category: res.data.category,
            }),
          );
          await dispatch(
            setField({ field: "root", value: `userid${clientId}` }),
          );
          await dispatch(authorizeFapi());
        })
        .catch((err) => {
          console.log(err);
          // alert("Incorrect login or password");
          dispatch(logout());
        });
    }
  }, []);

  return <></>;
}
