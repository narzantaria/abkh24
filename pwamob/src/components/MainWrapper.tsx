import { Box, styled } from "@mui/material";
import React, { ReactNode } from "react";
import { ReduxState, useAppSelector } from "../lib";

const StyledBox = styled(Box)(({ thx }: { thx: "light" | "dark" }) => {
  return `
  background-color: ${thx === "light" ? "#fff" : "#151516"};
  `;
});

interface IProps {
  children: ReactNode;
}

export default function MainWrapper({ children }: IProps) {
  const { color } = useAppSelector((state: ReduxState) => state?.theme);

  return (
    <StyledBox component="main" thx={color}>
      {children}
    </StyledBox>
  );
}
