import React from "react";
import SectionTitle from "./SectionTitle";
import { XContainer } from "./StyledUI";
import { Box, Grid, useMediaQuery } from "@mui/material";
import { LS } from "../misc/tv";
import { tagColor } from "../misc/tools";
import { IPost } from "../misc/types";
import ThemeWrapper from "./ThemeWrapper";
import PrevAlt from "./PrevAlt";

interface IData {
  [key: string]: IPost[];
}

export default function SimpleNews({ data }: { data: IPost[] }) {
  const matches = useMediaQuery("(max-width:900px)");

  if (data) {
    return (
      <Box sx={{ paddingBlock: LS }}>
        <XContainer>
          <SectionTitle first="Свежие" second="новости" />
          <ThemeWrapper>
            <Grid container spacing={{ xs: 0, md: 2 }}>
              {data
                .filter((x, i) => {
                  if ((matches && i <= 5) || !matches) return x;
                })
                .map((x) => (
                  <Grid item xs={12} key={x.id}>
                    <PrevAlt params={[]} tagClr={tagColor(x.tags[0])} {...x} />
                  </Grid>
                ))}
            </Grid>
          </ThemeWrapper>
        </XContainer>
      </Box>
    );
  } else return null;
}
