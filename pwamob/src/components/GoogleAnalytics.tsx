import { Box } from "@mui/material";
import React, { Fragment } from "react";

export default function GoogleAnalytics() {
  return (
    <Fragment>
      <Box
        component="script"
        async
        src="https://www.googletagmanager.com/gtag/js?id=G-C2DECDPLSP"
      />
      <Box
        component="script"
        dangerouslySetInnerHTML={{
          __html: `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-C2DECDPLSP');
        `,
        }}
      />
    </Fragment>
  );
}
