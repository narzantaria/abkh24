import {
  Button,
  styled,
  IconButton,
  Toolbar,
  Typography,
  Stack,
} from "@mui/material";
import { AiOutlineClose } from "react-icons/ai";
import { RxVideo } from "react-icons/rx";
import {
  COLOR1,
  CONTAINER_WIDTH,
  GREEN_CLR,
  MS,
  PAPER_CLR,
  TRANS_EASE_IN_OUT,
} from "../misc/tv";
import { Link } from "react-router-dom";

const VITE_API = import.meta.env.VITE_API;

export const XContainer = styled("div")`
  max-width: ${CONTAINER_WIDTH};
  margin: 0 auto;
  padding-inline: ${MS};
`;

const XButton = styled(Button)`
  width: max-content;
  padding: 5px 15px;
`;

export const XButton2 = styled(XButton)`
  background: transparent;
  color: #000;
  border: 1px solid #000;
  &:hover {
    color: #fff;
    background-color: #000;
  }
`;

export const XButton3 = styled(XButton)`
  background: transparent;
  color: #fff;
  border-color: rgba(255, 255, 255, 0.2);
  &:hover {
    background-color: #151515;
    border-color: rgba(255, 255, 255, 0.2);
  }
`;

export const XButton4 = styled(XButton)`
  background: transparent;
  color: ${GREEN_CLR};
  border: 1px solid ${GREEN_CLR};
  &:hover {
    color: #fff;
    background-color: ${GREEN_CLR};
  }
`;

export const XButton5 = styled(XButton)`
  background: ${GREEN_CLR};
  color: #fff;
  border: 1px solid ${GREEN_CLR};
  &:hover {
    color: ${GREEN_CLR};
    background-color: #fff;
  }
`;

export const XButtonFW = styled(XButton5)`
  width: initial;
`;

export const XDarkIconButton = styled(IconButton)`
  display: flex;
  justify-content: flex-end;
  padding: 0;
  svg {
    transition: ${TRANS_EASE_IN_OUT};
    font-size: 30px;
    color: #777;
    &:hover {
      color: ${GREEN_CLR};
    }
  }
`;

export const XDarkIconButton2 = styled(XDarkIconButton)`
  svg {
    font-size: 23px;
  }
`;

export const XDarkTextButton = styled(Button)`
  display: flex;
  justify-content: flex-end;
  color: #777;
  &:hover {
    color: ${GREEN_CLR};
  }
`;

export const ActionButton = styled(Button)`
  color: #7d7c7c;
  svg {
    color: #7d7c7c !important;
  }
`;

// Это то, что содержит дополнительные UI в превью
export const Caption = styled(Stack)`
  position: absolute;
  color: #fff;
  bottom: 0;
  padding: ${MS};
`;

export const TagButton = styled(Button)(({
  background,
  bordercolor,
  txtcolor,
}: {
  background?: string;
  bordercolor?: string;
  txtcolor?: string;
}) => {
  return `
    background: ${background || "transparent"};
    border: 1px solid;
    border-color: ${bordercolor || background || "#fff"};
    color: ${txtcolor || "#fff"};
    width: min-content;
    white-space: nowrap;
    line-height: initial;
    font-size: 13px;
    `;
});

export const TagButtonSharp = styled(TagButton)`
  border-radius: 0;
`;

export const HeaderButtons = styled("div")`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  column-gap: 10px;
`;

export const Close = styled(AiOutlineClose)`
  z-index: 222;
  color: #ddd;
  background-color: #444;
  width: 29px;
  height: 29px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  svg {
    font-size: 18px;
  }
  transition: ${TRANS_EASE_IN_OUT};
  &:hover {
    color: ${COLOR1};
  }
`;

export const StyledLink = styled(Link)`
  text-decoration: none;
  color: inherit;
  transition: ${TRANS_EASE_IN_OUT};
  &:hover {
    color: ${GREEN_CLR};
  }
`;

/*
export const BackgroundImage = styled("div")(
  ({ scale }: { scale: boolean }) => {
    let result: string = `
    height: 100%;
    transition: ${TRANS_EASE_IN_OUT};
    position: absolute;
    width: 100%;
    left: 0;
    top: 0;
    `;
    if (scale) result += "\n" + "&hover {transform: scale(1.05);}";
    return result;
  }
);
*/

export const BackgroundImage = styled("div")(({
  url,
  hover,
}: {
  url: string;
  hover?: boolean;
}) => {
  return `
    height: 100%;
    transition: ${TRANS_EASE_IN_OUT};
    position: absolute;
    width: 100%;
    left: 0;
    top: 0;
    background: url(${VITE_API}/dist/${url}) no-repeat center center / cover;
    transform: ${hover ? "scale(1.05)" : "none"}; 
    `;
});

export const BackgroundImage2 = styled("div")(({
  url,
  hover,
}: {
  url: string;
  hover: boolean;
}) => {
  return `
    height: 100%;
    transition: ${TRANS_EASE_IN_OUT};
    position: absolute;
    width: 100%;
    left: 0;
    top: 0;
    background: url(${url}) no-repeat center center / cover;
    transform: ${hover ? "scale(1.05)" : "none"}; 
    `;
});

export const VideoIcon = styled(RxVideo)(({
  fontSize,
  color,
}: {
  fontSize: number;
  color?: string | null;
}) => {
  return `
    position: absolute;
    transform: translate(-50%, -50%);
    top: 50%;
    left: 50%;
    color: ${color ? color : PAPER_CLR};
    z-index: 1;
    font-size: ${fontSize || 65}px;
    `;
});

// export const Decoration = styled("div")`
//   background-color: ${GREEN_CLR};
//   bottom: -1px;
//   height: 2px;
//   left: 0;
//   position: absolute;
//   width: 40px;
// `;

export const Decoration = styled("div")`
  background-color: ${GREEN_CLR};
  height: 2px;
  left: 0;
  width: 40px;
  transform: translateY(-2px);
`;

export const Decoration2 = styled("div")`
  background-color: ${GREEN_CLR};
  bottom: -1px;
  height: 2px;
  left: 0;
  width: 40px;
`;

export const Border = styled("div")`
  cursor: pointer;
  position: relative;
  overflow: hidden;
`;

export const Overlay = styled("div")(
  ({ hover }: { hover: boolean }) => `
position: absolute;
width: 100%;
height: 100%;
top: 0;
left: 0;
background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75));
opacity: 0.7;
transition: ${TRANS_EASE_IN_OUT};
opacity: ${hover ? 1 : 0.7}
`,
);

export const Turkish = styled("div")(
  ({ hover }: { hover: boolean }) => `
position: absolute;
width: 100%;
height: 100%;
top: 0;
left: 0;
background: #fff;
opacity: 0;
transition: ${TRANS_EASE_IN_OUT};
opacity: ${hover ? 0.3 : 0}
`,
);

export const StyledBorder = styled(Border)`
  padding-bottom: 100%;
  background-color: #f0f0f0;
`;

export const BachgroundContain = styled("div")(
  ({ url }: { url: string }) => `
position: absolute;
left: 0;
top: 0;
height: 100%;
width: 100%;
background: url(${VITE_API}/dist/${url}) no-repeat center center / contain;
`,
);

export const ColTitle = styled("div")`
  background-color: #000;
  color: #fff;
  padding: 13px;
  text-transform: uppercase;
`;

export const Tag = styled("div")(
  ({ tagClr }: { tagClr: string }) => `
position: absolute;
left: 0;
bottom: 0;
width: min-content;
z-index: 1;
background-color: ${tagClr};
color: #fff;
padding: 3px 10px;
`,
);

export const DarkSection = styled("section")`
  background-color: #0a0a0a;
  padding-block: 65px;
`;

export const Coloured = styled("span")`
  color: ${GREEN_CLR};
`;

export const CenteredToolbar = styled(Toolbar)`
  display: flex;
  justify-content: center;
  min-height: unset !important;
`;

const Title = styled(Typography)`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
`;

export const Title1 = styled(Title)`
  -webkit-line-clamp: 2;
  font-size: 33px;
  font-weight: 700;
  text-shadow: 0 1px 2px rgba(0, 0, 0, 0.35);
`;

export const Title2 = styled(Title)`
  -webkit-line-clamp: 2;
  font-size: 25px;
  font-weight: 700;
  text-shadow: 0 1px 2px rgba(0, 0, 0, 0.35);
`;

export const Title3 = styled(Title)`
  -webkit-line-clamp: 3;
  font-size: 20px;
  font-weight: 700;
`;

export const Title4 = styled(Title)`
  -webkit-line-clamp: 3;
  font-size: 16px;
  font-weight: 700;
`;

export const Title5 = styled(Title)`
  -webkit-line-clamp: 3;
  font-size: 14px;
  font-weight: 600;
`;
