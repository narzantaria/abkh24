import { styled } from "@mui/material";
import React from "react";
import { tagColor } from "../misc/tools";
import { IPost } from "../misc/types";
import PrevBigAlt from "./PrevBigAlt";

const NewsWrapper = styled("section")`
  display: block;
`;

export default function UpperNewsAlt({ data }: { data: IPost[] }) {
  if (data) {
    return (
      <NewsWrapper>
        {data.map((x) => (
          <PrevBigAlt
            key={x.id}
            params={[]}
            tagClr={tagColor(x.tags[0])}
            {...x}
          />
        ))}
      </NewsWrapper>
    );
  } else return null;
}
