import { Box, Card, CardMedia } from "@mui/material";
import React from "react";
import { VideoIcon } from "./StyledUI";
import { GREEN_CLR } from "../misc/tv";

const VITE_API = import.meta.env.VITE_API;

interface IProps {
  dir: string;
  video: string;
}

export default function LocalVideoPrev({ dir, video }: IProps) {
  return (
    <Card
      sx={{
        position: "absolute",
        width: "100%",
        height: "100%",
        backgroundColor: "#888",
        borderRadius: 0,
      }}
    >
      <CardMedia
        component="video"
        image={`${VITE_API}/dist/${dir}/${video}`}
        sx={{
          position: "absolute",
          height: "100%",
          width: "100%",
        }}
        disablePictureInPicture={true}
      />
      <Box
        sx={{
          position: "absolute",
          zIndex: 1,
          width: "100%",
          height: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <VideoIcon fontSize={65} color={GREEN_CLR} />
      </Box>
    </Card>
  );
}
