import { Button, Stack, styled } from "@mui/material";
import React from "react";
import { CenteredToolbar, StyledLink, XContainer } from "./StyledUI";
import Social from "./Social";
import { LS, TRANS_EASE_IN_OUT } from "../misc/tv";
import { useGetMenuDataQuery } from "../lib/menuApi";

const MainWrapper = styled("div")`
  background-color: #040404;
  padding-block: ${LS};
`;

const ContentWrapper = styled(Stack)``;

const CenterWrapper = styled("div")`
  display: flex;
  justify-content: center;
`;

const StyledUrl = styled("a")`
  margin-left: 8px;
  color: #bbb;
  transition: ${TRANS_EASE_IN_OUT};
  &:hover {
    color: #f79327;
  }
`;

const CopyRight = styled(Stack)`
  color: #777;
`;

const StyledToolbar = styled(CenteredToolbar)`
  flex-wrap: wrap;
`;

export default function LowerFooter() {
  const { data, error, isLoading } = useGetMenuDataQuery(null);

  if (data) {
    return (
      <MainWrapper>
        <XContainer>
          <ContentWrapper spacing={3}>
            <CenterWrapper>
              <Social facebook="fb" instagram="ins" vk="vk" />
            </CenterWrapper>
            <StyledToolbar>
              {data.map((x, i: number) => {
                return (
                  <StyledLink to={x.link || "/"} key={i}>
                    <Button color="inherit" key={i}>
                      {x.title}
                    </Button>
                  </StyledLink>
                );
              })}
            </StyledToolbar>
            {/* <CenterWrapper>
              <CopyRight direction="row" spacing={1}>
                {`© ${new Date().getFullYear().toString()} Разработка сайта `}
                <StyledUrl
                  to="https://xgroup2022.com"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Xgroup
                </StyledUrl>
              </CopyRight>
            </CenterWrapper> */}
          </ContentWrapper>
        </XContainer>
      </MainWrapper>
    );
  } else return null;
}
