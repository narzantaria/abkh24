// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAC3_L-WVsf34hpudsXxRr8wmtJcbtWZss",
  authDomain: "abkhazia-24.firebaseapp.com",
  projectId: "abkhazia-24",
  storageBucket: "abkhazia-24.appspot.com",
  messagingSenderId: "269429690725",
  appId: "1:269429690725:web:91e6b5167772b4a594518c",
  measurementId: "G-E1K6ZGSQX4",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
export const auth = getAuth();
