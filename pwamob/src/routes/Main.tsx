import React, { Fragment } from "react";
import { useGetMainDataQuery } from "../lib/mainApi";
import { useGetSettingsDataQuery } from "../lib/settingsApi";
import MainWrapper from "../components/MainWrapper";
import NodataGrid from "../components/NodataGrid";
import UpperNewsAlt from "../components/UpperNewsAlt";
import SimpleNews from "../components/SimpleNews";
import { XContainer } from "../components/StyledUI";
import BigBanner from "../components/BigBanner";
import MiscApi from "../components/MiscApi";
import { ITV } from "../misc/types";
import MainTV from "../components/MainTV";
import MainItems from "../components/MainItems";

export default function Main() {
  const { data, error, isLoading } = useGetMainDataQuery(null);
  const { data: settings } = useGetSettingsDataQuery(null);

  return (
    <MainWrapper>
      {data && data[0].banners ? (
        <Fragment>
          <UpperNewsAlt data={data[0].news.slice(0, 3)} />
          <SimpleNews data={data[0].news.slice(3)} />
          <XContainer sx={{ pb: 5 }}>
            <BigBanner {...data[0].banners[0]} />
            <MiscApi />
          </XContainer>
          <MainTV
            data={data[0].videos.map((x: ITV) => {
              return {
                ...x,
                source:
                  x.source === "https://www.youtube.com/@abaza-tv"
                    ? "Абаза-ТВ"
                    : "АГТРК",
              };
            })}
          />
          <XContainer>
            <BigBanner {...data[0].banners[1]} />
          </XContainer>
          {data[0].goods && <MainItems data={data[0].goods} />}
          <XContainer>
            <BigBanner {...data[0].banners[2]} />
          </XContainer>
        </Fragment>
      ) : (
        <NodataGrid />
      )}
    </MainWrapper>
  );
}
