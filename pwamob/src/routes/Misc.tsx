import { Box, Card, Stack } from "@mui/material";
import React, { useEffect, useState } from "react";
import ReactPlayer from "react-player";
import { IPage } from "../misc/types";
import MainWrapper from "../components/MainWrapper";
import { XContainer } from "../components/StyledUI";
import NegativeWrapper from "../components/NegativeWrapper";
import SectionTitle from "../components/SectionTitle";
import PhotoSlider from "../components/PhotoSlider";
import { LS } from "../misc/tv";
import { useParams } from "react-router-dom";
import axios from "axios";
import NodataSingle from "../components/NodataSingle";

const VITE_API = import.meta.env.VITE_API;

export default function Misc() {
  const { name } = useParams();
  const [data, setData] = useState<IPage | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/sdocs/${name}`)
      .then((res) => setData(res.data))
      .catch((err) => console.log(err));
  }, [name]);

  if (data) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <NegativeWrapper>
              <Stack spacing={4}>
                <SectionTitle first={data.title} decorated mb={0} />
                {data.photos?.length === 1 && data.dir && (
                  <Box
                    component="img"
                    sx={{ width: 555, maxWidth: "100%" }}
                    src={`${VITE_API}/dist/${data.dir}/${data.photos[0]}`}
                  />
                )}
                {data.photos?.length && data.photos?.length > 1 && data.dir && (
                  <PhotoSlider
                    images={data.photos.map(
                      (x) => `${VITE_API}/dist/${data.dir}/${x}`,
                    )}
                  />
                )}
                {data.video && (
                  <Card sx={{ width: "min-content", maxWidth: "100%" }}>
                    <ReactPlayer
                      style={{ maxWidth: "100%" }}
                      controls
                      url={`${VITE_API}/dist/${data.dir}/${data.video}`}
                    />
                  </Card>
                )}
                {data.text && (
                  <Box
                    component="div"
                    dangerouslySetInnerHTML={{ __html: data.text }}
                  />
                )}
              </Stack>
            </NegativeWrapper>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataSingle />;
}
