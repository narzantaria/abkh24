import Wrapper from "../components/Wrapper";
import { Box, Snackbar, Stack } from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import MainForm from "../form/MainForm";
import NodataLines from "../components/NodataLines";
import { reset, setForm } from "../lib/formSlice";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { IField, IFolder } from "../misc/types";
import { useGetMainDataQuery } from "../lib/mainApi";
import { useNavigate, useParams } from "react-router-dom";

const VITE_API = import.meta.env.VITE_API;

const primaryFields: IField[] = [
  {
    name: "title",
    required: true,
    label: "Заголовок",
    type: "string",
  },
  {
    name: "text",
    required: true,
    label: "Текст",
    type: "text",
  },
  {
    name: "name",
    required: true,
    label: "Имя автора",
    type: "string",
  },
  {
    name: "phone",
    required: true,
    label: "Телефон",
    type: "phone",
    extra: [
      { name: "Абхазия", code: "+7", flag: "abkh.png" },
      { name: "Россия", code: "+7", flag: "rus.png" },
    ],
  },
  {
    name: "email",
    label: "Email",
    type: "email",
  },
  {
    name: "photos",
    label: "Фотографии",
    type: "gallery",
  },
  {
    name: "sold",
    label: "Продано",
    type: "switch",
  },
];

export default function EditGood() {
  const navigate = useNavigate();
  const { model, slug } = useParams();
  const [ready, setReady] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const { refetch: refetchMainData } = useGetMainDataQuery(null);
  const { id, token } = useAppSelector((state: ReduxState) => state.auth);
  const form = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();

  // =================================

  const [data2, setData2] = useState<{ [key: string]: any } | null>(null);
  const [fields2, setFields2] = useState<IField[] | null>(null);

  async function getData(model: string, slug: string): Promise<void> {
    const res1 = await axios.get(`${VITE_API}/api/pub/goods/${model}/${slug}`);
    const dataProxy = res1.data;
    setData2(() => res1.data);
    const res2 = await axios.get(`${VITE_API}/api/goods/router`);
    const rawMenu: IFolder[] | null = res2.data;
    const fieldsProxy: IField[] | null =
      rawMenu
        ?.filter((x) => x.name == model)[0]
        ?.files.filter((x) => x.name == dataProxy.sector)[0]
        .data // .filter((x) => x.name !== "title")
        .map((x) => {
          if (x.type === "checkbox") return { ...x, type: "select" };
          if (x.type === "slider") return { ...x, type: "int" };
          if (x.type === "dslider") return { ...x, type: "double" };
          return x;
        }) || null;
    setFields2(() => fieldsProxy);
    // return { dataProxy, fieldsProxy };
  }

  // =================================

  useEffect(() => {
    if (model && slug) getData(model, slug);
  }, [model, slug]);

  useEffect(() => {
    (async () => {
      await dispatch(reset());
      await dispatch(setForm(data2));
      setReady(true);
    })();
    return () => {
      dispatch(reset());
    };
  }, [data2]);

  useEffect(() => {
    if (!token) navigate("/cabinet");
  }, [token]);

  if (data2 && fields2 && ready) {
    return (
      <Fragment>
        <Wrapper>
          <Box sx={{ width: 555, maxWidth: "100%" }}>
            <Stack spacing={3}>
              <MainForm
                fields={primaryFields.concat(fields2 || [])}
                callBack={() => {
                  //
                  const { id, clearTemp, createdAt, sector, ...rest } = form;
                  axios
                    .patch(`${VITE_API}/api/pub/goods/${model}/${slug}`, rest, {
                      headers: {
                        "Content-Type": "application/json",
                        "auth-token": token,
                      },
                    })
                    .then((res) => {
                      setMessage("Объявление успешно добавлено");
                      refetchMainData();
                    })
                    .catch((err) => {
                      const resStatus: number = err.response.status;
                      const errMsg: string = err.response.data.message;
                      // dispatch(logout());
                      setMessage(errMsg);
                    });
                }}
              />
            </Stack>
          </Box>
        </Wrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else {
    return <NodataLines />;
  }
}
