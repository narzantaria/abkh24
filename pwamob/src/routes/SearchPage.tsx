import MainWrapper from "../components/MainWrapper";
import NegativeWrapper from "../components/NegativeWrapper";
import NodataLines from "../components/NodataLines";
import SectionTitle from "../components/SectionTitle";
import { StyledLink, XContainer } from "../components/StyledUI";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { resetSparams } from "../lib/searchSlice";
import { up } from "../misc/tools";
import { LS } from "../misc/tv";
import { Box, Stack } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const VITE_API = import.meta.env.VITE_API;

interface ISearchResult {
  id: number;
  title: string;
  source?: string | null;
  sector?: string | null;
}

export default function SearchPage() {
  // SIC!
  const navigate = useNavigate();
  const { path, word } = useAppSelector((state: ReduxState) => state.search);
  const dispatch = useAppDispatch();
  const [data, setData] = useState<ISearchResult[] | null>(null);

  useEffect(() => {
    up();
  }, []);

  useEffect(() => {
    if (path?.length && word?.length) {
      axios
        .get(`${VITE_API}/api/search/${path}`, {
          params: { word: word },
        })
        .then((res) => {
          setData(res.data);
        })
        .catch((err) => console.log(err));
    } else navigate("/");
    return () => {
      dispatch(resetSparams());
    };
  }, [path, word]);

  if (data) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <SectionTitle first="Результаты" second="поиска" decorated />
            <NegativeWrapper>
              <Stack spacing={3}>
                {data.map((x) => {
                  if (path == "goods" && x.sector && x.source) {
                    return (
                      <StyledLink to={`/goods/${x.source}/${x.sector}/${x.id}`}>
                        {x.title}
                      </StyledLink>
                    );
                  } else {
                    return (
                      <StyledLink to={`/news/${x.id}`}>{x.title}</StyledLink>
                    );
                  }
                })}
              </Stack>
            </NegativeWrapper>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else {
    return <NodataLines />;
  }
}
