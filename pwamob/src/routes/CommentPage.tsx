import MainWrapper from "../components/MainWrapper";
import NegativeWrapper from "../components/NegativeWrapper";
import NodataSingle from "../components/NodataSingle";
import { XContainer } from "../components/StyledUI";
import MainForm from "../form/MainForm";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { reset, setForm } from "../lib/formSlice";
import { LS } from "../misc/tv";
import { IField } from "../misc/types";
import { Box, Snackbar } from "@mui/material";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import React, { Fragment, useEffect, useState } from "react";

const VITE_API = import.meta.env.VITE_API;

const fields: IField[] = [
  {
    name: "title",
    label: "Title",
    type: "string",
  },
  {
    name: "text",
    label: "Text",
    type: "text",
  },
  {
    name: "img",
    label: "Image",
    type: "img",
  },
];

export default function CommentPage() {
  // const router = useRouter();
  const navigate = useNavigate();
  const { author, ...form } = useAppSelector((state: ReduxState) => state.form);
  const { category, id, token } = useAppSelector(
    (state: ReduxState) => state.auth,
  );
  const dispatch = useAppDispatch();

  const [message, setMessage] = useState<string>("");
  const [ready, setReady] = useState<boolean>(false);

  const { id: pid } = useParams();
  // const [data, setData] = useState<IPost | null>(null)

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/comments/${pid}`)
      .then(async (res) => {
        await dispatch(reset());
        await dispatch(setForm(res.data));
        setReady(true);
      })
      .catch((err) => console.log(err));
  }, [pid]);

  /*
  useEffect(() => {
    // axios.get(`${VITE_API}/api/comments/${slug}`).then(async (res) => {
    //   await dispatch(reset());
    //   await dispatch(setForm(res.data));
    //   setReady(true);
    // });
    (async ()=>{
      await dispatch(reset());
      await dispatch(setForm(data));
      setReady(true);
    })()
  }, [data]);
  */

  useEffect(() => {
    if (author?.id && author?.id != id) navigate("/");
  }, [author?.id, id]);

  if (ready) {
    return (
      <Fragment>
        <MainWrapper>
          <Box sx={{ paddingBlock: LS }}>
            <XContainer>
              <NegativeWrapper>
                <MainForm
                  fields={fields}
                  callBack={() => {
                    if (author?.id == id) {
                      const { clearTemp, category, ...rest } = form;
                      axios
                        .patch(
                          `${VITE_API}/api/pub/comments/${pid}`,
                          { ...rest, editorId: id },
                          {
                            headers: {
                              "Content-Type": "application/json",
                              "auth-token": token,
                            },
                          },
                        )
                        .then(async (res) => {
                          await setForm(res.data);
                          setMessage("Комментарий обновлен");
                        })
                        .catch((err) => {
                          const resStatus: number = err.response.status;
                          const errMsg: string = err.response.data.message;
                          // dispatch(logout());
                          setMessage(errMsg);
                        });
                    }
                  }}
                />
              </NegativeWrapper>
            </XContainer>
          </Box>
        </MainWrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else return <NodataSingle />;
}
