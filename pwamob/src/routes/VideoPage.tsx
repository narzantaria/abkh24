import { useParams } from "react-router-dom";
import MainSider from "../components/MainSider";
import MainWrapper from "../components/MainWrapper";
import NegativeWrapper from "../components/NegativeWrapper";
import SectionTitle from "../components/SectionTitle";
import { XContainer } from "../components/StyledUI";
import YoutubePlayer from "../components/YoutubePlayer";
import { renderDate } from "../misc/tools";
import { LS } from "../misc/tv";
import { ITV } from "../misc/types";
import { Box, Card, Grid, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import axios from "axios";
import NodataSingle from "../components/NodataSingle";

const VITE_API = import.meta.env.VITE_API;

interface IProps {
  data: ITV;
}

export default function VideoPage() {
  const { slug } = useParams();
  const [data2, setData2] = useState<ITV | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/videos/${slug}`)
      .then((res) => setData2(res.data))
      .catch((err) => console.log(err));
  }, []);

  if (data2) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <NegativeWrapper>
                  <Stack spacing={4}>
                    <SectionTitle first={data2.title} decorated mb={0} />
                    <Card>
                      <YoutubePlayer videoId={data2.videoId} />
                    </Card>
                    <Box component="div" sx={{ fontStyle: "italic" }}>
                      {renderDate(data2.createdAt)}
                    </Box>
                    {data2.source && (
                      <Stack spacing={2} direction="row" flexWrap="wrap">
                        <Typography variant="h6">
                          Ссылка на источник:
                        </Typography>
                        <Typography variant="h6" sx={{ fontStyle: "italic" }}>
                          {data2.source}
                        </Typography>
                      </Stack>
                    )}
                  </Stack>
                </NegativeWrapper>
              </Grid>
              <Grid item xs={12}>
                <MainSider />
              </Grid>
            </Grid>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataSingle />;
}
