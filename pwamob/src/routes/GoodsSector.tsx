import Crumbs from "../components/Crumbs";
import MainWrapper from "../components/MainWrapper";
import NegativeWrapper from "../components/NegativeWrapper";
import { StyledLink, Title3, XContainer } from "../components/StyledUI";
import { LS } from "../misc/tv";
import { IFolder } from "../misc/types";
import { Box, Stack } from "@mui/material";
import React, { useEffect, useState } from "react";
import NodataGrid from "../components/NodataGrid";
import { useParams } from "react-router-dom";
import axios from "axios";

const VITE_API = import.meta.env.VITE_API;

export default function GoodsSector() {
  const { model } = useParams();
  const [data, setData] = useState<IFolder[] | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/goods/router`)
      .then((res) => {
        setData(() => res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  if (data) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <NegativeWrapper>
            <XContainer>
              <Stack spacing={4}>
                <Crumbs
                  data={[
                    {
                      link: "/",
                      title: "Главная",
                    },
                    {
                      link: "goods",
                      title: "Объявления",
                    },
                    {
                      title: data.filter((x) => x.name == model)[0]?.title,
                    },
                  ]}
                />
                <Stack spacing={2}>
                  {data
                    .filter((x) => x.name == model)[0]
                    ?.files.map((y) => {
                      return (
                        <Title3 variant="h3" key={y.name}>
                          <StyledLink to={`/goods/${model}/${y.name}`}>
                            {y.data.filter((z) => z.name === "title")[0].label}
                          </StyledLink>
                        </Title3>
                      );
                    })}
                </Stack>
              </Stack>
            </XContainer>
          </NegativeWrapper>
        </Box>
      </MainWrapper>
    );
  } else return <NodataGrid />;
}
