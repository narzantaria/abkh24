import { useNavigate } from "react-router-dom";
import GoodsPrev from "../components/GoodsPrev";
import NodataSingle from "../components/NodataSingle";
import SectionTitle from "../components/SectionTitle";
import { StyledLink, XContainer } from "../components/StyledUI";
import { ReduxState, useAppSelector } from "../lib";
import { LS } from "../misc/tv";
import { IFolder } from "../misc/types";
import { Box, Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import axios from "axios";

const VITE_API = import.meta.env.VITE_API;

export default function AddGoods() {
  const navigate = useNavigate();

  const { avatar, category, id, token, vendor } = useAppSelector(
    (state: ReduxState) => state.auth,
  );

  const [data, setData] = useState<IFolder[] | null>(null);

  useEffect(() => {
    console.log(`${VITE_API}/api/goods/router`);
    axios
      .get(`${VITE_API}/api/goods/router`)
      .then((res) => setData(res.data))
      .catch((err) => console.log(err));
  }, []);

  // useEffect(() => {
  //   if ((category !== "google" && !token) || (category === "google" && !id))
  //     navigate("/login");
  // }, [token]);

  // useEffect(() => {
  //   if ((category !== "anon" && !token) || (category === "anon" && !vendor))
  //     navigate("/login");
  // }, [category]);

  useEffect(() => {
    if (!token && !vendor) navigate("/login");
  }, [token, vendor]);

  if (data) {
    return (
      <main>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <SectionTitle first="Объявления" decorated />
            <Grid container spacing={4}>
              {data.map((x) => (
                <Grid item xs={6} key={x.title}>
                  <StyledLink to={`/addgoods/${x.name}`}>
                    <GoodsPrev {...x} />
                  </StyledLink>
                </Grid>
              ))}
            </Grid>
          </XContainer>
        </Box>
      </main>
    );
  } else return <NodataSingle />;
}
