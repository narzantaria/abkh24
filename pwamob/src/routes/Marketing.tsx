import axios from "axios";
import BannerPrev from "../components/BannerPrev";
import BigBanner from "../components/BigBanner";
import MainWrapper from "../components/MainWrapper";
import NodataGrid from "../components/NodataGrid";
import SectionTitle from "../components/SectionTitle";
import { XContainer } from "../components/StyledUI";
import { LS } from "../misc/tv";
import { IBanner } from "../misc/types";
import { Box, Grid } from "@mui/material";
import React, { useEffect, useState } from "react";

const VITE_API = import.meta.env.VITE_API;

export default function Marketing() {
  const [data2, setData2] = useState<IBanner[] | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/banners`)
      .then((res) => setData2(res.data))
      .catch((err) => console.log(err));
  }, []);

  if (data2) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <SectionTitle first="Реклама" decorated />
            {data2.slice(0, 3).map((x) => (
              <BigBanner key={x.id} {...x} />
            ))}
            <Grid container spacing={3}>
              {data2.slice(3).map((x) => (
                <Grid item xs={12} sm={4} key={x.id}>
                  <BannerPrev {...x} />
                </Grid>
              ))}
            </Grid>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataGrid />;
}
