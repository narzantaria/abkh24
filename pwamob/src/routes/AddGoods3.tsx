import { useNavigate, useParams } from "react-router-dom";
import NodataSingle from "../components/NodataSingle";
import Wrapper from "../components/Wrapper";
import MainForm from "../form/MainForm";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { reset, setForm } from "../lib/formSlice";
import { useGetMainDataQuery } from "../lib/mainApi";
import { IField, IFolder } from "../misc/types";
import { Snackbar, Stack } from "@mui/material";
import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";

const VITE_API = import.meta.env.VITE_API;

interface IProps {
  data: IFolder[] | null;
  model: string | null;
  sector: string | null;
}

const primaryFields: IField[] = [
  {
    name: "title",
    required: true,
    label: "Заголовок",
    type: "string",
  },
  {
    name: "text",
    required: true,
    label: "Текст",
    type: "text",
  },
  {
    name: "name",
    required: true,
    label: "Имя автора",
    type: "string",
  },
  {
    name: "phone",
    required: true,
    label: "Телефон",
    type: "phone",
    extra: [
      { name: "Абхазия", code: "+7", flag: "abkh.png" },
      { name: "Россия", code: "+7", flag: "rus.png" },
    ],
  },
  {
    name: "email",
    label: "Email",
    type: "email",
  },
  {
    name: "photos",
    label: "Фотографии",
    type: "gallery",
  },
];

export default function AddGoods3() {
  // console.log('AddGoods3 render');
  const { model, sector } = useParams();
  const navigate = useNavigate();
  const { refetch: refetchMainData } = useGetMainDataQuery(null);
  const form = useAppSelector((state: ReduxState) => state.form);
  const { avatar, id, token, category, vendor } = useAppSelector(
    (state: ReduxState) => state.auth,
  );
  const dispatch = useAppDispatch();
  const [message, setMessage] = useState<string>("");
  const [ready, setReady] = useState<boolean>(false);
  const [fields, setFields] = useState<IField[] | null>(null);

  const [data, setData] = useState<IFolder[] | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/goods/router`)
      .then((res) => setData(res.data))
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    if (data && model && sector)
      setFields(
        data
          ?.filter((x) => x.name == model)[0]
          ?.files.filter((x) => x.name == sector)[0]
          .data.map((x) => {
            if (x.type === "checkbox") return { ...x, type: "select" };
            if (x.type === "slider") return { ...x, type: "int" };
            if (x.type === "dslider") return { ...x, type: "double" };
            return x;
          })
          .filter((x) => x.name !== "description"),
      );
  }, [data, model, sector]);

  useEffect(() => {
    (async () => {
      await dispatch(reset());
      await dispatch(setForm({ root: `userid${id}`, sector: sector }));
      setReady(true);
    })();
  }, [id]);

  useEffect(() => {
    if (!token && !vendor) navigate("/login");
  }, [token, vendor]);

  if (data && fields && ready) {
    return (
      <Fragment>
        <Wrapper>
          <Stack spacing={4}>
            <Stack spacing={2}>
              <MainForm
                fields={primaryFields.concat(fields || [])}
                callBack={() => {
                  const { clearTemp, detector, looks, ...rest } = form;
                  const keys: string[] = Object.keys(rest);
                  // НЕПРАВИЛЬНЫЙ ПРИЕМ_КОСТЫЛЬ
                  for (let x = 0; x < keys.length; x++) {
                    if (rest[keys[x]] == "да") rest[keys[x]] = true;
                    if (rest[keys[x]] == "нет") rest[keys[x]] = false;
                  }
                  console.log(rest);
                  /**
                   * ЕЩЕ ОДНО ИНТЕРЕСНОЕ ИЗМЕНЕНИЕ, "author" заменили на "authorId". Связано это
                   * с тем, что этот запрос обрабатывается не TypeORM, а нативным SQL.
                   **/
                  if (category === "anon") {
                    console.log({ ...rest, vendor: vendor });
                    axios
                      .post(
                        `${VITE_API}/api/pub/agoods/${model}`,
                        { ...rest, vendor: vendor },
                        {
                          headers: {
                            "Content-Type": "application/json",
                          },
                        },
                      )
                      .then((res) => {
                        //
                        setMessage("Объявление успешно добавлено");
                        setTimeout(() => {
                          refetchMainData();
                          dispatch(reset());
                          navigate("/cabinet");
                        }, 3000);
                      })
                      .catch((err) => {
                        console.error(err);
                        const resStatus: number = err.response.status;
                        const errMsg: string = err.response.data.message;
                        if (resStatus === 401) {
                          // dispatch(logout());
                        } else {
                          setMessage(
                            errMsg,
                            // "Пожалуйста заполните все необходимые поля"
                          );
                        }
                      });
                  } else {
                    axios
                      .post(
                        `${VITE_API}/api/pub/goods/${model}`,
                        { ...rest, authorId: id },
                        {
                          headers: {
                            "Content-Type": "application/json",
                            "auth-token": token,
                          },
                        },
                      )
                      .then((res) => {
                        //
                        setMessage("Объявление успешно добавлено");
                        setTimeout(() => {
                          refetchMainData();
                          navigate("/cabinet");
                        }, 3000);
                      })
                      .catch((err) => {
                        console.error(err);
                        const resStatus: number = err.response.status;
                        const errMsg: string = err.response.data.message;
                        if (resStatus === 401) {
                          // dispatch(logout());
                        } else {
                          setMessage(
                            "Пожалуйста заполните все необходимые поля",
                          );
                        }
                      });
                  }
                }}
              />
            </Stack>
          </Stack>
        </Wrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else {
    return <NodataSingle />;
  }
}
