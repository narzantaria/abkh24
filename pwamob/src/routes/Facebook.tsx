import { Box } from "@mui/material";
import React, { useState, useEffect, Fragment } from "react";
import Spinner from "../components/Spinner";
import { useIsMount } from "../misc/tools";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { setAuth } from "../lib/authSlice";
import { setField } from "../lib/formSlice";
import { useNavigate } from "react-router-dom";

const ACCESS_TOKEN = import.meta.env.VITE_API_ACCESS_TOKEN;

function stringBetweenStrings(
  startStr: string,
  endStr: string,
  str: string,
): string {
  const pos = str.indexOf(startStr) + startStr.length;
  return str.substring(pos, str.indexOf(endStr, pos));
}

export default function Facebook() {
  // const router = useRouter();
  const navigate = useNavigate();
  const { name, avatar, category } = useAppSelector(
    (state: ReduxState) => state.auth,
  );
  const dispatch = useAppDispatch();
  const isMount = useIsMount();
  const [facebookId, setFacebookId] = useState<string | null>(null);
  useEffect(() => {
    const myUrl = window.location.href;
    setFacebookId(stringBetweenStrings("id=", "#_=_", myUrl));
  }, []);

  useEffect(() => {
    if (!isMount && facebookId) {
      (async () => {
        const response = await fetch(
          `https://graph.facebook.com/${facebookId}?fields=name,picture&access_token=${ACCESS_TOKEN}`,
        );
        const data = await response.json();
        await dispatch(
          setAuth({
            avatar: data.picture.data.url,
            category: "facebook",
            name: data.name,
            id: facebookId,
          }),
        );
        // А ЭТО ПРАВИЛЬНО???
        await dispatch(
          setField({ field: "root", value: `userid${facebookId}` }),
        );
      })();
    }
  }, [facebookId]);

  useEffect(() => {
    // Что интересно, без проверки не работает!
    if (!isMount && category) {
      navigate("/");
    }
  }, [category]);

  return (
    <Fragment>
      <Box
        sx={{
          position: "fixed",
          zIndex: 111,
          top: 0,
          left: 0,
          height: "100vh",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          background: "#fff",
        }}
      ></Box>
      <Spinner />
    </Fragment>
  );
}
