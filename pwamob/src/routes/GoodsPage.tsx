import Crumbs from "../components/Crumbs";
import ItemPrev from "../components/ItemPrev";
import MainWrapper from "../components/MainWrapper";
import NegativeWrapper from "../components/NegativeWrapper";
import NodataGrid from "../components/NodataGrid";
import { StyledLink, XContainer } from "../components/StyledUI";
import SuperMenu from "../form/SuperMenu";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { setForm } from "../lib/formSlice";
import { useIsMount } from "../misc/tools";
import { LS } from "../misc/tv";
import { IField, IFolder, Item } from "../misc/types";
import { Box, Grid, Stack } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid";
import { useParams } from "react-router-dom";

const VITE_API = import.meta.env.VITE_API;

export default function GoodsPage() {
  const { model, sector } = useParams();
  const form = useAppSelector((state: ReduxState) => state?.form);
  const dispatch = useAppDispatch();
  const isMount = useIsMount();
  const [goods, setGoods] = useState<Item[] | null>(null);

  const [data, setData] = useState<IFolder[] | null>(null);
  const [xmenu, setXmenu] = useState<IField[] | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/goods/router`)
      .then((res) => {
        setData(() => res.data);
      })
      .catch((err) => console.log(err));
    axios
      .get(`${VITE_API}/api/goods/xmenu/${model}/${sector}`)
      .then((res) => {
        setXmenu(() => res.data);
      })
      .catch((err) => console.log(err));
  }, [model, sector]);

  useEffect(() => {
    (async () => {
      if (xmenu) {
        const argsProxy: { [key: string]: any } = {};
        const filteredData = xmenu.filter((x: IField) => x.type === "slider");
        if (filteredData.length) {
          for (let x = 0; x < filteredData.length; x++) {
            argsProxy[filteredData[x].name] = filteredData[x].extra;
          }
        }
        const newDetector: string = uuidv4();
        await dispatch(setForm({ ...argsProxy, detector: newDetector }));
      }
    })();
  }, [xmenu]);

  useEffect(() => {
    if (!isMount && form.detector) fetchData();
  }, [form.detector]);

  function fetchData() {
    const { clearTemp, root, detector, ...rest } = form;
    axios
      .get(`${VITE_API}/api/goods/market/${model}/${sector}`, {
        headers: { "Content-Type": "application/json" },
        params: rest,
      })
      .then((res) => {
        setGoods(res.data);
      })
      .catch((error) => console.log(error));
  }

  if (data && xmenu) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <Stack spacing={4}>
              <NegativeWrapper>
                <Crumbs
                  data={[
                    {
                      link: "/",
                      title: "Главная",
                    },
                    {
                      link: "goods",
                      title: "Объявления",
                    },
                    {
                      link: `goods/${model}`,
                      title: data.filter((x) => x.name == model)[0].title,
                    },
                    {
                      title:
                        data
                          .filter((x) => x.name == model)[0]
                          .files.filter((x) => x.name == sector)[0]
                          .data.filter((x) => x.name === "title")[0].label ||
                        "",
                    },
                  ]}
                />
              </NegativeWrapper>
              <Grid container spacing={3} sx={{ maxWidth: "100%" }}>
                <Grid item sm={12} md={3}>
                  {/* <h3 onClick={()=>{
                    console.log(xmenu)
                  }}>Islamabad</h3> */}
                  <NegativeWrapper>
                    {xmenu && <SuperMenu fields={xmenu} />}
                  </NegativeWrapper>
                </Grid>
                <Grid item sm={12} md={9}>
                  {goods && (
                    <Grid container spacing={3}>
                      {goods.map((x) => (
                        <Grid item sm={12} md={4} key={x.id}>
                          <StyledLink to={`/goods/${model}/${sector}/${x.id}`}>
                            <ItemPrev {...x} key={x.id} />
                          </StyledLink>
                        </Grid>
                      ))}
                    </Grid>
                  )}
                </Grid>
              </Grid>
            </Stack>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataGrid />;
}
