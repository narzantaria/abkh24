import { useParams } from "react-router-dom";
import MainWrapper from "../components/MainWrapper";
import NegativeWrapper from "../components/NegativeWrapper";
import NodataSingle from "../components/NodataSingle";
import SectionTitle from "../components/SectionTitle";
import { XContainer } from "../components/StyledUI";
import { renderDate } from "../misc/tools";
import { LS } from "../misc/tv";
import { IFuneral } from "../misc/types";
import { Box, Stack } from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";

const VITE_API = import.meta.env.VITE_API;

export default function Funeral() {
  const { id } = useParams();
  const [data, setData] = useState<IFuneral | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/funerals/${id}`)
      .then((res) => {
        setData(() => res.data);
      })
      .catch((err) => console.log(err));
  }, [id]);

  if (data) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <NegativeWrapper>
              <Stack spacing={4}>
                <SectionTitle first={data.title} decorated={true} mb={0} />
                {data.photo && data.dir && (
                  <Box
                    component="img"
                    sx={{ width: 555, maxWidth: "100%" }}
                    src={`${VITE_API}/dist/${data.dir}/${data.photo}`}
                  />
                )}
                <Box component="div" sx={{ fontStyle: "italic" }}>
                  {renderDate(data.createdAt)}
                </Box>
                {data.text && (
                  <Box
                    component="div"
                    dangerouslySetInnerHTML={{ __html: data.text }}
                  />
                )}
              </Stack>
            </NegativeWrapper>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataSingle />;
}
