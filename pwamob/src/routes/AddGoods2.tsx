import { useNavigate, useParams } from "react-router-dom";
import Crumbs from "../components/Crumbs";
import NodataSingle from "../components/NodataSingle";
import { StyledLink, Title3, XContainer } from "../components/StyledUI";
import { ReduxState, useAppSelector } from "../lib";
import { LS } from "../misc/tv";
import { IFolder } from "../misc/types";
import { Box, Stack } from "@mui/material";
import React, { useEffect, useState } from "react";
import axios from "axios";

const VITE_API = import.meta.env.VITE_API;

interface IProps {
  data: IFolder[] | null;
  model: string | null;
}

export default function AddGoods2() {
  const { model } = useParams();
  const navigate = useNavigate();
  const { avatar, category, id, token, vendor } = useAppSelector(
    (state: ReduxState) => state.auth,
  );
  const [data, setData] = useState<IFolder[] | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/goods/router`)
      .then((res) => setData(res.data))
      .catch((err) => console.log(err));
  }, []);

  // useEffect(() => {
  //   if ((category !== "google" && !token) || (category === "google" && !id))
  //     navigate("/login");
  // }, [token]);

  // useEffect(() => {
  //   if ((category !== "anon" && !token) || (category === "anon" && !vendor))
  //     navigate("/login");
  // }, [category]);

  useEffect(() => {
    if (!token && !vendor) navigate("/login");
  }, [token, vendor]);

  if (data && model) {
    return (
      <main>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <Stack spacing={4}>
              <Crumbs
                data={[
                  {
                    link: "/",
                    title: "Главная",
                  },
                  {
                    link: "addgoods", // addgoods
                    title: "Объявления",
                  },
                  {
                    title: data.filter((x) => x.name == model)[0].title, // animal
                  },
                ]}
              />
              <Stack spacing={2}>
                {data
                  .filter((x) => x.name == model)[0]
                  .files.map((y) => (
                    <Title3 variant="h3" key={y.name}>
                      <StyledLink to={`/addgoods/${model}/${y.name}`}>
                        {y.data.filter((z) => z.name === "title")[0].label}
                      </StyledLink>
                    </Title3>
                  ))}
              </Stack>
            </Stack>
          </XContainer>
        </Box>
      </main>
    );
  } else return <NodataSingle />;
}
