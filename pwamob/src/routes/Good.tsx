import MainSider from "../components/MainSider";
import MainWrapper from "../components/MainWrapper";
import NegativeWrapper from "../components/NegativeWrapper";
import NodataSingle from "../components/NodataSingle";
import PhotoSlider from "../components/PhotoSlider";
import SectionTitle from "../components/SectionTitle";
import { Title4, XContainer } from "../components/StyledUI";
import { renderDate } from "../misc/tools";
import { LS } from "../misc/tv";
import { IField, Item } from "../misc/types";
import { Box, Grid, Stack, Typography } from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { hparse } from "../misc/stools";

const VITE_API = import.meta.env.VITE_API;

interface IGallery {
  dir: string;
  photos: string[];
}

interface IParams {
  title: string;
  createdAt?: string | null;
  text?: string | null;
  gallery?: IGallery | null;
  phone?: string | null;
  price: string;
}

export default function Good() {
  const { model, sector, slug } = useParams();
  const [data2, setData2] = useState<Item | null>(null);
  const [fields2, setFields2] = useState<IField[] | null>(null);
  const [specialParams2, setSpecialParams2] = useState<IParams | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/${model}s/${slug}`)
      .then((res) => {
        const rawData = res.data;
        const {
          id,
          sector,
          sold,
          dir,
          phone,
          photos,
          price,
          text,
          title,
          createdAt,
          ...rest
        } = rawData;
        setData2(() => rest);
        const parsedPhone = hparse(phone);
        setSpecialParams2(() => ({
          title: title,
          createdAt: createdAt,
          gallery: { dir: dir, photos: photos },
          text: text,
          phone: parsedPhone.code + parsedPhone.value,
          price: price,
        }));
      })
      .catch((err) => console.log(err));
    axios
      .get(`${VITE_API}/api/builder/${model}`)
      .then((res) => {
        const rawFields = res.data;
        setFields2(() => rawFields.filter((x: IField) => x.type !== "tree"));
      })
      .catch((err) => console.log(err));
  }, [model, sector]);

  if (data2 && fields2 && specialParams2) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <Grid container spacing={3}>
              <Grid item xs={12} md={8}>
                <Stack spacing={2}>
                  <SectionTitle first={specialParams2.title} decorated mb={0} />
                  {specialParams2.gallery?.photos?.length === 1 &&
                    specialParams2.gallery.dir && (
                      <Box
                        component="img"
                        sx={{ width: 555, maxWidth: "100%" }}
                        src={`${VITE_API}/dist/${specialParams2.gallery.dir}/${specialParams2.gallery.photos[0]}`}
                      />
                    )}
                  {specialParams2.gallery?.photos &&
                    specialParams2.gallery?.photos?.length > 1 &&
                    specialParams2.gallery.dir && (
                      <PhotoSlider
                        images={specialParams2.gallery.photos.map(
                          (x) =>
                            `${VITE_API}/dist/${specialParams2.gallery?.dir}/${x}`,
                        )}
                      />
                    )}
                  {specialParams2.createdAt && (
                    <NegativeWrapper>
                      <Stack spacing={2} direction="row">
                        <Typography variant="body1">
                          Дата размещения:
                        </Typography>
                        <Box component="div" sx={{ fontStyle: "italic" }}>
                          {renderDate(new Date(specialParams2.createdAt))}
                        </Box>
                      </Stack>
                    </NegativeWrapper>
                  )}

                  {specialParams2.text ? (
                    <NegativeWrapper>
                      <Box
                        component="div"
                        dangerouslySetInnerHTML={{
                          __html: specialParams2.text,
                        }}
                      />
                    </NegativeWrapper>
                  ) : null}

                  {specialParams2.phone ? (
                    <NegativeWrapper>
                      <ItemField
                        first="Номер телефона"
                        second={specialParams2.phone}
                      />
                    </NegativeWrapper>
                  ) : null}

                  {specialParams2.price ? (
                    <NegativeWrapper>
                      <ItemField
                        first="Цена"
                        second={specialParams2.price + " р."}
                      />
                    </NegativeWrapper>
                  ) : null}

                  {Object.keys(data2).map((x) => (
                    <Fragment>
                      {data2[x] ? (
                        <ItemField
                          first={
                            fields2.filter((elem) => elem.name === x)[0]
                              .label || ""
                          }
                          second={String(data2[x])}
                        />
                      ) : null}
                    </Fragment>
                  ))}
                </Stack>
              </Grid>
              <Grid item xs={12} md={4}>
                <MainSider />
              </Grid>
            </Grid>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataSingle />;
}

function ItemField({ first, second }: { first: string; second: string }) {
  if (first?.length && second?.length) {
    return (
      <Stack spacing={3} direction="row">
        <Title4 variant="h4">{`${first}:`}</Title4>
        <Typography variant="body1">{second}</Typography>
      </Stack>
    );
  } else return null;
}
