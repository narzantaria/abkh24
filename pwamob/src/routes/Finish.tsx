import { useNavigate } from "react-router-dom";
import {
  StyledLink,
  Title2,
  XButton4,
  XContainer,
} from "../components/StyledUI";
import MainForm from "../form/MainForm";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { reset } from "../lib/formSlice";
import { isEmail, isPhone, parsePhone, up, useIsMount } from "../misc/tools";
import { XLS } from "../misc/tv";
import { Box, Snackbar, Stack, Typography } from "@mui/material";
import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";
import { BiUserCircle } from "react-icons/bi";

const VITE_API = import.meta.env.VITE_API;

const fields = [
  {
    name: "sms",
    required: true,
    label: "Проверочный код",
    type: "string",
  },
  {
    name: "title",
    required: true,
    label: "Имя",
    type: "string",
  },
  {
    name: "alias",
    label: "Никнейм",
    type: "string",
  },
  {
    name: "phone",
    label: "Телефон",
    type: "phone",
    extra: [
      { name: "Абхазия", code: "+7", flag: "abkh.png" },
      { name: "Россия", code: "+7", flag: "rus.png" },
    ],
  },
  {
    name: "email",
    label: "Email",
    type: "email",
  },
  {
    name: "password",
    required: true,
    label: "Пароль",
    type: "password",
  },
  {
    name: "photos",
    label: "Фотографии",
    type: "gallery",
  },
  {
    name: "birthDate",
    label: "Дата рождения",
    type: "date",
  },
  {
    name: "bio",
    label: "О себе",
    type: "description",
  },
  {
    name: "subcription",
    label: "Подпись",
    type: "description",
  },
  {
    name: "education",
    label: "Образование",
    type: "select",
    extra: [
      "незаконченное среднее",
      "среднее",
      "незаконченное высшее",
      "высшее",
    ],
  },
  {
    name: "gender",
    required: true,
    label: "Пол",
    type: "radio",
    extra: ["муж", "жен", "не указывать"],
  },
  {
    name: "religion",
    label: "Отношение к религии",
    type: "select",
    extra: [
      "христианство",
      "ислам",
      "иудаизм",
      "буддизм",
      "конфуцианство",
      "даосизм",
      "атеизм",
      "не указывать",
    ],
  },
];

export default function Finish() {
  // const router = useRouter();
  const navigate = useNavigate();
  const [message, setMessage] = useState<string>("");
  const [finished, setFinished] = useState<boolean>(false);
  const form = useAppSelector((state: ReduxState) => state?.form);
  const dispatch = useAppDispatch();
  const isMount = useIsMount();

  useEffect(() => {
    up();
  }, []);

  useEffect(() => {
    (async () => {
      if (!isMount) {
        await dispatch(reset());
        up();
        setTimeout(() => {
          navigate("/");
        }, 3000);
      }
    })();
  }, [finished]);

  return (
    <Fragment>
      <main>
        <Box sx={{ paddingBlock: XLS }}>
          <XContainer>
            <Box sx={{ width: 555, maxWidth: "100%" }}>
              <Stack spacing={3}>
                {finished ? (
                  <Fragment>
                    <Title2>Поздравляем! Регистрация завершена!</Title2>
                    <Typography variant="body1">
                      Теперь вы можете авторизироваться:
                    </Typography>
                    <StyledLink to="/login">
                      <XButton4
                        sx={{ color: "#777" }}
                        startIcon={<BiUserCircle />}
                        variant="text"
                      >
                        Вход
                      </XButton4>
                    </StyledLink>
                  </Fragment>
                ) : (
                  <Fragment>
                    <Title2>Завершение регистрации</Title2>
                    <Typography variant="body1">
                      Введите оставшиеся данные для завершения регистрации. Не
                      забудьте указать проверочный код, высланный на электронный
                      адрес или через смс.
                    </Typography>
                    <MainForm
                      fields={fields}
                      btntext="Отправить"
                      callBack={() => {
                        const parsedPhone = parsePhone(form["phone"]);
                        if (!parsedPhone) {
                          setMessage("Введите правильный номер телефона");
                          return;
                        }
                        const phoneNumber =
                          parsedPhone.code + parsedPhone.value;
                        const isPhoneNumber = isPhone(phoneNumber);
                        const isEmailResult =
                          !form["email"] || isEmail(form["email"]);
                        if (isPhoneNumber || isEmailResult) {
                          const { clearTemp, ...rest } = form;
                          axios
                            .post(`${VITE_API}/api/register/finish`, rest, {
                              headers: {
                                "Content-Type": "application/json",
                              },
                            })
                            .then((res) => {
                              setMessage("Данные сохранены");
                              setFinished(true);
                            })
                            .catch((err) => {
                              const resStatus: number = err.response.status;
                              const errMsg: string = err.response.data.message;
                              setMessage(errMsg);
                            });
                        } else {
                          setMessage("Неправильный номер телефона или email!");
                        }
                      }}
                    />
                  </Fragment>
                )}
              </Stack>
            </Box>
          </XContainer>
        </Box>
      </main>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
