import Wrapper from "../components/Wrapper";
import { Box, Snackbar, Stack } from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import MainForm from "../form/MainForm";
import NodataLines from "../components/NodataLines";
import { IField, IPost } from "../misc/types";
import { reset, setField, setForm } from "../lib/formSlice";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { useNavigate, useParams } from "react-router-dom";

const VITE_API = import.meta.env.VITE_API;

export default function EditPost() {
  const navigate = useNavigate();
  const { slug } = useParams();
  const { id, token } = useAppSelector((state: ReduxState) => state.auth);
  const form = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();
  const [message, setMessage] = useState<string>("");
  const [ready, setReady] = useState<boolean>(false);

  const [data2, setData2] = useState<IPost | null>(null);
  const [fields2, setFields2] = useState<IField[] | null>(null);

  useEffect(() => {
    axios.get(`${VITE_API}/api/pub/posts/${slug}`).then((res) => {
      const { looks, ...rest } = res.data[0];
      setData2(rest);
    });
    axios.get(`${VITE_API}/api/builder/post`).then((res) => {
      setFields2(
        res.data.filter(
          (x: IField) =>
            x.type !== "relation" && x.type !== "youtube" && x.name !== "looks",
        ),
      );
    });
  }, [slug]);

  useEffect(() => {
    (async () => {
      if (data2 && fields2 && !ready) {
        await dispatch(reset());
        await dispatch(setForm(data2));
        setReady(true);
      }
    })();
  }, [data2, fields2]);

  useEffect(() => {
    if (!token) navigate("/cabinet");
  }, [token]);

  useEffect(() => {
    (async () => {
      await dispatch(reset());
      await dispatch(
        setField({ field: "root", value: data2?.dir || `userid${id}` }),
      );
      // setReady(true);
    })();
  }, [id]);

  if (ready && fields2) {
    return (
      <Fragment>
        {/* <h2 onClick={() => {
          console.log(data2);
          console.log(fields2);
          console.log(form);
        }}>gkjgkj</h2> */}
        <Wrapper>
          <Box sx={{ width: 555, maxWidth: "100%" }}>
            <Stack spacing={3}>
              <MainForm
                fields={fields2}
                callBack={() => {
                  const { id, clearTemp, category, ...rest } = form;
                  axios
                    .patch(`${VITE_API}/api/pub/posts/${id}`, rest, {
                      headers: {
                        "Content-Type": "application/json",
                        "auth-token": token,
                      },
                    })
                    .then((res) => {
                      console.log(res.data);
                    })
                    .catch((err) => {
                      const resStatus: number = err.response.status;
                      const errMsg: string = err.response.data.message;
                      // dispatch(logout());
                      setMessage(errMsg);
                    });
                }}
              />
            </Stack>
          </Box>
        </Wrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else {
    return <NodataLines />;
  }
}
