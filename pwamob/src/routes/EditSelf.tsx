import Wrapper from "../components/Wrapper";
import MainForm from "../form/MainForm";
import { useIsMount } from "../misc/tools";
import { Box, Snackbar } from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import NodataForm from "../components/NodataForm";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { reset, setForm } from "../lib/formSlice";
import { useNavigate } from "react-router-dom";

const VITE_API = import.meta.env.VITE_API;

const formProps = [
  {
    name: "title",
    required: true,
    label: "Имя",
    type: "string",
  },
  {
    name: "phone",
    required: true,
    label: "Телефон",
    type: "phone",
    extra: [
      { name: "Абхазия", code: "+7", flag: "abkh.png" },
      { name: "Россия", code: "+7", flag: "rus.png" },
    ],
  },
  {
    name: "email",
    label: "Email",
    type: "email",
  },
  {
    name: "password",
    required: true,
    label: "Пароль",
    type: "password",
  },
  {
    name: "photos",
    label: "Фотографии",
    type: "gallery",
  },
  {
    name: "birthDate",
    label: "Дата рождения",
    type: "date",
  },
  {
    name: "bio",
    label: "О себе",
    type: "text",
  },
  {
    name: "subcription",
    label: "Подпись",
    type: "description",
  },
  {
    name: "education",
    label: "Образование",
    type: "select",
    extra: [
      "незаконченное среднее",
      "среднее",
      "незаконченное высшее",
      "высшее",
    ],
  },
  {
    name: "gender",
    required: true,
    label: "Пол",
    type: "radio",
    extra: ["муж", "жен", "не указывать"],
  },
  {
    name: "religion",
    label: "Отношение к религии",
    type: "select",
    extra: [
      "христианство",
      "ислам",
      "иудаизм",
      "буддизм",
      "конфуцианство",
      "даосизм",
      "атеизм",
      "не указывать",
    ],
  },
  {
    name: "security",
    label: "Конфиденциальность",
    type: "radio",
    extra: ["открыто", "блокировка сообщений", "закрыто"],
  },
];

export default function EditSelf() {
  // const router = useRouter();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { id, name, avatar, category, religion, subcription, token } =
    useAppSelector((state: ReduxState) => state.auth);
  const form = useAppSelector((state: ReduxState) => state.form);
  const isMount = useIsMount();
  const [message, setMessage] = useState<string>("");
  const [ready, setReady] = useState<boolean>(false);
  useEffect(() => {
    if (id) {
      axios
        .post(
          `${VITE_API}/api/pub/check/${id}`,
          {},
          {
            headers: {
              "Content-Type": "application/json",
              "auth-token": token,
            },
          },
        )
        .then(async (res) => {
          const { token, ...rest } = res.data;
          await dispatch(reset());
          await dispatch(setForm(rest));
          setTimeout(() => {
            setReady(true);
          }, 1000);
        })
        .catch((err) => {
          const resStatus: number = err.response.status;
          const errMsg: string = err.response.data.message;
          // dispatch(logout());
          setMessage(errMsg);
        });
    }
  }, [id, token]);

  // "Прогоняет" если не положено тут быть
  useEffect(() => {
    if ((category !== "google" && !token) || (category === "google" && !id))
      navigate("/login");
  }, [id, token, category]);

  if (form && ready) {
    return (
      <Fragment>
        <Wrapper>
          <Box sx={{ width: 555, maxWidth: "100%" }}>
            <MainForm
              fields={formProps}
              callBack={() => {
                const { clearTemp, status, category, ...rest } = form;
                axios
                  .patch(`${VITE_API}/api/pub/persons/${id}`, rest, {
                    headers: {
                      "Content-Type": "application/json",
                      "auth-token": token,
                    },
                  })
                  .then((res) => {
                    setMessage("Данные успешно обновлены");
                  })
                  .catch((err) => {
                    const resStatus: number = err.response.status;
                    const errMsg: string = err.response.data.message;
                    // dispatch(logout());
                    setMessage(errMsg);
                  });
              }}
            />
          </Box>
        </Wrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else {
    return <NodataForm />;
  }
}
