import MainWrapper from "../components/MainWrapper";
import NodataGrid from "../components/NodataGrid";
import SectionTitle from "../components/SectionTitle";
import { XContainer } from "../components/StyledUI";
import ThemeWrapper from "../components/ThemeWrapper";
import VPrev from "../components/VPrev";
import { tagColor, useScrollLock } from "../misc/tools";
import { LS } from "../misc/tv";
import { ITV } from "../misc/types";
import { Box, Grid, useMediaQuery } from "@mui/material";
import axios from "axios";
import React, { Fragment, useEffect, useRef, useState } from "react";

const VITE_API = import.meta.env.VITE_API;
const LIMIT = 6;
const OFFSET = 6;

export default function Videos() {
  const matches = useMediaQuery("(max-width: 900px)");
  const [posts, setPosts] = useState<ITV[]>([]);
  const [loading, setLoading] = useState(true);
  const [finished, setFinished] = useState(false);
  const hiddenRef = useRef<HTMLDivElement>(null);
  const [page, setPage] = useState<number>(1);
  const { lockScroll, unlockScroll } = useScrollLock();

  const loadPosts = async (pageNumber: number) => {
    try {
      const response = await axios.get(`${VITE_API}/api/videos`, {
        params: {
          offset: (pageNumber - 1) * OFFSET,
          limit: LIMIT,
        },
      });

      const newPosts = response.data.map((x: ITV) => {
        return {
          ...x,
          source:
            x.source === "https://www.youtube.com/@abaza-tv"
              ? "Абаза-ТВ"
              : "АГТРК",
        };
      });
      if (!newPosts.length) {
        setFinished(true);
        window.removeEventListener("scroll", handleScroll);
      } else if (pageNumber === 1) {
        setPosts(newPosts);
      } else {
        setPosts((prevPosts) => [...prevPosts, ...newPosts]);
      }
    } catch (error) {
      console.error(error);
    }
    unlockScroll();
    setLoading(false);
  };

  useEffect(() => {
    loadPosts(page);
  }, [page]);

  const handleScroll = () => {
    if (
      hiddenRef.current &&
      window.scrollY + window.innerHeight >= hiddenRef.current.offsetTop + 300
    ) {
      lockScroll();
      setLoading(true);
      setTimeout(() => {
        const pageNumber = Math.ceil(posts.length / LIMIT) + 1;
        setPage(pageNumber);
      }, 500);
    }
  };

  useEffect(() => {
    if (!finished) window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [posts, loading]);

  if (posts.length) {
    return (
      <Fragment>
        <MainWrapper>
          <Box sx={{ paddingBlock: LS }}>
            <XContainer>
              <SectionTitle first="Новости ТВ" decorated />
              <Grid container spacing={4}>
                <Grid item sm={12}>
                  <ThemeWrapper>
                    <Grid container spacing={4}>
                      {posts?.map((x) => {
                        return (
                          <Grid item xs={12}>
                            <VPrev
                              tagClr={tagColor(x.source)}
                              {...x}
                              key={x.id}
                            />
                          </Grid>
                        );
                      })}
                    </Grid>
                  </ThemeWrapper>
                  {loading && <NodataGrid />}
                </Grid>
                {/* {!matches && (
                <Grid item sm={12}>
                  <MainSider />
                </Grid>
              )} */}
              </Grid>
            </XContainer>
          </Box>
        </MainWrapper>
        <div ref={hiddenRef}></div>
        {loading && <NodataGrid />}
      </Fragment>
    );
  } else return <NodataGrid />;
}
