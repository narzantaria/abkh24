import axios from "axios";
import MainSider from "../components/MainSider";
import MainWrapper from "../components/MainWrapper";
import NodataGrid from "../components/NodataGrid";
import PlaybillPrev from "../components/PlaybillPrev";
import SectionTitle from "../components/SectionTitle";
import { XContainer } from "../components/StyledUI";
import ThemeWrapper from "../components/ThemeWrapper";
import { LS } from "../misc/tv";
import { IPlaybill } from "../misc/types";
import { Box, Grid } from "@mui/material";
import React, { useEffect, useState } from "react";

const VITE_API = import.meta.env.VITE_API;

export default function Events() {
  const [data2, setData2] = useState<IPlaybill[] | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/pub/playbills`)
      .then((res) => setData2(res.data))
      .catch((err) => console.log(err));
  }, []);

  if (data2) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <SectionTitle first="Новости ТВ" decorated />
            <Grid container spacing={4}>
              <Grid item sm={12}>
                <ThemeWrapper>
                  <Grid container spacing={4}>
                    {data2?.map((x) => {
                      return (
                        <Grid item xs={12}>
                          <PlaybillPrev key={x.id} {...x} />
                        </Grid>
                      );
                    })}
                  </Grid>
                </ThemeWrapper>
              </Grid>
              <Grid item sm={12}>
                <MainSider />
              </Grid>
            </Grid>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataGrid />;
}
