import GoodsPrev from "../components/GoodsPrev";
import ItemPrev from "../components/ItemPrev";
import MainWrapper from "../components/MainWrapper";
import NodataGrid from "../components/NodataGrid";
import SectionTitle from "../components/SectionTitle";
import { StyledLink, XContainer } from "../components/StyledUI";
import ThemeWrapper from "../components/ThemeWrapper";
import { LS } from "../misc/tv";
import { IFolder, Item } from "../misc/types";
import { Box, Grid, Stack } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";

const VITE_API = import.meta.env.VITE_API;

interface IProps {
  data: IFolder[];
  goods: Item[];
}

export default function Goods() {
  const [data, setData] = useState<IFolder[] | null>(null);
  const [goods, setGoods] = useState<Item[] | null>(null);

  useEffect(() => {
    (async () => {
      //
      const res = await axios.get(`${VITE_API}/api/goods/router`);
      const res2 = await axios.get(`${VITE_API}/api/pub/goods`);
      setData(() => res.data);
      setGoods(() => res2.data);
    })();
  }, []);

  if (data && goods) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <SectionTitle first="Объявления" decorated />
            <ThemeWrapper>
              <Grid container spacing={4}>
                {data.map((x) => {
                  return (
                    <Grid item xs={6} key={x.title}>
                      <StyledLink to={`/goods/${x.name}`}>
                        <GoodsPrev {...x} />
                      </StyledLink>
                    </Grid>
                  );
                })}
              </Grid>
              <Grid container spacing={3} sx={{ mt: 4 }}>
                {goods.map((x) => (
                  <Grid item xs={12}>
                    <StyledLink to={`/goods/${x.source}/${x.sector}/${x.id}`}>
                      <ItemPrev {...x} key={x.id} />
                    </StyledLink>
                  </Grid>
                ))}
              </Grid>
            </ThemeWrapper>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataGrid />;
}
