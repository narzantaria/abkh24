import axios from "axios";
import MainSider from "../components/MainSider";
import MainWrapper from "../components/MainWrapper";
import NegativeWrapper from "../components/NegativeWrapper";
import NodataSingle from "../components/NodataSingle";
import SectionTitle from "../components/SectionTitle";
import { XContainer } from "../components/StyledUI";
import { LS } from "../misc/tv";
import { IPlaybill } from "../misc/types";
import { Box, Grid, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const VITE_API = import.meta.env.VITE_API;

export default function EventPage() {
  const { slug } = useParams();
  const [data2, setData2] = useState<IPlaybill | null>(null);

  useEffect(() => {
    axios
      .get(`${VITE_API}/api/playbills/${slug}`)
      .then((res) => setData2(res.data))
      .catch((err) => console.log(err));
  }, []);

  if (data2) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <NegativeWrapper>
                  <Stack spacing={4}>
                    {data2.title && (
                      <SectionTitle first={data2.title} decorated mb={0} />
                    )}
                    {data2.photo && data2.dir && (
                      <Box
                        component="img"
                        sx={{ width: 555, maxWidth: "100%" }}
                        src={`${VITE_API}/dist/${data2.dir}/${data2.photo}`}
                      />
                    )}
                    {data2.premiere && (
                      <Box component="div" sx={{ fontStyle: "italic" }}>
                        {new Date(data2.premiere).toISOString().slice(0, 10)}
                      </Box>
                    )}
                    {data2.text && (
                      <Box
                        component="div"
                        dangerouslySetInnerHTML={{ __html: data2.text }}
                      />
                    )}
                    {data2.source && (
                      <Stack spacing={2} direction="row" flexWrap="wrap">
                        <Typography variant="h6">
                          Ссылка на источник:
                        </Typography>
                        <Typography variant="h6" sx={{ fontStyle: "italic" }}>
                          {data2.source}
                        </Typography>
                      </Stack>
                    )}
                  </Stack>
                </NegativeWrapper>
              </Grid>
              <Grid item xs={12}>
                <MainSider />
              </Grid>
            </Grid>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataSingle />;
}
