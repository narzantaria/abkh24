import MainWrapper from "../components/MainWrapper";
import NegativeWrapper from "../components/NegativeWrapper";
import { Title2, XContainer } from "../components/StyledUI";
import MainForm from "../form/MainForm";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { reset } from "../lib/formSlice";
import { XLS } from "../misc/tv";
import { Box, Snackbar, Stack, Typography } from "@mui/material";
import axios from "axios";
import React, { Fragment, useState } from "react";
import { logout } from "../lib/authSlice";

const VITE_API = import.meta.env.VITE_API;

const fields = [
  {
    name: "title",
    required: true,
    label: "Имя",
    type: "string",
  },
  {
    name: "phone",
    label: "Телефон",
    type: "phone",
    extra: [
      { name: "Абхазия", code: "+7", flag: "abkh.png" },
      { name: "Россия", code: "+7", flag: "rus.png" },
    ],
  },
  {
    name: "email",
    label: "Email",
    type: "email",
  },
  {
    name: "text",
    required: true,
    label: "Сообщение",
    type: "text",
  },
];

export default function Contacts() {
  const { id, name, avatar, category, religion, subcription, token } =
    useAppSelector((state: ReduxState) => state.auth);
  const form = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();
  const [message, setMessage] = useState<string>("");

  return (
    <Fragment>
      <MainWrapper>
        <main>
          <Box sx={{ paddingBlock: XLS }}>
            <XContainer>
              <NegativeWrapper>
                <Box sx={{ width: 555, maxWidth: "100%" }}>
                  <Stack spacing={3}>
                    <Title2>Контакты</Title2>
                    <Typography variant="body1">
                      По вопросам сотрудничества или рекламы вы можете связаться с
                      нами напрямую или отправить заявку через форму обратной связи
                      (авторизированные пользователи).
                    </Typography>
                    <Stack direction="row" spacing={3}>
                      <Typography variant="body1" sx={{ fontWeight: 600 }}>
                        Номер телефона:
                      </Typography>
                      <Typography variant="body1">+7 (940) 713 92 77</Typography>
                    </Stack>
                    <Stack direction="row" spacing={3}>
                      <Typography variant="body1" sx={{ fontWeight: 600 }}>
                        Email:
                      </Typography>
                      <Typography variant="body1">info@abkhazia24.com</Typography>
                    </Stack>
                    {(id || category === "google") && (
                      <MainForm
                        fields={fields}
                        callBack={() => {
                          if (!form.phone && !form.email) {
                            setMessage(
                              "Необходимо ввести номер телефона или Email",
                            );
                            return;
                          }
                          axios
                            .post(`${VITE_API}/api/orders`, form, {
                              headers: {
                                "Content-Type": "application/json",
                                "auth-token": token,
                              },
                            })
                            .then((res) => {
                              setMessage("Заявка успешно отправлена");
                              dispatch(reset());
                            })
                            .catch((err) => {
                              const resStatus: number = err.response.status;
                              const errMsg: string = err.response.data.message;
                              if (resStatus === 401) {
                                dispatch(logout());
                              } else {
                                setMessage(errMsg);
                              }
                            });
                        }}
                      />
                    )}
                  </Stack>
                </Box>
              </NegativeWrapper>
            </XContainer>
          </Box>
        </main>
      </MainWrapper>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
