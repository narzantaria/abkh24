import { Box, Typography, styled } from '@mui/material'

const StyledHeader = styled(Box)`
width: 100%;
text-align: center;
padding-block: 20px;
background-color: #000;
`

const StyledContent = styled(Box)`
flex-grow: 1;
display: flex;
justify-content: center;
flex-direction: column;
align-items: center;
padding: 45px;
background-color: #f0f0f0;
`

const StyledImg = styled('img')`
max-width: 350px;
width: 100%;
`

const StyledWrapper = styled(Box)`
height: 100vh;
display: flex;
align-items: center;
flex-direction: column;
`

export default function BigScreen() {
  return (
    <StyledWrapper component="div">
      <StyledHeader component='header'>
        <StyledImg src="logo.png" />
      </StyledHeader>
      <StyledContent component="div">
        <Typography variant='h4'>
          Система определила, что страница открыта на персональном компьютере. Для установки приложения нужно открыть эту страницу на смартфоне. Поддерживаемые системы: Android, IOS.
        </Typography>
      </StyledContent>
    </StyledWrapper>
  )
}
