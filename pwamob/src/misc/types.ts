/**
 * Поле схемы 'universal'.
 * Используется также в построении формы
 **/
export interface IField {
  name: string;
  type: string;
  required?: boolean;
  schema?:
    | "one-to-one"
    | "one-to-many"
    | "many-to-many"
    | "many-to-many-bi"
    | undefined;
  level?: "host" | "reecipient" | undefined;
  ref?: string | undefined;
  limit?: number;
  label?: string;
  extra?: any[];
  data?: any[];
  unique?: boolean;
  value?: any;
}

// Данные юзера, localStorage
export interface IAuth {
  category: "google" | "user" | "editor" | "moderator" | "anon" | null;
  avatar: string | null;
  token: string | null;
  name: string | null;
  id: string | null;
  vendor: string | null;
  [field: string]: any;
}

/**
 * Данные самой формы, контролируют MainForm.
 * Одна из важнейших составляющих
 * Пар-р fapi, если что, перенесен в отдельный slice
 **/
export interface IForm {
  [field: string]: any;
  clearTemp: boolean;
  root: string;
  // fapi?: string | null;
}

/**
 * Типы данных меню товаров. Само меню приходит из сервера на основе файлов и папок "xmenu".
 * (/api/goods/router). Используются в store и кое-где в UI, потому экспортируем
 **/
export interface IFile {
  name: string;
  data: IField[];
}

export interface IFolder {
  name: string;
  title: string;
  files: IFile[];
}

// Общие данные (email, title etc)
export interface ISettings {
  title: string;
  description: string;
  email: string;
  phone: string;
  address: string;
  [key: string]: any;
}

// Интерфейс меню (самое обычное меню)
export interface IMenuElement {
  title: string;
  link?: string;
  children?: IMenuElement[];
}

// Общий интерфейс новости
export interface Item {
  id: string;
  title: string;
  text: string;
  // author: string;
  // author?: { id: string; name: string } | null;
  createdAt: Date;
  dir: string;
  photos: string[];
  price: number;
  district: string;
  source?: string;
  sector?: string;
  [key: string]: any;
}

// Общий интерфейс новости
export interface IPost {
  id: string;
  title: string;
  description?: string;
  text: string;
  // author: string;
  author?: [{ id: string; name: string }] | null;
  authorid?: string;
  authorname?: string;
  createdAt: Date;
  dir: string;
  photos?: string[] | null;
  video?: string | null;
  category?: string;
  textclr?: string | null;
  tagClr?: { background: string; color: string; bordercolor: string } | null;
  comment: IComment[];
  key?: string;
  sourceurl?: string;
  tags: string[];
  youtube?: string | null;
  rutube?: string | null;
  looks?: string | null;
}

// Интерфейс комментария
export interface IComment {
  id: string;
  title: string;
  text: string;
  author?: {
    id: string;
    title: string;
    dir?: string | null;
    photos: string[] | null;
  } | null;
  createdAt: Date;
  dir: string;
  img: string;
}

/*
interface Item {
  id: string;
  title: string;
  author?: { id: string; name: string } | null;
  createdAt: Date;
  dir: string;
  photos: string[];
  category: string;
  textclr?: string | null;
  tagClr?: { background: string; color: string; bordercolor: string } | null;
}
*/

// Интерфейс фильма
export interface IMovie {
  id: string;
  title?: string;
  premiere?: Date;
  dir: string;
  photo: string;
  rating: number;
  url: string;
  textclr?: string | null;
  tagClr?: { background: string; color: string; bordercolor: string } | null;
  pb?: number | null;
}

// Интерфейс афиши
export interface IPlaybill {
  id: string;
  dir: string;
  title?: string;
  text?: string;
  photo: string;
  createdAt?: Date;
  premiere?: string;
  author: string;
  director: string;
  genre: string;
  url: string;
  source: string;
  textclr?: string | null;
  tagClr?: { background: string; color: string; bordercolor: string } | null;
}

export interface IBanner {
  id: string;
  dir: string;
  title?: string;
  url: string;
  description?: string;
  text?: string;
  img: string;
  position: number;
  createdAt?: Date;
}

export interface IFuneral {
  id: string;
  title: string;
  text: string;
  createdAt: Date;
  dir: string;
  photo: string;
}

// Данные ГС
export interface IMain {
  [field: string]: any;
}

// Tags:
export interface ITags {
  // category: string[];
  // source: string[];
  unique_tags: string[];
}

export interface ITV {
  id: string;
  title: string;
  videoId: string;
  source: string;
  createdAt: Date;
}

// ИНтерфейс общей страницы
export interface IPage {
  id: string;
  title: string;
  description: string;
  text: string;
  createdAt: Date;
  dir: string;
  photos: string[];
  video: string;
}
