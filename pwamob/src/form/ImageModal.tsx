import { Box, Modal, styled } from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { SlPicture } from "react-icons/sl";
import FileLoad from "./FileLoad";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { setField } from "../lib/formSlice";

// const VITE_API = process.env.VITE_API;
const VITE_API = import.meta.env.VITE_API;

const ModalWrapper = styled("div")(() => ({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "100%",
  maxWidth: "1111px",
}));

interface IProps {
  clearTemp?: boolean;
  name: string;
  callBack?: any;
  root?: string;
}

export default function ImageModal({
  clearTemp = false,
  name,
  callBack,
  root = "common",
}: IProps) {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const state = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();

  const photo = state[name];

  useEffect(() => {
    if (photo) dispatch(setField({ field: "clearTemp", value: false }));
  }, [photo]);

  return (
    <Fragment>
      <Box
        sx={{
          fontSize: "100px",
          lineHeight: 0,
          cursor: "pointer",
          width: "max-content",
          border: "1px solid #ddd",
          borderRadius: "4px",
          overflow: "hidden",
        }}
        onClick={handleOpen}
      >
        {photo ? (
          <img
            width={200}
            src={
              VITE_API +
              "/dist/" +
              (state.dir || state.root || "common") +
              "/" +
              photo
            }
          />
        ) : (
          <SlPicture />
        )}
      </Box>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <ModalWrapper>
          <FileLoad
            callBack={(arg: string) => {
              dispatch(setField({ field: [name], value: arg }));
            }}
            root={state.root}
            clearTemp={state.clearTemp}
            title="Upload files"
            close={handleClose}
          >
            {photo && (
              <img
                width={200}
                src={VITE_API + "/dist/" + state.root + "/" + photo}
                style={{ marginBottom: "25px", cursor: "pointer" }}
              />
            )}
          </FileLoad>
        </ModalWrapper>
      </Modal>
    </Fragment>
  );
}
