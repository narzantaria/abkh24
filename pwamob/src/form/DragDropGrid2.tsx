import {
  DndContext,
  DragEndEvent,
  DragOverlay,
  DragStartEvent,
  MouseSensor,
  PointerSensor,
  TouchSensor,
  closestCenter,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import {
  SortableContext,
  arrayMove,
  rectSortingStrategy,
  useSortable,
} from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import { Box, Grid, styled } from "@mui/material";
import React, {
  forwardRef,
  FC,
  HTMLAttributes,
  CSSProperties,
  useCallback,
  useState,
  useEffect,
  memo,
} from "react";
import { useIsMount } from "../misc/tools";
import DeleteModal from "../components/DeleteModal";

export type ItemProps = HTMLAttributes<HTMLDivElement> & {
  id: string;
  withOpacity?: boolean;
  isDragging?: boolean;
  cb?: () => void;
};

interface IProps {
  data: string[];
  cb: (arg: number[]) => void;
  remove: (arg: number) => void;
}

function compareArrays(arr1: any[], arr2: any[]): number[] {
  const indexes = [];
  for (let i = 0; i < arr2.length; i++) {
    const index = arr1.indexOf(arr2[i]);
    indexes.push(index);
  }
  return indexes;
}

function DragDropGrid2({ cb, data, remove }: IProps) {
  const isMount = useIsMount();
  const [items, setItems] = useState<string[]>(data);
  const [activeId, setActiveId] = useState<string | null>(null);
  const sensors = useSensors(
    useSensor(PointerSensor, {
      activationConstraint: {
        delay: 300,
        tolerance: 300,
      },
    }),
    useSensor(MouseSensor),
    useSensor(TouchSensor),
  );

  const handleDragStart = useCallback((event: DragStartEvent) => {
    setActiveId(String(event.active.id));
  }, []);

  const handleDragEnd = useCallback((event: DragEndEvent) => {
    const { active, over } = event;
    if (active.id !== over?.id) {
      setItems((items) => {
        const oldIndex = items.indexOf(String(active.id));
        const newIndex = items.indexOf(String(over!.id));

        return arrayMove(items, oldIndex, newIndex);
      });
    }
    setActiveId(null);
  }, []);

  const handleDragCancel = useCallback(() => {
    setActiveId(null);
  }, []);

  useEffect(() => {
    if (!isMount && data) {
      cb(compareArrays(data, items));
    }
  }, [items]);

  if (items.length) {
    return (
      <DndContext
        sensors={sensors}
        collisionDetection={closestCenter}
        onDragStart={handleDragStart}
        onDragEnd={handleDragEnd}
        onDragCancel={handleDragCancel}
      >
        <SortableContext items={items} strategy={rectSortingStrategy}>
          <Grid container spacing={2}>
            {items?.length &&
              items.map((id, index) => (
                <SortableItem key={id} id={id} cb={() => remove(index)} />
              ))}
          </Grid>
        </SortableContext>
        <DragOverlay adjustScale style={{ transformOrigin: "0 0 " }}>
          {activeId ? <Item id={activeId} isDragging /> : null}
        </DragOverlay>
      </DndContext>
    );
  } else return <></>;
}

const SortableItem: FC<ItemProps> = (props: any) => {
  const {
    isDragging,
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
  } = useSortable({ id: props.id });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition: transition || undefined,
  };

  return (
    <Item
      ref={setNodeRef}
      style={style}
      withOpacity={isDragging}
      {...props}
      {...attributes}
      {...listeners}
    />
  );
};

const Item = forwardRef<HTMLDivElement, ItemProps>(
  ({ id, withOpacity, isDragging, style, ...props }, ref) => {
    const inlineStyles: CSSProperties = {
      position: "relative", // new
      opacity: withOpacity ? "0.5" : "1",
      cursor: isDragging ? "grabbing" : "grab",
      backgroundColor: "#ffffff",
      boxShadow: isDragging
        ? "rgb(63 63 68 / 5%) 0px 2px 0px 2px, rgb(34 33 81 / 15%) 0px 2px 3px 2px"
        : "rgb(63 63 68 / 5%) 0px 0px 0px 1px, rgb(34 33 81 / 15%) 0px 1px 3px 0px",
      transform: isDragging ? "scale(1.05)" : "scale(1)",
      ...style,
    };

    return (
      <Grid item xs={3} ref={ref} {...props}>
        <Box sx={{ pb: "100%", position: "relative", ...inlineStyles }}>
          <BachgroundContain url={id} />
          <Box
            component="div"
            sx={{
              position: "absolute",
              top: "8px",
              right: "8px",
            }}
          >
            <DeleteModal
              callBack={(_: any) => {
                if (props.cb) props.cb();
              }}
            />
          </Box>
        </Box>
      </Grid>
    );
  },
);

const BachgroundContain = styled("div")(
  ({ url }: { url: string }) => `
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  background: url(${url}) no-repeat center center / contain;
  `,
);

export default memo(DragDropGrid2);
