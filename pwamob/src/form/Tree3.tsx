import React, { Fragment, useState } from "react";
import {
  FormControl,
  FormLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { IField } from "../misc/types";
import { v4 as uuidv4 } from "uuid";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { removeField, setField } from "../lib/formSlice";

// Пропс MainForm
interface IProps {
  tree: any[];
  fields: IField[];
}

// Ветвь дерева
interface IBranch {
  name: string;
  branch: string;
  children: any[];
}

// Глубина ветви дерева = новый Select
interface ISelectsLevel {
  level: number;
  label?: string | null;
  name: string;
  data: string[];
}

// --------------------------------
interface TreeNode {
  name: string;
  branch: string;
  children?: TreeNode[];
}

interface Data {
  [key: string]: string;
}

interface Result {
  name: string;
  data: string[];
  level: number;
  label?: string | null;
}

/*
function findElement(
  tree: TreeNode[], // НЕИЗМЕННО
  key: string, // ЗНАЧЕНИЕ branch!!! типа Toyota, Audi, X8, Pajero и тд.
  fields: IField[] // НЕИЗМЕННО
): Result[] {
  // let result: Result | null = null;
  let result: Result[] = [];
  let level = 0;
  function seek(node: TreeNode[], key: string, level: number) {
    // for (let x = 0; x < node.length; x++) {
    //   const item = node[x];
    //   if (item.name === key) {
    //     const data = node.map((child) => child.name);
    //     result = {
    //       name: item.branch,
    //       data,
    //       level,
    //       label: fields.filter((x) => x.name == node[0].branch)[0].label,
    //     };
    //     break;
    //   } else if (item.children) {
    //     seek(item.children, key, level + 1);
    //   }
    // }
    if (node.some((x) => x.name === key)) {
      const item = node.filter((x) => x.name === key)[0];
      result.push({
        name: item.name,
        data: node.map((child) => child.name),
        level,
        label: item.branch,
      });
      if (item.children) {
        level++;
        seek(item.children);
      }
    }
    // const item = node.filter(x=>x.name===key)[0]
  }
  seek(tree, key, 0);
  return result;
}
*/

function processTree(
  tree: TreeNode[], // НЕИЗМЕННО, дерево целиком
  data: Data, // form state (это значения товара)
  fields: IField[], // НЕИЗМЕННО (это formProps)
): Result[] {
  const result: Result[] = [];
  // const keys = Object.keys(data);
  const keys = fields.filter((x) => x.type === "branch").map((x) => x.name);
  // console.log(keys); // [ "brand", "model" ]
  // console.log(tree);

  // for (let x = 0; x < keys.length; x++) {
  //   const key = data[keys[x]];
  //   const xxx = findElement(tree, key, fields);
  //   // console.log(xxx);
  //   if (xxx) result.push(xxx);
  // }
  // let level = 0;
  function seek(node: TreeNode[], level: number) {
    // console.log(node);
    // console.log(level);
    let item: TreeNode | null = null;
    for (let x = 0; x < keys.length; x++) {
      const value = data[keys[x]];
      if (node.some((x) => x.name === value)) {
        item = node.filter((x) => x.name === value)[0];
        break;
      }
    }
    if (item) {
      result.push({
        name: item.branch, // БЫЛО name !!!!!
        data: node.map((child) => child.name),
        level,
        // label: item.branch,
        label: fields.filter((x) => x.name == item?.branch)[0].label,
      });
      if (item.children) {
        seek(item.children!, level + 1);
      }
    }
  }
  seek(tree, 0);
  return result;
}

/**
 * Пока не сильно заморачивался, - главное сейчас работает.
 * Возможно, есть вариант упростить, но вроде все правильно...
 * **/
function getBranches(arr: IBranch[], name: string) {
  let result: IBranch[] = [];
  if (arr && arr.length)
    for (let x = 0; x < arr.length; x++) {
      if (arr[x].name == name && arr[x].children && arr[x].children.length) {
        result = result.concat(arr[x].children);
      } else getBranches(arr[x].children, name);
    }
  return result;
}

export default function Tree3({ tree, fields }: IProps) {
  const form = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();
  const [branches, setBranches] = useState<ISelectsLevel[] | null>(
    processTree(tree, form, fields).length
      ? processTree(tree, form, fields)
      : [
          {
            name: tree[0].branch,
            data: tree.map((x) => x.name),
            label: fields.filter((x) => x.name == tree[0].branch)[0].label,
            level: 0,
          },
        ],
  );

  /**
   * Обновление массива ветвей при изменении Select...
   * И перерендеринг...
   * **/
  function handleSelect(level: number, value: string) {
    const children: IBranch[] = getBranches(tree, value);
    let branchesProxy: ISelectsLevel[];
    if (branches && children.length) {
      branchesProxy = branches.filter((x) => x.level <= level);
      const newBranchLevel: ISelectsLevel = {
        level: level + 1,
        data: children.map((item: IBranch) => item.name),
        label: fields.filter((x) => x.name == children[0].branch)[0].label,
        name: children[0].branch,
      };
      branchesProxy.push(newBranchLevel);
      setBranches(branchesProxy);
    }
  }

  return (
    <Fragment>
      {/* <h3
        onClick={() => {
          console.log(form);
          console.log(branches);
          console.log(processTree(tree, form, fields));
        }}
      >
        11111
      </h3> */}
      {form &&
        branches?.map((branch: ISelectsLevel) => (
          <FormControl key={branch.name}>
            <FormLabel>{branch.label}</FormLabel>
            {branch.data && (
              <Select
                name={String(branch.name)}
                label={branch.label}
                value={form[branch.name]}
                onChange={async (e: SelectChangeEvent) => {
                  const detector: string = uuidv4();
                  handleSelect(branch.level, e.target.value);
                  await dispatch(
                    setField({ field: branch.name, value: e.target.value }),
                  );
                  await dispatch(
                    setField({ field: "detector", value: detector }),
                  );
                  const higherBranches = branches.filter(
                    (higherBranch) => higherBranch.level > branch.level,
                  );
                  if (higherBranches.length) {
                    for (let x = 0; x < higherBranches.length; x++) {
                      await dispatch(removeField(higherBranches[x].name));
                    }
                  }
                }}
              >
                {branch.data.map((x: string) => (
                  <MenuItem key={x} value={x}>
                    {x}
                  </MenuItem>
                ))}
              </Select>
            )}
          </FormControl>
        ))}
    </Fragment>
  );
}
