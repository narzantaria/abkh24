import { Box, Chip, Paper } from "@mui/material";
import React from "react";
import { modifyArr } from "../misc/tools";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { setField } from "../lib/formSlice";

interface Iprops {
  callBack: any;
  data?: string[];
  name: string;
  extra: string[];
}

export default function Tags({ callBack, data, name, extra }: Iprops) {
  const state = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();
  const tags2 = state[name];

  return (
    <Paper variant="outlined" elevation={0} sx={{ p: 2 }}>
      {extra.length && (
        <Box
          sx={{
            display: "flex",
            rowGap: 2,
            columnGap: 2,
            flexWrap: "wrap",
          }}
        >
          {extra.map((item, index) => (
            <Chip
              key={index}
              sx={{
                cursor: "pointer",
                backgroundColor: state[name]?.some((y: string) => y == item)
                  ? "#ddd"
                  : "initial",
              }}
              label={item}
              variant="outlined"
              onClick={() => {
                dispatch(
                  setField({
                    field: [name],
                    value: modifyArr(state[name] || [], item),
                  }),
                );
              }}
            />
          ))}
        </Box>
      )}
    </Paper>
  );
}
