import React, { Fragment, useEffect, useState } from "react";
import FileLoad from "./FileLoad";
import { Box, Modal, Snackbar, styled } from "@mui/material";
import { TfiGallery } from "react-icons/tfi";
import DragDropGrid2 from "./DragDropGrid2";
import { ReduxState, useAppDispatch, useAppSelector } from "../lib";
import { setField } from "../lib/formSlice";
import { isEqual } from "lodash";
import ErrorBoundary from "../misc/ErrorBoundary";

const VITE_API = import.meta.env.VITE_API;

function arraysEqual(arr1: any[], arr2: any[]) {
  return isEqual(arr1.sort(), arr2.sort());
}

const ModalWrapper = styled("div")(() => ({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "100%",
  maxWidth: "1111px",
}));

interface IProps {
  name: string;
}

function processArray(arr1: any[], arr2: number[]): any[] | null {
  if (arr2.length > arr1.length) return null;
  if (arr2.reduce((a, b) => (b > a ? b : a)) >= arr2.length) return null;
  const result = [];
  for (let i = 0; i < arr2.length; i++) {
    const index = arr2[i];
    if (index >= 0 && index < arr1.length) {
      result.push(arr1[index]);
    }
  }
  return result;
}

function GalleryModal({
  name,
}: IProps) {
  const [open, setOpen] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const state = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();

  const data: string[] = state[name] || [];

  useEffect(() => {
    if (data.length) dispatch(setField({ field: "clearTemp", value: false }));
  }, [data]);

  return (
    <Fragment>
      <Box
        sx={{
          fontSize: "100px",
          lineHeight: 0,
          cursor: "pointer",
          width: "max-content",
          border: "1px solid #ddd",
          borderRadius: "4px",
          overflow: "hidden",
        }}
        onClick={handleOpen}
      >
        <TfiGallery />
      </Box>
      <ErrorBoundary>
        {data?.length > 0 ? (
          <DragDropGrid2
            data={data.map(
              (x: string) =>
                `${VITE_API}/dist/${state.dir || state.root || "common"}/${x}`,
            )}
            cb={(arg) => {
              const newData = processArray(data, arg);
              if (newData) {
                dispatch(
                  setField({
                    field: [name],
                    value: newData,
                  }),
                );
              }
            }}
            remove={(arg) => {
              const dataProxy = [...data];
              dataProxy.splice(arg, 1);
              dispatch(
                setField({
                  field: [name],
                  value: dataProxy,
                }),
              );
            }}
          />
        ) : <></>}
      </ErrorBoundary>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <ModalWrapper>
          <FileLoad
            callBack={(arg: string) => {
              if (data.some((elem: string) => elem == arg)) {
                setMessage("Изображение уже добавлено");
              } else {
                dispatch(setField({ field: [name], value: [...data, arg] }));
              }
            }}
            root={state.root}
            clearTemp={state.clearTemp}
            title="Upload files"
            close={handleClose}
          />
        </ModalWrapper>
      </Modal>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}

export default GalleryModal;
