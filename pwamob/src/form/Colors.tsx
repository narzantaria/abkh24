import { useIsMount } from "../misc/tools";
import { Box, MenuItem, Select, SelectChangeEvent, Stack } from "@mui/material";
import { capitalize } from "lodash";
import React from "react";

interface IColor {
  name: string;
  value: string;
}

interface IProps {
  label: string;
  value: string;
  extra: IColor[];
  callBack: (arg: string) => void;
}

export default function Colors({ value, extra, label, callBack }: IProps) {
  const isMount = useIsMount();
  if (extra) {
    return (
      <div>
        {/* <h3 onClick={()=>console.log(extra)}>lkhljkhlk</h3> */}
        <Select
          defaultValue={value}
          value={value}
          label={label}
          onChange={(e: SelectChangeEvent) => {
            // dispatch(setField({ field: name, value: e.target.value }));
            callBack(e.target.value);
          }}
          sx={{ width: "100%" }}
        >
          {extra?.length &&
            extra.map((item: IColor, index: number) => {
              // const itemKey: string = uuidv4();
              return (
                <MenuItem value={item.name} key={index}>
                  <Stack direction="row" spacing={1}>
                    <Color value={item.value} />
                    <div>{capitalize(item.name)}</div>
                  </Stack>
                </MenuItem>
              );
            })}
        </Select>
      </div>
    );
  } else return null;
}

const Color = ({ value }: { value: string }) => {
  return (
    <Box
      sx={{
        width: "25px",
        height: "25px",
        backgroundColor: value,
        border: "1px solid #ddd",
      }}
    />
  );
};
