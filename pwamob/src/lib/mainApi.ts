// Общие данные
import { IMain } from "../misc/types";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const VITE_API = import.meta.env.VITE_API;

export const mainApi = createApi({
  reducerPath: "mainApi",
  baseQuery: fetchBaseQuery({ baseUrl: `${VITE_API}/api` }),
  endpoints: (builder) => ({
    getMainData: builder.query<IMain[], null>({
      query: () => "pub",
    }),
  }),
});

export const { useGetMainDataQuery } = mainApi;
