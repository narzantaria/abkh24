import { createSlice } from "@reduxjs/toolkit";

export interface Item {
  id: string;
  title: string;
  image: string;
}

const initialState: { items: Item[] | null } = {
  items: null,
};

export const itemsSlice = createSlice({
  name: "items",
  initialState,
  reducers: {
    setItems: (state, action) => {
      state.items = action.payload;
    },
    clearItems: (state) => {
      return initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setItems, clearItems } = itemsSlice.actions;

export default itemsSlice.reducer;
