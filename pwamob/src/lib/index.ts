import {
  configureStore,
  type ThunkAction,
  type Action,
} from "@reduxjs/toolkit";
import {
  useSelector as useReduxSelector,
  useDispatch as useReduxDispatch,
  type TypedUseSelectorHook,
} from "react-redux";

import itemsSlice from "./itemsSlice";
import textSlice from "./textSlice";
import { settingsApi } from "./settingsApi";
import { mainApi } from "./mainApi";
import { menuApi } from "./menuApi";
import themeSlice from "./themeSlice";
import formSlice from "./formSlice";
import authSlice from "./authSlice";
import miscSlice from "./miscSlice";
import searchSlice from "./searchSlice";
import tagsSlice from "./tagsSlice";
import { tagsApi } from "./tagsApi";
import { userContentApi } from "./userContentApi";
import { setupListeners } from "@reduxjs/toolkit/query";
import devSlice from "./devSlice";
// ...

export const store = configureStore({
  reducer: {
    auth: authSlice,
    items: itemsSlice,
    text: textSlice,
    theme: themeSlice,
    form: formSlice,
    misc: miscSlice,
    search: searchSlice,
    tags: tagsSlice,
    device: devSlice, 
    // Add the generated reducer as a specific top-level slice
    [settingsApi.reducerPath]: settingsApi.reducer,
    [mainApi.reducerPath]: mainApi.reducer,
    [menuApi.reducerPath]: menuApi.reducer,
    [tagsApi.reducerPath]: tagsApi.reducer,
    [userContentApi.reducerPath]: userContentApi.reducer,
  },
  devTools: process.env.NODE_ENV !== "production",
  // Adding the api middleware enables caching, invalidation, polling,
  // and other useful features of `rtk-query`.
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({}).concat([
      settingsApi.middleware,
      mainApi.middleware,
      menuApi.middleware,
      tagsApi.middleware,
      userContentApi.middleware,
    ]),
});

// optional, but required for refetchOnFocus/refetchOnReconnect behaviors
// see `setupListeners` docs - takes an optional callback as the 2nd arg for customization
setupListeners(store.dispatch);

/* Types */
export type ReduxStore = typeof store;
export type ReduxState = ReturnType<typeof store.getState>;
export type ReduxDispatch = typeof store.dispatch;
export type ReduxThunkAction<ReturnType = void> = ThunkAction<
  ReturnType,
  ReduxState,
  unknown,
  Action
>;

export const useAppDispatch = () => useReduxDispatch<ReduxDispatch>();
export const useAppSelector: TypedUseSelectorHook<ReduxState> =
  useReduxSelector;
