// Общие данные
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const VITE_API = import.meta.env.VITE_API;

export const tagsApi = createApi({
  reducerPath: "tagsApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${VITE_API}/api/pub`,
  }),
  endpoints: (builder) => ({
    getTagsData: builder.query<string[], null>({
      query: () => "tags",
      // Pick out data and prevent nested properties in a hook or selector
      transformResponse: (response: [{ unique_tags: string[] }], meta, arg) =>
        response[0].unique_tags,
    }),
  }),
});

export const { useGetTagsDataQuery } = tagsApi;
