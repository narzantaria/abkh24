// Общие данные
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { ISettings } from "../misc/types";

const VITE_API = import.meta.env.VITE_API;

export const settingsApi = createApi({
  reducerPath: "settingsApi",
  baseQuery: fetchBaseQuery({ baseUrl: `${VITE_API}/api` }),
  endpoints: (builder) => ({
    getSettingsData: builder.query<ISettings, null>({
      query: () => "settings",
    }),
  }),
});

export const { useGetSettingsDataQuery } = settingsApi;
