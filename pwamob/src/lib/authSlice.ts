import { createSlice } from "@reduxjs/toolkit";
import { IAuth } from "../misc/types";

const VITE_API = import.meta.env.VITE_API;

const initialState: IAuth = {
  category: null,
  avatar: null,
  token: null,
  name: null,
  id: null,
  vendor: null,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    nnmAuth: (state, action) => {
      const { vendor } = action.payload;
      state.avatar = "anon.png";
      state.token = null;
      state.name = null;
      state.category = "anon";
      state.id = null;
      state.vendor = vendor || null;
      localStorage.setItem("avatar", "anon.png");
      localStorage.setItem("token", "");
      localStorage.setItem("name", "");
      localStorage.setItem("category", "anon");
      localStorage.setItem("vendor", vendor || "");
    },
    setAuth: (state, action) => {
      const { avatar, token, name, category, id, ...rest } = action.payload;
      state.avatar = avatar;
      state.token = token || null;
      state.name = name;
      state.category = category;
      state.id = id || null;
      state.vendor = null;
      localStorage.setItem("avatar", avatar);
      localStorage.setItem("token", token || "");
      localStorage.setItem("name", name);
      localStorage.setItem("category", category);
      localStorage.setItem("id", id || "");
      localStorage.setItem("vendor", "");
      const keys = Object.keys(rest);
      for (let x = 0; x < keys.length; x++) {
        state[keys[x]] = action.payload[keys[x]];
      }
    },
    logout: (state) => {
      state.avatar = null;
      state.token = null;
      state.name = null;
      state.category = null;
      state.id = null;
      localStorage.setItem("avatar", "");
      localStorage.setItem("token", "");
      localStorage.setItem("name", "");
      localStorage.setItem("category", "");
      localStorage.setItem("id", "");
    },
  },
});

// Action creators are generated for each case reducer function
export const { nnmAuth, setAuth, logout } = authSlice.actions;

export default authSlice.reducer;
