import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { ReduxState } from ".";
import { IForm } from "../misc/types";

const VITE_API = import.meta.env.VITE_API;

interface Item {
  id: number;
  title: string;
  source: string;
}

interface Post {
  id: number;
  title: string;
}

interface Content {
  goods: Item[] | null;
  posts: Post[] | null;
}

// const { id, token } = useAppSelector((state: ReduxState) => state.auth);

export const userContentApi = createApi({
  reducerPath: "userContentApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${VITE_API}/api/pub`,
    prepareHeaders: (headers, { getState }) => {
      const token = (getState() as ReduxState).auth.token;

      // If we have a token set in state, let's assume that we should be passing it.
      if (token) {
        headers.set("auth-token", token);
      }

      return headers;
    },
  }),
  endpoints: (builder) => ({
    getUserContentData: builder.query<Content, string>({
      query: (id: string) => `master/${id}`,
    }),
    addPost: builder.mutation<IForm, Partial<IForm>>({
      query(body) {
        return {
          url: `posts`,
          method: "POST",
          body,
        };
      },
    }),
  }),
});

export const { useGetUserContentDataQuery, useAddPostMutation } =
  userContentApi;
