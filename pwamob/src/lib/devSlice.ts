import { createSlice } from "@reduxjs/toolkit";

export interface IDevice {
  device: "pc" | "mob";
}

const initialState: IDevice = {
  device: "pc",
};

export const devSlice = createSlice({
  name: "dev",
  initialState,
  reducers: {
    setDevice: (state, action) => {
      localStorage.setItem("dev", action.payload);
      state.device = action.payload;
    },
    resetDevice: (state) => {
      localStorage.setItem("dev", "pc");
      return initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setDevice, resetDevice } = devSlice.actions;

export default devSlice.reducer;
