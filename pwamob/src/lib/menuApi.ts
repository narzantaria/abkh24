import { IMenuElement } from "../misc/types";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const VITE_API = import.meta.env.VITE_API;

export const menuApi = createApi({
  reducerPath: "menuApi",
  baseQuery: fetchBaseQuery({ baseUrl: `${VITE_API}/api` }),
  endpoints: (builder) => ({
    getMenuData: builder.query<IMenuElement[], null>({
      query: () => "menu",
    }),
  }),
});

export const { useGetMenuDataQuery } = menuApi;
