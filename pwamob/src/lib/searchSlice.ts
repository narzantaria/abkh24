import { createSlice } from "@reduxjs/toolkit";

export interface ISparams {
  path: string;
  word: string;
}

const initialState: ISparams = {
  path: "posts",
  word: "",
};

export const searchSlice = createSlice({
  name: "search",
  initialState,
  reducers: {
    setSparams: (state, action) => {
      return action.payload;
    },
    resetSparams: (state) => {
      return initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setSparams, resetSparams } = searchSlice.actions;

export default searchSlice.reducer;
