/**
 * 1. Проблема с ThinkTank.
 * **/
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Main from "./routes/Main";
import GoogleAnalytics from "./components/GoogleAnalytics";
import YM from "./components/YM";
import ThinkTank from "./components/ThinkTank";
import SuperHeader from "./components/SuperHeader";
import Basement from "./components/Basement";
import Up from "./components/Up";
import Login from "./routes/Login";
import Register from "./routes/Register";
import Login2 from "./routes/Login2";
import { Box, useMediaQuery } from "@mui/material";
import Misc from "./routes/Misc";
import { DIRTY_CLR } from "./misc/tv";
import News from "./routes/News";
import Post from "./routes/Post";
import Goods from "./routes/Goods";
import AddPost from "./routes/AddPost";
import AddGoods from "./routes/AddGoods";
import AddGoods2 from "./routes/AddGoods2";
import AddGoods3 from "./routes/AddGoods3";
import Cabinet from "./routes/Cabinet";
import CommentPage from "./routes/CommentPage";
import EditSelf from "./routes/EditSelf";
import EditGood from "./routes/EditGood";
import EditPost from "./routes/EditPost";
import Contacts from "./routes/Contacts";
import Facebook from "./routes/Facebook";
import Finish from "./routes/Finish";
import Funeral from "./routes/Funeral";
import Funerals from "./routes/Funerals";
import GoodsSector from "./routes/GoodsSector";
import GoodsPage from "./routes/GoodsPage";
import Good from "./routes/Good";
import Marketing from "./routes/Marketing";
import EventPage from "./routes/EventPage";
import Events from "./routes/Events";
import SearchPage from "./routes/SearchPage";
import Videos from "./routes/Videos";
import VideoPage from "./routes/VideoPage";
import BigScreen from "./BigScreen";
import { ReduxState, useAppDispatch, useAppSelector } from "./lib";
import { useEffect } from "react";
import { useIsMount } from "./misc/tools";
import { setDevice } from "./lib/devSlice";

function App() {
  const matches = useMediaQuery("(max-width: 555px)");
  const isMount = useIsMount();
  const { device } = useAppSelector((state: ReduxState) => state.device);
  const dispatch = useAppDispatch();

  useEffect(() => {
    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

    if (isMobile) {
      console.log('Устройство - телефон');
      dispatch(setDevice("mob"))
    } else {
      console.log('Устройство - ПК');
      dispatch(setDevice("pc"))
    }
  }, [])

  if (matches && device === "mob") {
    return (
      <Box
        sx={{
          width: "100%",
          maxWidth: "555px",
          margin: "0 auto",
          borderLeft: `1px solid ${DIRTY_CLR}`,
          borderRight: `1px solid ${DIRTY_CLR}`,
        }}
      >
        <BrowserRouter>
          <GoogleAnalytics />
          <YM />
          <ThinkTank />
          <SuperHeader />
          <Routes>
            <Route path="/" element={<Main />} />
            <Route path="login" element={<Login />} />
            <Route path="login2" element={<Login2 />} />
            <Route path="register" element={<Register />} />
            <Route path="misc/:name" element={<Misc />} />
            <Route path="news" element={<News />} />
            <Route path="news/:pid" element={<Post />} />
            <Route path="goods" element={<Goods />} />
            <Route path="goods/:model" element={<GoodsSector />} />
            <Route path="goods/:model/:sector" element={<GoodsPage />} />
            <Route path="goods/:model/:sector/:slug" element={<Good />} />
            <Route path="addpost" element={<AddPost />} />
            <Route path="addgoods" element={<AddGoods />} />
            <Route path="addgoods/:model" element={<AddGoods2 />} />
            <Route path="addgoods/:model/:sector" element={<AddGoods3 />} />
            <Route path="cabinet" element={<Cabinet />} />
            <Route path="cabinet/comments/:id" element={<CommentPage />} />
            <Route path="cabinet/edit" element={<EditSelf />} />
            <Route path="cabinet/goods/:model/:slug" element={<EditGood />} />
            <Route path="cabinet/posts/:slug" element={<EditPost />} />
            <Route path="contacts" element={<Contacts />} />
            <Route path="facebook" element={<Facebook />} />
            <Route path="finish" element={<Finish />} />
            <Route path="funerals" element={<Funerals />} />
            <Route path="funerals/:id" element={<Funeral />} />
            <Route path="marketing" element={<Marketing />} />
            <Route path="playbill" element={<Events />} />
            <Route path="playbill/:slug" element={<EventPage />} />
            <Route path="search" element={<SearchPage />} />
            <Route path="videos" element={<Videos />} />
            <Route path="videos/:slug" element={<VideoPage />} />
          </Routes>
          <Basement />
          <Up />
        </BrowserRouter>
      </Box>
    );
  } else return <BigScreen />
}

export default App;
