--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2
-- Dumped by pg_dump version 16.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: agro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.agro (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    brand character varying,
    sector character varying NOT NULL,
    category character varying NOT NULL,
    condition character varying,
    age integer DEFAULT 0,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.agro OWNER TO postgres;

--
-- Name: agro_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.agro_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.agro_id_seq OWNER TO postgres;

--
-- Name: agro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.agro_id_seq OWNED BY public.agro.id;


--
-- Name: animal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.animal (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    age integer DEFAULT 0,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.animal OWNER TO postgres;

--
-- Name: animal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.animal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.animal_id_seq OWNER TO postgres;

--
-- Name: animal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.animal_id_seq OWNED BY public.animal.id;


--
-- Name: anon; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.anon (
    id integer NOT NULL,
    title character varying NOT NULL,
    vendor character varying NOT NULL,
    status boolean DEFAULT true,
    lastad timestamp without time zone DEFAULT now(),
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.anon OWNER TO postgres;

--
-- Name: anon_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.anon_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.anon_id_seq OWNER TO postgres;

--
-- Name: anon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.anon_id_seq OWNED BY public.anon.id;


--
-- Name: answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.answer (
    id integer NOT NULL,
    title character varying NOT NULL,
    text text,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "authorId" integer,
    "commentId" integer
);


ALTER TABLE public.answer OWNER TO postgres;

--
-- Name: answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.answer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.answer_id_seq OWNER TO postgres;

--
-- Name: answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.answer_id_seq OWNED BY public.answer.id;


--
-- Name: auto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auto (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    brand character varying NOT NULL,
    model character varying,
    sector character varying NOT NULL,
    condition character varying,
    year integer,
    body character varying,
    color character varying,
    mileage integer DEFAULT 0,
    volume real,
    trans character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.auto OWNER TO postgres;

--
-- Name: auto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auto_id_seq OWNER TO postgres;

--
-- Name: auto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auto_id_seq OWNED BY public.auto.id;


--
-- Name: autopart; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.autopart (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    brand character varying,
    sector character varying NOT NULL,
    category character varying NOT NULL,
    condition character varying NOT NULL,
    year integer,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.autopart OWNER TO postgres;

--
-- Name: autopart_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.autopart_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.autopart_id_seq OWNER TO postgres;

--
-- Name: autopart_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.autopart_id_seq OWNED BY public.autopart.id;


--
-- Name: banner; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.banner (
    id integer NOT NULL,
    dir character varying NOT NULL,
    title character varying NOT NULL,
    url character varying NOT NULL,
    description character varying NOT NULL,
    text text NOT NULL,
    img character varying NOT NULL,
    "position" integer NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.banner OWNER TO postgres;

--
-- Name: banner_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.banner_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.banner_id_seq OWNER TO postgres;

--
-- Name: banner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.banner_id_seq OWNED BY public.banner.id;


--
-- Name: beauty; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beauty (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    brand character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.beauty OWNER TO postgres;

--
-- Name: beauty_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beauty_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.beauty_id_seq OWNER TO postgres;

--
-- Name: beauty_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beauty_id_seq OWNED BY public.beauty.id;


--
-- Name: building; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.building (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    brand character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.building OWNER TO postgres;

--
-- Name: building_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.building_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.building_id_seq OWNER TO postgres;

--
-- Name: building_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.building_id_seq OWNED BY public.building.id;


--
-- Name: children; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.children (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    brand character varying,
    type character varying,
    age character varying,
    blocks character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.children OWNER TO postgres;

--
-- Name: children_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.children_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.children_id_seq OWNER TO postgres;

--
-- Name: children_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.children_id_seq OWNED BY public.children.id;


--
-- Name: comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comment (
    id integer NOT NULL,
    dir character varying,
    title character varying,
    text text,
    img character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "authorId" integer,
    "postId" integer
);


ALTER TABLE public.comment OWNER TO postgres;

--
-- Name: comment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.comment_id_seq OWNER TO postgres;

--
-- Name: comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comment_id_seq OWNED BY public.comment.id;


--
-- Name: complaint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.complaint (
    id integer NOT NULL,
    title character varying NOT NULL,
    url character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "authorId" integer
);


ALTER TABLE public.complaint OWNER TO postgres;

--
-- Name: complaint_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.complaint_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.complaint_id_seq OWNER TO postgres;

--
-- Name: complaint_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.complaint_id_seq OWNED BY public.complaint.id;


--
-- Name: electro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.electro (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying,
    brand character varying,
    corpus character varying,
    architecture character varying,
    system character varying,
    ram character varying,
    disk character varying,
    vram character varying,
    cpu character varying,
    hashrate real,
    symbols text[],
    consumption integer,
    vclass character varying,
    sim character varying,
    diag character varying,
    capacity character varying,
    condition character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.electro OWNER TO postgres;

--
-- Name: electro_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.electro_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.electro_id_seq OWNER TO postgres;

--
-- Name: electro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.electro_id_seq OWNED BY public.electro.id;


--
-- Name: estate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estate (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    operation character varying,
    condition character varying,
    renovated character varying,
    rooms character varying,
    level integer,
    area integer,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.estate OWNER TO postgres;

--
-- Name: estate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.estate_id_seq OWNER TO postgres;

--
-- Name: estate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estate_id_seq OWNED BY public.estate.id;


--
-- Name: fashion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fashion (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    gender character varying,
    kids boolean DEFAULT false,
    brand character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.fashion OWNER TO postgres;

--
-- Name: fashion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fashion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.fashion_id_seq OWNER TO postgres;

--
-- Name: fashion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fashion_id_seq OWNED BY public.fashion.id;


--
-- Name: funeral; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.funeral (
    id integer NOT NULL,
    dir character varying,
    title character varying,
    text text,
    photo character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.funeral OWNER TO postgres;

--
-- Name: funeral_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.funeral_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.funeral_id_seq OWNER TO postgres;

--
-- Name: funeral_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.funeral_id_seq OWNED BY public.funeral.id;


--
-- Name: gallery; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gallery (
    id integer NOT NULL,
    dir character varying NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    img character varying NOT NULL,
    gallery text[] NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.gallery OWNER TO postgres;

--
-- Name: gallery_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gallery_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.gallery_id_seq OWNER TO postgres;

--
-- Name: gallery_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gallery_id_seq OWNED BY public.gallery.id;


--
-- Name: hobby; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hobby (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    format character varying,
    genre character varying,
    platform character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.hobby OWNER TO postgres;

--
-- Name: hobby_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hobby_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.hobby_id_seq OWNER TO postgres;

--
-- Name: hobby_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.hobby_id_seq OWNED BY public.hobby.id;


--
-- Name: home; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.home (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.home OWNER TO postgres;

--
-- Name: home_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.home_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.home_id_seq OWNER TO postgres;

--
-- Name: home_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.home_id_seq OWNED BY public.home.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.message (
    id integer NOT NULL,
    text text,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "senderId" integer,
    "recieverId" integer
);


ALTER TABLE public.message OWNER TO postgres;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.message_id_seq OWNER TO postgres;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.message_id_seq OWNED BY public.message.id;


--
-- Name: movie; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movie (
    id integer NOT NULL,
    dir character varying NOT NULL,
    title character varying NOT NULL,
    excerpt character varying NOT NULL,
    photo character varying NOT NULL,
    rating real NOT NULL,
    premiere timestamp without time zone DEFAULT now() NOT NULL,
    url character varying NOT NULL,
    director character varying NOT NULL,
    genre character varying NOT NULL
);


ALTER TABLE public.movie OWNER TO postgres;

--
-- Name: movie_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movie_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.movie_id_seq OWNER TO postgres;

--
-- Name: movie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movie_id_seq OWNED BY public.movie.id;


--
-- Name: order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."order" (
    id integer NOT NULL,
    title character varying NOT NULL,
    phone character varying,
    email character varying,
    text text NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public."order" OWNER TO postgres;

--
-- Name: order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.order_id_seq OWNER TO postgres;

--
-- Name: order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_id_seq OWNED BY public."order".id;


--
-- Name: page; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.page (
    id integer NOT NULL,
    dir character varying NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    text text NOT NULL,
    photos text[],
    video character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.page OWNER TO postgres;

--
-- Name: page_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.page_id_seq OWNER TO postgres;

--
-- Name: page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.page_id_seq OWNED BY public.page.id;


--
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    alias character varying,
    phone character varying,
    email character varying,
    password character varying NOT NULL,
    status boolean NOT NULL,
    photos text[],
    "birthDate" timestamp without time zone DEFAULT now(),
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    bio text,
    subcription character varying,
    education character varying,
    gender character varying,
    religion character varying,
    category character varying NOT NULL,
    security character varying NOT NULL,
    lastad timestamp without time zone DEFAULT now()
);


ALTER TABLE public.person OWNER TO postgres;

--
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.person_id_seq OWNER TO postgres;

--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- Name: person_likes_post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_likes_post (
    "personId" integer NOT NULL,
    "postId" integer NOT NULL
);


ALTER TABLE public.person_likes_post OWNER TO postgres;

--
-- Name: playbill; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.playbill (
    id integer NOT NULL,
    dir character varying NOT NULL,
    title character varying NOT NULL,
    text text,
    photo character varying NOT NULL,
    url character varying NOT NULL,
    source character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    premiere character varying NOT NULL,
    author character varying,
    director character varying,
    genre character varying
);


ALTER TABLE public.playbill OWNER TO postgres;

--
-- Name: playbill_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.playbill_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.playbill_id_seq OWNER TO postgres;

--
-- Name: playbill_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.playbill_id_seq OWNED BY public.playbill.id;


--
-- Name: post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.post (
    id integer NOT NULL,
    dir character varying NOT NULL,
    title character varying NOT NULL,
    description character varying,
    text text NOT NULL,
    photos text[],
    video character varying,
    youtube character varying,
    rutube character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    tags character varying[] NOT NULL,
    sourceurl character varying,
    looks integer DEFAULT 0,
    "authorId" integer
);


ALTER TABLE public.post OWNER TO postgres;

--
-- Name: post_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.post_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.post_id_seq OWNER TO postgres;

--
-- Name: post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.post_id_seq OWNED BY public.post.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.product OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.product_id_seq OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: service; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.service (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.service OWNER TO postgres;

--
-- Name: service_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.service_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.service_id_seq OWNER TO postgres;

--
-- Name: service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.service_id_seq OWNED BY public.service.id;


--
-- Name: social; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.social (
    id integer NOT NULL,
    title character varying NOT NULL,
    sid character varying NOT NULL,
    status boolean NOT NULL,
    lastad timestamp without time zone DEFAULT now(),
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    category character varying NOT NULL
);


ALTER TABLE public.social OWNER TO postgres;

--
-- Name: social_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.social_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.social_id_seq OWNER TO postgres;

--
-- Name: social_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.social_id_seq OWNED BY public.social.id;


--
-- Name: subscriber; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subscriber (
    id integer NOT NULL,
    email character varying NOT NULL
);


ALTER TABLE public.subscriber OWNER TO postgres;

--
-- Name: subscriber_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.subscriber_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.subscriber_id_seq OWNER TO postgres;

--
-- Name: subscriber_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.subscriber_id_seq OWNED BY public.subscriber.id;


--
-- Name: tourism; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tourism (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.tourism OWNER TO postgres;

--
-- Name: tourism_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tourism_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.tourism_id_seq OWNER TO postgres;

--
-- Name: tourism_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tourism_id_seq OWNED BY public.tourism.id;


--
-- Name: video; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.video (
    id integer NOT NULL,
    title character varying NOT NULL,
    "videoId" character varying NOT NULL,
    source character varying,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.video OWNER TO postgres;

--
-- Name: video_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.video_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.video_id_seq OWNER TO postgres;

--
-- Name: video_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.video_id_seq OWNED BY public.video.id;


--
-- Name: work; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.work (
    id integer NOT NULL,
    dir character varying,
    title character varying NOT NULL,
    text text,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying,
    photos text[],
    sector character varying NOT NULL,
    category character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    looks integer DEFAULT 0,
    sold boolean DEFAULT false,
    district character varying NOT NULL,
    "authorId" integer
);


ALTER TABLE public.work OWNER TO postgres;

--
-- Name: work_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.work_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.work_id_seq OWNER TO postgres;

--
-- Name: work_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.work_id_seq OWNED BY public.work.id;


--
-- Name: agro id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agro ALTER COLUMN id SET DEFAULT nextval('public.agro_id_seq'::regclass);


--
-- Name: animal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animal ALTER COLUMN id SET DEFAULT nextval('public.animal_id_seq'::regclass);


--
-- Name: anon id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.anon ALTER COLUMN id SET DEFAULT nextval('public.anon_id_seq'::regclass);


--
-- Name: answer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer ALTER COLUMN id SET DEFAULT nextval('public.answer_id_seq'::regclass);


--
-- Name: auto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auto ALTER COLUMN id SET DEFAULT nextval('public.auto_id_seq'::regclass);


--
-- Name: autopart id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.autopart ALTER COLUMN id SET DEFAULT nextval('public.autopart_id_seq'::regclass);


--
-- Name: banner id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banner ALTER COLUMN id SET DEFAULT nextval('public.banner_id_seq'::regclass);


--
-- Name: beauty id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beauty ALTER COLUMN id SET DEFAULT nextval('public.beauty_id_seq'::regclass);


--
-- Name: building id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.building ALTER COLUMN id SET DEFAULT nextval('public.building_id_seq'::regclass);


--
-- Name: children id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.children ALTER COLUMN id SET DEFAULT nextval('public.children_id_seq'::regclass);


--
-- Name: comment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comment ALTER COLUMN id SET DEFAULT nextval('public.comment_id_seq'::regclass);


--
-- Name: complaint id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.complaint ALTER COLUMN id SET DEFAULT nextval('public.complaint_id_seq'::regclass);


--
-- Name: electro id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.electro ALTER COLUMN id SET DEFAULT nextval('public.electro_id_seq'::regclass);


--
-- Name: estate id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estate ALTER COLUMN id SET DEFAULT nextval('public.estate_id_seq'::regclass);


--
-- Name: fashion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fashion ALTER COLUMN id SET DEFAULT nextval('public.fashion_id_seq'::regclass);


--
-- Name: funeral id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.funeral ALTER COLUMN id SET DEFAULT nextval('public.funeral_id_seq'::regclass);


--
-- Name: gallery id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gallery ALTER COLUMN id SET DEFAULT nextval('public.gallery_id_seq'::regclass);


--
-- Name: hobby id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hobby ALTER COLUMN id SET DEFAULT nextval('public.hobby_id_seq'::regclass);


--
-- Name: home id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home ALTER COLUMN id SET DEFAULT nextval('public.home_id_seq'::regclass);


--
-- Name: message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message ALTER COLUMN id SET DEFAULT nextval('public.message_id_seq'::regclass);


--
-- Name: movie id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie ALTER COLUMN id SET DEFAULT nextval('public.movie_id_seq'::regclass);


--
-- Name: order id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."order" ALTER COLUMN id SET DEFAULT nextval('public.order_id_seq'::regclass);


--
-- Name: page id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.page ALTER COLUMN id SET DEFAULT nextval('public.page_id_seq'::regclass);


--
-- Name: person id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- Name: playbill id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.playbill ALTER COLUMN id SET DEFAULT nextval('public.playbill_id_seq'::regclass);


--
-- Name: post id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post ALTER COLUMN id SET DEFAULT nextval('public.post_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: service id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.service ALTER COLUMN id SET DEFAULT nextval('public.service_id_seq'::regclass);


--
-- Name: social id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.social ALTER COLUMN id SET DEFAULT nextval('public.social_id_seq'::regclass);


--
-- Name: subscriber id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscriber ALTER COLUMN id SET DEFAULT nextval('public.subscriber_id_seq'::regclass);


--
-- Name: tourism id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tourism ALTER COLUMN id SET DEFAULT nextval('public.tourism_id_seq'::regclass);


--
-- Name: video id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video ALTER COLUMN id SET DEFAULT nextval('public.video_id_seq'::regclass);


--
-- Name: work id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work ALTER COLUMN id SET DEFAULT nextval('public.work_id_seq'::regclass);


--
-- Data for Name: agro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.agro (id, dir, title, text, name, phone, email, photos, brand, sector, category, condition, age, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: animal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.animal (id, dir, title, text, name, phone, email, photos, sector, category, age, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: anon; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.anon (id, title, vendor, status, lastad, "createdAt") FROM stdin;
\.


--
-- Data for Name: answer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.answer (id, title, text, "createdAt", "authorId", "commentId") FROM stdin;
\.


--
-- Data for Name: auto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auto (id, dir, title, text, name, phone, email, photos, brand, model, sector, condition, year, body, color, mileage, volume, trans, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
1	cd/cdf7k4vf6zrhwxtujqaa-9edb2f8e6fdfbc94ddade0efebfe5417	Автомобиль Фантомаса	<p>Продается автомобиль Фантомаса. Это вымышленное и, по совместительству, первое объявление, так ка наш сайт только что запущен. Вы можете разместить свое объявление в соответствующем разделе. Будьте первыми!</p>	Фантомас	{'name':'Абхазия','code':'+7','flag':'abkh.png','value':'1111111'}	\N	{mffkswg0pknpbbevg6qu.jpg}	Citroen	другая	auto	БУ	1950	хэтчбек	Белый	1	5	механическая	2024-04-04 17:43:05.491253	10000000	0	f	сухумский	1
\.


--
-- Data for Name: autopart; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.autopart (id, dir, title, text, name, phone, email, photos, brand, sector, category, condition, year, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: banner; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.banner (id, dir, title, url, description, text, img, "position", "createdAt") FROM stdin;
1	3h/3hp0bg9wwn2w0dpuxo03-14f6fc9cd37bf2fcd8369a44eb2a0433	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	6uwb5wh3szpkk78nt5nw.jpg	0	2024-04-04 16:44:03.676072
2	5y/5ya6ncyavl95hy5jdafo-aaa10436b4380940f9d3f03bf53977eb	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	b40krub8bv4ggdjifvqw.jpg	3	2024-04-04 16:45:26.370414
3	ex/exblny2lwavnifcpknq0-77cd67835ef7985b04afba8a25559cf9	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	kbilwrf0o65wxudbzezc.jpg	2	2024-04-04 16:45:48.483737
4	zo/zo653xls3e7nnodn5ua8-8fc524055a751de67d75aee42c2229bb	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	c4fv2j17anckqjaxds7r.jpg	3	2024-04-04 16:51:49.929917
5	e5/e5pirwp1mus20g7u94gv-5345147384eb5e3768a4aedcac8dfec9	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	w7hn4mrfio0y0qv5f8ny.jpg	4	2024-04-04 16:52:16.445858
6	ev/ev7ewu10788wuh73jlhn-15ba34e0bacabeffd3fca95c0d1e9af7	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	dn8ohk8oyyiv08r6alkc.jpg	5	2024-04-04 16:52:55.77689
7	ov/ov89s9b68qskuddclnex-ac47e083a9e647212040c210debc9a74	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	cy29yg0ppn7gy1utoasw.jpg	6	2024-04-04 16:53:30.871593
8	u5/u56gwd0je65f8x18ux22-51785fc4e72ad705adc48a972cf8c1a5	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	k05ko6y5d3pqtuishawc.jpg	7	2024-04-04 16:54:08.575549
9	ra/rajrqwq85v0zycayt1s8-e7b1e8c9f05a009c8013485e9dbdecaf	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	b20jdisbsp7mifjokpmm.jpg	8	2024-04-04 16:54:31.579223
10	ly/lyqht7wth0scfmogt2jb-3dc9386ad0158e0c35375578eb0c5faf	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	gmr66c0k6grmmy9m3p1u.jpg	9	2024-04-04 16:54:53.93956
11	mc/mc0sjbkoxf8wasm2230n-30ae56501bbf29b02d27d4bbfb4d2173	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	qo3yedvw1beo6danqsok.jpg	10	2024-04-04 16:55:18.608907
12	rf/rf2ieybj9v88l5rkqiku-8aa1f9985f806ea20de49e05e9e84ab0	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	xo6y0j6tuib7ccgs1cia.jpg	11	2024-04-04 16:55:41.540452
13	bm/bmgu5cviurlfyhti5ofm-541d5b1cc7718dd5c5bcaf76b355c46a	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	v1sau5gz8ldzqpdgi7sk.jpg	12	2024-04-04 16:56:16.497176
14	hg/hgz1v22garmpd6jtnbhm-a5b8e298148cab662d1e97bb72662e2f	Ваша реклама может быть здесь	#	Ваша реклама может быть здесь	<p>Ваша реклама может быть здесь</p>	1ao1xu2tsf7pfkgjvp4z.jpg	13	2024-04-04 16:56:39.423786
\.


--
-- Data for Name: beauty; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beauty (id, dir, title, text, name, phone, email, photos, sector, category, brand, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: building; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.building (id, dir, title, text, name, phone, email, photos, sector, category, brand, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: children; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.children (id, dir, title, text, name, phone, email, photos, sector, category, brand, type, age, blocks, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: comment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comment (id, dir, title, text, img, "createdAt", "authorId", "postId") FROM stdin;
\.


--
-- Data for Name: complaint; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.complaint (id, title, url, "createdAt", "authorId") FROM stdin;
\.


--
-- Data for Name: electro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.electro (id, dir, title, text, name, phone, email, photos, sector, category, brand, corpus, architecture, system, ram, disk, vram, cpu, hashrate, symbols, consumption, vclass, sim, diag, capacity, condition, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: estate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estate (id, dir, title, text, name, phone, email, photos, sector, category, operation, condition, renovated, rooms, level, area, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: fashion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fashion (id, dir, title, text, name, phone, email, photos, sector, category, gender, kids, brand, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: funeral; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.funeral (id, dir, title, text, photo, "createdAt") FROM stdin;
\.


--
-- Data for Name: gallery; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gallery (id, dir, title, description, img, gallery, "createdAt") FROM stdin;
\.


--
-- Data for Name: hobby; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.hobby (id, dir, title, text, name, phone, email, photos, sector, category, format, genre, platform, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: home; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.home (id, dir, title, text, name, phone, email, photos, sector, category, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.message (id, text, "createdAt", "senderId", "recieverId") FROM stdin;
\.


--
-- Data for Name: movie; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movie (id, dir, title, excerpt, photo, rating, premiere, url, director, genre) FROM stdin;
1	q6/q6savfq7lcu574wk9be9-a908ddb78936613ed139caca550e70bb	Переводчик	Афганистан, март 2018 года. Во время спецоперации по поиску оружия талибов отряд сержанта армии США Джона Кинли попадает в засаду. В живых остаются только сам Джон, получивший ранение, и местный переводчик Ахмед, который сотрудничает с американцами. Очнувшись на родине, Кинли не помнит, как ему удалось выжить, но понимает, что именно Ахмед спас ему жизнь, протащив на себе через опасную территорию. Теперь чувство вины не даёт Джону покоя, и он решает вернуться за Ахмедом и его семьёй, которых в Афганистане усиленно ищут талибы.	7brygjn88kzionrzxsr6.jpg	7.9	2024-04-04 17:52:22.865378	https://www.kinopoisk.ru/film/927898	Гай Ричи	боевик
2	kf/kfghtqov6puu3jngi5sf-292a0d1dfe8ef6c739df38f83a9ef0d5	Метаморф	После личной трагедии агент ЦРУ Тара Кройдон решается на операцию, которая должна превратить ее в суперагента. Теперь с помощью инъекций ДНК она может принимать облик любого человека. Она вездесуща и практически неуязвима. Но постепенно она осознает, что, превращаясь в других, она все больше утрачивает саму себя. Девушка начинает сомневаться в том, кто она на самом деле, каков истинный смысл ее миссий и каковы мотивы ее босса. Тара решает докопаться до правды.	yboe1auvsbqbqdo10x5f.jpg	5.2	2024-04-04 17:53:59.085568	https://www.kinopoisk.ru/film/5143633/	Мэттью Мардер	фантастика
3	hq/hq5wg2u0i3cjsat07y08-0dd2116c8de63ba8871f6e2c6c7e27a8	Гипнотик	Цель жизни детектива Дэнни Рурка — найти пропавшую дочь. Параллельное расследование серии преступлений, совершенных при необъяснимых обстоятельствах, все глубже погружает Рурка в ирреальность происходящего и ставит под сомнение все его принципы и убеждения. Заручившись поддержкой одаренного экстрасенса Дианы Крус, Дэнни вступает в смертельно опасное противостояние за гранью реальности с единственным человеком, который может дать ему ключ к поиску дочери.	1rpxeohmo0trlpf2ql92.jpg	6.1	2024-04-04 17:55:27.31323	https://www.kinopoisk.ru/film/1319157/	Роберт Родригес	боевик
4	wz/wz3zmzvfyy7u52holndu-f94b1642223615396034abc9c69f32e2	Ренфилд	Новый Орлеан. Ренфилд, верный слуга графа Дракулы, подыскивает своему хозяину пропитание в группе психологической поддержки пострадавших от токсичных отношений: разыскивает их мучителей и скармливает вампиру. Внезапно он осознаёт, что сам стал жертвой нарцисса с клыками, и решает избавиться от его власти. А в это время неподкупная сотрудница полиции, пытаясь прижать местную банду наркоторговцев, оказывается под дулом пистолета одного из её главарей. На помощь девушке внезапно приходит оказавшийся рядом Ренфилд.	f0nasoc1nshrqp0ar48r.jpg	6.4	2024-04-04 17:56:45.102929	https://www.kinopoisk.ru/film/4703238/	Крис МакКей	ужасы
5	9p/9pcgnhfazrcyzwlizyil-1cff67d829efdc110794a7bbfbf6a321	Кокаиновый медведь	1985 год. Из пролетающего над лесами штата Джорджия самолёта наркокурьер выбрасывает несколько пакетов кокаина, часть из них находит барибал и съедает содержимое. Животное приходит в неистовство, и теперь всем туристам, рейнджерам и случайно попавшимся ему на пути бедолагам сильно не поздоровится.	tyb41m0qqqd3rsutez6x.jpg	5.9	2024-04-04 17:58:14.301698	https://www.kinopoisk.ru/film/4476889/	Элизабет Бэнкс	триллер
6	nw/nwno3v14b0mpl1ot0ql0-0effc107296d6a3e1e63e9cb57a4e2d2	Бескрайний бассейн	Писатель Джеймс Фостер отдыхает с женой на вип-курорте в стране третьего мира. Несколько лет назад он выпустил книгу, с тех пор не написал ни строчки и выбрал это место, надеясь найти здесь вдохновение. Супруги знакомятся с другой парой и, несмотря на запрет покидать охраняемую территорию, от скуки отправляются с ними на пикник. На обратном пути сидящий за рулём Джеймс насмерть сбивает местного жителя, и теперь ему предстоит понести наказание по всей строгости местных законов.	r6f9usddqoo2oe0ryijl.jpg	6.2	2024-04-04 17:59:44.203482	https://www.kinopoisk.ru/film/4499838/	 Брэндон Кроненберг	фантастика
7	es/eswgo3f2p67utg7xs6w6-af19d419d015f2a9d86e8444a5ce2ea5	1993	1993 год, дни больших надежд и грозных потрясений. Семья оказывается расколотой надвое — любящие супруги оказываются по разные стороны баррикад в самом центре Москвы.	9rcz9ydv9t0es5v8y1yy.jpg	7.4	2024-04-04 18:01:07.583877	https://www.kinopoisk.ru/film/5088866/	Александр Велединский	драма
8	aj/ajtfu6z1u4dhmfy7tet6-612d876e3bf1f78e84cab289632ed1ce	Дурные деньги	Говорят, что миром правят деньги. А деньгами распоряжается Уолл-Стрит. И если на самом верху решили, что небольшая сеть магазинов видеоигр должна обанкротиться, то под это можно брать кредит в банке. Но однажды обычные люди сказали «нет» и дали бой мировым биржам. Геймеры, тик-токеры, домохозяйки и мелкие инвесторы принялись скупать акции и «сломали» Уолл-Стрит, попутно заработав дурные деньги.	9ppuqlgicrhfaxx7xmyo.jpg	7.1	2024-04-04 18:03:33.967741	https://www.kinopoisk.ru/film/4917532/	Крэйг Гиллеспи	биография
9	8t/8t0j5k1t066jxkgyjzd4-308ccdfc2acbeda327a913270bd11857	С детьми что-то не так	Семейная пара с двумя маленькими детьми в компании давних друзей отправляется на выходные в лес, где компания сталкивается с какими-то необъяснимыми явлениями. А после возращения их дети начинают вести себя очень странно.	5xj7e4vlx2m060562utj.jpg	5.2	2024-04-04 18:04:51.615956	https://www.kinopoisk.ru/film/4749613/	 Роксанна Бенжамин	ужасы
10	vo/vozpevgkbdn4pfrjakup-a014f0bbab4d094a419785fe48882511	Подземелья и драконы: Честь среди воров	Бард и авантюрист Эдгин Дарвис вместе с боевой подругой Хольгой сбегает из тюрьмы и отправляется повидать дочку, которую последние годы опекал его бывший подельник Фордж. По прибытии выясняется, что тот при поддержке подозрительной колдуньи Софины не только сказочно разбогател и сделался губернатором, но и настроил дочь против отца, да и вообще — не против избавиться от Эдгина и Хольги. Умудрившись отбиться от палачей, отчаянная парочка планирует страшную месть, а для этого им придётся объединиться с рассеянным магом Саймоном, ловкой друидкой Дорик и отыскать считавшийся утерянным волшебный шлем.	ks2w80tdblix6olw0pg6.jpg	7.3	2024-04-04 18:06:11.119188	https://www.kinopoisk.ru/film/762646/	Джон Фрэнсис Дейли	 фэнтези
11	h3/h3rpa7tvwlwpavn78b6r-90e0795a67e620b1a74f977debe79fb3	Водопад	Для девочки-подростка и её друзей прогулка по дикой местности становится кошмаром, когда они натыкаются на самолёт с наркотиками и попадают в эпицентр борьбы за контроль над ним.	6fd1g32s0hhajd6owuca.jpg	5.2	2024-04-04 18:07:37.808221	https://www.kinopoisk.ru/film/1373031/	 Egidio Coccimiglio	триллер
\.


--
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."order" (id, title, phone, email, text, "createdAt") FROM stdin;
\.


--
-- Data for Name: page; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.page (id, dir, title, description, text, photos, video, "createdAt") FROM stdin;
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person (id, dir, title, alias, phone, email, password, status, photos, "birthDate", "createdAt", bio, subcription, education, gender, religion, category, security, lastad) FROM stdin;
1	n5/n5t8dzlszbs00xsmb688-3434950d913a4533d1bcd7cc373dbe0e	Абхазия 24	abkh24	{'name':'Абхазия','code':'+7','flag':'abkh.png','value':'9409903337'}	narzantaria@gmail.com	U2FsdGVkX1/jwQhBYVzxVnzKJ/9+GCZj8B4F2mTF5fA=	t	{je5rzhtbne10j1zmzsum.jpg}	2024-04-04 00:00:00	2024-04-04 17:38:21.476049	12345	12345	высшее	муж	иудаизм	moderator	открыто	2024-04-04 17:38:21.476049
\.


--
-- Data for Name: person_likes_post; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_likes_post ("personId", "postId") FROM stdin;
\.


--
-- Data for Name: playbill; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.playbill (id, dir, title, text, photo, url, source, "createdAt", premiere, author, director, genre) FROM stdin;
1	zl/zl0n96dtjra0j5951gw9-9ee961c7b727022ea74a90717e995373	В лучах	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Режиссер: </span><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;"><strong>Антон Корнилов</strong></span></p>	ycz3tr6ykoprkleg2yz8.jpg	https://rusdram.org/posters	https://rusdram.org/posters	2024-04-04 17:19:00.267837	2024-05-06	Артур Палыга	Антон Корнилов	Совершенно неизвестные письма Марии Склодовской-Кюри
2	1k/1khzorsleo107d8m3jtf-524b6f81085c4240f0663e48cb511e5a	Последний из ушедших	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Режиссер: </span><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;"><strong>Джамбул Жордания</strong></span></p>	jjifov3per48bj2rlvoo.jpg	https://rusdram.org/posters	https://rusdram.org/posters	2024-04-04 17:21:22.145519	2024-05-07	Баграт Шинкуба	Джамбул Жордания	Спектакль в одном действии
3	4c/4c429y5kvfyiqyzg6l2h-fcefe7b9345f7abaca44aff2f56fe44b	Софичка	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Режиссер: </span><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;"><strong>Елена Одинцова</strong></span></p>	uz598wd5jekn175q36w6.jpg	https://rusdram.org/posters	https://rusdram.org/posters	2024-04-04 17:23:42.364968	2024-05-08	Фазиль Искандер	Елена Одинцова	Рассказ о любви, верности и прощении
4	vf/vfbfuqxqwmfp0l11bsi6-f6ea77342c6c87e8ce88330e092bebcb	Кровавая свадьба	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Режиссер: </span><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;"><strong>Мария Романова</strong></span></p>	p1fo60p9lqdj926gngwf.jpg	https://rusdram.org/posters	https://rusdram.org/posters	2024-04-04 17:26:16.349385	2024-05-09	Федерико Гарсиа Лорка	Мария Романова	Спектакль в 2-х актах
5	f6/f6y9hy906ti48ih2qehj-1448c4b0369312de81eb5bdf65ad3de3	В лучах	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Режиссер: </span><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;"><strong>Антон Корнилов</strong></span></p>	5pk6g9g21qe6ih0my939.jpg	https://rusdram.org/posters	https://rusdram.org/posters	2024-04-04 17:28:30.683646	2024-05-14	Артур Палыга	Антон Корнилов	Совершенно неизвестные письма Марии Склодовской-Кюри
6	pb/pbmjmu3wsintc9vgpb08-df7fdc687740497acebf3b5b8c7987e6	Шествие. Джаз	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Режиссер: </span><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;"><strong>Мария Романова</strong></span></p>	g9v4ngduinh1d01u0i5j.jpg	https://rusdram.org/posters	https://rusdram.org/posters	2024-04-04 17:30:37.964853	2024-05-15	Иосиф Бродский	Мария Романова	По поэме Иосифа Бродского "Шествие"
\.


--
-- Data for Name: post; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.post (id, dir, title, description, text, photos, video, youtube, rutube, "createdAt", tags, sourceurl, looks, "authorId") FROM stdin;
3	dd/ddt76wzskgicbgyxp9o2-73921d735cae3117e0262319fefa7517	Волейбольный клуб «Апсны» завершил первый тур чемпионата России среди мужских команд	\N	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Волейбольный клуб «Апсны» из Республики Абхазия принял участие в первом туре чемпионата России по волейболу среди мужских команд Высшей лиги «Б». Соревнования прошли с 11 по 14 октября в городе Грозном Чеченской Республике. Абхазская команда провела по два матча с командами «Грозный» &nbsp;и «Дагестан». В первом матче «Апсны» сыграла с командой «Грозный», игра завершилась со счетом 3:2 (25:18, 21:25, 22:25, 25:19, 15:11) в пользу грозненских волейболистов. Во втором матче абхазские волейболисты уступили дагестанской команде со счетом 3:0 (25:14, 25:19, 25:15). Во втором матче против чеченской команды сильнее оказались хозяева, матч завершился со счетом 3:0 (25:19, 25:21, 25:18). В заключительный игровой день клуб «Апсны» уступил дагестанским волейболистам со счетом 3:0 (25:20, 25:18, 25:22). Следующий тур пройдет в Гудауте с 21 по &nbsp;24 октября.</span></p>	{rvj2aypm0d9trioykm8w.jpg}	\N	\N	\N	2023-10-15 19:00:46	{Спорткомитет,Абхазия,спорт}	http://gosmolsportabh.ru/	0	1
17	6x/6xy3xxiuxrhyrz5561d1-d30f8be567f3df01da627996dc813152	В Абхазии простились с участником СВО Владимиром Тумаком	В Абхазии простились с участником СВО — уроженцем поселка Агудзера Владимиром Тумаком	<p>В Абхазии простились с участником СВО — уроженцем поселка Агудзера Владимиром Тумаком.</p><p>Тумаку было 36 лет. Он погиб в бою в поселке Крынки на Херсонском направлении.</p><p>В Ростове-на-Дону у него осталась семья – супруга и десятилетний сын.</p><p>Фото &copy; Михаил Хашба</p><p>Sputnik Абхазия</p>	{77ibzsp1ooa86tcux87k.jpg}	\N	\N	\N	2024-04-10 09:25:42.259463	{"ЧП Абхазии",политика,конфликты,Абхазия,Россия}	https://t.me/chp_abh/8692	0	1
7	pa/pao2v7pdpij1zhcfszmy-04590383ca1e8ef07cecacea39e8a657	Министерство внутренних дел Республики Абхазия объявляет набор абитуриентов	Министерство внутренних дел Республики Абхазия объявляет набор абитуриентов на поступление в Высшую школу милиции МВД Абхазии	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Министерство внутренних дел Республики Абхазия объявляет набор абитуриентов на поступление в Высшую школу милиции МВД Абхазии. https://www.mvdra.org/documents/14106/ В Государственное образовательное учреждение высшего образования «Высшая школа милиции Министерства внутренних дел Республики Абхазия» им. Героя Абхазии В.Х. Смыр зачисляются граждане Республики Абхазия в возрасте от 17 до 27 лет, имеющие общее среднее образование. Обучение проводится по очной форме на бюджетной основе (бесплатно). Срок обучения 4 года. Вступительные экзамены предусматривают сдачу нормативов по физической подготовке, экзамен по истории и Конституции РА, диктант по русскому языку. За справками обращаться по адресу: г. Сухум ул. Кодорское шоссе, 29; телефон: + 7 (940) 925-08-26.</span></p>	{4hhx9xab1bcqv7her44c.jpg}	\N	\N	\N	2024-04-03 19:09:48	{"МВД Абхазии",Абхазия}	https://t.me/mvd_raa	0	1
1	ff/ff37va1gej7cdovhmyet-6dbff85abd7b67b6f1f20fa228a35596	Сериал "Однажды в Абхазии" вышел на платформе Start	Это история про обычного жителя республики, сотрудника МЧС, который столкнулся с несправедливостью	<p style="text-align: start;">Это история про обычного жителя республики, сотрудника МЧС, который столкнулся с несправедливостью.</p><p style="text-align: start;">Продюсер и исполнитель главной роли Теймураз Тания признался, что переживает в день премьеры, но, по его словам, первые две серии уже набрали позитивные отзывы и более 100 тысяч просмотров.</p><p style="text-align: start;">"Однажды в Абхазии" — сериал, снятый чемпионами Высшей лиги КВН, командой "Нарты из Абхазии". Продюсер и исполнитель главной роли – Теймураз Тания, соавтор сценария – Руслан Шакая, режиссер — Теймураз Квеквескири. На этапе съемок, которые продлились 51 день, сериал назывался "Независимый". Картина состоит из 8 серий, две уже доступны для просмотра.</p>	{ijn1x2zfd3bqn8zlvq2i.jpg}	\N	T8GV_KbV2y8	\N	2023-10-13 18:56:02	{"ЧП Абхазии",Абхазия,происшествия}	https://t.me/chp_abh	0	1
8	0m/0my2k9nxsodnl2hxjm6g-7c6859c2c7fe71fd0baa4284f85fc19a	Съёмочная группа «RT» снова в гостях у «Пятнашки»	Съёмочная группа «RT» снова в гостях у «Пятнашки». Снимают новый фильм из цикла «Свои» про молодых	<p style="text-align: start;">Съёмочная группа «RT» снова в гостях у «Пятнашки».</p><p style="text-align: start;">Снимают новый фильм из цикла «Свои» про молодых артиллеристов Интербригады.</p><p style="text-align: start;">О дате премьеры на канале "Россия - это я!" сообщу своевременно.</p><p style="text-align: start;">@ttambyl</p>	\N	963lxgyh5eu59p4boris.mp4	\N	\N	2023-08-29 19:17:14	{"ИБ Пятнашка",Абхазия,Россия,политика,конфликты}	https://t.me/pyatnashka_abkhazi	0	1
9	tq/tq8jaybthd7f5yv1phjh-cdce0deb35274a349296ae89bfca1d97	Жириновский прогнозировал войну на ближнем востоке	Жириновский делал много заявлений, содержащих точный анализ будущих событий и процессов в мировых делах	<p style="text-align: start;">Пресс-секретарь российского президента Дмитрий Песков заявил, что заявления бывшего лидера ЛДПР Владимира Жириновского содержали точный анализ мировой политики. Он подтвердил слова нынешнего председателя партии Леонида Слуцкого, который указал на сбывшийся прогноз Жириновского о конфликте на Ближнем Востоке к 2024 году. Песков отметил, что заявления Жириновского в свое время аккуратно предсказывали тенденции в мировой политике. Он отреагировал на комментарии Леонида Слуцкого, нынешнего председателя ЛДПР, который заявил, что прогноз Жириновского относительно конфликта на Ближнем Востоке сбылся. Песков заявил:</p><p style="text-align: start;">"Вы знаете, что Жириновский делал много заявлений, содержащих точный анализ будущих событий и процессов в мировых делах. Это лишнее подтверждение тому".</p><p style="text-align: start;">Песков также отметил, что Москва с озабоченностью следит за ситуацией на Ближнем Востоке и что она является объектом особого внимания в настоящее время. Он также подтвердил, что специальная военная операция в Украине продолжается и ее цели будут достигнуты.</p>	{zl4l67bamp7psqfaf0nt.jpg}	\N	\N	\N	2023-10-09 19:25:53	{"новости мира",политика,конфликты}	\N	0	1
10	jp/jp4wihil268w3hzkn0vr-e72b699c94cfc4c9fa8a1af74581cfc2	Открыта регистрация на «Сухум-Марафон»	Открыта регистрация на «Сухум-Марафон», который состоится 27 октября 2024 года	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Открыта регистрация на «Сухум-Марафон», который состоится 27 октября 2024 года 04.12.2023 27 октября 2024 года в Республике Абхазия впервые состоится «Сухум-Марафон». Забеги пройдут на 42,2, 21,1 и 5 километров. Принять участие в забеге можно с 18 лет. Маршрут марафона будет проходить по столице Абхазии: стартуем на набережной Махаджиров, по Кодорскому шоссе, улица Акиртава и Лакоба, по проспекту Аиааира по набережной Диоскуров, порт Сухум. Регистрация завершится 27 сентября &nbsp;2024 года. Пройти регистрацию и узнать более подробную информацию на марафон,вы сможете по ссылке на официальном сайте: https://sukhummarathon.org</span></p>	{kcib6m21a88zsqk979ds.jpg}	\N	\N	\N	2023-12-04 19:31:21	{спорт,Спорткомитет,Абхазия}	http://gosmolsportabh.ru/	0	1
12	8u/8u16kgkd0jgqj5h9ehga-92fe69dcfedcd25ae0a7fe528ac03cc9	Алиас Авидзба с позывным Горец, доброволец из Абхазии	Алиас Авидзба с позывным Горец, актёр и режиссёр, доброволец из Абхазии	<p style="text-align: start;">Горец. Позывной. Доброволец из Абхазии. Интернациональный батальон &nbsp;"Пятнашка". Алиас Авидзба &nbsp;актёр и режиссёр, служил в молодёжном театре.</p><p style="text-align: start;">В августе 2022 Горец получил ранение, подорвавшись на мине. Никаких криков. Он даже не потерял сознания. Просто с ледяным спокойствием попросил, чтобы ему что-нибудь укололи. Эти кадры попали на плёнку документалистов. Лишился ноги. Восстановился. И вернулся на фронт. На передовую. На протезе. Сила.</p><p style="text-align: start;">Я снимала Алиаса для нашего фильма "Агитбригада". Пока брала интервью &nbsp;рисовала его портрет. Алиас отвечает за боевой дух ребят. Разговаривает с каждым, поёт под гитару. Примером своим мотивирует. Пока такие ребята на фронте &nbsp;мы непобедимы. Послушайте, как Алиас поднимает ребят на штурм</p>	\N	z5tag13tc8mc9tcyzn2o.mp4	\N	\N	2023-12-02 19:33:52	{"ЧП Абхазии",Россия,конфликты}	https://t.me/chp_abh	0	1
14	t9/t9kp49v32uefk9wukoz6-5e9195d96df4ed4fdf766f2a4e9343c3	"Общественный туалет" около смотровой площадки на горе Мамзышха	Недалеко от Гагры расположена гора Мамзышха. По дороге к ней есть три смотровые площадки, с которых открываются удивительные глазу пейзажи	<p style="text-align: start;">Недалеко от Гагры расположена гора Мамзышха. По дороге к ней есть три смотровые площадки, с которых открываются удивительные глазу пейзажи. &nbsp;</p><p style="text-align: start;">Но находится там долго, туристам мешает удушающее зловоние. Все дело в том, что на территории первой смотровой нет ни одного туалета.</p><p style="text-align: start;">Попытки уговорить Гагрскую администрацию предпринять какие-то действия, чтобы территория не превратилась в общественный туалет, ни к чему не привели. Местные предприниматели и жители просили руководителей района поставить на сезон хотя бы несколько биотуалетов, но и это предложение было отвергнуто.</p><p style="text-align: start;">Из официальной информации известно, что только в первом квартале текущего 2023 года Гагрский район перевыполнил план по доходам местного бюджета на 242 %, в цифрах – 112 миллионов рублей. &nbsp;</p><p style="text-align: start;">И я не поверю, что готовясь к курортному сезону, глава Администрации Юрий Хагуш не смог бы выделить небольшие средства на приобретение биотуалетов для смотровой площадки. И ведь эта услуга вполне себе может быть платной. Любой турист с удовольствием раскошелился бы на 10 рублей, вместо поиска более ли менее чистого от отходов местечка в лесу. Но не случилось. &nbsp;</p><p style="text-align: start;">Вот и приходится тем самым туристам, которым в нашей стране уделяют внимания порой больше чем собственным жителям, перепрыгивать через кучи разных отходов и справлять нужду порой на глазах у всех.</p>	\N	heyun9x4sfk4sffsdovi.mp4	\N	\N	2023-09-02 19:40:43	{Республика,Абхазия,политика}	https://t.me/chp_abh	0	1
15	0i/0ii6fszlmj90889i7zij-8b8dcfe2633b712e2a069c859d01ba9e	Житель Турции нашел при ремонте в доме подземный город	Житель турецкого Кайсери нашел при ремонте в доме подземный город	<p>TRT Haber: житель турецкого Кайсери нашел при ремонте в доме подземный город.</p><p>Житель города Кайсери на юге Турции обнаружил под своим домом подземный город, сообщил телеканал TRT Haber. Мужчина сделал необычное открытие во время ремонта. Специалисты позднее выяснили, что найденный им город имеет историю длиною 2000 лет.</p><p>Профессор Осман Озсой рассказал, что под домом есть как минимум два археологических слоя. Прежде чем жители покинули город впервые, часть, находящуюся под домом мужчины, они использовали в качестве амбара, сеновала и склада. Позднее на этом месте были построены дома. По информации канала, речь может идти о трех-четырех подземных этажах.</p><p>Сам мужчина признался, что он и его семья использовали пространство под домом в качестве сарая. По его словам, все боялись заходить туда без света. Теперь он планирует сделать там туристическое место. &nbsp;</p><p>Мы хотим, чтобы туристы, приезжающие сюда, видели не руины, а что-то другое, — поделился мужчина.</p>	{vxwl71ques19xx3nc4gr.jpg}	\N	\N	\N	2023-10-13 19:44:56	{"новости мира"}	https://t.me/arch_history_news/12049	0	1
6	ks/ks5h1sr2ruw40cv0yxjg-5fab44dcf637a107a1a21d6bcd0fc736	Ростов-на-Дону 88 000 гумнаборов сформировали активисты Молодёжки НФ на складе проекта "Всё для Победы"	88 000 гумнаборов сформировали активисты Молодёжки НФ на складе проекта "Всё для Победы!" в Ростове-на-Дону и отправили мирным жителям.	<p style="text-align: start;">Ростов-на-Дону.</p><p style="text-align: start;">88 000 гумнаборов сформировали активисты Молодёжки НФ на складе проекта "Всё для Победы!" в Ростове-на-Дону и отправили мирным жителям.</p><p style="text-align: start;">7 500 гигиенических и лекарственных наборов также были собраны и посланы на передовую.</p><p style="text-align: start;">183 дня были потрачены на это.</p><p style="text-align: start;">ДНР</p><p style="text-align: start;">43 региона России сформировали свои мобильные отряды. Парни и девушки сейчас помогают мирным жителям ДНР.</p><p style="text-align: start;">6 детей было в многодетной семье Мариуполя, которой недавно помогли молодёжкинцы из Ингушетии. Их дом отремонтировали, ранее он был разбомблен. &nbsp;</p><p style="text-align: start;">Это всё – наша Молодёжка. Лайк, если тоже гордитесь поколением Z.</p>	{6etutf59luu65iduku4n.jpg,1y2931dz7hgaj9yu7azx.jpg,hfqk8ykmjmw3gyn7wxzk.jpg,1teumkbxa3gnyqq96rji.jpg,fyrzr5oezphwynh159lj.jpg,mf1yvqj30pdp1e9bd6jj.jpg}	\N	\N	\N	2023-10-18 19:08:38	{"Народный фронт",Россия,политика,конфликты}	https://t.me/onf_front	3	1
5	4f/4fvsg2v99gsnmzzsaymx-99b5ff896d8e08e3213c951c520a94df	Ахра Авидзба в эфире СоловьёвLive	Ахра Авидзба, командир батальона «Пятнашка», позывной Абхаз, в эфире СоловьёвLive	<p style="text-align: start;">Они начали насыщать тот участок, где мы находимся. Появились у них грузинские добровольцы, раньше мы их не наблюдали.</p><p style="text-align: start;">Ахра Авидзба, командир батальона «Пятнашка», позывной Абхаз, в эфире СоловьёвLive.</p>	\N	e2dbk5tutpqqi3rgi1c9.mp4	\N	\N	2023-08-30 19:05:14	{"ИБ Пятнашка",Абхазия,Россия,политика,конфликты}	https://t.me/pyatnashka_abkhazi	7	1
16	op/opy6jgtct8sprqmwhpu9-9d4696253c844c97689081802a94e762	Заседание между Кабинетом министров Абхазии и Советом министров Республики Крым	Пятое заседание межправительственной комиссии по торгово-экономическому, научно-техническому и гуманитарному сотрудничеству между Кабинетом министров Абхазии и Советом министров Республики Крым открылось в Сухуме	<p>Пятое заседание межправительственной комиссии по торгово-экономическому, научно-техническому и гуманитарному сотрудничеству между Кабинетом министров Абхазии и Советом министров Республики Крым открылось в Сухуме.</p><p>С абхазской стороны комиссию возглавляет первый вице-премьер, министр энергетики и транспорта Джансух Нанба, с крымской – вице-премьер Правительства Крыма Георгий Мурадов.</p><p>В 2016 году Абхазия и Крым подписали Соглашение о развитии отношений в торгово-экономической, научно-технической и гуманитарной областях. Каждые три года стороны подписывают новые программы мероприятий.</p>	{cthguuo8hk7r2ljt6blm.jpg}	\N	\N	\N	2024-04-10 09:23:59.543945	{"ЧП Абхазии",политика,Абхазия}	https://t.me/chp_abh/8686	0	1
18	0t/0t7moq39fxde7yryaj4l-0a23366cc3c09debcaaf1c51f6728c7c	Свалка горит в селе Алахадзы	По словам главы "Спецавтохозяйства" Тимура Бигвава, горит 700-800 квадратных метров, дым накрыл часть Гагры, Пицунды и все село Алахадзы	<p>Свалка горит в селе Алахадзы.</p><p>По словам главы "Спецавтохозяйства" Тимура Бигвава, горит 700-800 квадратных метров, дым накрыл часть Гагры, Пицунды и все село Алахадзы.</p><p>"У нас уже подготовлены два участка, куда свезли по 200 КАМАЗов земли, но ветер не дает подъехать к очагу возгорания. Мы вынуждены находить момент, когда он дует в другую сторону. Также от высоких температур у техники взрываются покрышки", — сказал он.</p><p>По словам Бигвава, мусор постоянно тлеет в глубине свалки, и пламя может вспыхнуть от любой искры. Нередко его причиной становятся выброшенные батарейки и аккумуляторы.</p>	\N	dw35rkz8rj8dqdfwp77i.mp4	\N	\N	2024-04-10 09:28:09.548909	{"ЧП Абхазии",происшествия,Абхазия}	https://t.me/chp_abh/8694	0	1
19	b7/b7mfwysa4z05yef4xoao-0a50faf875f6e7d5ba39eca027d856e2	Ученые оценили опасность космического луча из-за пределов Галактики	На прошлой неделе ученые сообщили о обнаружении космического луча, который гораздо слабее ультрафиолета от солнца. Его природа неизвестна	<p>На прошлой неделе ученые сообщили о обнаружении космического луча, который гораздо слабее ультрафиолета от солнца. Его природа неизвестна, и он прилетел издалека, за пределами Млечного Пути, как отмечается в исследовании. Астрофизики подчеркнули, что хоть космические лучи бывают рядом с Землей довольно часто, их природу определить довольно сложно. Лучи не представляют опасности для человека, так как они задерживаются и гасятся в атмосфере. Космические лучи возникают в разных процессах от разных источников в космосе, таких как сверхновые звезды, квазары и другие энергетические объекты. Кроме того, ультрафиолетовое излучение от солнца обладает гораздо большей энергией, чем космические лучи, которые проходят сквозь атмосферу и гаснут.</p>	{efpbowem9u8v6lrp1swv.jpg}	\N	\N	\N	2024-04-10 09:38:49.813226	{"новости мира",наука}	https://www.rbc.ru/technology_and_media/28/11/2023/6560b9389a7947596396c44a	2	1
2	hn/hnl718hlmzawwkaqxfix-d74d96934934318cc3a0878d76af66f9	Жители южных регионов России видели в небе красное сияние	Красное полярное сияние в небе после магнитной бури в южных регионах России	<p style="text-align: start;"><span style="color: rgb(140, 140, 140);">Фото: телеграм itsdonetsk</span></p><p style="text-align: start;">Жители Крыма, Ростовской и Воронежской областей, делятся кадрами северного сияния после начала сильнейшей магнитной бури.</p><p style="text-align: start;">Также красное свечение видно в Киеве и других городах Украины.</p>	{m20pw84et70kcxxwi7a2.jpg,8tsnttcilsog0fc2f2tb.jpg,crj6wq8ce0d18o8dyvr5.jpg,9j8c1bx5vzwgyhqprbys.jpg}	\N	\N	\N	2023-11-06 18:58:30	{"новости мира"}	https://t.me/dmitrynikotin/14825	3	1
20	f3/f3dr3uwok68l6d7mqy8f-1112a56b45382cf49346213cdafa1220	За незаконное хранение наркотика к уголовной ответственности привлечен житель г. Пицунда	За незаконное хранение наркотика к уголовной ответственности привлечен житель г. Пицунда. Наркотическое средство у гражданина изъято сотрудниками Госавтоинспекции и уголовного розыска	<p>За незаконное хранение наркотика к уголовной ответственности привлечен житель г. Пицунда.</p><p>Наркотическое средство у гражданина изъято сотрудниками Госавтоинспекции и уголовного розыска Галского РОВД на посту ГАИ «Канал» в ходе профилактических мероприятий.</p><p>- В соответствии с ч. 7 ст. 19 прим.1 &nbsp;УПК РА названное или изображенное лицо является виновным после вступления в законную силу приговора или итогового судебного решения.</p>	\N	ykt216ofokkjhjslcit6.mp4	\N	\N	2024-04-10 09:44:40.009514	{"МВД Абхазии",Абхазия,криминал}	https://t.me/mvd_raa/1082	24	1
13	ar/arbuv9rzjamysa3eaedk-9c76467c1639296b3e0498a19885dd7a	Батал Чежия уступил Одилжону Аслонову решением судей	Боксер из Абхазии Батал Чежия уступил Одилжону Аслонову решением судей по итогам восьми раундов	<p><span style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); font-family: &quot;Times New Roman&quot;;">Батал Чежия уступил Одилжону Аслонову решением судей по итогам восьми раундов 11.11.2023 Батал Чежия уступил в боксерском поединке спортсмену из Узбекистана Одилжону Аслонову решением судей по итогам восьми раундов. Бой прошел на арене Академии единоборств РМК в Екатеринбурге в рамках турнира по боксу от RCC Boxing Promotions.</span></p>	{xxnikyyzrwcx91jecia4.jpg}	\N	\N	\N	2024-04-04 19:38:26.824073	{Спорткомитет,Абхазия,спорт}	http://gosmolsportabh.ru/	12	1
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product (id, dir, title, text, name, phone, email, photos, sector, category, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.service (id, dir, title, text, name, phone, email, photos, sector, category, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: social; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.social (id, title, sid, status, lastad, "createdAt", category) FROM stdin;
\.


--
-- Data for Name: subscriber; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subscriber (id, email) FROM stdin;
\.


--
-- Data for Name: tourism; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tourism (id, dir, title, text, name, phone, email, photos, sector, category, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Data for Name: video; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.video (id, title, "videoId", source, "createdAt") FROM stdin;
1	Аԥсны Афырхацәа ирызкыз агәаларшәаратә  хәылԥазы   мҩаԥысит Џьырхәа  абжьаратә школ  аҿы .	O6YAyYvkQGg	https://www.youtube.com/@apsuatv	2024-03-31 11:41:04.817
2	Амшеикушара 23.03.2024	wxjMkuYba8k	https://www.youtube.com/@apsuatv	2024-03-31 15:41:05.032
3	АРГАМА. Пороки взрослых	TWp98x10eWE	https://www.youtube.com/@apsuatv	2024-04-01 11:41:14.603
4	Аҧсуа культура ашҭа Виолетта Маан	rOho9eh1wJs	https://www.youtube.com/@apsuatv	2024-04-01 17:41:05.24
5	Амшеикушара 29.03.2024	uOA_GttlAcs	https://www.youtube.com/@apsuatv	2024-04-03 11:41:35.275
6	Амшеикушара 29.03.2024	2zThZU2j-bw	https://www.youtube.com/@apsuatv	2024-04-03 21:41:04.223
\.


--
-- Data for Name: work; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.work (id, dir, title, text, name, phone, email, photos, sector, category, "createdAt", price, looks, sold, district, "authorId") FROM stdin;
\.


--
-- Name: agro_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.agro_id_seq', 1, false);


--
-- Name: animal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.animal_id_seq', 1, false);


--
-- Name: anon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.anon_id_seq', 3, true);


--
-- Name: answer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.answer_id_seq', 1, false);


--
-- Name: auto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auto_id_seq', 1, true);


--
-- Name: autopart_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.autopart_id_seq', 1, false);


--
-- Name: banner_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.banner_id_seq', 14, true);


--
-- Name: beauty_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beauty_id_seq', 1, false);


--
-- Name: building_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.building_id_seq', 1, false);


--
-- Name: children_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.children_id_seq', 1, false);


--
-- Name: comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comment_id_seq', 1, false);


--
-- Name: complaint_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.complaint_id_seq', 1, false);


--
-- Name: electro_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.electro_id_seq', 1, false);


--
-- Name: estate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estate_id_seq', 1, false);


--
-- Name: fashion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fashion_id_seq', 1, true);


--
-- Name: funeral_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.funeral_id_seq', 1, false);


--
-- Name: gallery_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gallery_id_seq', 1, false);


--
-- Name: hobby_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hobby_id_seq', 1, false);


--
-- Name: home_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.home_id_seq', 1, false);


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.message_id_seq', 1, false);


--
-- Name: movie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movie_id_seq', 11, true);


--
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_id_seq', 1, false);


--
-- Name: page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.page_id_seq', 1, false);


--
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_id_seq', 1, true);


--
-- Name: playbill_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.playbill_id_seq', 6, true);


--
-- Name: post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.post_id_seq', 20, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_id_seq', 1, false);


--
-- Name: service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.service_id_seq', 1, false);


--
-- Name: social_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.social_id_seq', 1, false);


--
-- Name: subscriber_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.subscriber_id_seq', 1, false);


--
-- Name: tourism_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tourism_id_seq', 1, false);


--
-- Name: video_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.video_id_seq', 6, true);


--
-- Name: work_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.work_id_seq', 1, false);


--
-- Name: person_likes_post PK_008031e359617e24ef57108220b; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_likes_post
    ADD CONSTRAINT "PK_008031e359617e24ef57108220b" PRIMARY KEY ("personId", "postId");


--
-- Name: home PK_012205783b51369c326a1ad4a64; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home
    ADD CONSTRAINT "PK_012205783b51369c326a1ad4a64" PRIMARY KEY (id);


--
-- Name: comment PK_0b0e4bbc8415ec426f87f3a88e2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT "PK_0b0e4bbc8415ec426f87f3a88e2" PRIMARY KEY (id);


--
-- Name: auto PK_0c3035ffcce0eac94e9284d8237; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auto
    ADD CONSTRAINT "PK_0c3035ffcce0eac94e9284d8237" PRIMARY KEY (id);


--
-- Name: funeral PK_0c4f987076d5f57dc6a9d1b5542; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.funeral
    ADD CONSTRAINT "PK_0c4f987076d5f57dc6a9d1b5542" PRIMARY KEY (id);


--
-- Name: order PK_1031171c13130102495201e3e20; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT "PK_1031171c13130102495201e3e20" PRIMARY KEY (id);


--
-- Name: video PK_1a2f3856250765d72e7e1636c8e; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video
    ADD CONSTRAINT "PK_1a2f3856250765d72e7e1636c8e" PRIMARY KEY (id);


--
-- Name: work PK_1ad2a9dfd058d66c37e6d495222; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work
    ADD CONSTRAINT "PK_1ad2a9dfd058d66c37e6d495222" PRIMARY KEY (id);


--
-- Name: playbill PK_1bf3273685b398f995e4d6dc812; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.playbill
    ADD CONSTRAINT "PK_1bf3273685b398f995e4d6dc812" PRIMARY KEY (id);


--
-- Name: subscriber PK_1c52b7ddbaf79cd2650045b79c7; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscriber
    ADD CONSTRAINT "PK_1c52b7ddbaf79cd2650045b79c7" PRIMARY KEY (id);


--
-- Name: tourism PK_2c3431137e7bac22ecd98685feb; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tourism
    ADD CONSTRAINT "PK_2c3431137e7bac22ecd98685feb" PRIMARY KEY (id);


--
-- Name: fashion PK_2ee2c1cd8522b08326114ba3e29; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fashion
    ADD CONSTRAINT "PK_2ee2c1cd8522b08326114ba3e29" PRIMARY KEY (id);


--
-- Name: beauty PK_4b62340e16c0821e4cd646f3673; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beauty
    ADD CONSTRAINT "PK_4b62340e16c0821e4cd646f3673" PRIMARY KEY (id);


--
-- Name: person PK_5fdaf670315c4b7e70cce85daa3; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT "PK_5fdaf670315c4b7e70cce85daa3" PRIMARY KEY (id);


--
-- Name: social PK_645aa1cff2b9f7b0e3e73d66b4d; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.social
    ADD CONSTRAINT "PK_645aa1cff2b9f7b0e3e73d66b4d" PRIMARY KEY (id);


--
-- Name: gallery PK_65d7a1ef91ddafb3e7071b188a0; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gallery
    ADD CONSTRAINT "PK_65d7a1ef91ddafb3e7071b188a0" PRIMARY KEY (id);


--
-- Name: banner PK_6d9e2570b3d85ba37b681cd4256; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banner
    ADD CONSTRAINT "PK_6d9e2570b3d85ba37b681cd4256" PRIMARY KEY (id);


--
-- Name: page PK_742f4117e065c5b6ad21b37ba1f; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.page
    ADD CONSTRAINT "PK_742f4117e065c5b6ad21b37ba1f" PRIMARY KEY (id);


--
-- Name: electro PK_7732a673a05251e7f630f285028; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.electro
    ADD CONSTRAINT "PK_7732a673a05251e7f630f285028" PRIMARY KEY (id);


--
-- Name: autopart PK_7c958cfb1b305fd3da97d2912cb; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.autopart
    ADD CONSTRAINT "PK_7c958cfb1b305fd3da97d2912cb" PRIMARY KEY (id);


--
-- Name: service PK_85a21558c006647cd76fdce044b; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.service
    ADD CONSTRAINT "PK_85a21558c006647cd76fdce044b" PRIMARY KEY (id);


--
-- Name: children PK_8c5a7cbebf2c702830ef38d22b0; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.children
    ADD CONSTRAINT "PK_8c5a7cbebf2c702830ef38d22b0" PRIMARY KEY (id);


--
-- Name: answer PK_9232db17b63fb1e94f97e5c224f; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT "PK_9232db17b63fb1e94f97e5c224f" PRIMARY KEY (id);


--
-- Name: agro PK_93e70884708601bb08289f2858d; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agro
    ADD CONSTRAINT "PK_93e70884708601bb08289f2858d" PRIMARY KEY (id);


--
-- Name: hobby PK_9cf21d5206ec584a4cc14a8703e; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hobby
    ADD CONSTRAINT "PK_9cf21d5206ec584a4cc14a8703e" PRIMARY KEY (id);


--
-- Name: complaint PK_a9c8dbc2ab4988edcc2ff0a7337; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.complaint
    ADD CONSTRAINT "PK_a9c8dbc2ab4988edcc2ff0a7337" PRIMARY KEY (id);


--
-- Name: animal PK_af42b1374c042fb3fa2251f9f42; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "PK_af42b1374c042fb3fa2251f9f42" PRIMARY KEY (id);


--
-- Name: message PK_ba01f0a3e0123651915008bc578; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT "PK_ba01f0a3e0123651915008bc578" PRIMARY KEY (id);


--
-- Name: building PK_bbfaf6c11f141a22d2ab105ee5f; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.building
    ADD CONSTRAINT "PK_bbfaf6c11f141a22d2ab105ee5f" PRIMARY KEY (id);


--
-- Name: post PK_be5fda3aac270b134ff9c21cdee; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT "PK_be5fda3aac270b134ff9c21cdee" PRIMARY KEY (id);


--
-- Name: product PK_bebc9158e480b949565b4dc7a82; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY (id);


--
-- Name: movie PK_cb3bb4d61cf764dc035cbedd422; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie
    ADD CONSTRAINT "PK_cb3bb4d61cf764dc035cbedd422" PRIMARY KEY (id);


--
-- Name: estate PK_cbb82b5600918dca1140ff8d276; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estate
    ADD CONSTRAINT "PK_cbb82b5600918dca1140ff8d276" PRIMARY KEY (id);


--
-- Name: anon PK_eae93c3cb4af0528b4552dc4838; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.anon
    ADD CONSTRAINT "PK_eae93c3cb4af0528b4552dc4838" PRIMARY KEY (id);


--
-- Name: subscriber UQ_073600148a22d05dcf81d119a6a; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscriber
    ADD CONSTRAINT "UQ_073600148a22d05dcf81d119a6a" UNIQUE (email);


--
-- Name: IDX_2335ba4938523040e1d54a9b47; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_2335ba4938523040e1d54a9b47" ON public.person_likes_post USING btree ("personId");


--
-- Name: IDX_bf4608b41eb83967bdf7b7db95; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_bf4608b41eb83967bdf7b7db95" ON public.person_likes_post USING btree ("postId");


--
-- Name: autopart FK_052070ed6914ba38621ca49cbdc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.autopart
    ADD CONSTRAINT "FK_052070ed6914ba38621ca49cbdc" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: service FK_05b32d54af51b5e3171112f7e45; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.service
    ADD CONSTRAINT "FK_05b32d54af51b5e3171112f7e45" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: complaint FK_0722574160f24d00c5dada22994; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.complaint
    ADD CONSTRAINT "FK_0722574160f24d00c5dada22994" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: beauty FK_07bf325d74c76690ec41d967597; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beauty
    ADD CONSTRAINT "FK_07bf325d74c76690ec41d967597" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: children FK_17f521dbd675c73e57537596a80; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.children
    ADD CONSTRAINT "FK_17f521dbd675c73e57537596a80" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: tourism FK_180a6667765c1afb078ecd891dc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tourism
    ADD CONSTRAINT "FK_180a6667765c1afb078ecd891dc" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: person_likes_post FK_2335ba4938523040e1d54a9b47c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_likes_post
    ADD CONSTRAINT "FK_2335ba4938523040e1d54a9b47c" FOREIGN KEY ("personId") REFERENCES public.person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comment FK_276779da446413a0d79598d4fbd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT "FK_276779da446413a0d79598d4fbd" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: fashion FK_2e9b87c84645f50db034a0ce64d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fashion
    ADD CONSTRAINT "FK_2e9b87c84645f50db034a0ce64d" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: answer FK_328f85639a97f8ff158e0cf7b1f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT "FK_328f85639a97f8ff158e0cf7b1f" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: estate FK_42c30686c3e18aabba16d518a16; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estate
    ADD CONSTRAINT "FK_42c30686c3e18aabba16d518a16" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: agro FK_5474ab0c7412e6176b231af6933; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agro
    ADD CONSTRAINT "FK_5474ab0c7412e6176b231af6933" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: work FK_7cc8be7d14a4623afec6dbb7d77; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work
    ADD CONSTRAINT "FK_7cc8be7d14a4623afec6dbb7d77" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: building FK_8af7033b15a63aa8ed480a4b8c4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.building
    ADD CONSTRAINT "FK_8af7033b15a63aa8ed480a4b8c4" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: answer FK_8d730381a7e1d7baf7aec45bdcc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT "FK_8d730381a7e1d7baf7aec45bdcc" FOREIGN KEY ("commentId") REFERENCES public.comment(id) ON DELETE CASCADE;


--
-- Name: comment FK_94a85bb16d24033a2afdd5df060; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT "FK_94a85bb16d24033a2afdd5df060" FOREIGN KEY ("postId") REFERENCES public.post(id) ON DELETE CASCADE;


--
-- Name: message FK_9cad3f146f607c5214f7b386ca5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT "FK_9cad3f146f607c5214f7b386ca5" FOREIGN KEY ("recieverId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: hobby FK_a176ec611867c96d551d325ed12; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hobby
    ADD CONSTRAINT "FK_a176ec611867c96d551d325ed12" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: animal FK_a7df79d7eb867398fa8438547db; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "FK_a7df79d7eb867398fa8438547db" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: electro FK_acafbf0f6910f92969047cf4470; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.electro
    ADD CONSTRAINT "FK_acafbf0f6910f92969047cf4470" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: message FK_bc096b4e18b1f9508197cd98066; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT "FK_bc096b4e18b1f9508197cd98066" FOREIGN KEY ("senderId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: person_likes_post FK_bf4608b41eb83967bdf7b7db956; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_likes_post
    ADD CONSTRAINT "FK_bf4608b41eb83967bdf7b7db956" FOREIGN KEY ("postId") REFERENCES public.post(id);


--
-- Name: auto FK_c28e77e27afd706ef643ebd4f05; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auto
    ADD CONSTRAINT "FK_c28e77e27afd706ef643ebd4f05" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: post FK_c6fb082a3114f35d0cc27c518e0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT "FK_c6fb082a3114f35d0cc27c518e0" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: home FK_c9576381437d9768fe06e09158a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home
    ADD CONSTRAINT "FK_c9576381437d9768fe06e09158a" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- Name: product FK_dddbf2ae70d3f6312a02458837a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT "FK_dddbf2ae70d3f6312a02458837a" FOREIGN KEY ("authorId") REFERENCES public.person(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

