import React, { ChangeEvent, useState } from "react";
// import { Button, Form, Input } from "antd";
import { Box, Button, Grid, Stack, TextField, Typography } from "@mui/material";
import axios from "axios";
import { useDispatch } from "react-redux";
import { setAuth } from "./store/authSlice";

const VITE_API = import.meta.env.VITE_API;

interface IValues {
  login: string;
  password: string;
}

export default function Login() {
  const dispatch = useDispatch();
  const [state, setState] = useState<IValues>({
    login: "",
    password: "",
  } as IValues);
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [e.target.name]: e.target.value });
  };

  return (
    // <div style={{ padding: "50px" }}>
    //   <Form
    //     layout="vertical"
    //     onFinish={(values: IValues) => {
    //       axios
    //         .post(`${VITE_API}/api/auth`, values)
    //         .then((res) => {
    //           dispatch(
    //             setAuth({ avatar: res.data.avatar, token: res.data.token })
    //           );
    //         })
    //         .catch((err) => {
    //           console.log(err);
    //           alert("Incorrect login or password");
    //         });
    //     }}
    //   >
    //     <Form.Item
    //       name="login"
    //       label="Login"
    //       rules={[
    //         {
    //           required: true,
    //           message: "Please enter user login!",
    //         },
    //       ]}
    //     >
    //       <Input placeholder="Enter user login" />
    //     </Form.Item>
    //     <Form.Item
    //       label="Password"
    //       name="password"
    //       rules={[
    //         {
    //           required: true,
    //           message: "Please input your password!",
    //         },
    //       ]}
    //     >
    //       <Input.Password />
    //     </Form.Item>
    //     <Form.Item>
    //       <Button type="primary" htmlType="submit">
    //         Submit
    //       </Button>
    //     </Form.Item>
    //   </Form>
    // </div>
    <Stack sx={{ padding: "50px" }} spacing={2}>
      <Typography variant="h4">Type your login and password</Typography>
      <Box
        component="form"
        onSubmit={(e) => {
          e.preventDefault();
          axios
            .post(`${VITE_API}/api/auth`, state)
            .then((res) => {
              console.log(res.data);
              dispatch(
                setAuth({
                  avatar: res.data.avatar,
                  token: res.data.token,
                  name: res.data.name,
                  id: res.data.id,
                })
              );
            })
            .catch((err) => {
              console.log(err);
              alert("Incorrect login or password");
            });
        }}
      >
        <Grid container rowSpacing={2}>
          <Grid item xs={12}>
            <TextField
              label="Login (login)"
              name="login"
              value={state.login}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="Password"
              name="password"
              type="password"
              value={state.password}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12}>
            <Button type="submit" variant="contained">
              Login
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Stack>
  );
}
