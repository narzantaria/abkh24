export interface Item {
  id: number;
  [key: string]: any;
}
