import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "./store";
import { Box, styled, useMediaQuery } from "@mui/material";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import SideNav from "./components/SideNav";
import TopNav from "./components/TopNav";
import Main from "./routes/Main";
import { cyan } from "@mui/material/colors";
import Users from "./routes/Users";
import User from "./routes/User";
import Page404 from "./routes/Page404";
import Funerals from "./routes/Funerals";
import Funeral from "./routes/Funeral";
import Movies from "./routes/Movies";
import Movie from "./routes/Movie";
import News from "./routes/News";
import Post from "./routes/Post";
import Comment from "./routes/Comment";
import Orders from "./routes/Orders";
import Order from "./routes/Order";
import Search from "./routes/Search";
import { PAPER_CLR } from "./misc/tv";
import SideNavRes from "./components/SideNavRes";
import Banners from "./routes/Banners";
import Banner from "./routes/Banner";
import Playbills from "./routes/Playbills";
import Playbill from "./routes/Playbill";
import SDocs from "./routes/SDocs";
import SDoc from "./routes/SDoc";
import SModel from "./routes/SModel";
import SModels from "./routes/SModels";
import MenuEditor from "./routes/MenuEditor";
import SettingsEditor from "./routes/SettingsEditor";

const MainWrapper = styled(Box)`
  position: fixed;
  display: grid;
  grid-template-columns: 300px 1fr;
  height: 100vh;
  width: 100%;
  left: 0;
  top: 0;
  a {
    color: inherit;
    text-decoration: none;
    transition: 0.3s ease-in-out;
    &:hover {
      color: ${cyan[300]};
    }
  }
  @media (max-width: 768px) {
    position: relative;
    display: block;
    height: auto;
  }
`;

export default function Private() {
  const matches = useMediaQuery("(max-width:768px)");

  return (
    <MainWrapper>
      <BrowserRouter>
        {matches ? <SideNavRes /> : <SideNav />}
        <Box
          sx={{
            backgroundColor: PAPER_CLR,
            display: "flex",
            flexDirection: "column",
          }}
        >
          <TopNav />
          <Box
            id="content-wrapper"
            sx={{
              flexGrow: 1,
              padding: "40px 30px 110px 30px",
              overflowY: "scroll",
              height: "calc(100vh - 105px)",
              scrollbarWidth: "none",
            }}
          >
            <Routes>
              <Route path="/" element={<Main />} />
              <Route path="smodels" element={<SModels />} />
              <Route path="smodels/:name" element={<SModel />} />
              <Route path="sdocs" element={<SDocs />} />
              <Route path="sdocs/:name" element={<SDoc />} />
              <Route path="playbills" element={<Playbills />} />
              <Route path="playbills/:id" element={<Playbill />} />
              <Route path="banners" element={<Banners />} />
              <Route path="banners/:id" element={<Banner />} />
              <Route path="funerals" element={<Funerals />} />
              <Route path="funerals/:id" element={<Funeral />} />
              <Route path="movies" element={<Movies />} />
              <Route path="movies/:id" element={<Movie />} />
              <Route path="news" element={<News />} />
              <Route path="news/:id" element={<Post />} />
              <Route path="comments/:id" element={<Comment />} />
              <Route path="users" element={<Users />} />
              <Route path="users/:id" element={<User />} />
              <Route path="orders" element={<Orders />} />
              <Route path="orders/:id" element={<Order />} />
              <Route path="search" element={<Search />} />
              <Route path="menu" element={<MenuEditor />} />
              <Route path="settings" element={<SettingsEditor />} />
              <Route path="*" element={<Page404 />} />
            </Routes>
          </Box>
        </Box>
      </BrowserRouter>
    </MainWrapper>
  );
}

/*
export default function Private() {
  return (
    <MainWrapper>
      <BrowserRouter>
        <SideNav />
        <Box>
          <TopNav />
          <Box
            sx={{
              padding: "30px 30px 100px 30px",
              overflowY: "scroll",
              height: "calc(100vh - 105px)",
              scrollbarWidth: "none",
            }}
          >
            <Routes>
              <Route path="/" element={<Main />} />
              <Route path="funerals" element={<Funerals />} />
              <Route path="funerals/:id" element={<Funeral />} />
              <Route path="movies" element={<Movies />} />
              <Route path="movies/:id" element={<Movie />} />
              <Route path="news" element={<News />} />
              <Route path="news/:id" element={<Post />} />
              <Route path="comments/:id" element={<Comment />} />
              <Route path="users" element={<Users />} />
              <Route path="users/:id" element={<User />} />
              <Route path="orders" element={<Orders />} />
              <Route path="orders/:id" element={<Order />} />
              <Route path="search" element={<Search />} />
              <Route path="*" element={<Page404 />} />
            </Routes>
          </Box>
        </Box>
      </BrowserRouter>
    </MainWrapper>
  );
}
*/