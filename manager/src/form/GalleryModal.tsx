import React, { Fragment, useEffect, useState } from "react";
import { AiOutlineAppstoreAdd } from "react-icons/ai";
import FileLoad from "../components/FileLoad";
import DeleteModal from "../components/DeleteModal";
import { Box, Modal, Snackbar, styled } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { setField } from "../store/formSlice";
import { RootState } from "../store";
import { BsImages } from "react-icons/bs";

const VITE_API = import.meta.env.VITE_API;

const GalleryWrapper = styled("div")(() => ({
  display: "grid",
  gridTemplateColumns: "repeat(5, 1fr)",
  gridColumnGap: "10px",
  gridRowGap: "10px",
  marginTop: "20px",
}));

const GalleryItem = styled("div")(() => ({
  position: "relative",
  paddingBottom: "100%",
}));

const ImgWrapper = styled("div")(() => ({
  position: "absolute",
  height: "100%",
  width: "100%",
  left: "0px",
  top: "0px",
  border: "1px solid #ddd",
}));

interface ImgProps {
  name: string;
  root: string;
}

const ImgX = styled("div")(({ name, root }: ImgProps) => ({
  height: "100%",
  transition: "all 0.4s ease-in-out 0s",
  width: "100%",
  background: `url(${
    VITE_API + "/dist/" + root + "/" + name
  }) center center / contain no-repeat`,
}));

const ModalWrapper = styled("div")(() => ({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "100%",
  maxWidth: "1111px",
}));

interface IProps {
  clearTemp?: boolean;
  root?: string;
  callBack?: any;
  name: string;
}

function GalleryModal({
  clearTemp = false,
  root = "common",
  callBack,
  name,
}: IProps) {
  const [open, setOpen] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const state = useSelector((state: RootState) => state.form);
  const dispatch = useDispatch();

  const data: string[] = state[name] || [];

  useEffect(() => {
    if (data.length) dispatch(setField({ field: "clearTemp", value: false }));
  }, [data]);

  return (
    <Fragment>
      <Box
        sx={{
          fontSize: "100px",
          lineHeight: 0,
          cursor: "pointer",
          width: "max-content",
          border: "1px solid #ddd",
          borderRadius: "4px",
          overflow: "hidden",
        }}
        onClick={handleOpen}
      >
        <BsImages />
      </Box>
      {data && data.length ? (
        <GalleryWrapper>
          {data.map((x: string) => (
            <GalleryItem key={x}>
              <ImgWrapper>
                <ImgX name={x} root={state.root || "common"} />
                <Box
                  sx={{
                    position: "absolute",
                    top: "8px",
                    right: "8px",
                    zIndex: 1,
                  }}
                >
                  <DeleteModal
                    callBack={(_: any) =>
                      dispatch(
                        setField({
                          field: [name],
                          value: data.filter((y: string) => y != x),
                        })
                      )
                    }
                  />
                </Box>
              </ImgWrapper>
            </GalleryItem>
          ))}
        </GalleryWrapper>
      ) : (
        ""
      )}
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <ModalWrapper>
          <FileLoad
            callBack={(arg: string) => {
              if (data.some((elem: string) => elem == arg)) {
                setMessage("Изображение уже добавлено");
              } else {
                dispatch(setField({ field: [name], value: [...data, arg] }));
              }
            }}
            root={state.root}
            clearTemp={state.clearTemp}
            title="Upload files"
            close={handleClose}
          />
        </ModalWrapper>
      </Modal>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}

export default GalleryModal;
