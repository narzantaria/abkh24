import { Box, Modal, styled } from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { SlPicture } from "react-icons/sl";
import { setField } from "../store/formSlice";
import { RootState } from "../store";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import VideoLoad from "../components/VideoLoad";
import { BsPersonVideo2 } from "react-icons/bs";

const VITE_API = import.meta.env.VITE_API;

const ModalWrapper = styled("div")(() => ({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "100%",
  maxWidth: "1111px",
}));

interface IProps {
  clearTemp?: boolean;
  name: string;
  callBack?: any;
  root?: string;
}

export default function VideoModal({
  clearTemp = false,
  name,
  callBack,
  root = "common",
}: IProps) {
  const [open, setOpen] = useState(false);
  // const [increment, setIncrement] = useState<number>(0);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const state = useAppSelector((state: RootState) => state.form);
  const dispatch = useAppDispatch();

  const photo = state[name];

  useEffect(() => {
    if (photo) dispatch(setField({ field: "clearTemp", value: false }));
  }, [photo]);

  return (
    <Fragment>
      <Box
        sx={{
          fontSize: "100px",
          lineHeight: 0,
          cursor: "pointer",
          width: "max-content",
          border: "1px solid #ddd",
          borderRadius: "4px",
          overflow: "hidden",
        }}
        onClick={handleOpen}
      >
        {photo ? (
          <img width={200} src={VITE_API + "/dist/common/film.png"} />
        ) : (
          <BsPersonVideo2 />
        )}
      </Box>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <ModalWrapper>
          <VideoLoad
            callBack={(arg: string) => {
              dispatch(setField({ field: [name], value: arg }));
              // setIncrement(increment + 1);
            }}
            root={state.root}
            clearTemp={state.clearTemp}
            title="Upload files"
            close={handleClose}
          >
            {photo && (
              <Box sx={{ mb: 2 }}>
                <video
                  width={200}
                  controls
                  src={VITE_API + "/dist/" + state.root + "/" + photo}
                />
              </Box>
            )}
          </VideoLoad>
        </ModalWrapper>
      </Modal>
    </Fragment>
  );
}
