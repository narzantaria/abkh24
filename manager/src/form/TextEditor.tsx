import "@wangeditor/editor/dist/css/style.css"; // import css

import React, { useState, useEffect } from "react";
import { Editor, Toolbar } from "@wangeditor/editor-for-react";
import { useIsMount } from "../misc/tools";
import { IDomEditor, IEditorConfig, IToolbarConfig } from "@wangeditor/editor";
// import { IDomEditor, IEditorConfig, IToolbarConfig } from "@wangeditor/editor";

interface Iprops {
  callBack: any;
  data?: string;
}

function TextEditor({ callBack, data }: Iprops) {
  const mounted = useIsMount();
  // editor instance
  const [editor, setEditor] = useState<IDomEditor | null>(null); // TS syntax
  //   const [editor, setEditor] = useState(null); // JS syntax

  // editor content
  const [html, setHtml] = useState(data || "<p>hello</p>");

  // Simulate ajax async set html
  useEffect(() => {
    if (!mounted) callBack(html);
  }, [html]);

  const toolbarConfig: Partial<IToolbarConfig> = {}; // TS syntax
  //   const toolbarConfig = {}; // JS syntax

  const editorConfig: Partial<IEditorConfig> = {
    // TS syntax
    //   const editorConfig = {
    // JS syntax
    placeholder: "Type here...",
  };

  // Timely destroy editor, important!
  useEffect(() => {
    return () => {
      if (editor == null) return;
      editor.destroy();
      setEditor(null);
    };
  }, [editor]);

  return (
    <div style={{ border: "1px solid #ccc", zIndex: 100 }}>
      <Toolbar
        editor={editor}
        defaultConfig={toolbarConfig}
        mode="default"
        style={{ borderBottom: "1px solid #ccc" }}
      />
      {/* <Editor
        defaultConfig={editorConfig}
        value={html}
        onCreated={setEditor}
        onChange={(editor) => setHtml(editor.getHtml())}
        mode="default"
        style={{ height: "350px", overflowY: "hidden" }}
      /> */}
      <Editor
        defaultConfig={editorConfig}
        value={data}
        onCreated={setEditor}
        onChange={(editor) => callBack(editor.getHtml())}
        mode="default"
        style={{ height: "350px", overflowY: "hidden" }}
      />
    </div>
  );
}

export default TextEditor;
