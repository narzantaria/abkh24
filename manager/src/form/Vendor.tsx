import React, { useEffect, useState } from "react";
import { makeId } from "../misc/tools";
import {
  FormControl,
  FormLabel,
  IconButton,
  Paper,
  TextField,
} from "@mui/material";
import { paper } from "../misc/colors";
import { GiCycle } from "react-icons/gi";

interface Iprops {
  callBack: any;
  label?: string;
  value?: string;
}

export default function Vendor({ callBack, label, value }: Iprops) {
  const [vendor, setVendor] = useState<string>(value ? value : makeId(20));
  useEffect(() => {
    callBack(vendor);
  }, [vendor]);

  return (
    <Paper
      variant="outlined"
      elevation={0}
      sx={{ p: 2, backgroundColor: paper }}
    >
      <FormControl>
        <FormLabel>{label}</FormLabel>
      </FormControl>
      <Paper
        component="form"
        elevation={0}
        sx={{
          p: "2px 4px",
          display: "grid",
          alignItems: "center",
          gridTemplateColumns: "auto min-content",
        }}
        variant="outlined"
      >
        <TextField
          variant="standard"
          value={vendor}
          InputProps={{
            disableUnderline: true,
          }}
          placeholder="Артикул"
          sx={{ paddingLeft: "10px" }}
        />
        <IconButton
          type="button"
          sx={{ p: "10px" }}
          aria-label="update"
          onClick={(_) => {
            setVendor(makeId(20));
          }}
        >
          <GiCycle />
        </IconButton>
      </Paper>
    </Paper>
  );
}
