import React, {
  ChangeEvent,
  FormEvent,
  Fragment,
  useEffect,
  useState,
} from "react";
import { useSelector } from "react-redux";
import { setField } from "../store/formSlice";
import { RootState } from "../store";
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  SelectChangeEvent,
  Snackbar,
  Stack,
  Switch,
  TextField,
} from "@mui/material";
import TextEditor from "./TextEditor";
import ImageModal from "./ImageModal";
import DatePicker from "./DatePicker";
import GalleryModal from "./GalleryModal";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import Vendor from "./Vendor";
import { hparse, hstr, useIsMount } from "../misc/tools";
import { capitalize } from "lodash";
import Tags from "./Tags";
// import Tree from "./Tree";
import VideoModal from "./VideoModal";
import Tree3 from "./Tree3";

const VITE_API = import.meta.env.VITE_API;

// Поле схемы
interface IFieldProps {
  label: string;
  name: string;
  type: string;
  value: any;
  required?: boolean;
  unique?: boolean;
  extra?: any;
  data: any[];
}

// Пропс MainForm
interface IProps {
  fields: any[] | [];
  callBack?: () => void;
  nopad?: boolean;
  btntext?: string;
}

// Ветвь дерева
interface IBranch {
  name: string;
  branch: string;
  children: any[];
}

// Фото
interface Image {
  dir: string;
  file: string;
  thumb: string;
}

// Создает случайный пароль
function newPassword(): string {
  const chars =
    "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const passwordLength = 10;
  let result = "";
  for (let i = 0; i <= passwordLength; i++) {
    const randomNumber = Math.floor(Math.random() * chars.length);
    result += chars.substring(randomNumber, randomNumber + 1);
  }
  return result;
}

/**
 * Этот компонент рендерится в цикле. В качестве пропса
 * принимает элемент массива-схемы, загруженной из файла.
 * Если есть данные из сервера, они в сторе formSlice.
 * Оттуда их значения подставляются по имени в RenderItem.
 **/
function RenderItem({
  extra,
  label,
  name,
  required,
  type,
  value,
}: IFieldProps) {
  const form = useSelector((state: RootState) => state.form);
  const dispatch = useAppDispatch();

  switch (type) {
    case "string":
    case "email":
    case "youtube":
      return (
        <TextField
          required={required}
          label={label}
          variant="outlined"
          value={value}
          onChange={(e: ChangeEvent<HTMLInputElement>) =>
            dispatch(setField({ field: name, value: e.target.value }))
          }
        />
      );

    case "switch":
      return (
        <FormControlLabel
          control={
            <Switch
              checked={value}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                dispatch(setField({ field: name, value: e.target.checked }))
              }
            />
          }
          label={label}
        />
      );

    case "int":
      return (
        <TextField
          label={label}
          variant="outlined"
          type="number"
          inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }}
          value={value}
          onChange={(e: ChangeEvent<HTMLInputElement>) =>
            dispatch(setField({ field: name, value: e.target.value }))
          }
        />
      );

    case "double":
      return (
        <TextField
          label={label}
          variant="outlined"
          type="number"
          inputProps={{ inputMode: "decimal", pattern: "d*" }}
          value={value}
          onChange={(e: ChangeEvent<HTMLInputElement>) =>
            dispatch(setField({ field: name, value: e.target.value }))
          }
        />
      );

    case "description":
      return (
        <Box sx={{ borderRadius: "4px", overflow: "hidden" }}>
          <TextField
            label={label}
            multiline
            rows={4}
            value={value}
            variant="filled"
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              dispatch(setField({ field: name, value: e.target.value }))
            }
            sx={{ width: "100%" }}
          />
        </Box>
      );

    case "text":
      return (
        <TextEditor
          data={value}
          callBack={(arg: any) => {
            dispatch(setField({ field: name, value: arg }));
          }}
        />
      );

      case "tags":
        if (extra?.length) {
          return (
            <FormControl>
              <FormLabel>{label}</FormLabel>
              <Tags
                name={name}
                data={value}
                extra={extra}
                callBack={(arg: string[]) => {
                  dispatch(setField({ field: name, value: arg }));
                }}
              />
            </FormControl>
          );
        } else return null;

    case "img":
      return (
        <FormControl>
          <FormLabel>{label}</FormLabel>
          <ImageModal name={name} />
        </FormControl>
      );

    case "gallery":
      return (
        <FormControl>
          <FormLabel>{label}</FormLabel>
          <GalleryModal name={name} />
        </FormControl>
      );

    case "video":
      return (
        <FormControl>
          <FormLabel>{label}</FormLabel>
          <VideoModal name={name} />
        </FormControl>
      );

    case "select":
      return (
        <FormControl fullWidth>
          <InputLabel>{label}</InputLabel>
          <Select
            value={value}
            label={label}
            onChange={(e: SelectChangeEvent) => {
              dispatch(setField({ field: name, value: e.target.value }));
            }}
          >
            {extra?.length &&
              extra.map((item: string, index: number) => (
                <MenuItem value={item}>{capitalize(item)}</MenuItem>
              ))}
          </Select>
        </FormControl>
      );

    case "radio":
      return (
        <FormControl>
          <FormLabel id="demo-row-radio-buttons-group-label">{label}</FormLabel>
          <RadioGroup
            row
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
            value={value}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              dispatch(setField({ field: name, value: e.target.value }))
            }
          >
            {extra?.length &&
              extra.map((item: string, index: number) => (
                <FormControlLabel
                  key={index}
                  value={item}
                  control={<Radio />}
                  label={item}
                />
              ))}
          </RadioGroup>
        </FormControl>
      );

    case "date":
      return (
        <DatePicker
          date={value}
          callBack={(arg: any) => {
            dispatch(setField({ field: name, value: arg }));
          }}
        />
      );

    case "phone":
      return (
        <Phone
          name={name}
          label={label}
          value={value ? hparse(value) : null}
          extra={extra}
          callBack={(arg: any) =>
            dispatch(setField({ field: name, value: hstr(arg) }))
          }
        />
      );

    case "vendor":
      return (
        <Vendor
          label={label}
          value={value}
          callBack={(arg: any) => {
            dispatch(setField({ field: name, value: arg }));
          }}
        />
      );

    case "password":
      return value ? (
        <TextField
          label={label}
          variant="outlined"
          // type="password"
          value={value}
          onChange={(e: ChangeEvent<HTMLInputElement>) =>
            dispatch(setField({ field: name, value: e.target.value }))
          }
        />
      ) : (
        <Button
          variant="outlined"
          sx={{ width: "max-content" }}
          onClick={(_) =>
            dispatch(setField({ field: name, value: newPassword() }))
          }
        >
          Enter / Reset password
        </Button>
      );

    case "checkbox":
      return (
        <CheckboxField
          name={name}
          label={label}
          value={value ? hparse(value) : []}
          extra={extra}
          callBack={(arg: any) =>
            dispatch(setField({ field: name, value: hstr(arg) }))
          }
        />
      );

    default:
      return <></>;
  }
}

function CheckboxField({
  label,
  name,
  extra,
  value = [],
  callBack,
}: {
  label: string;
  name: string;
  extra: string[];
  value: string[];
  callBack: (arg: any) => void;
}) {
  const [state, setState] = useState<string[]>(value);
  const mount = useIsMount();
  useEffect(() => {
    if (!mount) callBack(state);
  }, [state]);

  return (
    <FormControl>
      <FormLabel>{label}</FormLabel>
      <FormGroup row>
        {extra &&
          extra.map((x: string) => (
            <FormControlLabel
              control={
                <Checkbox
                  checked={state.some((y) => y == x)}
                  onChange={(e: ChangeEvent<HTMLInputElement>) => {
                    setState(
                      state.some((y) => y == x)
                        ? state.filter((y) => y != x)
                        : [...state, x]
                    );
                  }}
                />
              }
              label={capitalize(x)}
            />
          ))}
      </FormGroup>
    </FormControl>
  );
}

interface Country {
  name: string;
  code: string;
  flag: string;
  value: string;
}

function Phone({
  label,
  name,
  extra,
  value,
  callBack,
}: {
  label: string;
  name: string;
  extra: Country[];
  value: any; //.........
  callBack: (arg: any) => void;
}) {
  const [state, setState] = useState<Country>(
    value
      ? value
      : {
          name: extra[0].name,
          code: extra[0].code,
          flag: extra[0].flag,
          value: "",
        }
  );
  const mount = useIsMount();
  useEffect(() => {
    if (!mount && state.code && state.value) callBack(state);
  }, [state]);

  return (
    <FormControl>
      <FormLabel>{label}</FormLabel>
      {extra && (
        <Box
          sx={{
            display: "grid",
            gridColumnGap: "10px",
            gridTemplateColumns: "115px auto",
          }}
        >
          <Select
            name={name}
            label="Country"
            value={extra.findIndex((z) => z.name == state.name).toString()}
            onChange={(e: SelectChangeEvent) => {
              const itemValue: Country = extra[Number(e.target.value)];
              setState({
                ...state,
                name: itemValue.name,
                code: itemValue.code,
                flag: itemValue.flag,
              });
            }}
          >
            {extra.map((y: Country, i: number) => (
              <MenuItem value={i}>
                <Box
                  sx={{
                    display: "flex",
                    columnGap: "8px",
                    alignItems: "center",
                  }}
                >
                  <img
                    src={`${VITE_API}/dist/common/${y.flag}`}
                    alt={y.name}
                    width={35}
                  />
                  {y.code}
                </Box>
              </MenuItem>
            ))}
          </Select>
          <TextField
            label={label}
            variant="outlined"
            value={state.value}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setState({ ...state, value: e.target.value })
            }
          />
        </Box>
      )}
    </FormControl>
  );
}

export default function MainForm({
  callBack,
  fields,
  nopad,
  btntext = "Submit",
}: IProps) {
  const [message, setMessage] = useState<string>("");
  function submit(e: FormEvent) {
    e.preventDefault();
    if (callBack) callBack();
  }

  const state = useAppSelector((state: RootState) => state.form);

  if (fields && fields.length) {
    return (
      <Fragment>
        {/* <h3 onClick={() => console.log(fields)}>12345</h3> */}
        <form onSubmit={submit} style={nopad ? {} : { paddingBottom: "45px" }}>
          <Stack spacing={3}>
            {fields
              .filter((x: IFieldProps) => x.label && x.type !== "tree")
              .map((field: any, i: number) => (
                <RenderItem key={i} {...field} value={state[field.name]} />
              ))}
            {fields
              .filter((field) => field.type === "tree")
              .map((field: any) => (
                <Tree3 tree={field.data} fields={fields} />
              ))}
            <Button
              variant="contained"
              type="submit"
              sx={{ width: "max-content" }}
            >
              {btntext}
            </Button>
          </Stack>
        </form>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else return <>Loading...</>;
}
