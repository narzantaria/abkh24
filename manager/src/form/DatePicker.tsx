import React, { useEffect, useState } from "react";
import dayjs, { Dayjs } from "dayjs";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { useIsMount } from "../misc/tools";

const dateFormat = "DD.MM.YYYY";

interface Iprops {
  callBack: any;
  date?: string;
}

export default function DatePicker({ callBack, date }: Iprops) {
  const isMount = useIsMount();
  const [value, setValue] = useState<Dayjs | null>(
    dayjs(date ? date : new Date().toISOString())
  );

  //   const handleChange = (newValue: string) => {
  //     setValue(newValue);
  //   };

  useEffect(() => {
    if (!isMount) {
      callBack(dayjs(value).toISOString());
    }
  }, [value]);

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DesktopDatePicker
        label="Дата"
        // inputFormat={dateFormat}
        value={value}
        // onChange={handleChange}
        onChange={(newValue) => setValue(newValue)}
        // renderInput={(params) => <TextField {...params} />}
      />
    </LocalizationProvider>
  );
}
