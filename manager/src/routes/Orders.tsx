/**
 * Здесь новая, более правильная концепция docimages, root и тд,
 * конкретно: Orders, Movie, controllers/orders !!!
 * Подробнее - см. Readme. Изменения сделаны не везде,
 * тк этот manager неактуален, и после окончания нашего
 * проекта будет разрабатываться новая версия Heroes/Megatron,
 * не на базе React, возможно, вообще на Rust/Wasm...
 **/
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Drawer,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import DeleteModal from "../components/DeleteModal";
import axios from "axios";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import NoData from "../components/NoData";
import { useAppDispatch, useAppSelector } from "../store/hooks";

const VITE_API = import.meta.env.VITE_API;

export interface IOrder {
  id: number;
  title: string;
  phone: string;
  email: string;
  text: string;
  createdAt: Date;
}

export default function Orders() {
  const [data, setData] = useState<IOrder[] | null>(null);
  const [message, setMessage] = useState<string>("");
  const { id, token } = useAppSelector((state: RootState) => state?.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  useEffect(() => {
    axios.get(`${VITE_API}/api/orders`).then((res) => {
      setData(res.data);
    });
  }, []);

  return (
    <Fragment>
      <Card>
        <CardHeader
          sx={{ borderBottom: "1px solid #ddd" }}
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                columnGap: "10px",
              }}
            >
              <Typography>Orders list</Typography>
            </Box>
          }
        />
        <CardContent>
          {data?.length ? (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <b>Name</b>
                  </TableCell>
                  <TableCell align="right">Remove</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((x: IOrder) => (
                  <TableRow key={uuidv4()}>
                    <TableCell>
                      <Link to={`/orders/${x.id}`}>{x.title}</Link>
                    </TableCell>
                    <TableCell align="right">
                      <DeleteModal
                        callBack={() => {
                          axios
                            .delete(`${VITE_API}/api/orders/${x.id}`, {
                              headers: {
                                "Content-Type": "application/json",
                                "auth-token": token,
                              },
                            })
                            .then(() => {
                              setData(data.filter((y) => y.id != x.id));
                              setMessage("Document deleted");
                            })
                            .catch((err) => {
                              const resStatus: number = err.response.status;
                              const errMsg: string = err.response.data.message;
                              if (resStatus === 401) {
                                dispatch(logout());
                              } else {
                                setMessage(errMsg);
                              }
                            });
                        }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          ) : (
            <NoData />
          )}
        </CardContent>
      </Card>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
