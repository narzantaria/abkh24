import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Drawer,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import DeleteModal from "../components/DeleteModal";
import axios from "axios";
import { logout } from "../store/authSlice";
import { reset } from "../store/formSlice";
import { RootState } from "../store";
import NoData from "../components/NoData";
import MainForm from "../form/MainForm";
import { useAppDispatch, useAppSelector } from "../store/hooks";

const VITE_API = import.meta.env.VITE_API;

interface INewModel {
  name: string;
  data: string;
}

interface IUser {
  id: number;
  name: string;
  login: string;
  // password: string;
  email: object;
  // createdAt: string;
}

const formProps = [
  {
    name: "login",
    required: true,
    label: "Login",
    type: "string",
  },
  {
    name: "password",
    required: true,
    label: "Password",
    type: "password",
  },
  {
    name: "name",
    required: true,
    label: "Name",
    type: "string",
  },
  {
    name: "email",
    required: true,
    label: "Email",
    type: "string",
  },
];

export default function Users() {
  const [open, setOpen] = useState<boolean>(false);
  const [data, setData] = useState<IUser[] | []>([]);
  const [message, setMessage] = useState<string>("");
  const [newModel, setNewModel] = useState<INewModel>({ name: "", data: "" });
  // const [formProps, setFormProps] = useState<any[] | []>([]);
  const { token } = useAppSelector((state: RootState) => state?.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  useEffect(() => {
    axios.get(`${VITE_API}/api/users`).then((res) => {
      setData(res.data);
    });
    // axios.get(`${VITE_API}/api/builder/user`).then((res) => {
    //   setFormProps(res.data);
    // });
  }, []);

  return (
    <Fragment>
      <Card>
        <CardHeader
          sx={{ borderBottom: "1px solid #ddd" }}
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                columnGap: "10px",
              }}
            >
              <Typography>Users list</Typography>
              <Button variant="contained" onClick={(_) => setOpen(true)}>
                Add new
              </Button>
            </Box>
          }
        />
        <CardContent>
          {data?.length ? (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <b>Name</b>
                  </TableCell>
                  <TableCell align="right">Remove</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((x: IUser) => (
                  <TableRow key={uuidv4()}>
                    <TableCell>
                      <Link to={`/users/${x.id}`}>{x.name}</Link>
                    </TableCell>
                    <TableCell align="right">
                      <DeleteModal
                        callBack={() => {
                          axios
                            .delete(`${VITE_API}/api/users/${x.id}`, {
                              headers: {
                                "Content-Type": "application/json",
                                "auth-token": token,
                              },
                            })
                            .then(() => {
                              setData(data.filter((y) => y.id != x.id));
                              setMessage("Document deleted");
                            })
                            .catch((err) => {
                              const resStatus: number = err.response.status;
                              const errMsg: string = err.response.data.message;
                              if (resStatus === 401) {
                                dispatch(logout());
                              } else {
                                setMessage(errMsg);
                              }
                            });
                        }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          ) : (
            <NoData />
          )}
        </CardContent>
      </Card>
      <Drawer
        anchor="right"
        open={open}
        onClose={(_) => setOpen(false)}
        PaperProps={{
          sx: { width: "100%", maxWidth: "555px", padding: "25px 15px" },
        }}
      >
        <MainForm
          fields={formProps}
          callBack={() => {
            // здесь тоже убираем root?
            const { clearTemp, ...rest } = form;
            axios
              .post(`${VITE_API}/api/users`, rest, {
                headers: {
                  "Content-Type": "application/json",
                  "auth-token": token,
                },
              })
              .then((res) => {
                setMessage("Document created successfully");
                setOpen(false);
                setData((prev) => [...prev, res.data]);
                dispatch(reset());
              })
              .catch((err) => {
                const resStatus: number = err.response.status;
                const errMsg: string = err.response.data.message;
                if (resStatus === 401) {
                  dispatch(logout());
                } else {
                  setMessage(errMsg);
                }
              });
          }}
        />
      </Drawer>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
