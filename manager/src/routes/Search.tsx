import React, { Fragment, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import axios from "axios";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  FormControl,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Snackbar,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import MainForm from "../form/MainForm";
import { reset, setForm } from "../store/formSlice";
import { IOrder } from "./Orders";
import { hparse } from "../misc/tools";
import { IPost } from "../misc/types";
import { v4 as uuidv4 } from "uuid";
import NoData from "../components/NoData";
import DeleteModal from "../components/DeleteModal";

const VITE_API = import.meta.env.VITE_API;

const formProps = [
  {
    name: "login",
    required: true,
    label: "Login",
    type: "string",
  },
  {
    name: "password",
    required: true,
    label: "Password",
    type: "password",
  },
  {
    name: "name",
    required: true,
    label: "Name",
    type: "string",
  },
  {
    name: "email",
    required: true,
    label: "Email",
    type: "string",
  },
];

export default function Search() {
  const { token } = useAppSelector((state: RootState) => state?.auth);
  const dispatch = useAppDispatch();
  const { id } = useParams();
  const [message, setMessage] = useState<string>("");
  const [searchParams, setSearchParams] = useState<string>("");
  const [data, setData] = useState<IPost[] | null>(null);
  function handleSearch() {
    axios
      .get(`${VITE_API}/api/search/posts`, {
        params: { word: searchParams },
      })
      .then((res) => {
        setData(res.data);
      })
      .catch((err) => console.log(err));
  }

  return (
    <Stack spacing={3}>
      <FormControl sx={{ width: "100%" }} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-password">
          Поиск по сайту
        </InputLabel>
        <OutlinedInput
          id="outlined-adornment-password"
          type="text"
          value={searchParams}
          onChange={(e) => setSearchParams(e.target.value)}
          endAdornment={
            <InputAdornment position="end">
              <Button
                onClick={() => {
                  handleSearch();
                }}
              >
                Найти
              </Button>
            </InputAdornment>
          }
          label="Password"
        />
      </FormControl>
      <Card>
        <CardHeader
          title="Single document (user)"
          sx={{ borderBottom: "1px solid #ddd" }}
        />
        <CardContent>
          {data?.length ? (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <b>Name</b>
                  </TableCell>
                  <TableCell align="right">Remove</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((x: IPost) => (
                  <TableRow key={uuidv4()}>
                    <TableCell>
                      <Link to={`/news/${x.id}`}>{x.title}</Link>
                    </TableCell>
                    <TableCell align="right">
                      <DeleteModal
                        callBack={() => {
                          axios
                            .delete(`${VITE_API}/api/posts/${x.id}`, {
                              headers: {
                                "Content-Type": "application/json",
                                "auth-token": token,
                              },
                            })
                            .then(() => {
                              setData(data.filter((y) => y.id != x.id));
                              setMessage("Document deleted");
                            })
                            .catch((err) => {
                              const resStatus: number = err.response.status;
                              const errMsg: string = err.response.data.message;
                              if (resStatus === 401) {
                                dispatch(logout());
                              } else {
                                setMessage(errMsg);
                              }
                            });
                        }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          ) : (
            <NoData />
          )}
        </CardContent>
      </Card>
    </Stack>
  );
}
