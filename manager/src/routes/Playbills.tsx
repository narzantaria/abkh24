/**
 * Здесь новая, более правильная концепция docimages, root и тд,
 * конкретно: Playbills, Playbill, controllers/playbills !!!
 * Подробнее - см. Readme. Изменения сделаны не везде,
 * тк этот manager неактуален, и после окончания нашего
 * проекта будет разрабатываться новая версия Heroes/Megatron,
 * не на базе React, возможно, вообще на Rust/Wasm...
 **/
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Drawer,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import DeleteModal from "../components/DeleteModal";
import axios from "axios";
import { logout } from "../store/authSlice";
import { reset, setField } from "../store/formSlice";
import { RootState } from "../store";
import NoData from "../components/NoData";
import MainForm from "../form/MainForm";
import { useAppDispatch, useAppSelector } from "../store/hooks";

const VITE_API = import.meta.env.VITE_API;

interface INewModel {
  name: string;
  data: string;
}

export interface IPlaybill {
  id: number;
  title: string;  
  text: string;
  photo: string;
  url: string;
  source: string;  
  createdAt: string;
  premiere: string;
  author: string;
  director: string;
  genre: string;
}

const formProps = [
  {
    name: "title",
    required: true,
    label: "Title",
    type: "string",
  },  
  {
    name: "text",
    required: true,
    label: "Text",
    type: "text",
  },
  {
    name: "photo",
    required: true,
    label: "Photo",
    type: "img",
  },
  {
    name: "url",
    required: true,
    label: "ссылка",
    type: "string",
  },
  {
    name: "source",
    required: true,
    label: "Source",
    type: "string",
  },
  {
    name: "createdAt",
    label: "Date",
    type: "date",
  },
  {
    name: "premiere",
    required: true,
    label: "Premiere",
    type: "string",
  },
  {
    name: "author",
    required: true,
    label: "Author",
    type: "string",
  },
  {
    name: "director",
    required: true,
    label: "Director",
    type: "string",
  },
  {
    name: "genre",
    required: true,
    label: "Genre",
    type: "string",
  },
];

export default function Playbills() {
  const [open, setOpen] = useState<boolean>(false);
  const [data, setData] = useState<IPlaybill[] | []>([]);
  const [message, setMessage] = useState<string>("");
  const [newModel, setNewModel] = useState<INewModel>({ name: "", data: "" });
  const { id, token } = useAppSelector((state: RootState) => state?.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  useEffect(() => {
    axios.get(`${VITE_API}/api/playbills`).then((res) => {
      setData(res.data);
    });
  }, []);
  async function handleOpen() {
    if(id) {
      await dispatch(reset());
      await dispatch(setField({ field: "root", value: `userid${id}` }));
      setOpen(true)
    }
  }

  return (
    <Fragment>
      <Card>
        <CardHeader
          sx={{ borderBottom: "1px solid #ddd" }}
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                columnGap: "10px",
              }}
            >
              <Typography>Playbills list</Typography>
              <Button variant="contained" onClick={(_) => handleOpen()}>
                Add new
              </Button>
            </Box>
          }
        />
        <CardContent>
          {data?.length ? (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <b>Name</b>
                  </TableCell>
                  <TableCell align="right">Remove</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((x: IPlaybill) => (
                  <TableRow key={uuidv4()}>
                    <TableCell>
                      <Link to={`/playbills/${x.id}`}>{x.title}</Link>
                    </TableCell>
                    <TableCell align="right">
                      <DeleteModal
                        callBack={() => {
                          axios
                            .delete(`${VITE_API}/api/playbills/${x.id}`, {
                              headers: {
                                "Content-Type": "application/json",
                                "auth-token": token,
                              },
                            })
                            .then(() => {
                              setData(data.filter((y) => y.id != x.id));
                              setMessage("Document deleted");
                            })
                            .catch((err) => {
                              const resStatus: number = err.response.status;
                              const errMsg: string = err.response.data.message;
                              if (resStatus === 401) {
                                dispatch(logout());
                              } else {
                                setMessage(errMsg);
                              }
                            });
                        }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          ) : (
            <NoData />
          )}
        </CardContent>
      </Card>
      <Drawer
        anchor="right"
        open={open}
        onClose={(_) => setOpen(false)}
        PaperProps={{
          sx: { width: "100%", maxWidth: "555px", padding: "25px 15px" },
        }}
      >
        <MainForm
          fields={formProps}
          callBack={() => {
            const { clearTemp, ...rest } = form;
            axios
              .post(`${VITE_API}/api/playbills`, rest, {
                headers: {
                  "Content-Type": "application/json",
                  "auth-token": token,
                },
              })
              .then((res) => {
                setMessage("Document created successfully");
                setOpen(false);
                setData((prev) => [...prev, res.data]);
                // dispatch(reset());
              })
              .catch((err) => {
                const resStatus: number = err.response.status;
                const errMsg: string = err.response.data.message;
                if (resStatus === 401) {
                  // dispatch(logout());
                } else {
                  setMessage(errMsg);
                }
              });
          }}
        />
      </Drawer>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
