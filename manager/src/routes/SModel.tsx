import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import axios from "axios";
import CodeForm from "../components/CodeForm";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Drawer,
  Snackbar,
  Stack,
} from "@mui/material";
import MainForm from "../form/MainForm";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import { reset } from "../store/formSlice";

const VITE_API = import.meta.env.VITE_API;

export default function SModel() {
  const { name } = useParams();
  const [open, setOpen] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const [data, setData] = useState<any[] | null>(null);
  const [docExisted, setDocExisted] = useState<boolean>(false);
  const { token } = useAppSelector((state: RootState) => state.auth);
  const dispatch = useAppDispatch();
  const form = useAppSelector((state: RootState) => state?.form);
  useEffect(() => {
    axios.get(`${VITE_API}/api/sbuilder/${name?.toLowerCase()}`).then((res) => {
      setData(res.data);
    });
    axios
      .get(`${VITE_API}/api/sdocs/${name?.toLowerCase()}`)
      .then((res) => {
        setDocExisted(true);
      })
      .catch((err) => console.log(err.response.status));
  }, []);

  return (
    <Stack spacing={3}>
      <Card>
        <CardHeader
          title={`Single model ${name}`}
          sx={{ borderBottom: "1px solid #ddd" }}
        />
        <CardContent>
          {data && (
            <CodeForm
              data={data}
              callBack={(arg: string) => {
                axios
                  .put(
                    `${VITE_API}/api/sbuilder/${name?.toLowerCase()}`,
                    { data: arg },
                    {
                      headers: {
                        "Content-Type": "application/json",
                        "auth-token": token,
                      },
                    }
                  )
                  .then((res) => {
                    setMessage("Document updated successfully");
                  })
                  .catch((err) => {
                    const resStatus: number = err.response.status;
                    const errMsg: string = err.response.data.message;
                    if (resStatus === 401) {
                      dispatch(logout());
                    } else {
                      setMessage(errMsg);
                    }
                  });
              }}
            />
          )}
        </CardContent>
      </Card>
      <Card>
        <CardHeader
          title={
            docExisted
              ? "Go to model's document"
              : `Create a document from model ${name}`
          }
          sx={{ borderBottom: "1px solid #ddd" }}
        />
        <CardContent>
          {docExisted ? (
            <Button>
              <Link to={`/sdocs/${name}`}>Go to doc</Link>
            </Button>
          ) : (
            <Button
              variant="outlined"
              onClick={() => {
                setOpen(true);
              }}
            >
              Create doc from model
            </Button>
          )}
        </CardContent>
      </Card>
      <Drawer
        anchor="right"
        open={open}
        onClose={(_) => {
          setOpen(false);
          dispatch(reset());
        }}
        PaperProps={{
          sx: { width: "100%", maxWidth: "555px", padding: "25px 15px" },
        }}
      >
        {data && (
          <MainForm
            fields={data.concat([
              {
                name: "id",
                label: "Пользовательский id",
                type: "string",
              },
            ])}
            callBack={() => {
              // здесь тоже убираем root?
              const { clearTemp, ...rest } = form;
              axios
                .post(
                  `${VITE_API}/api/sdocs`,
                  { name: name, data: rest },
                  {
                    headers: {
                      "Content-Type": "application/json",
                      "auth-token": token,
                    },
                  }
                )
                .then((res) => {
                  setMessage("Document created successfully");
                  setOpen(false);
                  setDocExisted(true);
                  dispatch(reset());
                })
                .catch((err) => {
                  const resStatus: number = err.response.status;
                  const errMsg: string = err.response.data.message;
                  if (resStatus === 401) {
                    dispatch(logout());
                  } else {
                    setMessage(errMsg);
                  }
                });
            }}
          />
        )}
      </Drawer>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Stack>
  );
}
