/**
 * Здесь новая, более правильная концепция docimages, root и тд,
 * конкретно: Posts, Post, controllers/posts !!!
 * Подробнее - см. Readme. Изменения сделаны не везде,
 * тк этот manager неактуален, и после окончания нашего
 * проекта будет разрабатываться новая версия Heroes/Megatron,
 * не на базе React, возможно, вообще на Rust/Wasm...
 **/
import React, { Fragment, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import axios from "axios";
import {
  Card,
  CardContent,
  CardHeader,
  ListItem,
  ListItemButton,
  ListItemText,
  Snackbar,
  Stack,
} from "@mui/material";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import MainForm from "../form/MainForm";
import { reset, setForm } from "../store/formSlice";
import { IComment, IPost } from "../misc/types";
import DeleteModal from "../components/DeleteModal";

const VITE_API = import.meta.env.VITE_API;

const formProps = [
  {
    name: "title",
    required: true,
    label: "Title",
    type: "string",
  },
  {
    name: "description",
    label: "Text",
    type: "description",
  },
  {
    name: "text",
    required: true,
    label: "Text",
    type: "text",
  },
  {
    name: "photos",
    label: "Photos",
    type: "gallery",
  },
  {
    name: "video",
    label: "Video",
    type: "video",
  },
  {
    name: "youtube",
    label: "Youtube video",
    type: "youtube",
  },
  {
    name: "createdAt",
    required: true,
    label: "Дата",
    type: "date",
  },
  {
    name: "tags",
    label: "Теги",
    type: "tags",
    extra: [
      "Абхазия 24",
      "АМРА-life",
      "Республика",
      "Народный фронт",
      "Спорткомитет",
      "ЧП Абхазии",
      "ЧП Сочи",
      "ИБ Пятнашка",
      "АГТРК",
      "Абаза ТВ",
      "Спутник",
      "Абхазия",
      "Россия",
      "политика",
      "новости мира",
      "спорт",
      "IT и технологии",
      "экономика",
      "культура",
      "искусство",
      "наука",
      "бизнес",
      "медицина",
      "туризм",
      "конфликты",
      "происшествия",
      "криминал",
      "медицина",
      "ТВ",
      "МВД Абхазии",
    ],
  },
  {
    name: "sourceurl",
    label: "Ссылка на источник",
    type: "string",
  },
];

export default function Post() {
  const { id } = useParams();
  const [message, setMessage] = useState<string>("");
  const [loaded, setLoaded] = useState<boolean>(false);
  const { token } = useAppSelector((state: RootState) => state.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  const handleData = (args: IPost) => {
    dispatch(
      setForm({
        ...args,
        root: args.dir || `userid${id}`,
        clearTemp: false,
      })
    );
  };
  useEffect(() => {
    axios.get(`${VITE_API}/api/posts/${id}`).then((res) => {
      setLoaded(true);
      handleData(res.data);
    });
    return () => {
      dispatch(reset());
    };
  }, []);

  if (loaded) {
    return (
      <Fragment>
        <Stack spacing={3}>
          <Card>
            <CardHeader
              title="Single document (funeral)"
              sx={{ borderBottom: "1px solid #ddd" }}
            />
            <CardContent>
              <MainForm
                nopad
                fields={formProps}
                callBack={() => {
                  const { clearTemp, ...rest } = form;
                  axios
                    .patch(`${VITE_API}/api/posts/common/${id}`, rest, {
                      headers: {
                        "Content-Type": "application/json",
                        "auth-token": token,
                      },
                    })
                    .then((res) => {
                      setMessage("Document created successfully");
                      handleData(res.data);
                    })
                    .catch((err) => {
                      console.log(err);
                      const resStatus: number = err.response.status;
                      const errMsg: string = err.response.data.message;
                      if (resStatus === 401) {
                        dispatch(logout());
                      } else {
                        setMessage(errMsg);
                      }
                    });
                }}
              />
            </CardContent>
          </Card>
          {form.comments.length ? (
            <Fragment>
              {form.comments.map((x: IComment) => (
                <ListItem
                  disablePadding
                  secondaryAction={
                    <DeleteModal
                      callBack={() => {
                        console.log(x.id);
                      }}
                    />
                  }
                >
                  <Link to={`/comments/${x.id}`}>
                    <ListItemButton>
                      <ListItemText primary={x.title || x.text} />
                    </ListItemButton>
                  </Link>
                </ListItem>
              ))}
            </Fragment>
          ) : null}
        </Stack>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else return <>Loading...</>;
}
