import { Box, Card, CardContent, CardHeader } from "@mui/material";
import React from "react";
import imgSource from "../img/obama.gif";

export default function Page404() {
  return (
    <Card>
      <CardHeader
        title="404 document not found"
        sx={{ borderBottom: "1px solid #ddd" }}
      />
      <CardContent>
        <Box sx={{ width: "550px", maxWidth: "100%" }}>
          <img src={imgSource} alt="" style={{ maxWidth: "100%" }} />
        </Box>
      </CardContent>
    </Card>
  );
}
