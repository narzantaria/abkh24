/**
 * Здесь новая, более правильная концепция docimages, root и тд,
 * конкретно: Playbills, Playbill, controllers/playbills !!!
 * Подробнее - см. Readme. Изменения сделаны не везде,
 * тк этот manager неактуален, и после окончания нашего
 * проекта будет разрабатываться новая версия Heroes/Megatron,
 * не на базе React, возможно, вообще на Rust/Wasm...
 **/
import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import axios from "axios";
import { Card, CardContent, CardHeader, Snackbar } from "@mui/material";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import MainForm from "../form/MainForm";
import { reset, setForm } from "../store/formSlice";
import { IPlaybill } from "./Playbills";

const VITE_API = import.meta.env.VITE_API;

const formProps = [
  {
    name: "title",
    required: true,
    label: "Title",
    type: "string",
  },  
  {
    name: "text",
    required: true,
    label: "Text",
    type: "text",
  },
  {
    name: "photo",
    required: true,
    label: "Photo",
    type: "img",
  },
  {
    name: "url",
    required: true,
    label: "ссылка",
    type: "string",
  },
  {
    name: "source",
    required: true,
    label: "Source",
    type: "string",
  },
  {
    name: "createdAt",
    label: "Date",
    type: "date",
  },
  {
    name: "premiere",
    required: true,
    label: "Premiere",
    type: "string",
  },
  {
    name: "author",
    required: true,
    label: "Author",
    type: "string",
  },
  {
    name: "director",
    required: true,
    label: "Director",
    type: "string",
  },
  {
    name: "genre",
    required: true,
    label: "Genre",
    type: "string",
  },
];

export default function Playbill() {
  const { id } = useParams();
  const [message, setMessage] = useState<string>("");
  const [loaded, setLoaded] = useState<boolean>(false);
  const { token } = useAppSelector((state: RootState) => state.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  const handleData = (args: any) => {
    dispatch(
      setForm({
        ...args,
        root: args.dir || `userid${id}`,
        clearTemp: false,
      })
    );
  };
  useEffect(() => {
    axios.get(`${VITE_API}/api/playbills/${id}`).then((res) => {
      setLoaded(true);
      handleData(res.data);
    });
    return () => {
      dispatch(reset());
    };
  }, []);

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Single document (banner)"
          sx={{ borderBottom: "1px solid #ddd" }}
        />
        <CardContent>
          {loaded && (
            <MainForm
              nopad
              fields={formProps}
              callBack={() => {
                const { clearTemp, ...rest } = form;
                axios
                  .patch(`${VITE_API}/api/playbills/${id}`, rest, {
                    headers: {
                      "Content-Type": "application/json",
                      "auth-token": token,
                    },
                  })
                  .then((res) => {
                    setMessage("Document created successfully");
                    handleData(res.data);
                  })
                  .catch((err) => {
                    const resStatus: number = err.response.status;
                    const errMsg: string = err.response.data.message;
                    if (resStatus === 401) {
                      dispatch(logout());
                    } else {
                      setMessage(errMsg);
                    }
                  });
              }}
            />
          )}
        </CardContent>
      </Card>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
