import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import axios from "axios";
import { Card, CardContent, CardHeader, Snackbar } from "@mui/material";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import MainForm from "../form/MainForm";
import { reset, setForm } from "../store/formSlice";

const VITE_API = import.meta.env.VITE_API;

const formProps = [
  {
    name: "login",
    required: true,
    label: "Login",
    type: "string",
  },
  {
    name: "password",
    required: true,
    label: "Password",
    type: "password",
  },
  {
    name: "name",
    required: true,
    label: "Name",
    type: "string",
  },
  {
    name: "email",
    required: true,
    label: "Email",
    type: "string",
  },
];

export default function User() {
  const { id } = useParams();
  const [message, setMessage] = useState<string>("");
  const [data, setData] = useState<object | null>(null);
  // const [formProps, setFormProps] = useState<any[] | []>([]);
  const { token } = useAppSelector((state: RootState) => state.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  useEffect(() => {
    axios.get(`${VITE_API}/api/users/${id}`).then((res) => {
      setData(res.data);
      dispatch(setForm(res.data));
    });
    // axios.get(`${VITE_API}/api/builder/user`).then((res) => {
    //   setFormProps(res.data);
    // });
    return () => {
      dispatch(reset());
    };
  }, []);

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Single document (user)"
          sx={{ borderBottom: "1px solid #ddd" }}
        />
        <CardContent>
          {data && formProps.length && (
            <MainForm
              nopad
              fields={formProps}
              callBack={() => {
                const { clearTemp, root, ...rest } = form;
                axios
                  .put(`${VITE_API}/api/users/${id}`, rest, {
                    headers: {
                      "Content-Type": "application/json",
                      "auth-token": token,
                    },
                  })
                  .then((res) => {
                    setMessage("Document created successfully");
                    setData(res.data);
                  })
                  .catch((err) => {
                    const resStatus: number = err.response.status;
                    const errMsg: string = err.response.data.message;
                    if (resStatus === 401) {
                      dispatch(logout());
                    } else {
                      setMessage(errMsg);
                    }
                  });
              }}
            />
          )}
        </CardContent>
      </Card>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
