import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import axios from "axios";
import CodeForm from "../components/CodeForm";
import { Card, CardContent, CardHeader, Snackbar } from "@mui/material";
import { useAppDispatch, useAppSelector } from "../store/hooks";

const VITE_API = import.meta.env.VITE_API;

export default function MenuEditor() {
  const { name } = useParams();
  const [message, setMessage] = useState<string>("");
  const [data, setData] = useState<string[] | null>(null);
  const { token } = useAppSelector((state: RootState) => state.auth);
  const dispatch = useAppDispatch();
  useEffect(() => {
    axios.get(`${VITE_API}/api/menu`).then((res) => {
      setData(res.data);
    });
  }, []);

  return (
    <Fragment>
      <Card>
        <CardHeader
          title={`Model ${name}`}
          sx={{ borderBottom: "1px solid #ddd" }}
        />
        <CardContent>
          {data && (
            <CodeForm
              data={data}
              callBack={(arg: string) => {
                axios
                  .put(
                    `${VITE_API}/api/menu`,
                    { data: arg },
                    {
                      headers: {
                        "Content-Type": "application/json",
                        "auth-token": token,
                      },
                    }
                  )
                  .then((res) => {
                    setMessage("Document updated successfully");
                  })
                  .catch((err) => {
                    const resStatus: number = err.response.status;
                    const errMsg: string = err.response.data.message;
                    if (resStatus === 401) {
                      dispatch(logout());
                    } else {
                      setMessage(errMsg);
                    }
                  });
              }}
            />
          )}
        </CardContent>
      </Card>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
