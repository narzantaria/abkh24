/**
 * Здесь новая, более правильная концепция docimages, root и тд,
 * конкретно: Banners, Banner, controllers/banners !!!
 * Подробнее - см. Readme. Изменения сделаны не везде,
 * тк этот manager неактуален, и после окончания нашего
 * проекта будет разрабатываться новая версия Heroes/Megatron,
 * не на базе React, возможно, вообще на Rust/Wasm...
 **/
import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import axios from "axios";
import { Card, CardContent, CardHeader, Snackbar } from "@mui/material";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import MainForm from "../form/MainForm";
import { reset, setForm } from "../store/formSlice";

const VITE_API = import.meta.env.VITE_API;

interface Image {
  dir: string;
  file: string;
  thumb: string;
}

interface IPost {
  id: number;
  dir?: string;
  title: string;
  description: string;
  text: string;
  img: string;
  createdAt: string;
}

const formProps = [
  {
    name: "title",
    required: true,
    label: "Title",
    type: "string",
  },
  {
    name: "description",
    required: true,
    label: "Description",
    type: "description",
  },
  {
    name: "text",
    required: true,
    label: "Text",
    type: "text",
  },
  {
    name: "img",
    required: true,
    label: "Image",
    type: "img",
  },
  {
    name: "url",
    required: true,
    label: "ссылка",
    type: "string",
  },
  {
    name: "position",
    required: true,
    label: "Позиция",
    type: "int",
  },
  {
    name: "createdAt",
    label: "Date",
    type: "date",
  },
];

export default function Banner() {
  const { id } = useParams();
  const [message, setMessage] = useState<string>("");
  const [loaded, setLoaded] = useState<boolean>(false);
  const { token } = useAppSelector((state: RootState) => state.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  const handleData = (args: IPost) => {
    dispatch(
      setForm({
        ...args,
        root: args.dir || `userid${id}`,
        clearTemp: false,
      })
    );
  };
  useEffect(() => {
    axios.get(`${VITE_API}/api/banners/${id}`).then((res) => {
      setLoaded(true);
      handleData(res.data);
    });
    return () => {
      dispatch(reset());
    };
  }, []);

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Single document (banner)"
          sx={{ borderBottom: "1px solid #ddd" }}
        />
        <CardContent>
          {loaded && (
            <MainForm
              nopad
              fields={formProps}
              callBack={() => {
                const { clearTemp, ...rest } = form;
                axios
                  .patch(`${VITE_API}/api/banners/${id}`, rest, {
                    headers: {
                      "Content-Type": "application/json",
                      "auth-token": token,
                    },
                  })
                  .then((res) => {
                    setMessage("Document created successfully");
                    handleData(res.data);
                  })
                  .catch((err) => {
                    const resStatus: number = err.response.status;
                    const errMsg: string = err.response.data.message;
                    if (resStatus === 401) {
                      dispatch(logout());
                    } else {
                      setMessage(errMsg);
                    }
                  });
              }}
            />
          )}
        </CardContent>
      </Card>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
