import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import axios from "axios";
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Snackbar,
  Stack,
  Typography,
} from "@mui/material";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import MainForm from "../form/MainForm";
import { reset, setForm } from "../store/formSlice";
import { IOrder } from "./Orders";
import { hparse } from "../misc/tools";

const VITE_API = import.meta.env.VITE_API;

const formProps = [
  {
    name: "login",
    required: true,
    label: "Login",
    type: "string",
  },
  {
    name: "password",
    required: true,
    label: "Password",
    type: "password",
  },
  {
    name: "name",
    required: true,
    label: "Name",
    type: "string",
  },
  {
    name: "email",
    required: true,
    label: "Email",
    type: "string",
  },
];

export default function Order() {
  const { id } = useParams();
  const [message, setMessage] = useState<string>("");
  const [data, setData] = useState<IOrder | null>(null);
  useEffect(() => {
    axios.get(`${VITE_API}/api/orders/${id}`).then((res) => {
      setData(res.data);
    });
  }, []);

  return (
    <Card>
      <CardHeader
        title="Single document (user)"
        sx={{ borderBottom: "1px solid #ddd" }}
      />
      <CardContent>
        {data && formProps.length && (
          <Stack spacing={3}>
            {/* <h3 onClick={() => console.log(data)}>kjhgkj</h3> */}
            <Typography variant="h5">{data.title}</Typography>
            {data.phone && (
              <Typography variant="body1">
                {hparse(data.phone).code + hparse(data.phone).value}
              </Typography>
            )}
            {data.email && (
              <Typography variant="body1">{data.email}</Typography>
            )}
            <Box
              component="div"
              dangerouslySetInnerHTML={{
                __html: data.text,
              }}
            />
          </Stack>
        )}
      </CardContent>
    </Card>
  );
}
