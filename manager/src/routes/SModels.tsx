import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Drawer,
  Snackbar,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import React, { ChangeEvent, Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import DeleteModal from "../components/DeleteModal";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import NoData from "../components/NoData";
import CodeEditor from "../components/CodeEditor";

const VITE_API = import.meta.env.VITE_API;

interface INewModel {
  name: string;
  data: string;
}

export default function SModels() {
  const [open, setOpen] = useState<boolean>(false);
  const [data, setData] = useState<string[] | null>(null);
  const [message, setMessage] = useState<string>("");
  const [newModel, setNewModel] = useState<INewModel>({ name: "", data: "" });
  const { token } = useSelector((state: RootState) => state?.auth);
  const dispatch = useDispatch();
  useEffect(() => {
    axios.get(`${VITE_API}/api/sbuilder`).then((res) => {
      setData(res.data);
    });
  }, []);

  return (
    <Fragment>
      {/* <button onClick={() => console.log(вфеф)}>hjgkjhgkj</button> */}
      <Card>
        <CardHeader
          sx={{ borderBottom: "1px solid #ddd" }}
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                columnGap: "10px",
              }}
            >
              <Typography>Single s list</Typography>
              <Button variant="contained" onClick={(_) => setOpen(true)}>
                Add new
              </Button>
            </Box>
          }
        />
        <CardContent>
          {data?.length ? (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <b>Name</b>
                  </TableCell>
                  <TableCell align="right">Remove</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((x: string) => (
                  <TableRow key={uuidv4()}>
                    <TableCell>
                      <Link to={`/smodels/${x.toLowerCase()}`}>{x}</Link>
                    </TableCell>
                    <TableCell align="right">
                      <DeleteModal
                        callBack={() => {
                          axios
                            .delete(
                              `${VITE_API}/api/sbuilder/${x.toLowerCase()}`,
                              {
                                headers: {
                                  "Content-Type": "application/json",
                                  "auth-token": token,
                                },
                              }
                            )
                            .then(() => {
                              setData(data.filter((y) => y != x));
                              setMessage("Document deleted");
                            })
                            .catch((err) => {
                              const resStatus: number = err.response.status;
                              const errMsg: string = err.response.data.message;
                              if (resStatus === 401) {
                                dispatch(logout());
                              } else {
                                setMessage(errMsg);
                              }
                            });
                        }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          ) : (
            <NoData />
          )}
        </CardContent>
      </Card>
      <Drawer
        anchor="right"
        open={open}
        onClose={(_) => setOpen(false)}
        PaperProps={{
          sx: { width: "100%", maxWidth: "555px", padding: "25px 15px" },
        }}
      >
        <Stack
          spacing={2}
          sx={{
            ".CodeMirror": {
              height: "auto !important",
              minHeight: "300px",
            },
          }}
        >
          <TextField
            label="Model name"
            variant="outlined"
            value={newModel.name}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setNewModel({ ...newModel, name: e.target.value })
            }
          />
          <CodeEditor
            callBack={(arg: string) => {
              setNewModel({ ...newModel, data: arg });
            }}
          />
          <Button
            variant="contained"
            onClick={() => {
              console.log(newModel);
              axios
                .post(`${VITE_API}/api/sbuilder`, newModel, {
                  headers: {
                    "Content-Type": "application/json",
                    "auth-token": token,
                  },
                })
                .then((res) => {
                  setMessage("Document created successfully");
                })
                .catch((err) => {
                  const resStatus: number = err.response.status;
                  const errMsg: string = err.response.data.message;
                  if (resStatus === 401) {
                    dispatch(logout());
                  } else {
                    setMessage(errMsg);
                  }
                });
            }}
            sx={{ width: "max-content" }}
          >
            Submit
          </Button>
        </Stack>
      </Drawer>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
