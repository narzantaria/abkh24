import {
  Card,
  CardContent,
  CardHeader,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import DeleteModal from "../components/DeleteModal";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import NoData from "../components/NoData";

const VITE_API = import.meta.env.VITE_API;

export default function SDocs() {
  const [data, setData] = useState<string[] | null>(null);
  const [message, setMessage] = useState<string>("");
  const { token } = useSelector((state: RootState) => state?.auth);
  const dispatch = useDispatch();
  useEffect(() => {
    axios.get(`${VITE_API}/api/sdocs`).then((res) => {
      setData(res.data);
    });
  }, []);

  return (
    <Fragment>
      {/* <button onClick={() => console.log(data)}>hjgkjhgkj</button> */}
      <Card>
        <CardHeader
          sx={{ borderBottom: "1px solid #ddd" }}
          title="Single docs list"
        />
        <CardContent>
          {data?.length ? (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <b>Name</b>
                  </TableCell>
                  <TableCell align="right">Remove</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((x: string, i: number) => (
                  <TableRow key={i}>
                    <TableCell>
                      <Link to={`/sdocs/${x.toLowerCase()}`}>{x}</Link>
                    </TableCell>
                    <TableCell align="right">
                      <DeleteModal
                        callBack={() => {
                          console.log(
                            `${VITE_API}/api/sdocs/${x.toLowerCase()}`
                          );
                          axios
                            .delete(
                              `${VITE_API}/api/sdocs/${x.toLowerCase()}`,
                              {
                                headers: {
                                  "Content-Type": "application/json",
                                  "auth-token": token,
                                },
                              }
                            )
                            .then(() => {
                              setData(data.filter((y) => y != x));
                              setMessage("Document deleted");
                            })
                            .catch((err) => {
                              const resStatus: number = err.response.status;
                              const errMsg: string = err.response.data.message;
                              if (resStatus === 401) {
                                dispatch(logout());
                              } else {
                                setMessage(errMsg);
                              }
                            });
                        }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          ) : (
            <NoData />
          )}
        </CardContent>
      </Card>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
