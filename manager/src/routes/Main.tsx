import { Box, Card, CardContent, CardHeader } from "@mui/material";
import React from "react";
import imgSource from "../img/scale_1200.jpg";

export default function Main() {
  return (
    <Card>
      <CardHeader title="Main page" sx={{ borderBottom: "1px solid #ddd" }} />
      <CardContent>
        <Box sx={{ width: "250px", maxWidth: "100%" }}>
          <img src={imgSource} alt="" style={{ maxWidth: "100%" }} />
        </Box>
        <p>
          "Я, Шутрук-Наххунте, сын Халлутуш-Иншушинака, царь Аншана и Суз, умноживший богатства моего царства, кто заботится о землях Элама. Бог Иншушинак покровительствовал мне, и я победил Сиппap, я захватил стелу, изображающую Нарам-Суэна, присвоил её и переправил её в Элам, а затем установил её перед моим богом Иншушинаком."
        </p>
      </CardContent>
    </Card>
  );
}
