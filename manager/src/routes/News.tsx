import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Drawer,
  Grid,
  Paper,
  Snackbar,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  styled,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import DeleteModal from "../components/DeleteModal";
import axios from "axios";
import { logout } from "../store/authSlice";
import { reset, setField } from "../store/formSlice";
import { RootState } from "../store";
import NoData from "../components/NoData";
import MainForm from "../form/MainForm";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import { IField, IPost } from "../misc/types";
import { BackgroundImage, Border } from "../components/StyledUI";
import LocalVideoPrev from "../components/LocalVideoPrev";

const VITE_API = import.meta.env.VITE_API;

// const formProps = [
//   {
//     name: "title",
//     required: true,
//     label: "Title",
//     type: "string",
//   },
//   {
//     name: "text",
//     required: true,
//     label: "Text",
//     type: "text",
//   },
//   {
//     name: "photos",
//     label: "Photos",
//     type: "gallery",
//   },
//   {
//     name: "video",
//     label: "Video",
//     type: "video",
//   },
//   {
//     name: "youtube",
//     label: "Youtube video",
//     type: "youtube",
//   },
//   {
//     name: "createdAt",
//     required: true,
//     label: "Дата",
//     type: "date",
//   },
// ];

const PostPaper = styled(Paper)`
  padding: 8px;
  position: relative;
`;

const PostWrapper = styled("div")`
  display: grid;
  grid-template-columns: 100px auto;
  grid-column-gap: 10px;
`;

export default function News() {
  const [open, setOpen] = useState<boolean>(false);
  const [data, setData] = useState<IPost[] | []>([]);
  const [message, setMessage] = useState<string>("");
  const [formProps, setFormProps] = useState<IField[] | null>(null);
  const { id, token } = useAppSelector((state: RootState) => state?.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  useEffect(() => {
    // axios
    //   .get(`${VITE_API}/api/posts`, { params: { limit: 100 } })
    //   .then((res) => {
    //     setData(res.data);
    //   });
    (async () => {
      try {
        const res1 = await axios.get(`${VITE_API}/api/posts`, {
          params: { limit: 100 },
        });
        setData(res1.data);
        const res2 = await axios.get(`${VITE_API}/api/builder/post`);
        setFormProps(res2.data.filter((x: IField) => x.name != "looks"));
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);
  async function handleOpen() {
    if (id) {
      await dispatch(reset());
      setOpen(true);
    }
  }

  return (
    <Fragment>
      <Card>
        <CardHeader
          sx={{ borderBottom: "1px solid #ddd" }}
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                columnGap: "10px",
              }}
            >
              <Typography>News list</Typography>
              <Button variant="contained" onClick={(_) => handleOpen()}>
                Add new
              </Button>
            </Box>
          }
        />
        <CardContent>
          {data?.length && formProps ? (
            <Stack spacing={1}>
              {data.map((x) => (
                <PostPaper elevation={1}>
                  <Link to={`/news/${x.id}`}>
                    <PostWrapper>
                      <Border>
                        {(x.dir && x.photos?.length) ? (
                          <BackgroundImage
                            url={`${x.dir && x.photos ? x.dir : "common"}/${x.photos[0]
                              }`}
                          />
                        ) : x.video ? <LocalVideoPrev dir={x.dir} video={x.video} /> : (<BackgroundImage
                          url='common/main.jpg'
                        />)}
                      </Border>
                      <Typography variant="caption">{x.title}</Typography>
                    </PostWrapper>
                  </Link>
                  <Box
                    sx={{
                      position: "absolute",
                      top: "8px",
                      right: "8px",
                      zIndex: 1,
                    }}
                  >
                    <DeleteModal
                      callBack={() => {
                        axios
                          .delete(`${VITE_API}/api/posts/${x.id}`, {
                            headers: {
                              "Content-Type": "application/json",
                              "auth-token": token,
                            },
                          })
                          .then(() => {
                            setData(data.filter((y) => y.id != x.id));
                            setMessage("Document deleted");
                          })
                          .catch((err) => {
                            const resStatus: number = err.response.status;
                            const errMsg: string = err.response.data.message;
                            if (resStatus === 401) {
                              dispatch(logout());
                            } else {
                              setMessage(errMsg);
                            }
                          });
                      }}
                    />
                  </Box>
                </PostPaper>
              ))}
            </Stack>
          ) : (
            <NoData />
          )}
        </CardContent>
      </Card>
      <Drawer
        anchor="right"
        open={open}
        onClose={(_) => setOpen(false)}
        PaperProps={{
          sx: { width: "100%", maxWidth: "555px", padding: "25px 15px" },
        }}
      >
        {formProps && <MainForm
          fields={formProps}
          callBack={() => {
            const { clearTemp, ...rest } = form;
            axios
              .post(
                `${VITE_API}/api/posts`,
                { author: 1, ...rest },
                {
                  headers: {
                    "Content-Type": "application/json",
                    "auth-token": token,
                  },
                }
              )
              .then((res) => {
                setMessage("Document created successfully");
                setOpen(false);
                setData((prev) => [...prev, res.data]);
                dispatch(reset());
              })
              .catch((err) => {
                const resStatus: number = err.response.status;
                const errMsg: string = err.response.data.message;
                if (resStatus === 401) {
                  dispatch(logout());
                } else {
                  setMessage(errMsg);
                }
              });
          }}
        />}
      </Drawer>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
