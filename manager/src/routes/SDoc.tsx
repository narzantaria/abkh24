import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { logout } from "../store/authSlice";
import { RootState } from "../store";
import axios from "axios";
import { Card, CardContent, CardHeader, Snackbar } from "@mui/material";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import MainForm from "../form/MainForm";
import { reset, setForm } from "../store/formSlice";

const VITE_API = import.meta.env.VITE_API;

export default function SDoc() {
  const { name } = useParams();
  const [message, setMessage] = useState<string>("");
  const [loaded, setLoaded] = useState<boolean>(false);
  const { token } = useAppSelector((state: RootState) => state.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  const [formProps, setFormProps] = useState<object[] | null>(null);
  const handleData = (args: object) => {
    dispatch(
      setForm({
        ...args,
        clearTemp: false,
      })
    );
  };
  useEffect(() => {
    axios.get(`${VITE_API}/api/sdocs/${name}`).then((res) => {
      setLoaded(true);
      handleData(res.data);
    });
    axios.get(`${VITE_API}/api/sbuilder/${name}`).then((res) => {
      setFormProps(res.data);
    });
    return () => {
      dispatch(reset());
    };
  }, []);

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Single document (banner)"
          sx={{ borderBottom: "1px solid #ddd" }}
        />
        <CardContent>
          {loaded && formProps && (
            <MainForm
              nopad
              fields={formProps}
              callBack={() => {
                // УБРАЛИ ROOT (СМ ТАКЖЕ РОУТ НА СЕРВЕРЕ!!! ПОЙМЕШЬ ПОЧЕМУ!!!)
                const { clearTemp, ...rest } = form;
                axios
                  .put(`${VITE_API}/api/sdocs/${name}`, rest, {
                    headers: {
                      "Content-Type": "application/json",
                      "auth-token": token,
                    },
                  })
                  .then((res) => {
                    setMessage("Document created successfully");
                  })
                  .catch((err) => {
                    const resStatus: number = err.response.status;
                    const errMsg: string = err.response.data.message;
                    if (resStatus === 401) {
                      dispatch(logout());
                    } else {
                      setMessage(errMsg);
                    }
                  });
              }}
            />
          )}
        </CardContent>
      </Card>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
