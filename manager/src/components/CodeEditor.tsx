/**
 * Это не форма, а поле отправляет данные сразу, без кнопки
 * Можно вставить в другие формы...
 * Почему сохранены оба - читай в CodeForm
 **/
import React, { useState } from "react";
import CodeMirror from "react-codemirror";
import "codemirror/lib/codemirror.css";
import "codemirror/theme/monokai.css";
import "codemirror/theme/neat.css";
import "codemirror/mode/xml/xml.js";
import "codemirror/mode/javascript/javascript.js";

const options: object = {
  mode: "xml",
  theme: "monokai",
  scrollbarStyle: "null",
  lineNumbers: true,
};

interface IProps {
  callBack?: any;
  data?: any;
}

export default function CodeEditor({ callBack, data }: IProps) {
  const [code, setCode] = useState<string>(JSON.stringify(data, null, 2));
  const onChange = (value: string, change: CodeMirror.EditorChange) =>
    callBack(value);

  return <CodeMirror value={code} options={options} onChange={onChange} />;
}
