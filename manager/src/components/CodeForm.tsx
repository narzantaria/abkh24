/**
 * Эта форма отправляет данные по подтверждению (есть кнопка)
 * В отличие от CodeEditor, у которого нет кнопки...
 * Почему сохранил оба, а не вставил CodeEditor в CodeForm? Да потому, что
 * получается двойная передача пропсов. Ужу лучше так...
 **/
import React, { useState } from "react";
import { Button, Stack } from "@mui/material";
import CodeMirror from "react-codemirror";
import "codemirror/lib/codemirror.css";
import "codemirror/theme/monokai.css";
import "codemirror/theme/neat.css";
import "codemirror/mode/xml/xml.js";
import "codemirror/mode/javascript/javascript.js";

const options: object = {
  mode: "xml",
  theme: "monokai",
  scrollbarStyle: "null",
  lineNumbers: true,
};

interface IProps {
  callBack?: any;
  data?: any;
}

export default function CodeForm({ callBack, data }: IProps) {
  const [code, setCode] = useState<string>(JSON.stringify(data, null, 2));
  const onChange = (value: string, change: CodeMirror.EditorChange) =>
    setCode(value);

  return (
    <Stack
      spacing={2}
      sx={{
        ".CodeMirror": {
          height: "auto !important",
          minHeight: "300px",
        },
      }}
    >
      <CodeMirror value={code} options={options} onChange={onChange} />
      <Button
        variant="contained"
        onClick={() => {
          callBack(code);
        }}
        sx={{ width: "max-content" }}
      >
        Submit
      </Button>
    </Stack>
  );
}
