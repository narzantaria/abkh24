import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Snackbar,
  Typography,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import { RootState } from "../store";
import { logout } from "../store/authSlice";
import axios from "axios";
import DeleteModal from "./DeleteModal";
import ItemsList from "./ItemsList";

const VITE_API = import.meta.env.VITE_API;

interface IProps {
  parentId: string | undefined;
  caption?: string;
  model: string | undefined;
  first: string | undefined;
  second: string | undefined;
  level: string;
  suffix?: string;
  direction?: string | undefined;
  limit?: number | undefined;
}

export default function Relations({
  parentId,
  caption,
  model,
  first,
  second,
  level,
  suffix = "",
  direction = "0",
  limit,
}: IProps) {
  const [open, setOpen] = useState<boolean>(false);
  // Если таблица СВЯЗЕЙ еще не создана, то будет true:
  const [createButton, setCreateButton] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const { token } = useAppSelector((state: RootState) => state.auth);
  const dispatch = useAppDispatch();
  const [data, setData] = useState<any[] | null>(null);
  const [excluded, setExcluded] = useState<string[]>([]);
  const [increment, setIncrement] = useState<number>(0);

  useEffect(() => {
    axios
      .get(
        `${VITE_API}/api/universal/rel/${first}_${
          second + (suffix ? "_" + suffix : "")
        }`,
        {
          headers: {
            "Content-Type": "application/json",
          },
          params: {
            id: parentId,
            first: first,
            second: second,
            level: level,
            suffix: suffix,
            model: model,
            direction: direction,
          },
        }
      )
      .then((res) => {
        setData(res.data);
        const ids: string[] = res.data.map((x: any) => x.id);
        if ((level == "side" || level == "sub") && parentId) {
          ids.push(parentId);
        }
        setExcluded(ids);
      })
      .catch((err) => {
        const status: number = err.response.status;
        const errMsg: string = err.response.data.message;
        if (status === 404) {
          setMessage(errMsg);
          setCreateButton(true);
        }
      });
  }, [increment]);

  return (
    <Fragment>
      <Card>
        <CardHeader
          sx={{ borderBottom: "1px solid #ddd" }}
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                columnGap: "10px",
              }}
            >
              <Typography>{`Many-to-many list ${
                caption ? `(${caption})` : ""
              }`}</Typography>
              <Button
                variant="outlined"
                sx={{ width: "max-content" }}
                onClick={(_) => {
                  if (limit && data && data.length >= limit) {
                    setMessage("Limit exceeded");
                  } else {
                    setOpen(true);
                  }
                }}
              >
                Bind record/s
              </Button>
            </Box>
          }
        />
        <CardContent>
          {createButton ? (
            <Button
              onClick={(_) => {
                axios
                  .post(
                    `${VITE_API}/api/universal/rel`,
                    {
                      first: first,
                      second: second,
                      suffix: suffix,
                    },
                    {
                      headers: {
                        "Content-Type": "application/json",
                        "auth-token": token,
                      },
                    }
                  )
                  .then((res) => {
                    setMessage("Table created successfully");
                    setCreateButton(false);
                    setData([]);
                  })
                  .catch((err) => {
                    const resStatus: number = err.response.status;
                    const errMsg: string = err.response.data.message;
                    if (resStatus === 401) {
                      dispatch(logout());
                    } else {
                      setMessage(errMsg);
                    }
                  });
              }}
            >{`Create relational table (${first}_${
              second + (suffix ? "_" + suffix : "")
            })`}</Button>
          ) : (
            <Fragment>
              {data && (
                <nav aria-label="secondary mailbox folders">
                  <List>
                    {data.map((x) => (
                      <ListItem
                        disablePadding
                        secondaryAction={
                          <DeleteModal
                            callBack={(_: any) => {
                              if (level && first && second)
                                axios
                                  .delete(
                                    `${VITE_API}/api/universal/rel/${first}_${
                                      second + (suffix ? "_" + suffix : "")
                                    }/${parentId}`,
                                    {
                                      headers: {
                                        "Content-Type": "application/json",
                                        "auth-token": token,
                                      },
                                      params: {
                                        level: level,
                                        first: first,
                                        second: second,
                                        xid: x.id,
                                      },
                                    }
                                  )
                                  .then((res) => {
                                    setMessage("Relation deleted successfully");
                                    setData(
                                      data.filter((item) => item.id != x.id)
                                    );
                                  })
                                  .catch((err) => {
                                    const resStatus: number =
                                      err.response.status;
                                    const errMsg: string =
                                      err.response.data.message;
                                    if (resStatus === 401) {
                                      dispatch(logout());
                                    } else {
                                      setMessage(errMsg);
                                    }
                                  });
                            }}
                          />
                        }
                      >
                        <ListItemButton>
                          <ListItemText primary={x.title} />
                        </ListItemButton>
                      </ListItem>
                    ))}
                  </List>
                </nav>
              )}
            </Fragment>
          )}
        </CardContent>
      </Card>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
      <Drawer
        anchor="right"
        open={open}
        onClose={(_) => {
          setOpen(false);
        }}
        PaperProps={{
          sx: { width: "100%", maxWidth: "555px", padding: "25px 15px" },
        }}
      >
        <ItemsList
          id={parentId}
          model={model}
          excluded={excluded}
          callBack={(args: string[]) => {
            if (
              limit &&
              (data?.length ? data.length : 0) + args.length > limit
            ) {
              setMessage("Limit exceeded");
            } else {
              axios
                .post(
                  `${VITE_API}/api/universal/rel/${first}_${
                    second + (suffix ? "_" + suffix : "")
                  }`,
                  {
                    id: parentId,
                    first: first,
                    second: second,
                    data: args,
                    limit: limit,
                    level: level,
                  },
                  {
                    headers: {
                      "Content-Type": "application/json",
                      "auth-token": token,
                    },
                  }
                )
                .then((res) => {
                  setIncrement(increment + 1);
                  setMessage("Relations created successfully");
                  setOpen(false);
                })
                .catch((err) => {
                  const resStatus: number = err.response.status;
                  const errMsg: string = err.response.data.message;
                  if (resStatus === 401) {
                    dispatch(logout());
                  } else {
                    setMessage(errMsg);
                  }
                });
            }
          }}
        />
      </Drawer>
    </Fragment>
  );
}
