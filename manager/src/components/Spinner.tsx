import { Box, keyframes, styled } from "@mui/material";
import React from "react";
import { ImSpinner3 } from "react-icons/im";

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const StyledSpinner = styled(ImSpinner3)`
  font-size: 50px;
  animation: ${rotate} 1s linear infinite;
`;

export default function Spinner() {
  return (
    <Box
      sx={{
        position: "fixed",
        zIndex: 111,
        top: 0,
        left: 0,
        height: "100vh",
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "rgba(69, 69, 69, 0.25)",
      }}
    >
      <StyledSpinner />
    </Box>
  );
}
