import { styled } from "@mui/material";

const VITE_API = import.meta.env.VITE_API;

export const Border = styled("div")`
cursor: pointer;
position: relative;
overflow: hidden;
padding-bottom: 65%;
`;

export const BackgroundImage = styled("div")(({
  url,
}: {
  url: string;
}) => {
  return `
    height: 100%;
    position: absolute;
    width: 100%;
    left: 0;
    top: 0;
    background: url(${VITE_API}/dist/${url}) no-repeat center center / cover;
    `;
});