import React, { Fragment, useState } from "react";
import axios from "axios";
import { Button, Snackbar } from "@mui/material";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import { RootState } from "../store";

const VITE_API = import.meta.env.VITE_API;

export default function SendToSubscribers({ data }: { data: string }) {
  const { token } = useAppSelector((state: RootState) => state.auth);
  const form = useAppSelector((state: RootState) => state?.form);
  const dispatch = useAppDispatch();
  const [message, setMessage] = useState<string>("");

  return (
    <Fragment>
      <Button
        variant="outlined"
        sx={{
          width: "max-content",
          border: "2px dashed #ef6c00",
          color: "initial",
        }}
        onClick={() => {
          axios
            .post(
              `${VITE_API}/api/mail/newsletter`,
              { data: data },
              {
                headers: {
                  "Content-Type": "application/json",
                  "auth-token": token,
                },
              }
            )
            .then((res) => {
              console.log(res);
              setMessage("Successfully sent to sunscribers");
            })
            .catch((err) => {
              console.log(err);
            });
        }}
      >
        Send to subscribers
      </Button>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
