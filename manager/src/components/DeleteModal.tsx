import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  styled,
} from "@mui/material";
import React, { Fragment, ReactNode, useState } from "react";
import { IoIosCloseCircleOutline } from "react-icons/io";

const CloseBtn = styled(IoIosCloseCircleOutline)(() => ({
  cursor: "pointer",
  color: "brown",
  fontSize: "16px",
}));

interface IProps {
  callBack: any;
  children?: ReactNode;
  style?: object;
}

export default function DeleteModal({
  callBack,
  children = <CloseBtn />,
  style,
}: IProps) {
  const [open, setOpen] = useState(false);
  return (
    <Fragment>
      <Box
        onClick={(e) => {
          e.stopPropagation();
          setOpen(true);
        }}
        sx={style}
      >
        {children}
      </Box>
      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>"Are you sure to delete the document"</DialogTitle>
        <DialogActions>
          <Button
            variant="contained"
            onClick={(e) => {
              setOpen(false);
            }}
          >
            Disagree
          </Button>
          <Button
            variant="contained"
            onClick={(e) => {
              callBack();
              setOpen(false);
            }}
            autoFocus
            color="error"
          >
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
}
