import React, { Fragment, ReactNode } from "react";
import {
  Box,
  Divider,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Skeleton,
  SxProps,
  Theme,
  styled,
} from "@mui/material";
import { Link } from "react-router-dom";
import { cyan, lightGreen, orange } from "@mui/material/colors";
import {
  GiAbacus,
  GiAbstract014,
  GiAbstract020,
  GiAbstract027,
  GiAbstract077,
  GiAbstract089,
  GiDogHouse,
  GiFiles,
  GiFountainPen,
  GiKnightBanner,
  GiMonaLisa,
  GiNinjaHead,
  GiRailway,
  GiRuleBook,
  GiSwordSpade,
  GiVideoCamera,
} from "react-icons/gi";
import ConditionalWrapper from "../misc/ConditionalWrapper";
import { DIRTY_CLR } from "../misc/tv";
import { capitalized } from "../misc/tools";
import { CgShapeRhombus } from "react-icons/cg";
import { AiOutlineFileSearch } from "react-icons/ai";

export const NavMainBox = styled(Box)`
  background-color: ${DIRTY_CLR};
  overflow-y: scroll;
  scrollbar-width: none;
`;

const StyledMenuItem = styled(MenuItem)`
  * {
    color: #fff;
    transition: 0.3s ease-in-out;
  }
  &:hover * {
    color: ${cyan[300]};
  }
  svg {
    font-size: 23px;
  }
  @media (max-width: 768px) {
    padding-inline: 0;
  }
`;

export interface INavItemProps {
  icon: ReactNode;
  label: ReactNode;
  link: string;
  wrap?: boolean;
  dashed?: boolean;
  color?: string;
  rotate?: string | null;
  sx?: SxProps<Theme>;
  onClick?: () => void;
}

export const NavItem = ({
  icon,
  label,
  link,
  wrap = false,
  dashed = false,
  color,
  rotate = null,
  sx = {},
  onClick,
}: INavItemProps) => {
  return (
    <StyledMenuItem sx={sx} onClick={onClick}>
      <ConditionalWrapper
        condition={wrap}
        render={(props) => (
          <NavItemWrapper color={color} dashed={dashed}>
            {props}
          </NavItemWrapper>
        )}
      >
        <ListItemIcon>
          <ConditionalWrapper
            condition={rotate !== null}
            render={(props) => (
              <Box sx={{ transform: `rotate(${rotate})` }}>{props}</Box>
            )}
          >
            {icon}
          </ConditionalWrapper>
        </ListItemIcon>
        <ListItemText>
          <Link to={link}>{label}</Link>
        </ListItemText>
      </ConditionalWrapper>
    </StyledMenuItem>
  );
};

export interface IWrapperProps {
  children: ReactNode;
  dashed?: boolean;
  color?: string;
}

export const NavItemWrapper = ({ dashed, children, color }: IWrapperProps) => {
  return (
    <Box
      sx={{
        width: "100%",
        borderRadius: "25px",
        padding: `7px 13px`,
        display: "flex",
        border: `1px ${dashed ? "dashed" : "solid"} ${color || orange[800]}`,
      }}
    >
      {children}
    </Box>
  );
};

export function NavSkeleton() {
  return (
    <Box sx={{ pb: 1 }}>
      {Array.from({ length: 5 }).map((_, i) => (
        <MenuItem key={i}>
          <ListItem disablePadding>
            <ListItemButton sx={{ pb: 0, paddingInline: 0 }}>
              <ListItemText
                primary={
                  <Skeleton
                    variant="rounded"
                    height={25}
                    sx={{ backgroundColor: "#ddd", borderRadius: "14px" }}
                    key={i}
                  />
                }
              />
            </ListItemButton>
          </ListItem>
        </MenuItem>
      ))}
    </Box>
  );
}

export function NavItems({ cb }: { cb?: () => void }) {
  return (
    <Fragment>
      <NavItem icon={<GiDogHouse />} label="Main" link="/" />
      <Divider sx={{ borderColor: "#555" }} />
      <NavItem
          icon={<GiAbstract027 />}
          label="Одиночные модели"
          link="smodels"
          color={lightGreen[800]}
          wrap
        />
      <NavItem
          icon={<GiAbacus />}
          label="Одиночные записи"
          link="sdocs"
          wrap
          dashed
        />
      <NavItem icon={<GiFountainPen />} label="Новости" link="news" />
      <NavItem icon={<AiOutlineFileSearch />} label="Поиск" link="search" />      
      <NavItem icon={<GiVideoCamera />} label="Кинопоиск" link="movies" />            
      <NavItem icon={<GiKnightBanner />} label="Баннеры" link="banners" />
      <NavItem icon={<GiMonaLisa />} label="Некролог" link="funerals" />      
      <NavItem icon={<GiAbstract014 />} label="Афиша" link="playbills" />
      <NavItem icon={<GiAbstract089 />} label="Заявки" link="orders" />
      <NavItem icon={<GiRailway />} label="Menu" link="menu" />
      <NavItem icon={<GiSwordSpade />} label="Settings" link="settings" />     
      <NavItem icon={<GiNinjaHead />} label="Пользователи" link="users" />
    </Fragment>
  );
}
