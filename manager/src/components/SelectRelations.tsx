import {
  Button,
  List,
  ListItemButton,
  ListItemText,
  Snackbar,
  Stack,
} from "@mui/material";
import React, { Fragment, useEffect, useState, MouseEvent } from "react";
import NoData from "./NoData";
import axios from "axios";
import { Item } from "../types";

const VITE_API = import.meta.env.VITE_API;

interface IProps {
  existed: number[];
  model: string;
  callBack: (arg: number) => void;
}

export default function SelectRelations({ model, existed, callBack }: IProps) {
  const [message, setMessage] = useState<string>("");
  const [data, setData] = useState<Item[] | []>([]);
  const [selectedIndex, setSelectedIndex] = useState<number>(-1);
  useEffect(() => {
    //
    axios
      .get(`${VITE_API}/api/${model}s`, {
        headers: {
          "Content-Type": "application/json",
          // name: name,
        },
        // params: { name: name },
      })
      .then((res) => {
        // console.log(data)
        // console.log(existed)
        setData(res.data.filter((x: Item) => !existed.includes(x.id)));
      })
      .catch((err) => {
        const status: number = err.response.status;
        const errMsg: string = err.response.data.message;
        if (status === 404) {
          setMessage(errMsg);
        }
      });
  }, [model]);

  if (data.length) {
    return (
      <Fragment>
        <Stack spacing={3}>
          <List>
            {data.map((x: Item, i: number) => (
              <ListItemButton
                key={i}
                selected={selectedIndex === x.id}
                onClick={(e: MouseEvent) => {
                  if (selectedIndex === x.id) setSelectedIndex(-1);
                  else setSelectedIndex(x.id);
                }}
              >
                <ListItemText primary={x.title} />
              </ListItemButton>
            ))}
          </List>
          <Button
            variant="outlined"
            sx={{ width: "max-content" }}
            onClick={() => callBack(selectedIndex)}
          >
            Select
          </Button>
        </Stack>

        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else return <NoData />;
}
