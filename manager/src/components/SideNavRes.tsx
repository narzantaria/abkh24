import * as React from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import { DIRTY_CLR } from "../misc/tv";
import { Box, styled } from "@mui/material";
import { Link } from "react-router-dom";
import { GiAbstract080, GiDogHouse } from "react-icons/gi";
import { NavItems } from "./NavUI";

const StyledAccordion = styled(Accordion)`
  margin: 0 !important;
  background-color: ${DIRTY_CLR};
  border-radius: 0 !important;
`;

const StyledAccordionSummary = styled(AccordionSummary)`
  height: 64px;
  padding-inline: 30px;
  border-bottom: 1px solid #555;
`;

const StyledAccordionDetails = styled(AccordionDetails)`
  padding-inline: 30px;
`;

const LogoWrapper = styled(Box)`
  display: flex;
  column-gap: 15px;
  align-items: baseline;
`;

const LogoCaption = styled(Typography)`
  color: #fff;
  font-style: italic;
  font-weight: 600;
  line-height: 0;
  transform: translateY(-5px);
  text-transform: uppercase;
`;

const StyledMenuIcon = styled(GiAbstract080)`
  color: #fff;
  font-size: 25px;
`;

function NavLogo() {
  return (
    <Link
      to="/"
      onClick={(e) => {
        e.stopPropagation();
      }}
    >
      <LogoWrapper>
        <GiDogHouse />
        <LogoCaption variant="h5">Main</LogoCaption>
      </LogoWrapper>
    </Link>
  );
}

export default function SideNavRes() {
  const [expanded, setExpanded] = React.useState(false);
  const handleExpansion = () => {
    setExpanded((prevExpanded) => !prevExpanded);
  };

  return (
    <StyledAccordion expanded={expanded} onChange={handleExpansion}>
      <StyledAccordionSummary
        expandIcon={<StyledMenuIcon />}
        aria-controls="panel1-content"
        id="panel1-header"
      >
        <NavLogo />
      </StyledAccordionSummary>
      <StyledAccordionDetails>
        <NavItems
          cb={() => {
            //
            setExpanded(() => false);
          }}
        />
      </StyledAccordionDetails>
    </StyledAccordion>
  );
}
