import { Box, Card, CardMedia, styled } from "@mui/material";
import React from "react";
import { RxVideo } from "react-icons/rx";
import { GREEN_CLR, PAPER_CLR } from "../misc/tv";

const VITE_API = import.meta.env.VITE_API;

interface IProps {
  dir: string;
  video: string;
}

const VideoIcon = styled(RxVideo)(({
  fontSize,
  color,
}: {
  fontSize: number;
  color?: string | null;
}) => {
  return `
    position: absolute;
    transform: translate(-50%, -50%);
    top: 50%;
    left: 50%;
    color: ${color ? color : PAPER_CLR};
    z-index: 1;
    font-size: ${fontSize || 65}px;
    `;
});

export default function LocalVideoPrev({ dir, video }: IProps) {
  return (
    <Card
      sx={{
        position: "absolute",
        width: "100%",
        height: "100%",
        backgroundColor: "#888",
        borderRadius: 0,
      }}
    >
      <CardMedia
        component="video"
        image={`${VITE_API}/dist/${dir}/${video}`}
        sx={{
          position: "absolute",
          height: "100%",
          width: "100%",
        }}
        disablePictureInPicture={true}
      />
      <Box
        sx={{
          position: "absolute",
          zIndex: 1,
          width: "100%",
          height: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <VideoIcon fontSize={35} color={GREEN_CLR} />
      </Box>
    </Card>
  );
}
