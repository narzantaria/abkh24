import {
  Button,
  Checkbox,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Snackbar,
} from "@mui/material";
import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";

const VITE_API = import.meta.env.VITE_API;

interface IProps {
  id: string | undefined;
  model: string | undefined;
  callBack: (args: string[]) => void;
  excluded: string[];
}

interface Item {
  id: string;
  [key: string]: any;
}

function filterByIds(data: Item[], ids: string[]): Item[] {
  let dataProxy = data;
  for (let x = 0; x < ids.length; x++) {
    dataProxy = dataProxy.filter((obj: Item) => obj.id != ids[x]);
  }
  return dataProxy;
}

export default function ItemsList({ id, model, callBack, excluded }: IProps) {
  const [message, setMessage] = useState<string>("");
  const [checked, setChecked] = useState<string[]>([]);
  function handleToggle(arg: string) {
    const currentIndex = checked.indexOf(arg);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(arg);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  }
  const [data, setData] = useState<any[] | null>(null);
  useEffect(() => {
    axios
      .get(`${VITE_API}/api/universal/common/${model}`, {
        headers: {
          "Content-Type": "application/json",
          // name: name,
        },
        params: { name: model },
      })
      .then((res) => {
        const filteredData: Item[] = filterByIds(res.data, excluded);
        setData(filteredData);
      })
      .catch((err) => {
        const status: number = err.response.status;
        const errMsg: string = err.response.data.message;
        if (status === 404) {
          setMessage(errMsg);
        }
      });
  }, [excluded]);

  return (
    <Fragment>
      <List sx={{ width: "100%", bgcolor: "background.paper" }}>
        {data?.map((x: Item, i: number) => {
          const labelId = `checkbox-list-label-${x.id}`;
          return (
            <ListItem key={x.id} disablePadding>
              <ListItemButton
                role={undefined}
                onClick={(_) => {
                  handleToggle(x.id);
                }}
                dense
              >
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    checked={Boolean(
                      checked.filter((item) => item == x.id).length
                    )}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                </ListItemIcon>
                <ListItemText id={labelId} primary={x.title} />
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>
      <Button
        variant="outlined"
        sx={{ width: "max-content", ml: 2, mt: 1 }}
        onClick={(_) => {
          callBack(checked);
          setChecked([]);
        }}
      >
        Submit
      </Button>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
