import React, { ReactNode } from "react";
import {
  Box,
  Button,
  Divider,
  ListItemIcon,
  ListItemText,
  MenuItem,
  MenuList,
  Typography,
} from "@mui/material";
import { Link } from "react-router-dom";
import { cyan, orange } from "@mui/material/colors";
import {
  GiAbstract089,
  GiDogHouse,
  GiFountainPen,
  GiMonaLisa,
  GiNinjaHead,
  GiVideoCamera,
} from "react-icons/gi";
import ConditionalWrapper from "../misc/ConditionalWrapper";
import { AiOutlineFileSearch } from "react-icons/ai";
import { NavItem, NavItems, NavMainBox } from "./NavUI";

export default function SideNav() {
  return (
    // <Box
    //   sx={{
    //     backgroundColor: "#3d3c42",
    //     overflowY: "scroll",
    //     padding: "10px 0",
    //     scrollbarWidth: "none",
    //   }}
    // >
    //   <MenuList>
    //     <NavItem icon={<GiDogHouse />} label="Main" link="/" />
    //     <Divider sx={{ borderColor: "#555" }} />
    //     <NavItem icon={<GiNinjaHead />} label="Пользователи" link="users" />
    //     <NavItem icon={<GiVideoCamera />} label="Кинопоиск" link="movies" />
    //     <NavItem icon={<GiMonaLisa />} label="Некролог" link="funerals" />
    //     <NavItem icon={<GiFountainPen />} label="Новости" link="news" />
    //     <NavItem icon={<GiAbstract089 />} label="Заявки" link="orders" />
    //     <NavItem icon={<AiOutlineFileSearch />} label="Поиск" link="search" />
    //   </MenuList>
    // </Box>
    <NavMainBox>
      <MenuList>
        {/* <NavItem icon={<GiDogHouse />} label="Main" link="/" />
        <Divider sx={{ borderColor: "#555" }} /> */}
        <NavItems />
      </MenuList>
    </NavMainBox>
  );
}
