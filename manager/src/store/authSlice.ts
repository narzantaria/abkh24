import { createSlice } from "@reduxjs/toolkit";

interface IState {
  avatar: string | null;
  token: string | null;
  name: string | null;
  id: string | null;
}

const initialState: IState = {
  avatar: null,
  token: null,
  name: null,
  id: null,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setAuth: (state, action) => {
      state.avatar = action.payload.avatar;
      state.token = action.payload.token;
      state.name = action.payload.name;
      state.id = action.payload.id;
      localStorage.setItem("avatar", action.payload.avatar);
      localStorage.setItem("token", action.payload.token);
      localStorage.setItem("name", action.payload.name);
      localStorage.setItem("id", action.payload.id);
    },
    logout: (state) => {
      state.avatar = null;
      state.token = null;
      state.name = null;
      state.id = null;
      localStorage.setItem("avatar", "");
      localStorage.setItem("token", "");
      localStorage.setItem("name", "");
      localStorage.setItem("id", "");
    },
  },
});

// Action creators are generated for each case reducer function
export const { setAuth, logout } = authSlice.actions;

export default authSlice.reducer;
