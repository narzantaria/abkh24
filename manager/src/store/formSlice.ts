import { createSlice } from "@reduxjs/toolkit";

export interface IFormState {
  [field: string]: any;
  clearTemp: boolean;
  root: string;
}

const initialState: IFormState = {
  clearTemp: true,
  root: "",
};

export const formSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    setField: (state, action) => {
      state[action.payload.field] = action.payload.value;
    },
    // Не очень мне нравится эта функция, но ничего другого придумать не удалось... с этим реактом...
    removeField: (state, action) => {
      const stateProxy: IFormState = {
        clearTemp: state.clearTemp,
        root: state.root,
      };
      const keys = Object.keys(state);
      for (let y = 0; y < keys.length; y++) {
        if (
          keys[y] != action.payload &&
          keys[y] != "clearTemp" &&
          keys[y] != "root"
        )
          stateProxy[keys[y]] = state[keys[y]];
      }
      return stateProxy;
    },
    // setForm: (state, action) => {
    //   return { ...state, ...action.payload };
    // },
    setForm: (state, action) => {
      const fields = { ...state, ...action.payload };
      if (action.payload.dir) fields.root = action.payload.dir;
      return fields;
    },
    reset: () => {
      const clientId = localStorage.getItem("id");
      return { clearTemp: true, root: `userid${clientId}` || "" };
    },
  },
});

// Action creators are generated for each case reducer function
export const { setField, removeField, setForm, reset } = formSlice.actions;

export default formSlice.reducer;
