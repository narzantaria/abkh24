import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setAuth } from "./store/authSlice";
import axios from "axios";
import { RootState } from "./store";
import Private from "./Private";
import Login from "./Login";
import { setField } from "./store/formSlice";

const VITE_API = import.meta.env.VITE_API;

function App() {
  const { avatar, token } = useSelector((state: RootState) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    const clientToken = localStorage.getItem("token");
    const clientAvatar = localStorage.getItem("avatar");
    const clientName = localStorage.getItem("name");
    const clientId = localStorage.getItem("id");
    if (clientToken && clientToken != "") {
      axios
        .post(
          `${VITE_API}/api/auth/check`,
          {},
          {
            headers: {
              "Content-Type": "application/json",
              "auth-token": clientToken,
            },
          }
        )
        .then(async () => {
          await dispatch(
            setAuth({
              avatar: clientAvatar,
              token: clientToken,
              name: clientName,
              id: clientId,
            })
          );
          await dispatch(
            setField({ field: "root", value: `userid${clientId}` })
          );
        })
        .catch((err) => {
          console.log(err);
          dispatch(setAuth(null));
        });
    }
  }, []);

  return <Fragment>{avatar && token ? <Private /> : <Login />}</Fragment>;
}

export default App;
