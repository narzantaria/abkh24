export interface IComment {
  id: number;
  dir?: string;
  title: string;
  text: string;
  img?: string | null;
  createdAt: string;
}

// export interface IPost {
//   id: number;
//   dir?: string;
//   title: string;
//   text: string;
//   photos?: string[] | null;
//   video?: string | null;
//   youtube?: string | null;
//   sourceurl?: string | null;
//   looks?: number;
//   tags?: string[] | null;
//   createdAt: string;
//   comments?: IComment[] | null;
// }

// Общий интерфейс новости
export interface IPost {
  id: string;
  title: string;
  text: string;
  description: string;
  author?: [{ id: string; name: string }] | null;
  authorid?: string;
  authorname?: string;
  createdAt: Date;
  dir: string;
  photos?: string[] | null;
  video?: string | null;
  category?: string;
  textclr?: string | null;
  tagClr?: { background: string; color: string; bordercolor: string } | null;
  comment: IComment[];
  key?: string;
  sourceurl?: string;
  tags: string[];
  youtube?: string | null;
  rutube?: string | null;
  looks?: string | null;
}

/**
 * Поле схемы 'universal'.
 * Используется также в построении формы
 **/
export interface IField {
  name: string;
  type: string;
  required?: boolean;
  schema?:
    | "one-to-one"
    | "one-to-many"
    | "many-to-many"
    | "many-to-many-bi"
    | undefined;
  level?: "host" | "reecipient" | undefined;
  ref?: string | undefined;
  limit?: number;
  label?: string;
  extra?: any[];
  data?: any[];
  unique?: boolean;
  value?: any;
}