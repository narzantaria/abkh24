import { useRef, useEffect } from "react";

const useIsMount = () => {
  const isMountRef = useRef(true);
  useEffect(() => {
    isMountRef.current = false;
  }, []);
  return isMountRef.current;
};

// random symbols string
const makeId = (length: number) => {
  let result = "";
  const characters = "abcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const capitalized = (word: string) => {
  return word.charAt(0).toUpperCase() + word.slice(1);
};

/**
 * Валидация некоторых полей формы. Функция специфична к данному проекту,
 * например обрабатывает formProps модели...
 **/
function validator(
  formProps: any[],
  data: any,
  checkRequired?: boolean
): string {
  for (let x = 0; x < formProps.length; x++) {
    if (
      formProps[x].required == true &&
      !data[formProps[x].name] &&
      checkRequired
    ) {
      return "Please fill all required inputs!";
    }
    if (formProps[x].type == "phone") {
      const phone = hparse(data[formProps[x].name]).value;
      const isPhone = phone.match(/^[0-9]+$/); // null - неправильный номер
      if (phone.length < 10 || phone.length > 11 || !isPhone) {
        return "Please type a valid phone number!";
      }
    }
    if (formProps[x].type == "email") {
      const isEmail = String(data[formProps[x].name])
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
      if (!isEmail) {
        return "Please type a valid email address!";
      }
    }
  }
  return "";
}

const isEmail = (arg: string): boolean => {
  const isMatching = String(arg)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
  if (!isMatching) return false;
  return true;
};

const isPhone = (arg: string): boolean => {
  const isMatching = arg.match(
    /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/
  ); // null - неправильный номер
  if (!isMatching) return false;
  if (arg.length < 12 || arg.length > 13 || arg.slice(0, 1) !== "+")
    return false;
  return true;
};

function hstr(arg: object | any[]): string {
  return JSON.stringify(arg).replaceAll('"', "'");
}

function hparse(arg: string) {
  return JSON.parse(arg.replaceAll("'", '"'));
}

interface Field {
  name: string;
  type: string;
  required?: boolean;
  schema?:
    | "one-to-one"
    | "one-to-many"
    | "many-to-many"
    | "many-to-many-bi"
    | undefined;
  level?: "host" | "reecipient" | undefined;
  ref?: string | undefined;
  limit?: number;
}

function removeRelations(body: { [field: string]: any }, model: Field[]) {
  const filteredBody: { [field: string]: any } = {};
  const keys = Object.keys(body);
  const relations = model
    .filter((x) => x.type === "relation")
    .map((x) => x.name);
  const filteredKeys = keys.filter((x) => !relations.includes(x));
  for (let x = 0; x < filteredKeys.length; x++) {
    filteredBody[filteredKeys[x]] = body[filteredKeys[x]];
  }
  return filteredBody;
}

function modifyArr(arr: any[], elem: any): any[] {
  const index = arr.indexOf(elem);
  if (index !== -1) {
    // Если элемент найден в массиве, удаляем его
    return arr.filter((x) => x !== elem);
  } else {
    // Если элемент не найден в массиве, добавляем его
    return arr.concat([elem]);
  }
}

export {
  capitalized,
  isPhone,
  isEmail,
  makeId,
  useIsMount,
  validator,
  hstr,
  hparse,
  modifyArr,
  removeRelations,
};
