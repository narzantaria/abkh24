// В зависимости от условия оборачивает компонент. Рендер-пропс.
import React, { Fragment, ReactNode } from "react";

interface IProps {
  condition: boolean;
  children: ReactNode;
  render: (arg: ReactNode) => JSX.Element;
}

export default function ConditionalWrapper({
  condition,
  render,
  children,
}: IProps) {
  return <Fragment>{condition ? render(children) : children}</Fragment>;
}
