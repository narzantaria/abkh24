# Abkh24

Abkhazia24 - pet проект. Задумывался как будущий главный информационный ресурс Абхазии.

Ссылка в сети: https://abkhazia24.com

##### _________________________

При проблемах с sharp:

> yarn remove sharp && yarn add sharp --ignore-engines --include=optional

> < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-50};echo;

```
sudo apt install postgresql
sudo -u postgres psql template1
ALTER USER postgres with encrypted password 'It2YiPmPGHjewiH8mTL7F_E0W15BIVTpC8VG2zJN5wkju7k_51';
CREATE DATABASE abkhnew OWNER postgres ENCODING UTF8 TABLESPACE pg_default;
```

> sudo service postgresql restart

Дамп базы данных:
> pg_dump -U postgres -h localhost abkhnew >> abkhnew.sql
Затем он попросит пароль соединения к БД!

Восстановление ДБ из файла (в данном сл локальный хост):
> psql -U postgres -d abkhnew -h localhost -p 5432 -f abkhnew.sql
Попросит пароль.

*Redis*
> sudo service redis-server start

Операции с БД в консоли:

```
sudo -u postgres psql template1    -- попросит пароль
\l                                                -- вывести список БД 
\c abkhnew                                  -- выбрать abkhnew
SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_type = 'BASE TABLE';
```

Далее можно делать запросы к таблицам. Если перед этим делали restore, то надо выйти и заити, чтобы увидеть изменения!

##### Mobile PWA

На базе клиента, но:

* SPA React Vite TS;
* только маленький экран;
* переделать в PWA;
* урезано;

========================

> yarn create vite pwamob --template react-ts

> yarn add @dnd-kit/core @dnd-kit/sortable @dnd-kit/utilities @emotion/react @emotion/styled @mui/material @mui/x-date-pickers @reduxjs/toolkit @wangeditor/editor @wangeditor/editor-for-react axios d3 dayjs firebase framer-motion lodash npm-run-all react-icons react-material-ui-carousel react-player react-redux react-share react-router-dom serialize-javascript socket.io-client uuid

> npm init @eslint/config

> yarn add -D --exact prettier

> yarn add -D @types/d3 @types/lodash @types/uuid @typescript-eslint/parser eslint-config-prettier eslint-plugin-react eslint-plugin-react-hooks eslint-plugin-react-refresh eslint-plugin-unused-imports prettier redux-devtools-extension sass

##### Window

Нужно перейти в папку C:\Program Files\PostgreSQL\16\bin и открыть ее в cmder.

```
psql.exe -U postgres template1
CREATE DATABASE abkhnew OWNER postgres ENCODING UTF8 TABLESPACE pg_default;
```

Дамп БД:
> pg_dump.exe -U postgres -d abkhnew -f D:\projects\abkh24\misc\abkhnew.sql

Восстановление БД:
> psql.exe -U postgres -d abkhnew -f D:\projects\abkh24\misc\abkhnew.sql

##### Копирование по SSH

> scp -r root@11.111.111.11:/root/qwerty ./
Попросит пароль.