/**
 * Создание первого пользователя, если файл "users.json" пуст,
 * А если не пуст, то роут "setup" не открывается.
 * Вторая опция защиты - переменная "SETUP".
 **/
import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import CryptoJS from "crypto-js";
import gravatar from "gravatar";
import { v4 as uuidv4 } from "uuid";

const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);

const DATA_FOLDER = `${process.cwd()}/data`;
const SECRET_KEY = String(process.env.SECRET_KEY);
const SETUP: boolean = process.env.SETUP === "true";

interface User {
  id: string;
  login: string;
  password: string;
  name: string;
  email: string;
  avatar?: string;
}

export const setupUser = async (req: Request, res: Response) => {
  const { login, password, name, email } = req.body;
  try {
    const rawData = await read(`${DATA_FOLDER}/users.json`, "utf8");
    let data = JSON.parse(rawData);
    if (data.length) {
      res.render("thresh");
    }
    const avatar = gravatar.url(email, {
      s: "200",
      r: "pg",
      d: "mm",
    });
    const id = uuidv4();
    const newUser: User = {
      id,
      login,
      password: CryptoJS.AES.encrypt(password, SECRET_KEY).toString(),
      name,
      email,
      avatar,
    };
    data = [newUser];
    await write(`${DATA_FOLDER}/users.json`, JSON.stringify(data));
    res.render("finish");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const searchUsers = async (req: Request, res: Response) => {
  try {
    const rawData = await read(`${DATA_FOLDER}/users.json`, "utf8");
    const parsedData = JSON.parse(rawData);
    if (!SETUP) {
      res.render("thresh");
    } else if (!parsedData.length) {
      res.render("setup");
    } else {
      res.render("thresh");
    }
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};
