import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Post } from "../models/Post";
import { Values } from "../types";
import {
  IDocDir,
  docDir,
  docFiles,
  rmDocDir,
} from "file-handlers";

export const createPost = async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { root, ...rest } = values;
  let dirs: IDocDir | null = null,
    data: any | null;
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    // Проверка медиа:
    if ((req.body.photos || req.body.video) && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    // Запрос:
    data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Post)
      .values(rest)
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ (ИЗМЕНИТЬ ТОЛЬКО ОТВЕТ!!!!!!!!!!!!!)
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

// export const getPosts = async (req: Request, res: Response) => {
//   try {
//     const data = await AppDataSource.getRepository(Post)
//       .createQueryBuilder("post")
//       .orderBy("post.createdAt", "DESC")
//       .getMany();
//     res.json(data);
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({ message: "Server error" });
//   }
// };

export const getPosts = async (req: Request, res: Response) => {
  const params: { [key: string]: any } = req.query;
  const { offset, limit } = params;
  try {
    const SELECT_POSTS = `SELECT * FROM post ORDER BY "createdAt" DESC${
      offset ? "\n" + `OFFSET ${offset}` : ""
    }${limit ? "\n" + `LIMIT ${limit}` : ""}`;
    const data = await AppDataSource.manager.query(SELECT_POSTS);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getPost = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Post)
      .createQueryBuilder("post")
      .leftJoinAndSelect("post.comments", "comments")
      .leftJoinAndSelect("post.likes", "likes")
      .where("post.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updatePost = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  // console.log(values);
  const { root, comments, likes, ...rest } = values; // ?????????????????? Че делать с этим???
  let dirs: IDocDir | null = null,
    data: any | null,
    updated: any | null;
  // Proverka
  try {
    data = await AppDataSource.getRepository(Post)
      .createQueryBuilder("post")
      .where("post.id = :id", { id })
      .getOne();
    if (!data) return res.status(404).send("Document not found");
  } catch (error) {
    return res.status(500).send("Server error");
  }
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    if ((req.body.photos || req.body.video) && !data.dir && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    updated = await AppDataSource.createQueryBuilder()
      .update(Post)
      .set(rest)
      .where("id = :id", { id })
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ (ИЗМЕНИТЬ ТОЛЬКО ОТВЕТ!!!!!!!!!!!!!)
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json({ ...rest, id: updated.affected });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

export const deletePost = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Post);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      if (dataToRemove.dir) await rmDocDir(dataToRemove.dir);
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
