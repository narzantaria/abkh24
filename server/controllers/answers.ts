import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Answer } from "../models/Answer";

export const createAnswer = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Answer)
      .values(req.body)
      .execute();
    res.json({ ...req.body, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getAnswers = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Answer)
      .createQueryBuilder("answer")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getAnswer = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Answer)
      .createQueryBuilder("answer")
      .where("answer.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateAnswer = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Answer)
      .createQueryBuilder("answer")
      .where("answer.id = :id", { id })
      .getOne();
    if (data) {
      const updated = await AppDataSource.createQueryBuilder()
        .update(Answer)
        .set(req.body)
        .where("id = :id", { id })
        .execute();
      res.json({ ...req.body, id: updated.affected });
    } else {
      return res.status(404).send("Document not found");
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteAnswer = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Answer);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
