import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { itemTables } from "./pub";

const PUB_SECRET_KEY = String(process.env.PUB_SECRET_KEY);

export const searchPosts = async (req: Request, res: Response) => {
  const { word } = req.query;
  try {
    const SEARCH_QUERY = `SELECT id, title FROM post WHERE title ILIKE '%${word}%' or text ILIKE '%${word}%' ORDER BY "createdAt" DESC LIMIT 50`;
    const data = await AppDataSource.manager.query(SEARCH_QUERY);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const searchGoods = async (req: Request, res: Response) => {
  const { word } = req.query;
  try {
    const SEARCH_QUERY = `${
      itemTables
        .map((x) => {
          return `SELECT id, title, sector, '${x}' AS source FROM ${x} WHERE title ILIKE '%${word}%' OR text ILIKE '%${word}%'`;
        })
        .join("\n" + "UNION ALL" + "\n") +
      "\n" +
      "LIMIT 50"
    }`;
    console.log(SEARCH_QUERY);
    const data = await AppDataSource.manager.query(SEARCH_QUERY);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
