/**
 * Операции CRUD с одиночными документами, кот-е хранятся в папке singledocs.
 * Исходные модели - в singlemodels. На клиенте этим занимаются комп-ты SDocs/SDoc.
 * Одно из самых и запутанных сложных мест, наряду с file-handlers.
 * Думаю, что можно упростить, убрав docImages, rmDocDir, а в качестве корня исп-ть "common"
 **/
import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import { v4 as uuidv4 } from "uuid";
import { docImages, rmDocDir } from "file-handlers";
import { capitalizeFirstLetter } from "../middleware/tools";
import { Field, Values } from "../types";

const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);
const readdir = util.promisify(fs.readdir);
const unlink = util.promisify(fs.unlink);

const DOCS_FOLDER = `${process.cwd()}/singledocs`;
const MODELS_FOLDER = `${process.cwd()}/singlemodels`;

export const createDoc = async (req: Request, res: Response) => {
  const { name, data } = req.body;
  const values: Values = data;
  const { root, ...rest } = values;
  const keys: string[] = Object.keys(data);
  try {
    const rawModel = await read(`${MODELS_FOLDER}/${name}.json`, "utf8");
    const model = JSON.parse(rawModel);
    // ТУТ ЧТО_ТО НЕ ТАК... кроме того, надо включить видео!!!
    for (let x = 0; x < keys.length; x++) {
      const field: Field = model[x];
      if (
        (field?.type === "img" ||
          field?.type === "gallery" ||
          field?.type === "video") &&
        root?.length
      ) {
        rest.dir = (await docImages("dist", root)) || "";
        break;
      }
    }
    // НОВОЕ!!! Возможность задать свой id (с проверкой уникальности)
    if (!rest.id) {
      rest.id = uuidv4();
    } else {
      //
      const files = await readdir(DOCS_FOLDER, { withFileTypes: true });
      const filtered = files
        .filter((dirent) => dirent.isFile())
        .map((dirent) => dirent.name);
      for (let x = 0; x < filtered.length; x++) {
        const rawDoc = await read(`${DOCS_FOLDER}/${filtered[x]}`, "utf8");
        const parsedDoc = JSON.parse(rawDoc);
        if (parsedDoc.id === rest.id) {
          return res
            .status(405)
            .json({ message: "The id is already in use! Try another one" });
        }
      }
    }
    await write(`${DOCS_FOLDER}/${name}.json`, JSON.stringify(rest));
    res.json(rest);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getDocs = async (req: Request, res: Response) => {
  try {
    const files = await readdir(DOCS_FOLDER);
    const data = files.map((x) => {
      const _x = x.replace(/\.[^/.]+$/, "");
      return capitalizeFirstLetter(_x);
    });
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const getDoc = async (req: Request, res: Response) => {
  const { name } = req.params;
  try {
    const rawData = await read(`${DOCS_FOLDER}/${name}.json`, "utf8");
    const data = JSON.parse(rawData);
    res.json(data);
  } catch (error) {
    res.status(404).json({ message: "File not found" });
  }
};

export const updateDoc = async (req: Request, res: Response) => {
  const { name } = req.params;
  const values: Values = req.body;
  const { root, ...rest } = values;
  const keys: string[] = Object.keys(rest);
  const rawItem = await read(`${DOCS_FOLDER}/${name}.json`, "utf8");
  const item = JSON.parse(rawItem);
  try {
    const rawModel = await read(`${MODELS_FOLDER}/${name}.json`, "utf8");
    const model = JSON.parse(rawModel);
    for (let x = 0; x < keys.length; x++) {
      const field: Field = model[x];
      if (
        (field?.type === "img" ||
          field?.type === "gallery" ||
          field?.type === "video") &&
        !item.dir &&
        root?.length
      ) {
        item.dir = (await docImages("dist", root)) || "";
        break;
      }
      item[keys[x]] = rest[keys[x]];
    }
    await write(`${DOCS_FOLDER}/${name}.json`, JSON.stringify(item));
    res.json(item);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteDoc = async (req: Request, res: Response) => {
  const { name } = req.params;
  try {
    const rawData = await read(`${DOCS_FOLDER}/${name}.json`, "utf8");
    const data = JSON.parse(rawData);
    if (data.dir) await rmDocDir(data.dir);
    await unlink(`${DOCS_FOLDER}/${name}.json`);
    res.json("Deleted");
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};
