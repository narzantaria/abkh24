/**
 * Работает с файлом "settings.json" в папке "data", который содержит
 * общую инфу для потенциального сайта/приложения.
 **/
import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";

const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);

const DATA_FOLDER = `${process.cwd()}/data`;
const SECRET_KEY = String(process.env.SECRET_KEY);

interface User {
  id: string;
  login: string;
  password: string;
  name: string;
  email: string;
  avatar?: string;
}

export const openSettings = async (req: Request, res: Response) => {
  try {
    const rawData = await read(`${DATA_FOLDER}/settings.json`, "utf8");
    const data = JSON.parse(rawData);
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const updateSettings = async (req: Request, res: Response) => {
  const { data } = req.body;
  try {
    await write(`${DATA_FOLDER}/settings.json`, data);
    res.json("Done");
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};
