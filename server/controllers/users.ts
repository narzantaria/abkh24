/**
 * Данные системных пользователей для админки. Хранятся не в БД, а в файле "data/users.json".
 * Если будут создаваться другие категории пользователей, то они будут обрабатываться
 * и храниться иным способом, на другом роуте/контроллере.
 **/
import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import CryptoJS from "crypto-js";
import gravatar from "gravatar";
import { v4 as uuidv4 } from "uuid";

const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);

const DATA_FOLDER = `${process.cwd()}/data`;
const SECRET_KEY = String(process.env.SECRET_KEY);

interface User {
  id: string;
  login: string;
  password: string;
  name: string;
  email: string;
  avatar?: string;
}

export const createUser = async (req: Request, res: Response) => {
  const { login, password, name, email } = req.body;
  try {
    const rawData = await read(`${DATA_FOLDER}/users.json`, "utf8");
    const data = JSON.parse(rawData);
    const filteredByEmail = data.filter(
      (x: User) => x.login.toLowerCase() === login.toLowerCase()
    );
    if (filteredByEmail.length) {
      return res.status(409).json({ message: "Email is already in use" });
    }
    const filteredByName = data.filter(
      (x: User) => x.name.toLowerCase() === name.toLowerCase()
    );
    if (filteredByName.length) {
      return res.status(409).json({ message: "Name is already in use" });
    }
    const avatar = gravatar.url(email, {
      s: "200",
      r: "pg",
      d: "mm",
    });
    const id = uuidv4();
    const newUser: User = {
      id,
      login,
      password: CryptoJS.AES.encrypt(password, SECRET_KEY).toString(),
      name,
      email,
      avatar,
    };
    data.push(newUser);
    await write(`${DATA_FOLDER}/users.json`, JSON.stringify(data));
    res.json(newUser);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getUsers = async (req: Request, res: Response) => {
  try {
    const rawData = await read(`${DATA_FOLDER}/users.json`, "utf8");
    const parsedData = JSON.parse(rawData);
    const data = parsedData.map(({ password, ...rest }: User) => rest);
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const getUser = async (req: Request, res: Response) => {
  console.log(req.params);
  try {
    const rawData = await read(`${DATA_FOLDER}/users.json`, "utf8");
    const parsedData = JSON.parse(rawData);
    const filteredData = parsedData.filter((x: User) => x.id === req.params.id);
    if (!filteredData.length) {
      return res.status(404).json({ message: "User not found" });
    }
    const { password, ...rest } = filteredData[0];
    res.json(rest);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const updateUser = async (req: Request, res: Response) => {
  try {
    const rawData = await read(`${DATA_FOLDER}/users.json`, "utf8");
    const parsedData = JSON.parse(rawData);
    const checkExists = parsedData.filter((x: User) => x.id === req.params.id);
    if (!checkExists.length) {
      return res.status(404).json({ message: "User not found" });
    }
    const user: User = checkExists[0];
    const { login, password, name, email } = req.body;

    const filteredData = parsedData.filter((x: User) => x.id !== req.params.id);

    const filteredByEmail = filteredData.filter(
      (x: User) => x.login.toLowerCase() === login.toLowerCase()
    );
    if (filteredByEmail.length) {
      return res.status(409).json({ message: "Email is already in use" });
    }
    const filteredByName = filteredData.filter(
      (x: User) => x.name.toLowerCase() === name.toLowerCase()
    );
    if (filteredByName.length) {
      return res.status(409).json({ message: "Name is already in use" });
    }

    if (login) user.login = login;
    if (password) {
      user.password = CryptoJS.AES.encrypt(password, SECRET_KEY).toString();
    }
    if (name) user.name = name;
    if (email) user.email = email;

    filteredData.push(user);
    await write(`${DATA_FOLDER}/users.json`, JSON.stringify(filteredData));

    const { password: _, ...rest } = user;

    res.json(rest);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteUser = async (req: Request, res: Response) => {
  try {
    const rawData = await read(`${DATA_FOLDER}/users.json`, "utf8");
    const parsedData = JSON.parse(rawData);
    const filteredData = parsedData.filter((x: User) => x.id !== req.params.id);
    if (filteredData.length === parsedData.length) {
      return res.status(404).json({ message: "User not found" });
    }
    await write(`${DATA_FOLDER}/users.json`, JSON.stringify(filteredData));
    res.json("Removed");
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};
