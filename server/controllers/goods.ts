import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import path from "path";
import { AppDataSource } from "../db";
import { Field } from "../types";
import { hparse } from "../middleware/tools";

const XMENU_DIR = `${process.cwd()}/xmenu`;
const UNIVERSAL_DIR = `${process.cwd()}/universal`;

const read = util.promisify(fs.readFile);
const readdir = util.promisify(fs.readdir);
const unlink = util.promisify(fs.unlink);
const mkdir = util.promisify(fs.mkdir);
// const access = util.promisify(fs.access);
// const stat = util.promisify(fs.stat);

type File = {
  name: string;
  data: any;
};

type Folder = {
  name: string;
  title: string;
  files: File[];
};

const titles: { [key: string]: string } = {
  agro: "Сельское хозяйство",
  animal: "Животные",
  auto: "Транспорт",
  autopart: "Автозапчасти",
  beauty: "Красота и здоровье",
  building: "Стоительство и ремонт",
  children: "Товары для детей",
  electro: "Электротовары",
  estate: "Недвижимость",
  fashion: "Одежда и обувь",
  hobby: "Хобби и отдых",
  home: "Товары для дома",
  product: "Продукты",
  service: "Услуги",
  tourism: "Туризм",
  work: "Работа",
};

async function readFolder(folderPath: string): Promise<Folder[]> {
  const folders: Folder[] = [];

  const subFolders = await fs.promises.readdir(folderPath);

  for (const subFolder of subFolders) {
    const subFolderPath = path.join(folderPath, subFolder);
    const subFolderStat = await fs.promises.stat(subFolderPath);

    if (subFolderStat.isDirectory()) {
      const files: File[] = [];

      const subFolderFiles = await fs.promises.readdir(subFolderPath);

      for (const subFolderFile of subFolderFiles) {
        const filePath = path.join(subFolderPath, subFolderFile);
        const fileData = await fs.promises.readFile(filePath, "utf8");

        const name = subFolderFile.slice(0, -5);

        files.push({
          name: name,
          data: JSON.parse(fileData),
        });
      }

      folders.push({
        name: subFolder,
        title: titles[subFolder],
        files,
      });
    }
  }

  return folders;
}

export const getRoutes = (req: Request, res: Response) => {
  readFolder(XMENU_DIR)
    .then((folders) => {
      res.json(folders);
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({ message: "Server error" });
    });
};

// Просто json модели xmenu
export const getXmodel = async (req: Request, res: Response) => {
  const { name, xname } = req.params;
  try {
    const rawData = await read(`${XMENU_DIR}/${name}/${xname}.json`, "utf8");
    const model: Field[] = JSON.parse(rawData);
    res.json(model);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

// name - название подпапки (или модели), xname - файла в подпапке:
export const getXmenu = async (req: Request, res: Response) => {
  const { name, xname } = req.params;
  try {
    const rawData = await read(`${XMENU_DIR}/${name}/${xname}.json`, "utf8");
    const model: Field[] = JSON.parse(rawData);
    const distinctFields: string[] = model
      .filter((x) => x.type === "slider")
      .map((x) => x.name);
    const DISTINCT_QUERY = distinctFields.length
      ? `SELECT
      ${distinctFields
        .map((x) => {
          return `CASE WHEN MIN(${x}) <> MAX(${x}) THEN ARRAY[MIN(${x}), MAX(${x})] END AS ${x}`;
        })
        .join("\n" + "," + "\n")}
    FROM
      ${name}`
      : "";
    const xmenu: Field[] = [];
    const responce = await AppDataSource.manager.query(DISTINCT_QUERY);
    const keys: string[] = Object.keys(responce[0]);
    const nonNullKeys: string[] = keys.filter((x) => responce[0][x]);
    if (nonNullKeys.length) {
      for (let x = 0; x < nonNullKeys.length; x++) {
        xmenu.push({
          name: nonNullKeys[x],
          label: model.filter((field) => field.name === nonNullKeys[x])[0]
            .label,
          type: "slider",
          extra: responce[0][nonNullKeys[x]],
        });
      }
    }
    for (let x = 0; x < model.length; x++) {
      if (model[x].type === "branch" || model[x].type === "tree") {
        xmenu.push(model[x]);
      }
      if (model[x].type === "select" || model[x].type === "checkbox") {
        xmenu.push({
          ...model[x],
          type: "checkbox",
        });
      }
    }
    res.json(xmenu);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

function mayBeParsedToArray(arg: any): boolean {
  try {
    const parsedArg: any[] = hparse(arg);
    if (typeof parsedArg === "object") return true;
    else return false;
  } catch (error) {
    return false;
  }
}

/**
 * Функция, создающая нативный SQL-запрос из параметров.
 * То, как это делает TypeORM, - оставляет желать лучшего...
 * Так что используем нативный запрос SQL
 **/
function createSelectQuery(
  tableName: string,
  params: { [key: string]: any },
  model: Field[]
): string {
  console.log("Table name: " + tableName);
  console.log(params);
  const { sector } = params;
  const filteredKeys = Object.keys(params)
    .filter((x) => model.map((y) => y.name).includes(x))
    .filter((x) => {
      const param = params[x];
      if (mayBeParsedToArray(param) && !hparse(params[x]).length) return false;
      else return true;
    });
  /**
   * КОСТЫЛЬ!!! Пар-р 'sector' возвращаем в filteredKeys, тк последний был отфильтрован
   * по xmenu, а не общей модели universal (возможно, это и не костыль, а правильный ход,
   * но выглядит абсурдно.)
   **/
  // filteredKeys.push('sector')
  console.log("filteredKeys: ");
  console.log(filteredKeys);
  const fields = filteredKeys
    .map((x) => {
      const fieldType: string = model.filter((y) => y.name === x)[0].type;
      console.log("fieldType: ", fieldType);
      console.log("x: ", x);
      console.log("params[x]: ", params[x]);
      if (fieldType === "checkbox")
        return `${x} IN (${hparse(params[x])
          .map((y: string) => `'${y}'`)
          .join(", ")})`;
      else if (fieldType === "slider")
        return `${x} BETWEEN ${params[x][0]} AND ${params[x][1]}`;
      else if (fieldType === "string" || fieldType === "branch")
        return `${x}='${params[x]}'`;
    })
    .join(" AND ");
  const query = `SELECT * FROM ${tableName} WHERE sector='${sector}' AND sold != true ${
    filteredKeys.length ? ` AND ${fields}` : ""
  };`;
  return query;
}

// export const goodsQuery
export const goodsQuery = async (req: Request, res: Response) => {
  console.log("============req.params");
  console.log(req.params);
  const { name, xname } = req.params;
  console.log("=============req.query");
  console.log(req.query);
  try {
    //
    const rawData = await read(`${XMENU_DIR}/${name}/${xname}.json`, "utf8");
    const model: Field[] = JSON.parse(rawData);
    const SELECT_GOODS = createSelectQuery(
      name,
      { ...req.query, sector: xname },
      model
    );
    console.log(SELECT_GOODS);
    const data = await AppDataSource.manager.query(SELECT_GOODS);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
