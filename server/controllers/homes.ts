import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Home } from "../models/Home";
import { Values } from "../types";
import { docImages, rmDocDir } from "file-handlers";

export const createHome = async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { root, ...rest } = values;
  try {
    if (req.body.photos && root?.length)
      rest.dir = (await docImages("dist", root)) || "";
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Home)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getHomes = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Home)
      .createQueryBuilder("home")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getHome = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Home)
      .createQueryBuilder("home")
      .where("home.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateHome = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  const { root, ...rest } = values;
  try {
    const data = await AppDataSource.getRepository(Home)
      .createQueryBuilder("home")
      .where("home.id = :id", { id })
      .getOne();
    if (data) {
      if (req.body.photos && !data.dir && root?.length)
        rest.dir = (await docImages("dist", root)) || "";
      const updated = await AppDataSource.createQueryBuilder()
        .update(Home)
        .set(rest)
        .where("id = :id", { id })
        .execute();
      res.json({ ...rest, id: updated.affected });
    } else {
      return res.status(404).send("Document not found");
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteHome = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Home);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      if (dataToRemove.dir) await rmDocDir(dataToRemove.dir);
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
