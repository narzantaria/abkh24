import { type Request, type Response } from "express";
import { AppDataSource } from "../db";

const DB = String(process.env.DB);

const restricted: string[] = [
  "banner",
  "subscriber",
  "post",
  "comment",
  "gallery",
  "video",
];

// Запрос всех таблиц из БД:
export const getTables = async (req: Request, res: Response) => {
  try {
    const TABLES_QUERY =
      "SELECT array_agg(table_name::TEXT) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='public'";
    const data = await AppDataSource.manager.query(TABLES_QUERY);
    res.json(data[0].array_agg.filter((x: string) => !restricted.includes(x)));
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

// Удаление таблицы (не пустой):
export const deleteTable = async (req: Request, res: Response) => {
  const { name } = req.params;
  try {
    if (restricted.some((x) => x == name))
      return res.status(405).json({ message: "Refused to remove this table!" });
    const NOTEMPTY_QUERY = `SELECT * FROM ${name}`;
    const notemptyQuery = await AppDataSource.manager.query(NOTEMPTY_QUERY);
    if (notemptyQuery.length)
      return res.status(406).json({ message: "The table is not empty!" });
    const DROP_TABLE = `DROP TABLE ${name}`;
    await AppDataSource.manager.query(DROP_TABLE);
    res.json("Removed");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
