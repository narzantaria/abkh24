// РАБОТАЕТ!!! Будем допиливать...

import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import sharp from "sharp";
import { checkFolderExists, makeId } from "file-handlers";
import client from "../store";
// import directoryExists from 'directory-exists'

const readdir = util.promisify(fs.readdir);
const unlink = util.promisify(fs.unlink);
const mkdir = util.promisify(fs.mkdir);
const rm = util.promisify(fs.rm);
const expire = util.promisify(client.expire);
// const access = util.promisify(fs.access);
// const stat = util.promisify(fs.stat);

const PREVIEW_PREFIX: string = process.env.PREVIEW_PREFIX as string;
const FILES_DIR: string =
  `${process.cwd()}/${(process.env.FILES_DIR as string) || "dist"}` + "/";

interface File {
  buffer: Buffer;
  originalname: string;
}

/**
 * Список файлов
 * Также чичтит папку с временными файлами
 **/
export const getFiles = async (req: Request, res: Response) => {
  const root = req.headers["docimages-root"]
    ? req.headers["docimages-root"]
    : "";
  console.log("root (getFiles): ", root);
  // Проверка, чтобы исключить удаление dist
  if (!root.length) {
    console.log("The userdir argument should be not empty!");
    return res.status(406).json({ message: "Unacceptable operation!" });
  } else if (root.slice(-4) === "dist") {
    console.log("Invalid root argument!!");
    return res.status(406).json({ message: "Unacceptable operation!" });
  }
  const clear = req.headers["clear-temp"];
  try {
    // Проверка существования папки, НЕблокирующий(!) вызов:
    // const exists = fs.existsSync(FILES_DIR + root);
    const DIR_NAME = FILES_DIR + root;
    console.log("DIR_NAME (getFiles): ", DIR_NAME);
    const exists = await checkFolderExists(DIR_NAME);
    if (!exists) {
      await mkdir(DIR_NAME);
    }

    const files = await readdir(DIR_NAME, { withFileTypes: true });
    let filtered = files
      .filter((dirent) => dirent.isFile())
      .map((dirent) => dirent.name);
    // проверка условия для удаления временных файлов, только если корневая папка temp:
    if (clear) {
      for (let x = 0; x < filtered.length; x++) {
        const fileToRemove = DIR_NAME + "/" + filtered[x];
        await unlink(fileToRemove);
      }
      filtered = [];
      await rm(DIR_NAME);
    }
    res.json(filtered);
  } catch (err) {
    res.status(500).send("Internal server error");
  }
};

/**
 * Одиночная загрузка файла на сервер
 * Параметр "keep" - загрузить оригинал или сжать в jpg...
 **/
export const uploadFile = async (req: Request, res: Response) => {
  const keep = req.headers["keep-original"] || false;
  const root = req.headers["docimages-root"]
    ? req.headers["docimages-root"] + "/"
    : "";
  console.log(req.files);
  // const file: File = req.file as File;
  const files = req.files as Express.Multer.File[];
  let names = [];
  try {
    // files.forEach(async file=>{
    //   console.log(file)
    //   const newName: string = makeId(20);
    //   if (keep) {
    //     sharp(file.buffer)
    //       .resize({ width: 1920, withoutEnlargement: true })
    //       .toFile(FILES_DIR + root + file.originalname)
    //       .then(() => res.json({ name: file.originalname }))
    //       .catch((err) => res.json({ message: err }));
    //   } else {
    //     await sharp(file.buffer)
    //       .resize({ width: 1920 })
    //       .jpeg({ quality: 50 })
    //       .toFile(FILES_DIR + root + newName + ".jpg");
    //     await sharp(file.buffer)
    //       .resize({ width: 480, withoutEnlargement: true })
    //       .jpeg({ quality: 50 })
    //       .toFile(FILES_DIR + root + PREVIEW_PREFIX + newName + ".jpg");
    //     res.json({ name: `${newName}.jpg` });
    //   }
    // })
    for (let x = 0; x < files.length; x++) {
      const file = files[x];
      console.log(file);
      const newName: string = makeId(20);
      if (keep) {
        sharp(file.buffer)
          .resize({ width: 1920, withoutEnlargement: true })
          .toFile(FILES_DIR + root + file.originalname)
          .then(() => res.json({ name: file.originalname }))
          .catch((err) => res.json({ message: err }));
      } else {
        await sharp(file.buffer)
          .resize({ width: 1920 })
          .jpeg({ quality: 50 })
          .toFile(FILES_DIR + root + newName + ".jpg");
        await sharp(file.buffer)
          .resize({ width: 480, withoutEnlargement: true })
          .jpeg({ quality: 50 })
          .toFile(FILES_DIR + root + PREVIEW_PREFIX + newName + ".jpg");
        // res.json({ name: `${newName}.jpg` });
        names.push(`${newName}.jpg`);
      }
    }
    res.json({ names });
  } catch (err) {
    res.status(500).send("Internal server error");
  }
};

// Удаление:
export const deleteFile = async (req: Request, res: Response) => {
  const file = req.headers.filename;
  const root = req.headers["docimages-root"]
    ? req.headers["docimages-root"] + "/"
    : "";
  const removePath1 = FILES_DIR + root + file;
  const removePath2 = FILES_DIR + root + PREVIEW_PREFIX + file;
  const filesList = await readdir(FILES_DIR + root);
  const exists = filesList.filter((x) => x == PREVIEW_PREFIX + file);
  try {
    if (exists.length > 0) {
      await unlink(removePath2);
    }
    await unlink(removePath1);
    res.json({ msg: `File \"${file}\" has been removed from server` });
  } catch (err) {
    res.json({ message: err });
  }
};

// Удаление:
export const test = async (req: Request, res: Response) => {
  console.log('ggggggggggggggggg')
  try {
    res.json('kjgkhjg')
  } catch (err) {
    res.json({ message: err });
  }
};