/**
 * Авторизация с получением токена(auth), и проверка токена(check) с помощью "middleware/auth".
 * Учитываются следующие сценарии:
 * 1. ошибка неправильного токена;
 * 2. ошибка просроченного токена;
 * 3. ошибка отсутствия токена;
 * 4. ответ в сл правильного токена;
 * 5. ответ в сл отключенной проверки (переменная "AUTHORISATION", см "middleware/auth");
 * Перем-я "SECRET_KEY" для шифрования токена.
 **/
import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import CryptoJS from "crypto-js";
import jwt, { type Secret } from "jsonwebtoken";

const read = util.promisify(fs.readFile);

const DATA_FOLDER = `${process.cwd()}/data`;
const SECRET_KEY: Secret = String(process.env.SECRET_KEY);

interface User {
  id: string;
  login: string;
  password: string;
  name: string;
  email: string;
  avatar: string;
}

// @route    POST api/auth
// @desc     Authenticate user & get token
// @access   Public
export const authorize = async (req: Request, res: Response) => {
  const { login, password } = req.body;
  try {
    const rawData = await read(`${DATA_FOLDER}/users.json`, "utf8");
    const parsedData = JSON.parse(rawData);
    const filteredData = parsedData.filter((u: User) => u.login === login);
    if (filteredData.length === 0) {
      return res.status(400).json({ message: "User not found" });
    }
    const user = filteredData[0];

    // Decrypt, compare password
    const decrypted = await CryptoJS.AES.decrypt(
      user.password,
      SECRET_KEY
    ).toString(CryptoJS.enc.Utf8);

    if (password != decrypted) {
      return res.status(400).json({ message: "Incorrect password" });
    }

    jwt.sign(
      {
        id: user.id,
      },
      SECRET_KEY,
      { expiresIn: "10h" }, // Expiration (optional)
      (err, token) => {
        if (err != null) throw err;
        res.json({ id: user.id, avatar: user.avatar, name: user.name, token });
      }
    );
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

// @check auth token
export const checkAuth = async (req: Request, res: Response) => {
  try {
    res.json("Token is correct");
  } catch (err) {
    res.status(500).send("Route error");
  }
};
