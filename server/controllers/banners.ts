import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Banner } from "../models/Banner";
import { Values } from "../types";
import { docImages, rmDocDir } from "file-handlers";

export const createBanner = async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { root, ...rest } = values;
  try {
    if (req.body.img && root?.length)
      rest.dir = (await docImages("dist", root)) || "";
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Banner)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getBanners = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Banner)
      .createQueryBuilder("banner")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getBanner = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Banner)
      .createQueryBuilder("banner")
      .where("banner.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateBanner = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  const { root, ...rest } = values;
  try {
    const data = await AppDataSource.getRepository(Banner)
      .createQueryBuilder("banner")
      .where("banner.id = :id", { id })
      .getOne();
    if (data) {
      if (rest.img && !data.dir && root?.length)
        rest.dir = (await docImages("dist", root)) || "";
      const updated = await AppDataSource.createQueryBuilder()
        .update(Banner)
        .set(rest)
        .where("id = :id", { id })
        .execute();
      res.json({ ...rest, id: updated.affected });
    } else {
      return res.status(404).send("Document not found");
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteBanner = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Banner);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      if (dataToRemove.dir) await rmDocDir(dataToRemove.dir);
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
