import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import { AppDataSource } from "../db";
import client from "../store";
import { IDocDir, docDir, docFiles, makeId } from "file-handlers";
import fetch from "node-fetch";
import nodemailer from "nodemailer";
import CryptoJS from "crypto-js";
import { Values } from "../types";
import { Person } from "../models/Person";
import { hparse } from "../middleware/tools";

const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);

const DATA_FOLDER = `${process.cwd()}/data`;
const SECRET_KEY = String(process.env.SECRET_KEY);
const PUB_SECRET_KEY = String(process.env.PUB_SECRET_KEY);
const PREVIEW_PREFIX = String(process.env.PREVIEW_PREFIX);
const COOKIE_EXPIRATION = Number(process.env.COOKIE_EXPIRATION);
const MAIL_API = String(process.env.MAIL_API);
const MAIL_ADDRESS = String(process.env.MAIL_ADDRESS);
const SMS_API_KEY = String(process.env.SMS_API_KEY);

const transporter = nodemailer.createTransport({
  host: "smtp.beget.com",
  // port: 2525,
  secure: true,
  auth: {
    user: "info@abkhazia24.com",
    pass: "Shutruk_Nahhunte113355%^&*",
  },
});

const mailOptions = {
  from: "Абхазия 24 <info@abkhazia24.com>",
  subject: "Проверочный код для регистрации",
};

interface CodeRequest {
  requestId: string;
  phone?: string;
  email?: string;
}

interface SecureReg extends Values {
  sms: string;
  requestId: string;
  password: string;
  root: string;
}

// Отправка проверочного кода на Email:
export const startByEmail = async (req: Request, res: Response) => {
  const { requestId, email }: CodeRequest = req.body;
  try {
    // Проверка email на занятость:
    const exist = await AppDataSource.manager.query(
      `SELECT * FROM person WHERE email='${email}'`
    );
    if (exist.length) {
      return res.status(409).json({ message: "Email already in use!" });
    }
    // const code: string = makeId(6);
    const code = String(Math.floor(Math.random() * 90000) + 10000);
    transporter.sendMail(
      { ...mailOptions, to: email, html: code },
      async function (error, info) {
        if (error != null) {
          console.log(error);
          return res.status(500).json({ message: error.message });
        } else {
          console.log("Email sent: " + info.response);
          await client.set(`${requestId}`, code);
          await client.expire(`${requestId}`, COOKIE_EXPIRATION);
          res.json("Initialized");
        }
      }
    );
    // console.log(code);
    // await client.set(`${requestId}`, code);
    // await client.expire(`${requestId}`, COOKIE_EXPIRATION);
    // res.json("Initialized");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

/*
Отправка проверочного кода на телефон (Только этот г..ный российский сервис не работает).
Ищем замену (sinch, twilio и тп).
*/
export const startByPhone = async (req: Request, res: Response) => {
  const { requestId, phone }: CodeRequest = req.body;
  const countryCode = phone?.slice(1, 4);
  console.log(typeof countryCode);
  try {
    // Проверка phone на занятость:
    const exist = await AppDataSource.manager.query(
      `SELECT * FROM person WHERE phone ILIKE '%${phone?.slice(1)}%'`
    );
    if (exist.length) {
      return res.status(409).json({ message: "Phone already in use!" });
    }
    const code = Math.floor(Math.random() * 90000) + 10000;
    const text = `Abkhazia 24, verification code: ${code}`;
    /**
     * Если в Абхазию - отправляем через одного агрегатора, а если в РФ - то через другого
     **/
    const uri =
      countryCode === "940"
        ? `http://api.smsfeedback.ru/messages/v2/send/?phone=%2B${phone}&login=crocuta&password=82gsu92x&text=${code}`
        : `https://smspilot.ru/api.php?send=${text}&to=${phone}&apikey=${SMS_API_KEY}&format=json`;
    console.log(uri);
    const result = await fetch(uri);
    await client.set(`${requestId}`, code);
    await client.expire(`${requestId}`, COOKIE_EXPIRATION);
    res.json("Sent");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

/*
Завершение регистрации (как sms так и email)
*/
export const finishReg = async (req: Request, res: Response) => {
  const values: SecureReg = req.body;
  if (values.password)
    values.password = CryptoJS.AES.encrypt(
      values.password,
      PUB_SECRET_KEY
    ).toString();
  const { requestId, root, security, sms, status, category, ...rest } = values;
  let dirs: IDocDir | null = null;
  // Misc:
  try {
    if (!values.phone && !values.email) {
      return res
        .status(400)
        .json({ message: "Необходим номер телефона или email" });
    }
    if (!requestId) {
      return res.status(408).json({ message: "The requestId is empty" });
    }
    // Проверка phone на занятость:
    if (values.phone) {
      const phone = hparse(values.phone).value;
      const phoneExist = await AppDataSource.manager.query(
        `SELECT * FROM person WHERE phone ILIKE '%${phone}%'`
      );
      if (phoneExist.length) {
        return res.status(409).json({ message: "Phone already in use!" });
      }
    }
    // Проверка email на занятость:
    if (values.email) {
      const emailExist = await AppDataSource.manager.query(
        `SELECT * FROM person WHERE email='${values.email}'`
      );
      if (emailExist.length) {
        return res.status(409).json({ message: "Email already in use!" });
      }
    }
    // Проверка кода активации:
    const rawData = await client.get(`${requestId}`);
    if (!rawData) {
      return res.status(440).json({ message: "Activation code expired" });
    }
    if (rawData != sms) {
      return res.status(403).json({ message: "Bad activation code" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
  rest.security = "открыто";
  rest.status = true;
  rest.category = "user";
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    // Проверка медиа:
    if (req.body.photos && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    // Запрос:
    await AppDataSource.createQueryBuilder()
      .insert()
      .into(Person)
      .values(rest)
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ (ИЗМЕНИТЬ ТОЛЬКО ОТВЕТ!!!!!!!!!!!!!)
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json("Registered");
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};
