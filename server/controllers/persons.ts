import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Person } from "../models/Person";
import { Values } from "../types";
import { docImages, rmDocDir } from "file-handlers";
import CryptoJS from "crypto-js";

const PUB_SECRET_KEY = String(process.env.PUB_SECRET_KEY);

export const createPerson = async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { root, ...rest } = values;
  if (rest.password)
    rest.password = CryptoJS.AES.encrypt(
      rest.password,
      PUB_SECRET_KEY
    ).toString();
  try {
    if (req.body.photos && root?.length)
      rest.dir = (await docImages("dist", root)) || "";
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Person)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getPersons = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Person)
      .createQueryBuilder("person")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getPerson = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Person)
      .createQueryBuilder("person")
      .leftJoinAndSelect("person.comments", "comments")
      .leftJoinAndSelect("person.answers", "answers")
      .leftJoinAndSelect("person.posts", "posts")
      .leftJoinAndSelect("person.complaints", "complaints")
      .leftJoinAndSelect("person.likes", "likes")
      .leftJoinAndSelect("person.sent", "sent")
      .leftJoinAndSelect("person.recieved", "recieved")
      .leftJoinAndSelect("person.agros", "agros")
      .leftJoinAndSelect("person.animals", "animals")
      .leftJoinAndSelect("person.autos", "autos")
      .leftJoinAndSelect("person.autoparts", "autoparts")
      .leftJoinAndSelect("person.works", "works")
      .leftJoinAndSelect("person.tourisms", "tourisms")
      .leftJoinAndSelect("person.services", "services")
      .leftJoinAndSelect("person.homes", "homes")
      .leftJoinAndSelect("person.hobbys", "hobbys")
      .leftJoinAndSelect("person.fashions", "fashions")
      .leftJoinAndSelect("person.estates", "estates")
      .leftJoinAndSelect("person.electros", "electros")
      .leftJoinAndSelect("person.childrens", "childrens")
      .leftJoinAndSelect("person.buildings", "buildings")
      .leftJoinAndSelect("person.beautys", "beautys")
      .leftJoinAndSelect("person.products", "products")
      .where("person.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json({ ...data, password: null });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updatePerson = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  const { root, ...rest } = values;
  console.log(rest.password);
  if (rest.password)
    rest.password = CryptoJS.AES.encrypt(
      rest.password,
      PUB_SECRET_KEY
    ).toString();
  console.log(rest.password);
  try {
    const data = await AppDataSource.getRepository(Person)
      .createQueryBuilder("person")
      .where("person.id = :id", { id })
      .getOne();
    if (data) {
      if (req.body.photos && !data.dir && root?.length)
        rest.dir = (await docImages("dist", root)) || "";
      const updated = await AppDataSource.createQueryBuilder()
        .update(Person)
        .set(rest)
        .where("id = :id", { id })
        .execute();
      res.json({ ...rest, id: updated.affected });
    } else {
      return res.status(404).send("Document not found");
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deletePerson = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Person);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      if (dataToRemove.dir) await rmDocDir(dataToRemove.dir);
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const relatePerson = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { recipient, recipientId, operation } = req.body;
  try {
    // Проверка, есть ли таблица реципиента:
    const REC_TABLE_QUERY = `SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='public' AND TABLE_NAME='${recipient}'`;
    const recTableQuery = await AppDataSource.manager.query(REC_TABLE_QUERY);
    if (!recTableQuery.length) {
      return res.status(404).json({ message: "Recipient table not found" });
    }
    // Проверка, есть ли таблица связей:
    const REL_TABLE_QUERY = `SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='public' AND TABLE_NAME='person_${recipient}s_${recipient}'`;
    const relTableQuery = await AppDataSource.manager.query(REL_TABLE_QUERY);
    if (!relTableQuery.length) {
      return res.status(404).json({ message: "Table not found" });
    }
    // Проверка наличия самих документов и создание связи:
    const PERSON_QUERY = `SELECT * FROM person WHERE id = ${id}`;
    const RECIPIENT_QUERY = `SELECT * FROM ${recipient} WHERE id = ${recipientId}`;
    const personQuery = await AppDataSource.manager.query(PERSON_QUERY);
    const recipientQuery = await AppDataSource.manager.query(RECIPIENT_QUERY);
    if (personQuery.length && recipientQuery.length) {
      switch (operation) {
        case "add":
          // Проверка, нет ли уже этой связи:
          const RELATION_EXISTS = `SELECT * FROM person_${recipient}s_${recipient} WHERE "personId"=${id} AND "${recipient}Id"=${recipientId};`;
          const relationExists = await AppDataSource.manager.query(
            RELATION_EXISTS
          );
          if (relationExists.length) {
            return res
              .status(409)
              .json({ message: "Relation already exists!" });
          }
          const INSERT_QUERY = `INSERT INTO person_${recipient}s_${recipient} ("personId", "${recipient}Id") VALUES (${id}, ${recipientId})`;
          const result = await AppDataSource.manager.query(INSERT_QUERY);
          res.json("Inserted");
          break;
        case "remove":
          const DELETE_QUERY = `DELETE FROM person_${recipient}s_${recipient} WHERE "personId"=${id} AND "${recipient}Id"=${recipientId}`;
          const result2 = await AppDataSource.manager.query(DELETE_QUERY);
          res.json("Removed");
          break;
        default:
          break;
      }
    } else {
      return res
        .status(404)
        .json({ message: "One or more documents not found" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
