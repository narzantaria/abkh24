/**
 * Основной клиентский контроллер. Здесь собраны обработчики запросов
 * для создания записей от зарегистрированных пользователей, регистрации,
 * авторизации, запроса данных главной страницы и многое другое.
 **/
import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Post } from "../models/Post";
import { Values } from "../types";
import jwt from "jsonwebtoken";
import CryptoJS from "crypto-js";
import {
  IDocDir,
  docDir,
  docFiles,
  rmDocDir,
} from "file-handlers";
import { Person } from "../models/Person";
import { hparse, isEmail, isPhone } from "../middleware/tools";
import { Comment } from "../models/Comment";
import { Social } from "../models/Social";
import util from "util";
import fs from "fs";
import { Anon } from "../models/Anon";

const PUB_SECRET_KEY = String(process.env.PUB_SECRET_KEY);
const DATA_FOLDER = `${process.cwd()}/data`;

const read = util.promisify(fs.readFile);

export const itemTables: string[] = [
  "agro",
  "animal",
  "auto",
  "autopart",
  "beauty",
  "building",
  "children",
  "electro",
  "estate",
  "fashion",
  "hobby",
  "home",
  "product",
  "service",
  "tourism",
  "work",
];

/*
1. Запрос для главной страницы.
2. Запрос для страницы объявлений.
*/
function innerQuery(value: string): string {
  return `
  SELECT 
    post.id, 
    post.title, 
    post.dir, 
    post.photos,
    post.video, 
    post."createdAt",
    post.tags,
    json_agg(json_build_object('id', person.id, 'name', person.title)) AS author
  FROM post
  INNER JOIN person ON post."authorId" = person.id
  WHERE post.tags[1]='${value}'
  GROUP BY post.id
  ORDER BY "createdAt" 
  DESC LIMIT 1
 `;
}

const MAIN_QUERY = `
SELECT
  (SELECT json_agg(goods.*)
    FROM
      (
        ${
          itemTables
            .map((x) => {
              return `SELECT id, title, dir, photos, price, district, "createdAt", sector, '${x}' AS source FROM ${x} WHERE sold != true`;
            })
            .join("\n" + "UNION ALL" + "\n") +
          "\n" +
          'ORDER BY "createdAt" DESC LIMIT 12'
        }
      ) AS goods) AS goods,
  (SELECT json_agg(post.*)
    FROM (
      SELECT 
        post.id, 
        post.title, 
        post.dir, 
        post.photos,
        post.video, 
        post."createdAt",
        post.tags,
        json_agg(json_build_object('id', person.id, 'name', person.title)) AS author
      FROM post
      INNER JOIN person ON post."authorId" = person.id
      GROUP BY post.id
      ORDER BY "createdAt" 
      DESC LIMIT 12
    ) AS post
  ) AS news,
  (SELECT json_agg(post.*)
    FROM (SELECT * FROM post ORDER BY "createdAt" DESC LIMIT 3) AS post
  ) AS upper,
  json_build_object(
    'sport', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("спорт")}
      ) AS post
  	),
    'amra', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("АМРА-life")}
      ) AS post
  	),
    'sochi', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("ЧП Сочи")}
      ) AS post
  	),
    'world', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("новости мира")}
      ) AS post
  	),
    'nf', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("Народный фронт")}
      ) AS post
  	),
    'resp', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("Республика")}
      ) AS post
  	),
    'mvd', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("МВД Абхазии")}
      ) AS post
  	),
    'ibp', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("ИБ Пятнашка")}
      ) AS post
  	),
    'chp', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("ЧП Абхазии")}
      ) AS post
  	)
  ) AS main,
  (SELECT json_agg(banner.*)
    FROM (SELECT * FROM banner ORDER BY "position" ASC) AS banner
  ) AS banners,
  (SELECT json_agg(post.*)
    FROM (
      SELECT 
        post.id, 
        post.title, 
        post.dir, 
        post.photos,
        post.video,
        post.tags, 
        post."createdAt", 
        json_agg(json_build_object('id', person.id, 'name', person.title)) AS author
      FROM post
      INNER JOIN person ON post."authorId" = person.id
      where post.tags[1] = 'новости мира'
      GROUP BY post.id
      ORDER BY "createdAt" 
      DESC LIMIT 6
    ) AS post
  ) AS world,
  (SELECT json_agg(post.*)
    FROM (
      SELECT 
        post.id, 
        post.title, 
        post.dir, 
        post.photos,
        post.video,
        post.tags,
        post."createdAt", 
        json_agg(json_build_object('id', person.id, 'name', person.title)) AS author
      FROM post
      INNER JOIN person ON post."authorId" = person.id
      GROUP BY post.id
      ORDER BY "looks" 
      DESC LIMIT 8
    ) AS post
  ) AS popular,
  (SELECT json_agg(video.*)
    FROM (SELECT * FROM video ORDER BY "createdAt" DESC LIMIT 6) AS video
  ) AS videos,
  (SELECT json_agg(movie.*)
    FROM (SELECT * FROM movie ORDER BY RANDOM() DESC LIMIT 5) AS movie
  ) AS movies,
  (SELECT json_agg(playbill.*)
    FROM (SELECT * FROM playbill WHERE premiere::timestamp >= CURRENT_TIMESTAMP ORDER BY "premiere" ASC LIMIT 6) AS playbill
  ) AS playbill
`;

export function goodsQuery(): string {
  return (
    itemTables
      .map((x) => {
        return `SELECT id, title, dir, photos, price, district, "createdAt", sector, '${x}' AS source
FROM ${x}`;
      })
      .join("\n" + "UNION ALL" + "\n") +
    "\n" +
    'ORDER BY "createdAt" DESC LIMIT 45'
  );
}

// Находит последнее объявление пользователя в 16 табл-х:
function personLastGoodQuery(id: number): string {
  return (
    itemTables
      .map((x) => {
        return `SELECT id, title, photos, price, "createdAt", '${x}' AS source
FROM ${x} WHERE "authorId" = ${id}`;
      })
      .join("\n" + "UNION ALL" + "\n") +
    "\n" +
    'ORDER BY "createdAt" DESC LIMIT 1'
  );
}

// Находит все объявления пользователя в 16 табл-х:
function personGoodsQuery(id: string): string {
  return (
    itemTables
      .map((x) => {
        return `SELECT id, title, photos, price, "createdAt", '${x}' AS source
FROM ${x} WHERE "authorId" = ${id} AND sold != true`;
      })
      .join("\n" + "UNION ALL" + "\n") +
    "\n" +
    'ORDER BY "createdAt" DESC'
  );
}

export const getData = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.manager.query(MAIN_QUERY);
    const rawFile = await read(`${DATA_FOLDER}/misc.json`, "utf8");
    const jsonFile = JSON.parse(rawFile);
    data[0].weather = jsonFile.weather;
    data[0].rates = jsonFile.rates;
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getGoods = async (req: Request, res: Response) => {
  try {
    const GOODS_QUERY: string = goodsQuery();
    const data = await AppDataSource.manager.query(GOODS_QUERY);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

/**
 * Запрос для афиши клиента. Особенность в том, что отсеиваются те, чья дата премьеры
 * старше текущей даты. Также сортировка по премьере и восходящая (ASC)
 **/
export const getPlaybillsPub = async (req: Request, res: Response) => {
  try {
    const PLAYBILLS_QUERY =
      "SELECT * from playbill WHERE premiere::timestamp >= CURRENT_TIMESTAMP ORDER BY premiere ASC";
    const data = await AppDataSource.manager.query(PLAYBILLS_QUERY);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

/**
 * Запрос тегов для повторных запросов новостей.
 * Можно его включить в запрос всех новостей, и возможно,
 * это правильней, но оставим это на потом (ЗАПРЕЩЕННЫЙ ПРИЕМ).
 * К тому же, можно сохранить результат в store.
 **/
export const getPostTags = async (req: Request, res: Response) => {
  try {
    const TAGS_QUERY = `SELECT array_agg(DISTINCT elem) AS unique_tags
    FROM post, unnest(tags) AS elem`;
    const data = await AppDataSource.manager.query(TAGS_QUERY);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

/**
 * ХИТРЫЙ запрос новостей на странице "news" с параметрами.
 * Стр-а может быть открыта через меню (тогда будут запрошены все новости).
 * Также можно открыть некоторые разделы (такие как "Нар-й фронт" и тд),
 * тогда страница открывается с параметром для выбора tags.
 * Как передавать и сохранять параметры - пока не знаю, будет ясно..
 * (помимо tags передаются параметры отступа и лимит)
 **/
export const getPosts = async (req: Request, res: Response) => {
  const params: { [key: string]: any } = req.query;
  const { offset, limit, popular, tags } = params;
  // const tags: string[] = ['наука', 'политика'];
  try {
    const SELECT_POSTS = `SELECT 
      p.id,
      p.title,
      p.tags,
      p.dir,
      p.photos,
      p.video,
      p."createdAt",
      COUNT(c."postId") AS comment
    FROM 
      post p  
    LEFT JOIN 
      comment c ON p.id = c."postId"${
        tags?.length
          ? "\n" +
            `WHERE ${tags
              .map((x: string) => {
                return `'${x}' = ANY(tags)`;
              })
              .join(" OR ")}`
          : ""
      }
    GROUP BY 
      p.id${
        popular
          ? "\n" + "ORDER BY looks DESC"
          : "\n" + 'ORDER BY "createdAt" DESC'
      }${offset ? "\n" + `OFFSET ${offset}` : ""}${
      limit ? "\n" + `LIMIT ${limit}` : ""
    }`;
    const data = await AppDataSource.manager.query(SELECT_POSTS);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

/**
 * ОЧЕНЬ ХИТРЫЙ запрос одной статьи с комментами.
 * В каждом комменте - автор.
 * Кроме того, есть второй запрос, кот-й обновляет просмотры
 **/
export const getPost = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const UPDATE_LOOKS = `UPDATE post
    SET looks = looks + 1
    WHERE id = ${id}`;
    const SELECT_POSTS = `SELECT
      post.*,
      author.id as authorid,
      author.title as authorname,
      author.photos as authorphotos,
      author.dir as authordir,
      -- комментарии, если есть
      CASE WHEN COUNT(comment.id) = 0 THEN JSON '[]'
        ELSE json_agg(
          json_build_object(
            'id', comment.id,
            'text', comment.text,
            'dir', comment.dir,
            'img', comment.img,
            'createdAt', comment."createdAt",
            'author', json_build_object(
              'id', person.id,
              'title', person.title,
              'photos', person.photos,
              'dir', person.dir
              -- другие поля из таблицы "person"
            )
            -- другие поля из таблицы "comment"
          ) -- ORDER BY comment."createdAt" DESC -- Добавляем сортировку по createdAt
        )
      END AS comment
    FROM
      post
    LEFT JOIN
      comment ON post.id = comment."postId"
    LEFT JOIN
      person ON comment."authorId" = person.id
    inner join person author on post."authorId" = author.id 
    WHERE
      post.id = ${id}
    GROUP BY
      post.id,
      post.title,
      author.id`;
    await AppDataSource.manager.query(UPDATE_LOOKS);
    const data = await AppDataSource.manager.query(SELECT_POSTS);
    if (!data.length) {
      return res.status(404).json({ message: "Document not found" });
    }
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

// Обновляет просмотры товара (КОСТЫЛЬ! ВРЕМЕННОЕ РЕШЕНИЕ!!!)
export const updateItemLooks = async (req: Request, res: Response) => {
  const { id, model } = req.params;
  try {
    const UPDATE_LOOKS = `UPDATE ${model}
    SET looks = looks + 1
    WHERE id = ${id}`;
    await AppDataSource.manager.query(UPDATE_LOOKS);
    res.json("Updated");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export function compareDates(date1: Date, date2: Date): boolean {
  // Проверяем, если вторая дата раньше первой, то возвращаем false
  if (date2 < date1) {
    return false;
  }
  // Вычисляем разницу в миллисекундах между двумя датами
  const diffInMilliseconds = Math.abs(date2.getTime() - date1.getTime());
  // Переводим разницу в часы
  const diffInHours = diffInMilliseconds / (1000 * 60 * 60);
  // Проверяем, если прошло менее 72 часов, то возвращаем false
  if (diffInHours < 72) {
    return false;
  }
  // Если больше либо равно 72 часа, то возвращаем true
  return true;
}

/*
Проверка полномочий перед созданием объявления. Если с даты последнего объявления прошло
более 72 часов, то можно давать объявление (true). Если пользователь - модератор, тоже...
*/
export const checkPerson = async (id: number): Promise<boolean> => {
  try {
    const person = await AppDataSource.getRepository(Person)
      .createQueryBuilder("person")
      .where("person.id = :id", { id })
      .getOne();
    if (person?.category === "moderator") return true;
    // Забанен:
    if (!person?.status) {
      console.log('pers status')
      return false;
    }
    // Получаем последнее объявление пользователя:
    const LAST_GOODS_QUERY: string = personLastGoodQuery(id);
    const lastGoods = await AppDataSource.manager.query(LAST_GOODS_QUERY);
    if (!lastGoods.length) {
      // Объявлений еще не было...
      return true;
    } else if (!compareDates(new Date(lastGoods[0].createdAt), new Date())) {
      // Не прошло 72 часа:
      return false;
    }
    return true;
  } catch (error) {
    console.log('xz')
    console.log(error);
    return false;
  }
};

export const getMasterContent = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const GOODS_QUERY: string = personGoodsQuery(id);
    const goods = await AppDataSource.manager.query(GOODS_QUERY);
    const posts = await AppDataSource.manager.query(
      `SELECT * FROM post WHERE "authorId"=${id} ORDER BY "createdAt" DESC`
    );
    res.json({ goods: goods, posts: posts });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getUserItem = async (req: Request, res: Response) => {
  const { id, model } = req.params;
  try {
    const ITEM_QUERY = `SELECT * FROM ${model} WHERE id=${id}`;
    const data = await AppDataSource.manager.query(ITEM_QUERY);
    const { looks, ...rest } = data[0];
    res.json(rest);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getUserPost = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const POST_QUERY = `SELECT * FROM post WHERE id=${id}`;
    const data = await AppDataSource.manager.query(POST_QUERY);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

/**
 * Функция, создающая нативный SQL-запрос из параметров.
 * Только вот делает она это УЖАСНО
 * Кое-как адаптировано под конкретный набор параметров.
 * Но неизвестно, как поведет себя с другим набором...
 **/
function createPostgresUpdateQuery(
  tableName: string,
  documentId: number,
  params: any
): string {
  // Генерируем список полей и их значения из параметров req.body
  const fields = Object.keys(params)
    .map(
      (key) =>
        `${key} = '${
          typeof params[key] === "object"
            ? "{" + params[key] + "}"
            : params[key]
        }'`
    )
    .join(", ");

  // Составляем и возвращаем запрос PostgreSQL
  return `UPDATE ${tableName} SET ${fields} WHERE id = ${documentId};`;
}

/**
 * Обновить товар от пользователя (с ограничениями). Здесь есть странности.
 * 1. в phone должно быть по 2 одинарных кавычки.
 * 2. createPostgresUpdateQuery по-особому обрабатывает массивы.
 * 3. Работает для данной ситуации, не универсально.
 * В общем, данный метод выглядит ужасно, не нравится, но другого пока нет...
 * (ispravleno)
 **/
export const updateUserItem = async (req: Request, res: Response) => {
  const { id, model } = req.params;
  const values: Values = req.body;
  const { authorId, root, sector, createdAt, clearTemp, looks, ...rest } =
    values;
  rest.sold = Boolean(rest.sold);
  if (rest.phone) {
    rest.phone = rest.phone.replaceAll("'", "''");
  }

  let dirs: IDocDir | null = null,
    data: any | null;
  // Zapros, dirs
  try {
    data = await AppDataSource.manager.query(
      `SELECT * from ${model} WHERE id=${id}`
    );
    if (rest.photos && !data[0].dir && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    const UPDATE_QUERY: string = createPostgresUpdateQuery(
      model,
      Number(id),
      rest
    );
    await AppDataSource.manager.query(UPDATE_QUERY);
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // Files
  try {
    if (rest.photos && !data[0].dir && root?.length) {
      if (!dirs) {
        return res.status(500).json({ message: "Server error, empty dirs" });
      }
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json("Done");
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

/**
 * Обновление статьи пользователем (с ограничениями)
 * Обычный юзер - не сможет.
 * Также из параметров удаляются просмотры и тд...
 * (ispravleno)
 **/
export const updateUserPost = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  const { authorId, root, clearTemp, looks, ...rest } = values;
  let dirs: IDocDir | null = null,
    data: any | null,
    updated: any | null;
  // Proverki
  const author = await AppDataSource.getRepository(Person)
    .createQueryBuilder("person")
    .where("person.id = :id", { id: authorId })
    .getOne();
  if (author?.category !== "moderator" && author?.category !== "editor") {
    return res.status(403).json({ message: "No credentials for user" });
  }
  data = await AppDataSource.getRepository(Post)
    .createQueryBuilder("post")
    .where("post.id = :id", { id })
    .getOne();
  if (!data) return res.status(404).send("Document not found");
  // Zapros
  try {
    if ((rest.photos || rest.video) && !data.dir && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    updated = await AppDataSource.createQueryBuilder()
      .update(Post)
      .set(rest)
      .where("id = :id", { id })
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ
  try {
    if (rest.img && !data.dir && root?.length) {
      if (!dirs) {
        return res.status(500).json({ message: "Server error, empty dirs" });
      }
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json({ ...rest, id: updated.affected });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

// PUBAUTH:
// Авторизация зарегистрированного пользователя:
export const loginUser = async (req: Request, res: Response) => {
  const { login, password } = req.body;
  try {
    const loginIsPhone = isPhone(login);
    const loginIsEmail = isEmail(login);
    let person: Person[] | null = null;
    if (loginIsPhone) {
      person = await AppDataSource.manager.query(
        `SELECT * FROM person WHERE phone ILIKE '%${login.slice(2)}%'`
      );
    }
    if (loginIsEmail) {
      person = await AppDataSource.manager.query(
        `SELECT * FROM person WHERE email='${login}'`
      );
    }
    if (!person) return res.status(404).json({ message: "User not found!" });

    if (!person[0].status) {
      return res.status(403).json({ message: "User temporary blocked!" });
    }

    // Decrypt, compare password
    const decrypted = await CryptoJS.AES.decrypt(
      person[0].password,
      PUB_SECRET_KEY
    ).toString(CryptoJS.enc.Utf8);

    if (password != decrypted) {
      return res.status(400).json({ errors: [{ msg: "Incorrect password" }] });
    }

    // JWT Token:
    const payload = {
      person: {
        id: person[0].id,
      },
    };

    jwt.sign(
      payload,
      PUB_SECRET_KEY,
      { expiresIn: "100h" }, // Expiration (optional)
      (err, token) => {
        if (err) throw err;
        const { status, password, ...rest } = person![0];
        res.json({ ...rest, token: token });
      }
    );
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

// Релогин через localStorage:
export const checkPubUser = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.manager.query(
      `SELECT * FROM person WHERE id='${req.params.id}'`
    );
    const { status, password, ...rest } = data![0];
    res.json(rest);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

// То же - для пользователя СС:
export const checkSocialUser = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.manager.query(
      `SELECT * FROM social WHERE sid='${req.params.id}'`
    );
    if (!data) return res.status(404).json({ message: "User not found" });
    if (!data[0].status)
      return res.status(403).json({ message: "User temporary blocked" });
    const { status, password, ...rest } = data![0];
    res.json(rest);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

// Новость от пользователя (ispravleno):
export const createPostPub = async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { root, ...rest } = values;
  let dirs: IDocDir | null = null,
    data: any | null;
  // Misc:
  const person = await AppDataSource.getRepository(Person)
    .createQueryBuilder("person")
    .where("person.id = :id", { id: values.author })
    .getOne();
  // Нет полномочий (такое не должно приходить от клиента, но на всякий):
  if (person?.category === "user")
    return res.status(403).json({ message: "No credentials for user" });
  // Забанен:
  if (!person?.status)
    return res.status(403).json({ message: "User temporary blocked" });
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    if ((req.body.photos || req.body.video) && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Post)
      .values(rest)
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

// Объявление от пользователя (ispravleno):
export const createItemPub = async (req: Request, res: Response) => {
  console.log(req.body);
  const modelName: string = req.params.model;
  let dirs: IDocDir | null = null;
  /**
   * ИЗМЕНЕНИЕ!!! п-р "authorId" включен в "values" (чего раньше не было,
   * и не пойму с какого ...). Вернули автора...
   **/
  const { root, looks, ...values } = req.body;
  if (values.phone) {
    values.phone = values.phone.replaceAll("'", "''");
  }
  // Проверяем, существует ли таблица с указанным именем
  try {
    // Проверяем пользователя на ограничения:
    const check = await checkPerson(values.authorId);
    if (!check) {
      return res.status(403).json({
        message: "Not enougn credentials or you have tempopary restrictions",
      });
    }      
  } catch (error) {
    console.log(error);
    return res.status(400).json({ message: "Invalid model name" });
  }
  // CНАЧАЛА ЗАПРОС:
  try {
    if (values.photos && root?.length) {
      dirs = docDir();
      values.dir = `${dirs.subDir}/${dirs.dir}`;
      values.photos = `{${values.photos.join(",")}}`;
    }
    const INSERT_QUERY = `INSERT INTO ${modelName} (${Object.keys(values)
      .map((x) => `"${x}"`)
      .join(", ")}) VALUES (${Object.keys(values)
      .map((x) => `'${values[x]}'`)
      .join(", ")})`;
    await AppDataSource.manager.query(INSERT_QUERY);
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
  res.json("Done");
};

// Объявление от пользователя СС (ispravleno):
export const createItemSocial = async (req: Request, res: Response) => {
  const modelName: string = req.params.model;
  const values: Values = req.body;
  const { root, authorId, ...rest } = values;
  if (rest.phone) {
    rest.phone = rest.phone.replaceAll("'", "''");
  }
  let dirs: IDocDir | null = null,
    user: any | null;
  // Проверяем, существует ли пользователь и есть ли ограничения:
  user = await AppDataSource.getRepository(Social)
    .createQueryBuilder("social")
    .where("social.sid = :sid", { sid: authorId })
    .getOne();
  if (!user) {
    return res.status(404).json({
      message: "User not found",
    });
  }
  // Проверяем пользователя на ограничения:
  if (!user?.status) {
    return res.status(403).json({
      message: "Not enougn credentials or you have tempopary restrictions",
    });
  }
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    if (rest.photos && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
      rest.photos = `{${rest.photos.join(",")}}`;
    }
    const INSERT_QUERY = `INSERT INTO ${modelName} (${Object.keys(rest)
      .map((x) => `"${x}"`)
      .join(", ")}) VALUES (${Object.keys(rest)
      .map((x) => `'${rest[x]}'`)
      .join(", ")})`;
    await AppDataSource.manager.query(INSERT_QUERY);
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // Дата последнего объявления (д.б. не более 3-х дней с того момента):
  const lastad = user.lastad;
  if (!compareDates(new Date(lastad), new Date())) {
    return res.status(403).json({
      message: "Not enough time since the last announcement",
    });
  } else {
    user.lastad = new Date();
    await AppDataSource.getRepository(Social).save(user);
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json("Done");
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

// Объявление от пользователя СС (ispravleno):
export const createItemAnon = async (req: Request, res: Response) => {
  const modelName: string = req.params.model;
  const values: Values = req.body;
  const { root, vendor, ...rest } = values;
  if (rest.phone) {
    rest.phone = rest.phone.replaceAll("'", "''");
  }
  let dirs: IDocDir | null = null,
    anon: any | null;
  // Проверяем, существует ли пользователь и есть ли ограничения:
  anon = await AppDataSource.getRepository(Anon)
    .createQueryBuilder("anon")
    .where("anon.vendor = :vendor", { vendor })
    .getOne();
  if (!anon) {
    const rawData = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Anon)
      .values({
        title: `anon-${new Date().toISOString()}`,
        vendor,
        lastad: new Date(1).toISOString()
      })
      .execute();
    anon = rawData.generatedMaps[0];
  }
  // Проверяем пользователя на ограничения:
  if (!anon?.status) {
    return res.status(403).json({
      message: "Not enougn credentials or you have tempopary restrictions",
    });
  }
  // Дата последнего объявления (д.б. не более 3-х дней с того момента):
  const lastad = anon.lastad;
  if (!compareDates(new Date(lastad), new Date())) {
    return res.status(403).json({
      message: "Not enough time since the last announcement",
    });
  } else {
    anon.lastad = new Date();
    await AppDataSource.getRepository(Anon).save(anon);
  }
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    if (rest.photos && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
      rest.photos = `{${rest.photos.join(",")}}`;
    }
    const INSERT_QUERY = `INSERT INTO ${modelName} (${Object.keys(rest)
      .map((x) => `"${x}"`)
      .join(", ")}) VALUES (${Object.keys(rest)
      .map((x) => `'${rest[x]}'`)
      .join(", ")})`;
    await AppDataSource.manager.query(INSERT_QUERY);
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }  
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }    
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
  res.json("Done");
};

// Коммент от пользователя (ispravleno):
export const createCommentPub = async (req: Request, res: Response) => {
  console.log(req.body);
  const values: Values = req.body;
  const { root, ...rest } = values;
  let dirs: IDocDir | null = null,
    data: any | null;
  if (!rest.text && !rest.img)
    return res.status(405).json({ message: "Fill the required inputs!" });
  // Proverki
  try {
    const person = await AppDataSource.getRepository(Person)
      .createQueryBuilder("person")
      .where("person.id = :id", { id: rest.author })
      .getOne();
    // Забанен:
    if (!person?.status)
      return res.status(403).json({ message: "User temporary blocked" });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    if (req.body.img && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Comment)
      .values(rest)
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

// Обновляем себя (ОГРАНИЧЕНИЯ!!! НЕ ГОТОВО):
export const updateSelfPub = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  const { root, category, status, fapi, ...rest } = values;
  if (req.body.password)
    rest.password = CryptoJS.AES.encrypt(
      req.body.password,
      PUB_SECRET_KEY
    ).toString();
  let dirs: IDocDir | null = null,
    data: any | null,
    updated: any | null;
  // Raznoe
  try {
    // Проверяем, не занят ли номер телефона:
    const phone = hparse(rest.phone).value;
    const isPhoneBusy = await AppDataSource.manager.query(
      `SELECT * FROM person WHERE phone ILIKE '%${phone}%' AND id <> ${id}`
    );
    if (isPhoneBusy.length) {
      return res.status(409).json({ message: "Phone is already in use" });
    }
    const isEmailBusy = await AppDataSource.manager.query(
      `SELECT * FROM person WHERE email='${rest.email}' AND id <> ${id}`
    );
    if (isEmailBusy.length) {
      return res.status(409).json({ message: "Email is already in use" });
    }
    data = await AppDataSource.getRepository(Person)
      .createQueryBuilder("person")
      .where("person.id = :id", { id })
      .getOne();
    if (!data) return res.status(404).json({ message: "Document not found" });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    if (req.body.photos && !data.dir && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    updated = await AppDataSource.createQueryBuilder()
      .update(Person)
      .set(rest)
      .where("id = :id", { id })
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json({ ...rest, id: updated.affected });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

export const updateCommentPub = async (req: Request, res: Response) => {
  console.log(req.body);
  const { id } = req.params;
  const values: Values = req.body;
  const { editorId, root, clearTemp, looks, answers, ...rest } = values;
  let dirs: IDocDir | null = null,
    data: any | null,
    updated: any | null;
  // Proverki
  const author = await AppDataSource.getRepository(Person)
    .createQueryBuilder("person")
    .where("person.id = :id", { id: editorId })
    .getOne();
  if (author?.category !== "moderator" && author?.category !== "editor") {
    return res.status(403).json({ message: "No credentials for user" });
  }
  data = await AppDataSource.getRepository(Comment)
    .createQueryBuilder("comment")
    .where("comment.id = :id", { id })
    .getOne();
  if (!data) return res.status(404).send("Document not found");
  // Zapros
  try {
    if (rest.img && !data.dir && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    updated = await AppDataSource.createQueryBuilder()
      .update(Comment)
      .set(rest)
      .where("id = :id", { id })
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ
  try {
    if (rest.img && !data.dir && root?.length) {
      if (!dirs) {
        return res.status(500).json({ message: "Server error, empty dirs" });
      }
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json({ ...rest, id: updated.affected });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

export const deleteCommentPub = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  const { authorId } = values;
  let data: any | null, dataRepository: any | null;
  // Proverki
  try {
    const author = await AppDataSource.getRepository(Person)
      .createQueryBuilder("person")
      .where("person.id = :id", { id: authorId })
      .getOne();
    if (!author?.status) {
      return res.status(403).json({ message: "No credentials for user" });
    }
    dataRepository = AppDataSource.getRepository(Comment);
    data = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (!data) return res.status(404).send("Document not found");
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
  // Zapros
  try {
    if (data.dir) await rmDocDir(data.dir);
    await dataRepository.remove(data);
    res.json("Removed");
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

// Backup(очень ценный пример)
/*
const MAIN_QUERY = `
SELECT
  (SELECT json_agg(goods.*)
    FROM
      (
        ${
          itemTables
            .map((x) => {
              return `SELECT id, title, dir, photos, price, district, "createdAt", sector, '${x}' AS source FROM ${x} WHERE sold != true`;
            })
            .join("\n" + "UNION ALL" + "\n") +
          "\n" +
          'ORDER BY "createdAt" DESC LIMIT 12'
        }
      ) AS goods) AS goods,
  (SELECT json_agg(post.*)
    FROM (SELECT * FROM post ORDER BY "createdAt" DESC LIMIT 3) AS post
  ) AS upper,
  json_build_object(
    'sport', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("спорт")}
      ) AS post
  	),
    'amra', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("АМРА-life")}
      ) AS post
  	),
    'sochi', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("ЧП Сочи")}
      ) AS post
  	),
    'world', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("новости мира")}
      ) AS post
  	),
    'nf', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("Народный фронт")}
      ) AS post
  	),
    'resp', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("Республика")}
      ) AS post
  	),
    'mvd', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("МВД Абхазии")}
      ) AS post
  	),
    'ibp', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("ИБ Пятнашка")}
      ) AS post
  	),
    'chp', (SELECT json_agg(post.*)
      FROM (
        ${innerQuery("ЧП Абхазии")}
      ) AS post
  	)
  ) AS main,
  (SELECT json_agg(banner.*)
    FROM (SELECT * FROM banner ORDER BY "position" ASC) AS banner
  ) AS banners,
  (SELECT json_agg(post.*)
    FROM (
      SELECT 
        post.id, 
        post.title, 
        post.dir, 
        post.photos,
        post.video,
        post.tags, 
        post."createdAt", 
        json_agg(json_build_object('id', person.id, 'name', person.title)) AS author
      FROM post
      INNER JOIN person ON post."authorId" = person.id
      where post.tags[1] = 'новости мира'
      GROUP BY post.id
      ORDER BY "createdAt" 
      DESC LIMIT 6
    ) AS post
  ) AS world,
  (SELECT json_agg(post.*)
    FROM (
      SELECT 
        post.id, 
        post.title, 
        post.dir, 
        post.photos,
        post.video,
        post.tags,
        post."createdAt", 
        json_agg(json_build_object('id', person.id, 'name', person.title)) AS author
      FROM post
      INNER JOIN person ON post."authorId" = person.id
      GROUP BY post.id
      ORDER BY "looks" 
      DESC LIMIT 8
    ) AS post
  ) AS popular,
  (SELECT json_agg(video.*)
    FROM (SELECT * FROM video ORDER BY "createdAt" DESC LIMIT 6) AS video
  ) AS videos,
  (SELECT json_agg(movie.*)
    FROM (SELECT * FROM movie ORDER BY RANDOM() DESC LIMIT 5) AS movie
  ) AS movies,
  (SELECT json_agg(playbill.*)
    FROM (SELECT * FROM playbill WHERE premiere::timestamp >= CURRENT_TIMESTAMP ORDER BY "premiere" ASC LIMIT 6) AS playbill
  ) AS playbill
`;
*/
