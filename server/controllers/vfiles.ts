import { type Request, type Response } from "express";
import fs, { promises } from "fs";
import util from "util";
import { makeId } from "file-handlers";
import { path as ffmpegPath } from "@ffmpeg-installer/ffmpeg";
import ffmpeg from "fluent-ffmpeg";
ffmpeg.setFfmpegPath(ffmpegPath);

const readdir = util.promisify(fs.readdir);
const unlink = util.promisify(fs.unlink);
const mkdir = util.promisify(fs.mkdir);
const rm = util.promisify(fs.rm);
const write = util.promisify(fs.writeFile);

const PREVIEW_PREFIX: string = process.env.PREVIEW_PREFIX as string;
const FILES_DIR: string =
  `${process.cwd()}/${(process.env.FILES_DIR as string) || "dist"}` + "/";

interface File {
  buffer: Buffer;
  originalname: string;
}

async function checkFolderExists(path: string): Promise<boolean> {
  try {
    await promises.access(path);
    return true;
  } catch (error) {
    return false;
  }
}

/**
 * Список файлов
 * Также чичтит папку с временными файлами
 **/
export const getVideos = async (req: Request, res: Response) => {
  const root = req.headers["docimages-root"]
    ? req.headers["docimages-root"]
    : "";
  const clear = req.headers["clear-temp"];
  try {
    // Проверка существования папки, НЕблокирующий(!) вызов:
    // const exists = fs.existsSync(FILES_DIR + root);
    const exists = await checkFolderExists(FILES_DIR + root);
    if (!exists) {
      await mkdir(FILES_DIR + root);
    }

    const files = await readdir(FILES_DIR + root, { withFileTypes: true });
    let filtered = files
      .filter((dirent) => dirent.isFile())
      .map((dirent) => dirent.name);
    // проверка условия для удаления временных файлов, только если корневая папка temp:
    if (clear) {
      for (let x = 0; x < filtered.length; x++) {
        const fileToRemove = FILES_DIR + root + "/" + filtered[x];
        await unlink(fileToRemove);
      }
      filtered = [];
      await rm(FILES_DIR + root);
    }
    res.json(filtered);
  } catch (err) {
    res.status(500).send("Internal server error");
  }
};

/**
 * Одиночная загрузка файла на сервер
 * Параметр "keep" - загрузить оригинал или сжать в jpg...
 **/
export const uploadVideo = (req: Request, res: Response) => {
  const root = req.headers["docimages-root"] || "temp";
  const file: File = req.file as File;
  const newName: string = makeId(20);

  // Convert video to mp4
  const inputPath = FILES_DIR + "temp/" + file.originalname;
  const convertedPath = FILES_DIR + root + "/" + newName + ".mp4";
  ffmpeg(inputPath)
    .videoCodec("libx264")
    .outputFormat("mp4")
    .outputOptions(["-movflags", "faststart"])
    .output(convertedPath)
    .on("end", async () => {
      ffmpeg(inputPath)
        .size(`360x?`)
        .videoCodec("libx264")
        .outputFormat("mp4")
        .outputOptions(["-movflags", "faststart"])
        .output(`${FILES_DIR + root}/${newName}_360.mp4`)
        .on("end", () => {
          console.log(`Video saved with resolution 360`);
          res.json({ name: `${newName}.mp4` });
        })
        .on("error", (error) => {
          console.error(`Error saving video with resolution 360:`, error);
          res.status(500).json({ message: "Conversion error" });
        })
        .run();
    })
    .on("error", (error) => {
      console.error("Error converting video:", error);
      res.status(500).json({ message: "Server error" });
    })
    .run();
};

// Удаление:
export const deleteVideo = async (req: Request, res: Response) => {
  const file = req.headers.filename;
  const root = req.headers["docimages-root"]
    ? req.headers["docimages-root"] + "/"
    : "";
  const removePath1 = FILES_DIR + root + file;
  const removePath2 = FILES_DIR + root + PREVIEW_PREFIX + file;
  const filesList = await readdir(FILES_DIR + root);
  const exists = filesList.filter((x) => x == PREVIEW_PREFIX + file);
  try {
    if (exists.length > 0) {
      await unlink(removePath2);
    }
    await unlink(removePath1);
    res.json({ msg: `File \"${file}\" has been removed from server` });
  } catch (err) {
    res.json({ message: err });
  }
};
