/**
 * Поле "email" является уникальным. Проверка осуществляется самой СУБД.
 * Прежняя версия с проверкой закомментирована... не будем удалять...
 **/
import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Subscriber } from "../models/Subscriber";

export const createSubscriber = async (req: Request, res: Response) => {
  const { email } = req.body;
  try {
    // const exists = await AppDataSource.getRepository(Subscriber)
    //   .createQueryBuilder("subscriber")
    //   .where("subscriber.email = :email", { email })
    //   .getOne();
    // if (exists != null) {
    //   res.status(403).send("Document already exists");
    //   return;
    // } else {
    //   const data = await AppDataSource.createQueryBuilder()
    //     .insert()
    //     .into(Subscriber)
    //     .values({ email })
    //     .execute();
    //   res.json(data);
    // }
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Subscriber)
      .values({ email })
      .execute();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getSubscribers = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Subscriber)
      .createQueryBuilder("subscriber")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getSubscriber = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Subscriber)
      .createQueryBuilder("subscriber")
      .where("subscriber.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateSubscriber = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { email } = req.body;
  try {
    const data = await AppDataSource.createQueryBuilder()
      .update(Subscriber)
      .set({ email: email })
      .where("id = :id", { id: id })
      .execute();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteSubscriber = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Subscriber);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
