import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Comment } from "../models/Comment";
import { Values } from "../types";
import { docImages, rmDocDir } from "file-handlers";

export const createComment = async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { root, ...rest } = values;
  try {
    if (req.body.img && root?.length)
      rest.dir = (await docImages("dist", root)) || "";
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Comment)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getComments = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Comment)
      .createQueryBuilder("comment")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getComment = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Comment)
      .createQueryBuilder("comment")
      .leftJoinAndSelect("comment.answers", "answers")
      .leftJoinAndSelect("comment.author", "author")
      .where("comment.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateComment = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  const { root, ...rest } = values;
  try {
    const data = await AppDataSource.getRepository(Comment)
      .createQueryBuilder("comment")
      .where("comment.id = :id", { id })
      .getOne();
    if (data) {
      if (req.body.img && !data.dir && root?.length)
        rest.dir = (await docImages("dist", root)) || "";
      const updated = await AppDataSource.createQueryBuilder()
        .update(Comment)
        .set(rest)
        .where("id = :id", { id })
        .execute();
      res.json({ ...rest, id: updated.affected });
    } else {
      return res.status(404).send("Document not found");
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteComment = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Comment);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      if (dataToRemove.dir) await rmDocDir(dataToRemove.dir);
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
