import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import svgCaptcha from "svg-captcha";

const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);

const DATA_FOLDER = `${process.cwd()}/data`;
const SECRET_KEY = String(process.env.SECRET_KEY);
const PREVIEW_PREFIX = String(process.env.PREVIEW_PREFIX);

export const getCaptcha = (req: Request, res: Response) => {
  try {
    const { data, text } = svgCaptcha.create();
    res.status(200).json({ data, text });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
