/**
 * Работает с универсальными моделями-схемами в папке "universal",
 * которые затем используются в роуте и контроллере "universal",
 * а также для рендеринга формы "Mainform" в админкею
 **/
import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import { capitalizeFirstLetter } from "../middleware/tools";

const readdir = util.promisify(fs.readdir);
const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);
const unlink = util.promisify(fs.unlink);

const UNIVERSAL_FOLDER = `${process.cwd()}/universal`;

export const createModel = async (req: Request, res: Response) => {
  const { data, name } = req.body;
  try {
    await write(`${UNIVERSAL_FOLDER}/${name}.json`, data);
    res.json("Done");
  } catch (error) {
    // console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getModels = async (req: Request, res: Response) => {
  try {
    const files = await readdir(UNIVERSAL_FOLDER);
    const data = files.map((x) => {
      const _x = x.replace(/\.[^/.]+$/, "");
      return capitalizeFirstLetter(_x);
    });
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const getModel = async (req: Request, res: Response) => {
  const { name } = req.params;
  console.log(`${UNIVERSAL_FOLDER}/${name}.json`);
  try {
    const rawData = await read(`${UNIVERSAL_FOLDER}/${name}.json`, "utf8");
    const data = JSON.parse(rawData);
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const updateModel = async (req: Request, res: Response) => {
  const { data } = req.body;
  try {
    await write(`${UNIVERSAL_FOLDER}/${req.params.name}.json`, data);
    res.json("Done");
  } catch (error) {
    // console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteModel = async (req: Request, res: Response) => {
  const { name } = req.params;
  try {
    await unlink(`${UNIVERSAL_FOLDER}/${name}.json`);
    res.json("Deleted");
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};
