import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Gallery } from "../models/Gallery";
import { Values } from "../types";
import { docImages, rmDocDir } from "file-handlers";

export const createGallery = async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { root, ...rest } = values;
  try {
    if (req.body.img || (req.body.gallery && root?.length))
      rest.dir = (await docImages("dist", root)) || "";
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Gallery)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getGallerys = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Gallery)
      .createQueryBuilder("gallery")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getGallery = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Gallery)
      .createQueryBuilder("gallery")
      .where("gallery.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateGallery = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  const { root, ...rest } = values;
  try {
    const data = await AppDataSource.getRepository(Gallery)
      .createQueryBuilder("gallery")
      .where("gallery.id = :id", { id })
      .getOne();
    if (data) {
      if ((values.img || values.gallery) && !data.dir && root?.length)
        rest.dir = (await docImages("dist", root)) || "";
      const updated = await AppDataSource.createQueryBuilder()
        .update(Gallery)
        .set(rest)
        .where("id = :id", { id })
        .execute();
      res.json({ ...rest, id: updated.affected });
    } else {
      return res.status(404).send("Document not found");
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteGallery = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Gallery);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      if (dataToRemove.dir) await rmDocDir(dataToRemove.dir);
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
