/**
 * Аналог "builder" с тем отличием, что создает схемы для одиночных
 * документов (папка singlemodels). Отличие только в пути к файлам.
 * Было решено сделать отдельный роут, что и правильно...
 * Изменены названия методов...
 **/
import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import { capitalizeFirstLetter } from "../middleware/tools";

const readdir = util.promisify(fs.readdir);
const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);
const unlink = util.promisify(fs.unlink);

const SINGLE_FOLDER = `${process.cwd()}/singlemodels`;

export const createSingleModel = async (req: Request, res: Response) => {
  const { data, name } = req.body;
  try {
    if (!name || !name?.length) {
      return res.status(405).json({ message: "Please enter a valid name" });
    }
    await write(`${SINGLE_FOLDER}/${name}.json`, data);
    res.json("Done");
  } catch (error) {
    // console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getSingleModels = async (req: Request, res: Response) => {
  try {
    const files = await readdir(SINGLE_FOLDER);
    const data = files.map((x) => {
      const _x = x.replace(/\.[^/.]+$/, "");
      return capitalizeFirstLetter(_x);
    });
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const getSingleModel = async (req: Request, res: Response) => {
  const { name } = req.params;
  try {
    const rawData = await read(`${SINGLE_FOLDER}/${name}.json`, "utf8");
    const data = JSON.parse(rawData);
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const updateSingleModel = async (req: Request, res: Response) => {
  const { data } = req.body;
  try {
    await write(`${SINGLE_FOLDER}/${req.params.name}.json`, data);
    res.json("Done");
  } catch (error) {
    // console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteSingleModel = async (req: Request, res: Response) => {
  const { name } = req.params;
  try {
    await unlink(`${SINGLE_FOLDER}/${name}.json`);
    res.json("Deleted");
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};
