import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Video } from "../models/Video";

export const createVideo = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Video)
      .values(req.body)
      .execute();
    res.json({ ...req.body, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

// export const getVideos = async (req: Request, res: Response) => {
//   try {
//     const data = await AppDataSource.getRepository(Video)
//       .createQueryBuilder("video")
//       .getMany();
//     res.json(data);
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({ message: "Server error" });
//   }
// };

export const getVideos = async (req: Request, res: Response) => {
  const params: { [key: string]: any } = req.query;
  const { offset, limit } = params;
  try {
    const SELECT_VIDEOS = `SELECT * FROM video ORDER BY "createdAt" DESC${
      offset ? "\n" + `OFFSET ${offset}` : ""
    }${limit ? "\n" + `LIMIT ${limit}` : ""}`;
    const data = await AppDataSource.manager.query(SELECT_VIDEOS);
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getVideo = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Video)
      .createQueryBuilder("video")
      .where("video.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateVideo = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.createQueryBuilder()
      .update(Video)
      .set(req.body)
      .where("id = :id", { id: id })
      .execute();
    res.json({ ...req.body, id: data.affected });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteVideo = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Video);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
