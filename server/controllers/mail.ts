/**
 * 1. Рассылка писем подписчикам - newsletter;
 * 2. Отправка одиночного email, например ордер или контактная форма;
 **/
import { type Request, type Response } from "express";
import fs from "fs";
import util from "util";
// import { Subscriber } from "../models/Subscriber";
import nodemailer from "nodemailer";

const transporter = nodemailer.createTransport({
  host: "smtp.beget.com",
  // port: 2525,
  secure: true,
  auth: {
    user: "no-reply@hch-app.ru",
    pass: "Narzantaria555@",
  },
});

const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);

const DATA_FOLDER = `${process.cwd()}/data`;
const SECRET_KEY = String(process.env.SECRET_KEY);
const PREVIEW_PREFIX = String(process.env.PREVIEW_PREFIX);

const MAIL_API = String(process.env.MAIL_API);
const MAIL_ADDRESS = String(process.env.MAIL_ADDRESS);

const HEADERS = {
  accept: "application/json",
  "api-key": MAIL_API,
  "content-type": "application/json",
};

const mailOptions = {
  from: "Heroes of Might and Magic <no-reply@hch-app.ru>",
  subject: "Message from tavern...✔",
};

export const sendMail = async (req: Request, res: Response) => {
  const { data } = req.body;
  try {
    transporter.sendMail(
      { ...mailOptions, to: MAIL_ADDRESS, html: data },
      function (error, info) {
        if (error != null) {
          console.log(error);
        } else {
          console.log("Email sent: " + info.response);
          res.json("Mail sent successfully");
        }
      }
    );
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const newsLetter = async (req: Request, res: Response) => {
  const { data } = req.body;
  try {
    // const subscribers = await AppDataSource.getRepository(Subscriber)
    //   .createQueryBuilder("subscriber")
    //   .getMany();
    // if (subscribers.length > 0) {
    //   const addresses = subscribers
    //     .map((x) => x.email)
    //     .reduce((accumulator, currentValue) => {
    //       return accumulator + "," + currentValue;
    //     });
    //   transporter.sendMail(
    //     { ...mailOptions, to: addresses, html: data },
    //     function (error, info) {
    //       if (error != null) {
    //         console.log(error);
    //       } else {
    //         console.log("Newsletter sent: " + info.response);
    //         res.json("Successfully sent to subscribers");
    //       }
    //     }
    //   );
    // } else res.json("No subscribers yet");
    res.json("12345");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
