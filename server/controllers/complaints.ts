import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Complaint } from "../models/Complaint";

export const createComplaint = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Complaint)
      .values(req.body)
      .execute();
    res.json({ ...req.body, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getComplaints = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Complaint)
      .createQueryBuilder("complaint")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getComplaint = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Complaint)
      .createQueryBuilder("complaint")
      .where("complaint.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateComplaint = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Complaint)
      .createQueryBuilder("complaint")
      .where("complaint.id = :id", { id })
      .getOne();
    if (data) {
      const updated = await AppDataSource.createQueryBuilder()
        .update(Complaint)
        .set(req.body)
        .where("id = :id", { id })
        .execute();
      res.json({ ...req.body, id: updated.affected });
    } else {
      return res.status(404).send("Document not found");
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteComplaint = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Complaint);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
