import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Social } from "../models/Social";

export const createSocial = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Social)
      .values(req.body)
      .execute();
    res.json({ ...req.body, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getSocials = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Social)
      .createQueryBuilder("social")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getSocial = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Social)
      .createQueryBuilder("social")
      .where("social.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateSocial = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Social)
      .createQueryBuilder("social")
      .where("social.id = :id", { id })
      .getOne();
    if (data) {
      const updated = await AppDataSource.createQueryBuilder()
        .update(Social)
        .set(req.body)
        .where("id = :id", { id })
        .execute();
      res.json({ ...req.body, id: updated.affected });
    } else {
      return res.status(404).send("Document not found");
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteSocial = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Social);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
