import { type Request, type Response } from "express";
import { AppDataSource } from "../db";
import { Movie } from "../models/Movie";
import { Values } from "../types";
import {
  IDocDir,
  docDir,
  docFiles,
  rmDocDir,
} from "file-handlers";

export const createMovie = async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { root, ...rest } = values;
  let dirs: IDocDir | null = null,
    data: any | null;
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    // Проверка медиа:
    if (req.body.photo && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    // Запрос:
    data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Movie)
      .values(rest)
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ (ИЗМЕНИТЬ ТОЛЬКО ОТВЕТ!!!!!!!!!!!!!)
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

export const getMovies = async (req: Request, res: Response) => {
  try {
    const data = await AppDataSource.getRepository(Movie)
      .createQueryBuilder("movie")
      .getMany();
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const getMovie = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const data = await AppDataSource.getRepository(Movie)
      .createQueryBuilder("movie")
      .where("movie.id = :id", { id })
      .getOne();
    if (data == null) {
      res.status(404).send("Document not found");
      return;
    } else {
      res.json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

export const updateMovie = async (req: Request, res: Response) => {
  const { id } = req.params;
  const values: Values = req.body;
  const { root, ...rest } = values;
  let dirs: IDocDir | null = null,
    data: any | null,
    updated: any | null;
  // Proverka
  try {
    data = await AppDataSource.getRepository(Movie)
      .createQueryBuilder("movie")
      .where("movie.id = :id", { id })
      .getOne();
    if (!data) return res.status(404).send("Document not found");
  } catch (error) {
    return res.status(500).send("Server error");
  }
  // 1. CНАЧАЛА ЗАПРОС:
  try {
    if (req.body.photo && !data.dir && root?.length) {
      dirs = docDir();
      rest.dir = `${dirs.subDir}/${dirs.dir}`;
    }
    updated = await AppDataSource.createQueryBuilder()
      .update(Movie)
      .set(rest)
      .where("id = :id", { id })
      .execute();
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ message: "Заполните пожалуйста все необходимые поля" });
  }
  // 2. ФАЙЛОВЫЕ ОПЕРАЦИИ (ИЗМЕНИТЬ ТОЛЬКО ОТВЕТ!!!!!!!!!!!!!)
  try {
    if (dirs) {
      const myDocFiles = await docFiles("dist", root, dirs);
      if (!myDocFiles) {
        return res
          .status(500)
          .json({ message: "Server error, docfiles failure" });
      }
    }
    res.json({ ...rest, id: updated.affected });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

export const deleteMovie = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const dataRepository = AppDataSource.getRepository(Movie);
    const dataToRemove = await dataRepository.findOneBy({
      id: Number(id),
    });
    if (dataToRemove != null) {
      if (dataToRemove.dir) await rmDocDir(dataToRemove.dir);
      await dataRepository.remove(dataToRemove);
      res.json("Removed");
    } else {
      res.status(404).send("Document not found");
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};
