import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

@Entity()
export class Playbill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dir: string;

  @Column()
  title: string;

  @Column("text", { nullable: true })
  text: string;

  @Column()
  photo: string;

  @Column()
  url: string;

  @Column()
  source: string;

  @CreateDateColumn()
  createdAt: Date;

  @Column()
  premiere: string;

  @Column({ nullable: true })
  author: string;

  @Column({ nullable: true })
  director: string;

  @Column({ nullable: true })
  genre: string;
}
