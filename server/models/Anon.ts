import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

@Entity()
export class Anon {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  vendor: string;

  @Column({ nullable: true, default: true })
  status: boolean;

  @CreateDateColumn({ nullable: true })
  lastad: Date;

  @CreateDateColumn()
  createdAt: Date;
}
