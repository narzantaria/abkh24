import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

@Entity()
export class Banner {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dir: string;

  @Column()
  title: string;

  @Column()
  url: string;

  @Column()
  description: string;

  @Column("text")
  text: string;

  @Column()
  img: string;

  @Column()
  position: number;

  @CreateDateColumn()
  createdAt: Date;
}
