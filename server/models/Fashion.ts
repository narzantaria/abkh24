import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from "typeorm";
import { Person } from "./Person";

@Entity()
export class Fashion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  dir: string;

  @Column()
  title: string;

  @Column("text", { nullable: true })
  text: string;

  @Column()
  name: string;

  @Column()
  phone: string;

  @Column({ nullable: true })
  email: string;

  @Column("text", { array: true, nullable: true })
  photos: string[];

  @Column()
  sector: string;

  @Column()
  category: string;

  @Column({ nullable: true })
  gender: string;

  @Column({ nullable: true, default: false })
  kids: boolean;

  @Column({ nullable: true })
  brand: string;

  @CreateDateColumn()
  createdAt: Date;

  @Column("int", { default: 0 })
  price: number;

  @Column("int", { nullable: true, default: 0 })
  looks: number;

  @Column({ nullable: true, default: false })
  sold: boolean;

  @Column()
  district: string;

  @ManyToOne(() => Person, (person) => person.fashions, { onDelete: "CASCADE" })
  author: Person;
}
