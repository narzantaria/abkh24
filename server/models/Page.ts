import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

@Entity()
export class Page {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dir: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column("text")
  text: string;

  @Column("text", { array: true, nullable: true })
  photos: string[];

  @Column({ nullable: true })
  video: string;

  @CreateDateColumn()
  createdAt: Date;
}
