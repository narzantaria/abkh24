import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

@Entity()
export class Funeral {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  dir: string;

  @Column({ nullable: true })
  title: string;

  @Column("text", { nullable: true })
  text: string;

  @Column({ nullable: true })
  photo: string;

  @CreateDateColumn()
  createdAt: Date;
}
