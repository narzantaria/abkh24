import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from "typeorm";
import { Person } from "./Person";

@Entity()
export class Electro {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  dir: string;

  @Column()
  title: string;

  @Column("text", { nullable: true })
  text: string;

  @Column()
  name: string;

  @Column()
  phone: string;

  @Column({ nullable: true })
  email: string;

  @Column("text", { array: true, nullable: true })
  photos: string[];

  @Column()
  sector: string;

  @Column({ nullable: true })
  category: string;

  @Column({ nullable: true })
  brand: string;

  @Column({ nullable: true })
  corpus: string;

  @Column({ nullable: true })
  architecture: string;

  @Column({ nullable: true })
  system: string;

  @Column({ nullable: true })
  ram: string;

  @Column({ nullable: true })
  disk: string;

  @Column({ nullable: true })
  vram: string;

  @Column({ nullable: true })
  cpu: string;

  @Column("float4", { nullable: true })
  hashrate: number;

  @Column("text", { array: true, nullable: true })
  symbols: string[];

  @Column("int", { nullable: true })
  consumption: number;

  @Column({ nullable: true })
  vclass: string;

  @Column({ nullable: true })
  sim: string;

  @Column({ nullable: true })
  diag: string;

  @Column({ nullable: true })
  capacity: string;

  @Column({ nullable: true })
  condition: string;

  @CreateDateColumn()
  createdAt: Date;

  @Column("int", { default: 0 })
  price: number;

  @Column("int", { nullable: true, default: 0 })
  looks: number;

  @Column({ nullable: true, default: false })
  sold: boolean;

  @Column()
  district: string;

  @ManyToOne(() => Person, (person) => person.electros, { onDelete: "CASCADE" })
  author: Person;
}
