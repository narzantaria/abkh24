import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

@Entity()
export class Gallery {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dir: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  img: string;

  @Column("text", { array: true })
  gallery: string[];

  @CreateDateColumn()
  createdAt: Date;
}
