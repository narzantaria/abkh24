import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
  JoinTable,
  ManyToMany,
} from "typeorm";
import { Comment } from "./Comment";
import { Answer } from "./Answer";
import { Post } from "./Post";
import { Complaint } from "./Complaint";
import { Message } from "./Message";
import { Agro } from "./Agro";
import { Animal } from "./Animal";
import { Auto } from "./Auto";
import { Autopart } from "./Autopart";
import { Work } from "./Work";
import { Tourism } from "./Tourism";
import { Service } from "./Service";
import { Home } from "./Home";
import { Hobby } from "./Hobby";
import { Fashion } from "./Fashion";
import { Estate } from "./Estate";
import { Electro } from "./Electro";
import { Children } from "./Children";
import { Building } from "./Building";
import { Beauty } from "./Beauty";
import { Product } from "./Product";

@Entity()
export class Person {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  dir: string;

  @Column()
  title: string;

  @Column({ nullable: true })
  alias: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  email: string;

  @Column()
  password: string;

  @Column()
  status: boolean;

  @Column("text", { array: true, nullable: true })
  photos: string[];

  @CreateDateColumn({ nullable: true })
  birthDate: Date;

  @CreateDateColumn()
  createdAt: Date;

  @Column("text", { nullable: true })
  bio: string;

  @Column({ nullable: true })
  subcription: string;

  @Column({ nullable: true })
  education: string;

  @Column({ nullable: true })
  gender: string;

  @Column({ nullable: true })
  religion: string;

  @Column()
  category: string;

  @Column()
  security: string;

  @CreateDateColumn({ nullable: true })
  lastad: Date;

  @OneToMany(() => Comment, (comment) => comment.author, { eager: true })
  @JoinTable()
  comments: Comment[];

  @OneToMany(() => Answer, (answer) => answer.author, { eager: true })
  @JoinTable()
  answers: Answer[];

  @OneToMany(() => Post, (post) => post.author, { eager: true })
  @JoinTable()
  posts: Post[];

  @OneToMany(() => Complaint, (complaint) => complaint.author, { eager: true })
  @JoinTable()
  complaints: Complaint[];

  @ManyToMany(() => Post, (post) => post.likes)
  @JoinTable()
  likes: Post[];

  @OneToMany(() => Message, (message) => message.sender, { eager: true })
  @JoinTable()
  sent: Message[];

  @OneToMany(() => Message, (message) => message.reciever, { eager: true })
  @JoinTable()
  recieved: Message[];

  @OneToMany(() => Agro, (agro) => agro.author, { eager: true })
  @JoinTable()
  agros: Agro[];

  @OneToMany(() => Animal, (animal) => animal.author, { eager: true })
  @JoinTable()
  animals: Animal[];

  @OneToMany(() => Auto, (auto) => auto.author, { eager: true })
  @JoinTable()
  autos: Auto[];

  @OneToMany(() => Autopart, (autopart) => autopart.author, { eager: true })
  @JoinTable()
  autoparts: Autopart[];

  @OneToMany(() => Work, (work) => work.author, { eager: true })
  @JoinTable()
  works: Work[];

  @OneToMany(() => Tourism, (tourism) => tourism.author, { eager: true })
  @JoinTable()
  tourisms: Tourism[];

  @OneToMany(() => Service, (service) => service.author, { eager: true })
  @JoinTable()
  services: Service[];

  @OneToMany(() => Home, (home) => home.author, { eager: true })
  @JoinTable()
  homes: Home[];

  @OneToMany(() => Hobby, (hobby) => hobby.author, { eager: true })
  @JoinTable()
  hobbys: Hobby[];

  @OneToMany(() => Fashion, (fashion) => fashion.author, { eager: true })
  @JoinTable()
  fashions: Fashion[];

  @OneToMany(() => Estate, (estate) => estate.author, { eager: true })
  @JoinTable()
  estates: Estate[];

  @OneToMany(() => Electro, (electro) => electro.author, { eager: true })
  @JoinTable()
  electros: Electro[];

  @OneToMany(() => Children, (children) => children.author, { eager: true })
  @JoinTable()
  childrens: Children[];

  @OneToMany(() => Building, (building) => building.author, { eager: true })
  @JoinTable()
  buildings: Building[];

  @OneToMany(() => Beauty, (beauty) => beauty.author, { eager: true })
  @JoinTable()
  beautys: Beauty[];

  @OneToMany(() => Product, (product) => product.author, { eager: true })
  @JoinTable()
  products: Product[];
}
