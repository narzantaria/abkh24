import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
  JoinTable,
  ManyToOne,
} from "typeorm";
import { Answer } from "./Answer";
import { Person } from "./Person";
import { Post } from "./Post";

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  dir: string;

  @Column({ nullable: true })
  title: string;

  @Column("text", { nullable: true })
  text: string;

  @Column({ nullable: true })
  img: string;

  @CreateDateColumn()
  createdAt: Date;

  @OneToMany(() => Answer, (answer) => answer.comment, { eager: true })
  @JoinTable()
  answers: Answer[];

  @ManyToOne(() => Person, (person) => person.comments, { onDelete: "CASCADE" })
  author: Person;

  @ManyToOne(() => Post, (post) => post.comments, { onDelete: "CASCADE" })
  post: Post;
}
