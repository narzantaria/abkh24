import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

@Entity()
export class Movie {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dir: string;

  @Column()
  title: string;

  @Column()
  excerpt: string;

  @Column()
  photo: string;

  @Column("float4")
  rating: number;

  @CreateDateColumn()
  premiere: Date;

  @Column()
  url: string;

  @Column()
  director: string;

  @Column()
  genre: string;
}
