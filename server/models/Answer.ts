import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from "typeorm";
import { Person } from "./Person";
import { Comment } from "./Comment";

@Entity()
export class Answer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column("text", { nullable: true })
  text: string;

  @CreateDateColumn()
  createdAt: Date;

  @ManyToOne(() => Person, (person) => person.answers, { onDelete: "CASCADE" })
  author: Person;

  @ManyToOne(() => Comment, (comment) => comment.answers, {
    onDelete: "CASCADE",
  })
  comment: Comment;
}
