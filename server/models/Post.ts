import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
  OneToMany,
  JoinTable,
  ManyToMany,
} from "typeorm";
import { Person } from "./Person";
import { Comment } from "./Comment";

@Entity()
export class Post {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dir: string;

  @Column()
  title: string;

  @Column({ nullable: true })
  description: string;

  @Column("text")
  text: string;

  @Column("text", { array: true, nullable: true })
  photos: string[];

  @Column({ nullable: true })
  video: string;

  @Column({ nullable: true })
  youtube: string;

  @Column({ nullable: true })
  rutube: string;

  @CreateDateColumn()
  createdAt: Date;

  @Column("varchar", { array: true })
  tags: string[];

  // @Column()
  // source: string;

  // @Column()
  // category: string;

  @Column({ nullable: true })
  sourceurl: string;

  // @Column({ nullable: true })
  // youtubevideo: string;

  @Column("int", { nullable: true, default: 0 })
  looks: number;

  @ManyToOne(() => Person, (person) => person.posts, { onDelete: "CASCADE" })
  author: Person;

  @OneToMany(() => Comment, (comment) => comment.post, { eager: true })
  @JoinTable()
  comments: Comment[];

  @ManyToMany(() => Person, (person) => person.likes)
  likes: Person[];
}
