import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from "typeorm";
import { Person } from "./Person";

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("text", { nullable: true })
  text: string;

  @CreateDateColumn()
  createdAt: Date;

  @ManyToOne(() => Person, (person) => person.sent, { onDelete: "CASCADE" })
  sender: Person;

  @ManyToOne(() => Person, (person) => person.recieved, { onDelete: "CASCADE" })
  reciever: Person;
}
