import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

@Entity()
export class Social {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  sid: string;

  @Column()
  status: boolean;

  @CreateDateColumn({ nullable: true })
  lastad: Date;

  @CreateDateColumn()
  createdAt: Date;

  @Column()
  category: string;
}
