import express, { Router } from "express";
import path from "path";

const router = Router();

// router.use(express.static(path.join(__dirname, "./dist")));
router.use(express.static(path.join(process.cwd(), "./dist")));

export default router;
