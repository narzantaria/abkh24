export interface Field {
  name: string;
  label?: string;
  type:
    | "int"
    | "double"
    | "string"
    | "description"
    | "text"
    | "password"
    | "phone"
    | "email"
    | "select"
    | "radio"
    | "img"
    | "gallery"
    | "video"
    | "checkbox"
    | "color"
    | "switch"
    | "vendor"
    | "date"
    | "branch"
    | "tree"
    | "relation"
    | "slider";
  required?: boolean;
  schema?: "one-to-one" | "one-to-many" | "many-to-many" | "many-to-many-bi";
  level?: "host" | "recipient" | "side";
  ref?: string;
  refname?: string;
  unique?: boolean;
  nocascade?: boolean;
  extra?: any;
}

export interface Values {
  [key: string]: any;
}
