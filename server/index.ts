import express, { type Express, type Request, type Response } from "express";
import cors from "cors";
import "dotenv/config";
import * as http from "http";
import bodyParser from "body-parser";

import api from "./routes";
import botapi from "./botapi";
import dist from "./dist";
import { connectDB } from "./db";

import cookieParser from "cookie-parser";
import RedisStore from "connect-redis";
import session from "express-session";
import redisClient from "./store";
import passport from "./passport";
import morgan from "morgan";
import path from "path";
import fs from "fs";
import { mkDirNotExists } from "file-handlers";

const LOGS_DIR = `${process.cwd()}/logs`;

(async () => {
  await mkDirNotExists(LOGS_DIR);
  await mkDirNotExists(`${process.cwd()}/singledocs`);
  await mkDirNotExists(`${process.cwd()}/singlemodels`);
})();

// Redis connection
(async () => {
  try {
    await redisClient.connect();
  } catch (error) {
    console.error;
  }
})();

// Initialize store.
const redisStore = new RedisStore({
  client: redisClient,
  prefix: "myapp:",
});

const app: Express = express();
const server: http.Server = http.createServer(app);

const PORT: number = process.env.PORT != null ? Number(process.env.PORT) : 5000;
const HOST: string =
  process.env.HOST != null ? String(process.env.HOST) : "localhost";

// Requests logger
app.use(
  morgan("combined", {
    skip: function (req, res) {
      return res.statusCode < 400;
    },
    stream: fs.createWriteStream(`${LOGS_DIR}/access.log`, { flags: "a" }),
  })
);

app.use(cookieParser());

// Initialize sesssion storage.
app.use(
  session({
    store: redisStore,
    resave: false, // required: force lightweight session keep alive (touch)
    saveUninitialized: false, // recommended: only save session when data exists
    secret: String(process.env.STORE_SECRET_KEY),
  })
);

// app.use(express.json())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(cors());

app.use(passport.initialize());
app.use(passport.session());

app.get("/", (req: Request, res: Response) => {
  // Это IP-адрес!!!
  console.log(req.socket.remoteAddress);
  res.send("Shutruk-Nahhunte!!!");
});

app.set("view engine", "pug");

app.use("/api", api);
app.use("/botapi", botapi);
app.use("/dist", dist);
app.use(express.static(path.join(process.cwd(), "./logs")));

server.listen(PORT, HOST, () => {
  console.log(`Server started at ${PORT}...`);
});

connectDB();
