import { createClient } from "redis";

const PUB_SECRET_KEY = String(process.env.PUB_SECRET_KEY);
const COOKIE_EXPIRATION = Number(process.env.COOKIE_EXPIRATION);

const redisClient = createClient();

redisClient.on("error", (err) => console.log("Redis Client Error", err));

export default redisClient;
