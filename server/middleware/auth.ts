import { type NextFunction, type Request, type Response } from "express";
import jwt, { type JwtPayload, type Secret } from "jsonwebtoken";

const SECRET_KEY: Secret = String(process.env.SECRET_KEY);
const PUB_SECRET_KEY: Secret = String(process.env.PUB_SECRET_KEY);
const AUTHORISATION: string = String(process.env.AUTHORISATION) || "true";

interface CustomRequest extends Request {
  id: number | JwtPayload;
}

export const auth = (req: Request, res: Response, next: NextFunction) => {
  try {
    if (AUTHORISATION != "true" || !AUTHORISATION) {
      console.log("Authorisation disabled");
      next();
    } else {
      const token = req.headers["auth-token"] as string;
      if (!token) {
        return res
          .status(401)
          .json({ message: "No token, authorisation denied" });
      }
      jwt.verify(token, SECRET_KEY, function (err, decoded) {
        if (err != null) {
          return res
            .status(401)
            .json({ message: "Invalid token, authorisation denied" });
        }
        const payload = decoded as JwtPayload;
        if (payload.exp && payload.exp <= new Date().getTime() / 1000) {
          return res.status(401).json({ message: "Token expired" });
        }
        (req as CustomRequest).id = payload.id;
        next();
      });
    }
  } catch (err) {
    res.status(401).json({ message: "Authentication error" });
  }
};

export const pauth = (req: Request, res: Response, next: NextFunction) => {
  try {
    if (AUTHORISATION != "true" || !AUTHORISATION) {
      console.log("Authorisation disabled");
      next();
    } else {
      const token = req.headers["auth-token"] as string;
      if (!token) {
        return res
          .status(401)
          .json({ message: "No token, authorisation denied" });
      }
      jwt.verify(token, PUB_SECRET_KEY, function (err, decoded) {
        if (err != null) {
          return res
            .status(401)
            .json({ message: "Invalid token, authorisation denied" });
        }
        const payload = decoded as JwtPayload;
        if (payload.exp && payload.exp <= new Date().getTime() / 1000) {
          return res.status(401).json({ message: "Token expired" });
        }
        (req as CustomRequest).id = payload.id;
        next();
      });
    }
  } catch (err) {
    res.status(401).json({ message: "Authentication error" });
  }
};
