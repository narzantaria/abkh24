export function capitalizeFirstLetter(arg: string): string {
  return arg.charAt(0).toUpperCase() + arg.slice(1);
}

export function hstr(arg: object | any[]): string {
  return JSON.stringify(arg).replaceAll('"', "'");
}

export function hparse(arg: string) {
  return JSON.parse(arg.replaceAll("'", '"'));
}

export const isEmail = (arg: string): boolean => {
  const isMatching = String(arg)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
  if (!isMatching) return false;
  return true;
};

export const isPhone = (arg: string): boolean => {
  const isMatching = arg.match(
    /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/
  ); // null - неправильный номер
  if (!isMatching) return false;
  if (arg.length < 12 || arg.length > 13 || arg.slice(0, 1) !== "+")
    return false;
  return true;
};
