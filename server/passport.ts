import passport from "passport";
import facebook from "passport-facebook";

const FACEBOOK_CLIENT_ID = String(process.env.FACEBOOK_CLIENT_ID);
const FACEBOOK_CLIENT_SECRET = String(process.env.FACEBOOK_CLIENT_SECRET);

passport.use(
  new facebook.Strategy(
    {
      clientID: FACEBOOK_CLIENT_ID,
      clientSecret: FACEBOOK_CLIENT_SECRET,
      callbackURL: "/api/pub/facebook/callback",
    },
    async (
      accessToken: string,
      refreshToken: string,
      profile: facebook.Profile,
      cb: any
    ) => {
      return cb(null, profile);
    }
  )
);

passport.serializeUser(function (user, cb) {
  cb(null, user);
});

passport.deserializeUser(function (obj: any, cb) {
  cb(null, obj);
});

export default passport;
