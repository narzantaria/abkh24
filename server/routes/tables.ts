import { Router } from "express";
import { auth } from "../middleware/auth";
import { deleteTable, getTables } from "../controllers/tables";

const router = Router();

router.get("/", auth, getTables);

router.delete("/:name", auth, deleteTable);

export default router;
