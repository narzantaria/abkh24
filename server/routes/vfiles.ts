import { Router } from "express";
import multer, { type Multer } from "multer";
import { auth } from "../middleware/auth";
import { deleteVideo, getVideos, uploadVideo } from "../controllers/vfiles";

const FILES_DIR: string =
  `${process.cwd()}/${(process.env.FILES_DIR as string) || "dist"}` + "/";

const router = Router();
// const storage = multer.memoryStorage();
// const upload: Multer = multer({ storage });

// Configure multer for file upload
const storage = multer.diskStorage({
  destination: "dist/temp/",
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});
const upload: Multer = multer({ storage });

router.get("/", getVideos);

router.post("/", upload.single("file"), uploadVideo);

router.delete("/", auth, deleteVideo);

export default router;
