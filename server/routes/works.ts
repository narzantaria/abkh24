import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createWork,
  deleteWork,
  getWork,
  getWorks,
  updateWork,
} from "../controllers/works";

const router = Router();

router.post("/", auth, createWork);

router.get("/", getWorks);

router.get("/:id", getWork);

router.patch("/common/:id", auth, updateWork);

router.delete("/:id", auth, deleteWork);

export default router;
