import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createTourism,
  deleteTourism,
  getTourism,
  getTourisms,
  updateTourism,
} from "../controllers/tourisms";

const router = Router();

router.post("/", auth, createTourism);

router.get("/", getTourisms);

router.get("/:id", getTourism);

router.patch("/common/:id", auth, updateTourism);

router.delete("/:id", auth, deleteTourism);

export default router;
