import { Router } from "express";
import { auth } from "../middleware/auth";

import {
  createPage,
  deletePage,
  getPage,
  getPages,
  updatePage,
} from "../controllers/pages";

const router = Router();

router.post("/", auth, createPage);

router.get("/", getPages);

router.get("/:id", getPage);

router.patch("/:id", auth, updatePage);

router.delete("/:id", auth, deletePage);

export default router;
