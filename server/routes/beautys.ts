import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createBeauty,
  deleteBeauty,
  getBeauty,
  getBeautys,
  updateBeauty,
} from "../controllers/beautys";

const router = Router();

router.post("/", auth, createBeauty);

router.get("/", getBeautys);

router.get("/:id", getBeauty);

router.patch("/common/:id", auth, updateBeauty);

router.delete("/:id", auth, deleteBeauty);

export default router;
