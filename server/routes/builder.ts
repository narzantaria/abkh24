import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createModel,
  deleteModel,
  getModel,
  getModels,
  updateModel,
} from "../controllers/builder";

const router = Router();

router.post("/", auth, createModel);

router.get("/", getModels);

router.get("/:name", getModel);

router.put("/:name", auth, updateModel);

router.delete("/:name", auth, deleteModel);

export default router;
