import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createAnon,
  deleteAnon,
  getAnon,
  getAnons,
  updateAnon,
} from "../controllers/anons";

const router = Router();

router.post("/", auth, createAnon);

router.get("/", getAnons);

router.get("/:id", getAnon);

router.patch("/common/:id", auth, updateAnon);

router.delete("/:id", auth, deleteAnon);

export default router;
