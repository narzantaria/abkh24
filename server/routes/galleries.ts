import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createGallery,
  deleteGallery,
  getGallery,
  getGallerys,
  updateGallery,
} from "../controllers/galleries";

const router = Router();

router.post("/", auth, createGallery);

router.get("/", getGallerys);

router.get("/:id", getGallery);

router.patch("/:id", auth, updateGallery);

router.delete("/:id", auth, deleteGallery);

export default router;
