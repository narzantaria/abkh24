import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createSingleModel,
  deleteSingleModel,
  getSingleModel,
  getSingleModels,
  updateSingleModel,
} from "../controllers/sbuilder";

const router = Router();

router.post("/", auth, createSingleModel);

router.get("/", getSingleModels);

router.get("/:name", getSingleModel);

router.put("/:name", auth, updateSingleModel);

router.delete("/:name", auth, deleteSingleModel);

export default router;
