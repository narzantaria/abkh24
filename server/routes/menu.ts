import { Router } from "express";
import { auth } from "../middleware/auth";
import { openMenu, updateMenu } from "../controllers/menu";

const router = Router();

router.get("/", openMenu);

router.put("/", auth, updateMenu);

export default router;
