import { Router } from "express";
import { AppDataSource } from "../db";
import { Post } from "../models/Post";

const router = Router();

// Чистка
function processText(text: string) {
  // Убираем лишние переводы строки
  text = text.replace(/\n+/g, "\n");

  // Убираем смайлики
  text = text.replace(
    /([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g,
    ""
  );

  return text;
}

// Create title
function trimText(text: string, len: number) {
  text = text.replace("\n", " ");
  const phrase1 = text.split(".")[0] + ".";
  const phrase2 = text.split("!")[0] + "!";
  const phrase3 = text.split("?")[0] + "?";
  const phrase = [phrase1, phrase2, phrase3].reduce((a, b) => (a > b ? b : a));
  if (phrase.length <= len) return phrase;
  const subPhrase = phrase.substring(0, len);
  const splittedPhrase = subPhrase.split(" ");
  const lastWord = splittedPhrase[splittedPhrase.length - 1];
  if (
    lastWord.slice(-1) === " " ||
    lastWord.slice(-1) === "." ||
    lastWord.slice(-1) === "?" ||
    lastWord.slice(-1) === "!"
  ) {
    return subPhrase.slice(0, -1) + lastWord.slice(-1);
  } else {
    return subPhrase.slice(0, -lastWord.length - 1);
  }
}

router.get("/", async (req, res) => {
  try {
    const dataRepository = AppDataSource.getRepository(Post);
    const data = await dataRepository.findBy({
      // tags: '{"ЧП Абхазии","Абхазия","происшествия"}',
      tags: '{"Народный фронт","Россия","политика","конфликты"}',
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

router.get("/parse", async (req, res) => {
  try {
    const dataRepository = AppDataSource.getRepository(Post);
    const data = await dataRepository.findBy({
      // tags: '{"Народный фронт","Россия","политика","конфликты"}',
      // tags: '{"Республика","Абхазия","политика"}',
      tags: '{"ИБ Пятнашка","Абхазия","Россия","политика","конфликты"}',
    });
    for (let x = 0; x < data.length; x++) {
      const item = data[x];
      const parsedText = processText(item.text);
      item.text = parsedText;
      item.title = trimText(parsedText, 60);
      await dataRepository.save(item);
    }
    res.json("Parsed");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

export default router;

// tags: '{"Народный фронт","Россия","политика","конфликты"}',
// tags: '{"Республика","Абхазия","политика"}',
// tags: '{"ИБ Пятнашка","Абхазия","Россия","политика","конфликты"}'
