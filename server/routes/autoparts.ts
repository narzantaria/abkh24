import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createAutopart,
  deleteAutopart,
  getAutopart,
  getAutoparts,
  updateAutopart,
} from "../controllers/autoparts";

const router = Router();

router.post("/", auth, createAutopart);

router.get("/", getAutoparts);

router.get("/:id", getAutopart);

router.patch("/common/:id", auth, updateAutopart);

router.delete("/:id", auth, deleteAutopart);

export default router;
