import { Router } from "express";
import {
  createUser,
  deleteUser,
  getUser,
  getUsers,
  updateUser,
} from "../controllers/users";
import { auth } from "../middleware/auth";

const router = Router();

router.post("/", auth, createUser);

router.get("/", getUsers);

router.get("/:id", getUser);

router.put("/:id", auth, updateUser);

router.delete("/:id", auth, deleteUser);

export default router;
