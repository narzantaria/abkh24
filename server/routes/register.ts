import { Router } from "express";
import { finishReg, startByEmail, startByPhone } from "../controllers/register";

const router = Router();

router.post("/email", startByEmail);

router.post("/phone", startByPhone);

router.post("/finish", finishReg);

export default router;
