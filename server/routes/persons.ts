import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createPerson,
  deletePerson,
  getPerson,
  getPersons,
  relatePerson,
  updatePerson,
} from "../controllers/persons";

const router = Router();

router.post("/", auth, createPerson);

router.get("/", getPersons);

router.get("/:id", getPerson);

router.patch("/common/:id", auth, updatePerson);

router.patch("/rel/:id", relatePerson);

router.delete("/:id", auth, deletePerson);

export default router;
