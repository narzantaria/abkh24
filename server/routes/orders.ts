import { Router } from "express";
import { auth } from "../middleware/auth";
import { createOrder, deleteOrder, getOrder, getOrders } from "../controllers/orders";

const router = Router();

router.post("/", createOrder);

router.get("/", getOrders);

router.get("/:id", getOrder);

router.delete("/:id", auth, deleteOrder);

export default router;
