import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createMovie,
  deleteMovie,
  getMovie,
  getMovies,
  updateMovie,
} from "../controllers/movies";

const router = Router();

router.post("/", auth, createMovie);

router.get("/", getMovies);

router.get("/:id", getMovie);

router.patch("/common/:id", auth, updateMovie);

router.delete("/:id", auth, deleteMovie);

export default router;
