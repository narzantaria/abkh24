import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createService,
  deleteService,
  getService,
  getServices,
  updateService,
} from "../controllers/services";

const router = Router();

router.post("/", auth, createService);

router.get("/", getServices);

router.get("/:id", getService);

router.patch("/common/:id", auth, updateService);

router.delete("/:id", auth, deleteService);

export default router;
