import { Router, type Request, type Response } from "express";
import passport from "../passport";
import url from "url";
import { pauth } from "../middleware/auth";
import {
  checkPubUser,
  checkSocialUser,
  createCommentPub,
  createItemAnon,
  createItemPub,
  createItemSocial,
  createPostPub,
  deleteCommentPub,
  getData,
  getGoods,
  getMasterContent,
  getPlaybillsPub,
  getPost,
  getPostTags,
  getPosts,
  getUserItem,
  getUserPost,
  loginUser,
  updateCommentPub,
  updateItemLooks,
  updateSelfPub,
  updateUserItem,
  updateUserPost,
} from "../controllers/pub";
import { AppDataSource } from "../db";
import { Social } from "../models/Social";
import { deleteFile } from "../controllers/files";

const CLIENT_URL = String(process.env.CLIENT_URL);

const router = Router();

// ========================

router.get("/login/facebook", passport.authenticate("facebook"));

// Маршрут для обработки ответа от Facebook после авторизации
router.get(
  "/facebook/callback",
  passport.authenticate("facebook", {
    failureRedirect: `${CLIENT_URL}/login2`,
  }),
  async (req: any, res) => {
    // Ищем юзера в БД, если нет - добавляем:
    let social: any | null;
    social = await AppDataSource.getRepository(Social)
      .createQueryBuilder("social")
      .where("social.sid = :sid", { sid: req.user.id })
      .getOne();
    if (!social) {
      await AppDataSource.createQueryBuilder()
        .insert()
        .into(Social)
        .values({
          title: req.user.displayName,
          sid: req.user.id,
          category: "facebook",
          status: true,
        })
        .execute();
    }
    // Не fb (такое не может прийти, но пусть будет для конструкции):
    if (social?.category != "facebook")
      return res.redirect(`${CLIENT_URL}/usererror`);
    // Забанен:
    if (!social?.status) return res.redirect(`${CLIENT_URL}/usererror`);
    // В этой функции можно редиректить на нужную страницу после успешной авторизации,
    // либо возвращать токен для дальнейшего использования на клиенте
    res.redirect(
      url.format({
        pathname: `${CLIENT_URL}/facebook`,
        query: {
          id: req.user.id,
        },
      })
    );
  }
);

// Выход пользователя Facebook:
router.post("/logout", function (req: Request, res: Response, next) {
  req.logout(function (err) {
    if (err) {
      return next(err);
    }
    res.redirect(CLIENT_URL);
  });
});

// ========================

router.get("/", getData);

// Запрос тегов для повторных запросов новостей
router.get("/tags", getPostTags);

// следующие два роута - для клиента, поэтому они созданы отдельно
router.get("/news", getPosts);

router.get("/news/:id", getPost);

// Получение последних 12 объявлений из 16 таблиц!
router.get("/goods", getGoods);

router.get("/master/:id", getMasterContent);

router.get("/goods/:model/:id", getUserItem);

router.get("/goods/looks/:model/:id", updateItemLooks);

router.get("/posts/:id", getUserPost);

router.get("/playbills", getPlaybillsPub);

router.patch("/goods/:model/:id", updateUserItem);

router.patch("/posts/:id", updateUserPost);

router.post("/login", loginUser);

router.post("/check/:id", checkPubUser);

router.post("/scheck/:id", checkSocialUser);

router.post("/posts", pauth, createPostPub);

router.post("/goods/:model", pauth, createItemPub);

router.post("/sgoods/:model", createItemSocial);

router.post("/agoods/:model", createItemAnon);

router.post("/comments", pauth, createCommentPub);

router.patch("/comments/:id", pauth, updateCommentPub);

router.delete("/comments/:id", pauth, deleteCommentPub);

router.patch("/persons/:id", pauth, updateSelfPub);

router.delete("/files", pauth, deleteFile);

export default router;
