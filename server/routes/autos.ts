import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createAuto,
  deleteAuto,
  getAuto,
  getAutos,
  updateAuto,
} from "../controllers/autos";

const router = Router();

router.post("/", auth, createAuto);

router.get("/", getAutos);

router.get("/:id", getAuto);

router.patch("/common/:id", auth, updateAuto);

router.delete("/:id", auth, deleteAuto);

export default router;
