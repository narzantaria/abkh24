import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createVideo,
  deleteVideo,
  getVideo,
  getVideos,
  updateVideo,
} from "../controllers/videos";

const router = Router();

router.post("/", auth, createVideo);

router.get("/", getVideos);

router.get("/:id", getVideo);

router.patch("/:id", auth, updateVideo);

router.delete("/:id", auth, deleteVideo);

export default router;
