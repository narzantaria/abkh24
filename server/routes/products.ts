import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createProduct,
  deleteProduct,
  getProduct,
  getProducts,
  updateProduct,
} from "../controllers/products";

const router = Router();

router.post("/", auth, createProduct);

router.get("/", getProducts);

router.get("/:id", getProduct);

router.patch("/common/:id", auth, updateProduct);

router.delete("/:id", auth, deleteProduct);

export default router;
