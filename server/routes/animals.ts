import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createAnimal,
  deleteAnimal,
  getAnimal,
  getAnimals,
  updateAnimal,
} from "../controllers/animals";

const router = Router();

router.post("/", auth, createAnimal);

router.get("/", getAnimals);

router.get("/:id", getAnimal);

router.patch("/common/:id", auth, updateAnimal);

router.delete("/:id", auth, deleteAnimal);

export default router;
