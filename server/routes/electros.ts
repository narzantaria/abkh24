import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createElectro,
  deleteElectro,
  getElectro,
  getElectros,
  updateElectro,
} from "../controllers/electros";

const router = Router();

router.post("/", auth, createElectro);

router.get("/", getElectros);

router.get("/:id", getElectro);

router.patch("/common/:id", auth, updateElectro);

router.delete("/:id", auth, deleteElectro);

export default router;
