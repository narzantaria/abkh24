import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createFashion,
  deleteFashion,
  getFashion,
  getFashions,
  updateFashion,
} from "../controllers/fashions";

const router = Router();

router.post("/", auth, createFashion);

router.get("/", getFashions);

router.get("/:id", getFashion);

router.patch("/common/:id", auth, updateFashion);

router.delete("/:id", auth, deleteFashion);

export default router;
