import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createDoc,
  getDoc,
  getDocs,
  deleteDoc,
  updateDoc,
} from "../controllers/sdocs";

const router = Router();

router.post("/", auth, createDoc);

router.get("/", getDocs);

router.get("/:name", getDoc);

router.put("/:name", auth, updateDoc);

router.delete("/:name", auth, deleteDoc);

export default router;
