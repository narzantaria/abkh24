import { Router } from "express";
import { auth } from "../middleware/auth";
import { newsLetter, sendMail } from "../controllers/mail";

const router = Router();

router.post("/send", sendMail);

router.post("/newsletter", auth, newsLetter);

export default router;
