import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createHome,
  deleteHome,
  getHome,
  getHomes,
  updateHome,
} from "../controllers/homes";

const router = Router();

router.post("/", auth, createHome);

router.get("/", getHomes);

router.get("/:id", getHome);

router.patch("/common/:id", auth, updateHome);

router.delete("/:id", auth, deleteHome);

export default router;
