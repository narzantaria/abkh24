import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createFuneral,
  deleteFuneral,
  getFuneral,
  getFunerals,
  updateFuneral,
} from "../controllers/funerals";

const router = Router();

router.post("/", auth, createFuneral);

router.get("/", getFunerals);

router.get("/:id", getFuneral);

router.patch("/common/:id", auth, updateFuneral);

router.delete("/:id", auth, deleteFuneral);

export default router;
