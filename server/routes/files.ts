import { Router } from "express";
import multer, { type Multer } from "multer";
import { auth } from "../middleware/auth";
import { deleteFile, getFiles, test, uploadFile } from "../controllers/files";

const router = Router();
const storage = multer.memoryStorage();
const upload: Multer = multer({ storage });
// const storage = multer.diskStorage({
//   destination: "dist/temp/",
//   filename: (req, file, cb) => {
//     cb(null, file.originalname);
//   },
// });
// const upload: Multer = multer({ storage });

router.get("/", getFiles);

// router.post("/", upload.single("file"), uploadFile);
router.post("/", upload.array("files"), uploadFile);

router.delete("/", auth, deleteFile);

router.post('/test', test)

export default router;
