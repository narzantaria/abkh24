import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createChildren,
  deleteChildren,
  getChildren,
  getChildrens,
  updateChildren,
} from "../controllers/childrens";

const router = Router();

router.post("/", auth, createChildren);

router.get("/", getChildrens);

router.get("/:id", getChildren);

router.patch("/common/:id", auth, updateChildren);

router.delete("/:id", auth, deleteChildren);

export default router;
