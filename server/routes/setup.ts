import { Router } from "express";
import { searchUsers, setupUser } from "../controllers/setup";

const router = Router();

router.post("/", setupUser);

router.get("/", searchUsers);

export default router;
