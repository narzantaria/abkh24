import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createComplaint,
  deleteComplaint,
  getComplaint,
  getComplaints,
  updateComplaint,
} from "../controllers/complaints";

const router = Router();

router.post("/", auth, createComplaint);

router.get("/", getComplaints);

router.get("/:id", getComplaint);

router.patch("/common/:id", auth, updateComplaint);

router.delete("/:id", auth, deleteComplaint);

export default router;
