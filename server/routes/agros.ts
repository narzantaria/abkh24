import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createAgro,
  deleteAgro,
  getAgro,
  getAgros,
  updateAgro,
} from "../controllers/agros";

const router = Router();

router.post("/", auth, createAgro);

router.get("/", getAgros);

router.get("/:id", getAgro);

router.patch("/common/:id", auth, updateAgro);

router.delete("/:id", auth, deleteAgro);

export default router;
