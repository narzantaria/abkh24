import { Router } from "express";
import { searchGoods, searchPosts } from "../controllers/search";

const router = Router();

router.get("/posts", searchPosts);

router.get("/goods", searchGoods);

export default router;
