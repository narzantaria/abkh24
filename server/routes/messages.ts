import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createMessage,
  deleteMessage,
  getMessage,
  getMessages,
  updateMessage,
} from "../controllers/messages";

const router = Router();

router.post("/", auth, createMessage);

router.get("/", getMessages);

router.get("/:id", getMessage);

router.patch("/common/:id", auth, updateMessage);

router.delete("/:id", auth, deleteMessage);

export default router;
