import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createSubscriber,
  deleteSubscriber,
  getSubscriber,
  getSubscribers,
  updateSubscriber,
} from "../controllers/subscribers";

const router = Router();

router.post("/", createSubscriber);

router.get("/", getSubscribers);

router.get("/:id", getSubscriber);

router.patch("/:id", auth, updateSubscriber);

router.delete("/:id", auth, deleteSubscriber);

export default router;
