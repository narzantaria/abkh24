import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createPost,
  deletePost,
  getPost,
  getPosts,
  updatePost,
} from "../controllers/posts";

const router = Router();

router.post("/", auth, createPost);

router.get("/", getPosts);

router.get("/:id", getPost);

router.patch("/common/:id", auth, updatePost);

router.delete("/:id", auth, deletePost);

export default router;
