import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createComment,
  deleteComment,
  getComment,
  getComments,
  updateComment,
} from "../controllers/comments";

const router = Router();

router.post("/", auth, createComment);

router.get("/", getComments);

router.get("/:id", getComment);

router.patch("/common/:id", auth, updateComment);

router.delete("/:id", auth, deleteComment);

export default router;
