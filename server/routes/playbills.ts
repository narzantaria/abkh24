import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createPlaybill,
  deletePlaybill,
  getPlaybill,
  getPlaybills,
  updatePlaybill,
} from "../controllers/playbills";

const router = Router();

router.post("/", auth, createPlaybill);

router.get("/", getPlaybills);

router.get("/:id", getPlaybill);

router.patch("/common/:id", auth, updatePlaybill);

router.delete("/:id", auth, deletePlaybill);

export default router;
