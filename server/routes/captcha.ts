import { Router } from "express";
import { getCaptcha } from "../controllers/captcha";

const router = Router();

router.get("/", getCaptcha);

export default router;
