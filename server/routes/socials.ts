import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createSocial,
  deleteSocial,
  getSocial,
  getSocials,
  updateSocial,
} from "../controllers/socials";

const router = Router();

router.post("/", auth, createSocial);

router.get("/", getSocials);

router.get("/:id", getSocial);

router.patch("/common/:id", auth, updateSocial);

router.delete("/:id", auth, deleteSocial);

export default router;
