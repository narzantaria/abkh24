import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createHobby,
  deleteHobby,
  getHobby,
  getHobbys,
  updateHobby,
} from "../controllers/hobbys";

const router = Router();

router.post("/", auth, createHobby);

router.get("/", getHobbys);

router.get("/:id", getHobby);

router.patch("/common/:id", auth, updateHobby);

router.delete("/:id", auth, deleteHobby);

export default router;
