import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createBanner,
  deleteBanner,
  getBanner,
  getBanners,
  updateBanner,
} from "../controllers/banners";

const router = Router();

router.post("/", auth, createBanner);

router.get("/", getBanners);

router.get("/:id", getBanner);

router.patch("/:id", auth, updateBanner);

router.delete("/:id", auth, deleteBanner);

export default router;
