import { Router } from "express";
import { getRoutes, getXmenu, getXmodel, goodsQuery } from "../controllers/goods";

const router = Router();

// router.post("/", auth, createFuneral);

router.get("/router", getRoutes);

router.get("/xmenu/:name/:xname", getXmenu);

router.get("/xmodel/:name/:xname", getXmodel);

// Запрос товаров по параметрам
router.get("/market/:name/:xname", goodsQuery);

// router.get("/:id", getFuneral);

// router.patch("/common/:id", auth, updateFuneral);

// router.delete("/:id", auth, deleteFuneral);

export default router;
