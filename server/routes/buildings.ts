import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createBuilding,
  deleteBuilding,
  getBuilding,
  getBuildings,
  updateBuilding,
} from "../controllers/buildings";

const router = Router();

router.post("/", auth, createBuilding);

router.get("/", getBuildings);

router.get("/:id", getBuilding);

router.patch("/common/:id", auth, updateBuilding);

router.delete("/:id", auth, deleteBuilding);

export default router;
