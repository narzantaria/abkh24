import { Router } from "express";
import { auth } from "../middleware/auth";
import { openSettings, updateSettings } from "../controllers/settings";

const router = Router();

router.get("/", openSettings);

router.put("/", auth, updateSettings);

export default router;
