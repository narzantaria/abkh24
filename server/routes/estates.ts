import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createEstate,
  deleteEstate,
  getEstate,
  getEstates,
  updateEstate,
} from "../controllers/estates";

const router = Router();

router.post("/", auth, createEstate);

router.get("/", getEstates);

router.get("/:id", getEstate);

router.patch("/common/:id", auth, updateEstate);

router.delete("/:id", auth, deleteEstate);

export default router;
