import { Router } from "express";
import { authorize, checkAuth } from "../controllers/auth";
import { auth } from "../middleware/auth";

const router = Router();

router.post("/", authorize);

router.post("/check", auth, checkAuth);

export default router;
