import { Router } from "express";
import { auth } from "../middleware/auth";
import {
  createAnswer,
  deleteAnswer,
  getAnswer,
  getAnswers,
  updateAnswer,
} from "../controllers/answers";

const router = Router();

router.post("/", auth, createAnswer);

router.get("/", getAnswers);

router.get("/:id", getAnswer);

router.patch("/common/:id", auth, updateAnswer);

router.delete("/:id", auth, deleteAnswer);

export default router;
