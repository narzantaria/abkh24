import "reflect-metadata";
import { DataSource } from "typeorm";

const DB_USER = String(process.env.DB_USER);
const DB_PASS = String(process.env.DB_PASS);
const DB_PORT: number = Number(process.env.DB_PORT) | 5432;
const DB = String(process.env.DB);
const DB_HOST = String(process.env.DB_HOST);

export const AppDataSource = new DataSource({
  type: "postgres",
  host: DB_HOST,
  port: DB_PORT,
  username: DB_USER,
  password: DB_PASS,
  database: DB,
  synchronize: true,
  logging: false,
  entities: ["models/*.ts"],
  // entities: ["output/models/*.{ts,js}"],
  migrations: [],
  subscribers: [],
});

export const connectDB = () => {
  AppDataSource.initialize()
    .then(() => {
      console.log("Data Source has been initialized!");
    })
    .catch((err) => {
      console.error("Error during Data Source initialization", err);
    });
};
