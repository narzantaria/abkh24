import { Router, type Request, type Response } from "express";
import fs from "fs";
import util from "util";
import { AppDataSource } from "./db";
import { Values } from "./types";
import { Post } from "./models/Post";
import { Funeral } from "./models/Funeral";
import { Video } from "./models/Video";
import { Banner } from "./models/Banner";
import { Playbill } from "./models/Playbill";
import { Movie } from "./models/Movie";
import sharp from "sharp";
import multer, { type Multer } from "multer";
import { path as ffmpegPath } from "@ffmpeg-installer/ffmpeg";
import ffmpeg from "fluent-ffmpeg";
import { makeId, uniqName } from "file-handlers";
ffmpeg.setFfmpegPath(ffmpegPath);

const unlink = util.promisify(fs.unlink);
const readdir = util.promisify(fs.readdir);
const mkdir = util.promisify(fs.mkdir);
const rename = util.promisify(fs.rename);
const rmdir = util.promisify(fs.rmdir);
const write = util.promisify(fs.writeFile);

const BOT_TOKEN = String(process.env.BOT_TOKEN);
const PREVIEW_PREFIX: string = process.env.PREVIEW_PREFIX as string;
const FILES_DIR = `${process.cwd()}/${process.env.FILES_DIR || "dist"}`;
const DATA_FOLDER = `${process.cwd()}/data`;

interface File {
  buffer: Buffer;
  originalname: string;
}

const router = Router();
const storage = multer.memoryStorage();
const upload: Multer = multer({ storage });

// Configure multer for file upload
const vstorage = multer.diskStorage({
  destination: "dist/temp/",
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});
const vupload: Multer = multer({ storage: vstorage });

/**
 * Собственная функция создания папки для файлов. Отличия:
 * Папка создается сразу, без временной папки, т.к. это бот
 * (работает не через форму, а напрямую)
 **/
async function filesDir(root = "dist"): Promise<string | null> {
  const ROOT_DIR = `${process.cwd()}/${root}`;
  const imagesFolderName = uniqName();
  const subDir = imagesFolderName.slice(0, 2).toLowerCase();
  try {
    const checkSubDir = await readdir(ROOT_DIR, { withFileTypes: true });
    const checkSubDir2 = checkSubDir
      .filter((dirent) => dirent.isDirectory())
      .map((dirent) => dirent.name)
      .some((x) => x == subDir);
    if (!checkSubDir2) {
      await mkdir(`${ROOT_DIR}/${subDir}`);
    }
    await mkdir(`${ROOT_DIR}/${subDir}/${imagesFolderName}`);
    return `${subDir}/${imagesFolderName}`;
  } catch (error) {
    console.log(error);
    return null;
  }
}

// Загрузка файлов от бота:
router.post(
  "/file",
  upload.single("file"),
  async (req: Request, res: Response) => {
    const file: File = req.file as File;
    const botToken = req.headers["bot-token"];
    const root = req.headers["docimages-root"]
      ? String(req.headers["docimages-root"])
      : null;
    if (botToken !== BOT_TOKEN)
      return res.status(403).json({ message: "Adios!" });
    try {
      const dir: string | null = root || (await filesDir("dist"));
      if (!dir) return res.status(500).json({ message: "filesDir error" });
      const newName: string = makeId(20);
      await sharp(file.buffer)
        .resize({ width: 1920 })
        .jpeg({ quality: 50 })
        .toFile(`${FILES_DIR}/${dir}/${newName}.jpg`);
      await sharp(file.buffer)
        .resize({ width: 480, withoutEnlargement: true })
        .jpeg({ quality: 50 })
        .toFile(`${FILES_DIR}/${dir}/${PREVIEW_PREFIX + newName}.jpg`);
      console.log({ dir: dir, file: newName + ".jpg" });
      res.json({ dir: dir, file: newName + ".jpg" });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "Server error" });
    }
  }
);

/*
// Загрузка файлов от бота:
router.post(
  "/file",
  upload.single("file"),
  async (req: Request, res: Response) => {
    const file: File = req.file as File;
    const botToken = req.headers["bot-token"];
    if (botToken !== BOT_TOKEN)
      return res.status(403).json({ message: "Adios!" });
    try {
      const dir: string | null = await filesDir("dist");
      if (!dir) return res.status(500).json({ message: "filesDir error" });
      const newName: string = makeId(20);
      await sharp(file.buffer)
        .resize({ width: 1920 })
        .jpeg({ quality: 50 })
        .toFile(`${FILES_DIR}/${dir}/${newName}.jpg`);
      await sharp(file.buffer)
        .resize({ width: 480, withoutEnlargement: true })
        .jpeg({ quality: 50 })
        .toFile(`${FILES_DIR}/${dir}/${PREVIEW_PREFIX + newName}.jpg`);
      console.log({ dir: dir, file: newName + ".jpg" });
      res.json({ dir: dir, file: newName + ".jpg" });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "Server error" });
    }
  }
);
*/

// Загрузка video от бота:
router.post(
  "/vfile",
  vupload.single("file"),
  async (req: Request, res: Response) => {
    const file: File = req.file as File;
    const botToken = req.headers["bot-token"];
    if (botToken !== BOT_TOKEN)
      return res.status(403).json({ message: "Adios!" });
    try {
      const dir: string | null = await filesDir("dist");
      if (!dir) return res.status(500).json({ message: "filesDir error" });
      const newName: string = makeId(20);
      const inputPath = FILES_DIR + "/temp/" + file.originalname;
      const convertedPath = FILES_DIR + "/" + dir + "/" + newName + ".mp4";
      ffmpeg(inputPath)
        .size(`480x?`)
        .videoCodec("libx264")
        .outputFormat("mp4")
        .outputOptions(["-movflags", "faststart"])
        .output(`${FILES_DIR}/${dir}/${newName}.mp4`)
        .on("end", () => {
          console.log(`Video saved with resolution 480`);
          res.json({ dir: dir, file: `${newName}.mp4` });
        })
        .on("error", (error) => {
          console.error(`Error saving video with resolution 480:`, error);
          res.status(500).json({ message: "Conversion error" });
        })
        .run();
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "Server error" });
    }
  }
);

// Баннер от бота (временно):
router.post("/banners", async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { botToken, ...rest } = values;
  if (botToken !== BOT_TOKEN)
    return res.status(403).json({ message: "Adios!" });
  try {
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Banner)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

// Новость от бота:
router.post("/posts", async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { botToken, ...rest } = values;
  if (botToken !== BOT_TOKEN)
    return res.status(403).json({ message: "Adios!" });
  try {
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Post)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
    // res.json('12345');
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

// Некролог от бота:
router.post("/nec", async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { botToken, ...rest } = values;
  if (botToken !== BOT_TOKEN)
    return res.status(403).json({ message: "Adios!" });
  try {
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Funeral)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

// Видео ТВ от бота:
router.post("/tv", async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { botToken, ...rest } = values;
  if (botToken !== BOT_TOKEN)
    return res.status(403).json({ message: "Adios!" });
  try {
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Video)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

// Афиша от бота:
router.post("/playbills", async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { botToken, ...rest } = values;
  if (botToken !== BOT_TOKEN)
    return res.status(403).json({ message: "Adios!" });
  try {
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Playbill)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

// Kinopoisk от бота:
router.post("/movies", async (req: Request, res: Response) => {
  const values: Values = req.body;
  const { botToken, ...rest } = values;
  if (botToken !== BOT_TOKEN)
    return res.status(403).json({ message: "Adios!" });
  try {
    // if (req.body.photo && root?.length) rest.dir = (await docImages("dist", root)) || "";
    const data = await AppDataSource.createQueryBuilder()
      .insert()
      .into(Movie)
      .values(rest)
      .execute();
    res.json({ ...rest, id: data.generatedMaps[0].id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

// Vremenn:
router.post("/misc", async (req: Request, res: Response) => {
  const { botToken, data } = req.body;
  try {
    if (botToken !== BOT_TOKEN)
      return res.status(403).json({ message: "Adios!" });
    await write(`${DATA_FOLDER}/misc.json`, data);
    res.json("Done");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

// Vremenn:
router.get("/temp", async (req: Request, res: Response) => {
  try {
    // const data = await AppDataSource.manager.query('SELECT * FROM post');
    // for (let x = 0; x < data.length; x++) {
    //   const tags = [data[x].category, data[x].source];
    //   await AppDataSource.createQueryBuilder()
    //     .update(Post)
    //     .set({ tags: tags })
    //     .where("id = :id", { id: data[x].id })
    //     .execute();
    // }
    // res.json(data);
    const data = await AppDataSource.manager.query(
      "SELECT * FROM post WHERE tags[1] = 'наука'"
    );
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

export default router;
