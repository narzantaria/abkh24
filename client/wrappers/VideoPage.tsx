"use client";

import MainSider from "@/components/MainSider";
import MainWrapper from "@/components/MainWrapper";
import NegativeWrapper from "@/components/NegativeWrapper";
import NodataSingle from "@/components/NodataSingle";
import PlayerFixed from "@/components/PlayerFixed";
import SectionTitle from "@/components/SectionTitle";
import { XContainer } from "@/components/StyledUI";
import YoutubePlayer from "@/components/YoutubePlayer";
import { renderDate } from "@/misc/tools";
import { LS } from "@/misc/tv";
import { ITV } from "@/misc/types";
import { Box, Card, Grid, Stack, Typography } from "@mui/material";
import React from "react";

interface IProps {
  data: ITV;
}

export default function VideoPage({ data }: IProps) {
  return (
    <MainWrapper>
      <Box sx={{ paddingBlock: LS }}>
        <XContainer>
          <Grid container spacing={3}>
            <Grid item xs={12} md={8}>
              <NegativeWrapper>
                <Stack spacing={4}>
                  <SectionTitle first={data.title} decorated mb={0} />
                  <Card>
                    {/* <PlayerFixed url={`https://youtu.be/${data.videoId}`} /> */}
                    <YoutubePlayer videoId={data.videoId} />
                  </Card>
                  <Box component="div" sx={{ fontStyle: "italic" }}>
                    {renderDate(data.createdAt)}
                  </Box>
                  {data.source && (
                    <Stack spacing={2} direction="row" flexWrap="wrap">
                      <Typography variant="h6">Ссылка на источник:</Typography>
                      <Typography variant="h6" sx={{ fontStyle: "italic" }}>
                        {data.source}
                      </Typography>
                    </Stack>
                  )}
                </Stack>
              </NegativeWrapper>
            </Grid>
            <Grid item xs={12} md={4}>
              <MainSider />
            </Grid>
          </Grid>
        </XContainer>
      </Box>
    </MainWrapper>
  );
}
