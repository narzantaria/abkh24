"use client";

import NodataSingle from "@/components/NodataSingle";
import Wrapper from "@/components/Wrapper";
import MainForm from "@/form/MainForm";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { reset, setForm } from "@/lib/formSlice";
import { useGetMainDataQuery } from "@/lib/mainApi";
import { IField, IFolder } from "@/misc/types";
import { Snackbar, Stack } from "@mui/material";
import axios from "axios";
import { useRouter } from "next/navigation";
import React, { Fragment, useEffect, useState } from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

interface IProps {
  data: IFolder[] | null;
  model: string | null;
  sector: string | null;
}

const primaryFields: IField[] = [
  {
    name: "title",
    required: true,
    label: "Заголовок",
    type: "string",
  },
  {
    name: "text",
    required: true,
    label: "Текст",
    type: "text",
  },
  {
    name: "name",
    required: true,
    label: "Имя автора",
    type: "string",
  },
  {
    name: "phone",
    required: true,
    label: "Телефон",
    type: "phone",
    extra: [
      { name: "Абхазия", code: "+7", flag: "abkh.png" },
      { name: "Россия", code: "+7", flag: "rus.png" },
    ],
  },
  {
    name: "email",
    label: "Email",
    type: "email",
  },
  {
    name: "photos",
    label: "Фотографии",
    type: "gallery",
  },
];

export default function AddGoods3({ data, model, sector }: IProps) {
  const router = useRouter();
  const { refetch: refetchMainData } = useGetMainDataQuery(null);
  const form = useAppSelector((state: ReduxState) => state.form);
  const { id, token, category, avatar } = useAppSelector(
    (state: ReduxState) => state.auth
  );
  const dispatch = useAppDispatch();
  const [message, setMessage] = useState<string>("");
  const [ready, setReady] = useState<boolean>(false);
  const [fields, setFields] = useState<IField[] | null>(null);

  useEffect(() => {
    if (data && model && sector)
      setFields(
        data
          ?.filter((x) => x.name == model)[0]
          ?.files.filter((x) => x.name == sector)[0]
          .data // .filter((x) => x.name !== "title")
          .map((x) => {
            if (x.type === "checkbox") return { ...x, type: "select" };
            if (x.type === "slider") return { ...x, type: "int" };
            if (x.type === "dslider") return { ...x, type: "double" };
            return x;
          })
      );
  }, [data, model, sector]);

  useEffect(() => {
    (async () => {
      await dispatch(reset());
      // await dispatch(setField({ field: "root", value: `userid${id}` }));
      await dispatch(setForm({ root: `userid${id}`, sector: sector }));
      setReady(true);
    })();
  }, [id]);

  useEffect(() => {
    if ((category !== "google" && !token) || (category === "google" && !id))
      router.push("/login");
  }, [token]);

  if (data && fields && ready) {
    return (
      <Fragment>
        <Wrapper>
          {/* <h3
            onClick={() => {
              console.log(fields);
            }}
          >
            Islamabad
          </h3> */}
          <Stack spacing={4}>
            <Stack spacing={2}>
              <MainForm
                fields={primaryFields.concat(fields || [])}
                callBack={() => {
                  const { clearTemp, detector, looks, ...rest } = form;
                  const keys: string[] = Object.keys(rest);
                  // НЕПРАВИЛЬНЫЙ ПРИЕМ_КОСТЫЛЬ
                  for (let x = 0; x < keys.length; x++) {
                    if (rest[keys[x]] == "да") rest[keys[x]] = true;
                    if (rest[keys[x]] == "нет") rest[keys[x]] = false;
                  }
                  console.log(rest);
                  /**
                   * ЕЩЕ ОДНО ИНТЕРЕСНОЕ ИЗМЕНЕНИЕ, "author" заменили на "authorId". Связано это
                   * с тем, что этот запрос обрабатывается не TypeORM, а нативным SQL.
                   **/
                  if (category)
                    axios
                      .post(
                        `${HOST}/api/pub/${
                          category === "google" ? "s" : ""
                        }goods/${model}`,
                        { ...rest, authorId: id },
                        {
                          headers: {
                            "Content-Type": "application/json",
                            "auth-token": token,
                          },
                        }
                      )
                      .then((res) => {
                        //
                        setMessage("Объявление успешно добавлено");
                        setTimeout(() => {
                          refetchMainData();
                          router.push("/cabinet");
                        }, 3000);
                      })
                      .catch((err) => {
                        console.error(err);
                        const resStatus: number = err.response.status;
                        const errMsg: string = err.response.data.message;
                        if (resStatus === 401) {
                          // dispatch(logout());
                        } else {
                          setMessage(
                            "Пожалуйста заполните все необходимые поля"
                          );
                        }
                      });
                }}
              />
            </Stack>
          </Stack>
        </Wrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else {
    return <NodataSingle />;
  }
}
