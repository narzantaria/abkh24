"use client";

import GoodsPrev from "@/components/GoodsPrev";
import ItemPrev from "@/components/ItemPrev";
import MainItems from "@/components/MainItems";
import MainWrapper from "@/components/MainWrapper";
import NodataGrid from "@/components/NodataGrid";
import SectionTitle from "@/components/SectionTitle";
import { StyledLink, XContainer } from "@/components/StyledUI";
import ThemeWrapper from "@/components/ThemeWrapper";
import { LS } from "@/misc/tv";
import { IFolder, Item } from "@/misc/types";
import { Box, Grid, Stack } from "@mui/material";
import React from "react";

interface IProps {
  data: IFolder[];
  goods: Item[];
}

export default function GoodsMain({ data, goods }: IProps) {
  return (
    <MainWrapper>
      <Box sx={{ paddingBlock: LS }}>
        <XContainer>
          <SectionTitle first="Объявления" decorated />
          <ThemeWrapper>
            <Stack spacing={3}>
              <Grid container spacing={4}>
                {data.map((x) => {
                  return (
                    <Grid item xs={6} md={3} key={x.title}>
                      <StyledLink href={`/goods/${x.name}`}>
                        <GoodsPrev {...x} />
                      </StyledLink>
                    </Grid>
                  );
                })}
              </Grid>
              <Grid container spacing={3}>
                {goods.map((x) => (
                  <Grid item xs={12} md={3}>
                    <StyledLink href={`/goods/${x.source}/${x.sector}/${x.id}`}>
                      <ItemPrev {...x} key={x.id} />
                    </StyledLink>
                  </Grid>
                ))}
              </Grid>
            </Stack>
          </ThemeWrapper>
        </XContainer>
      </Box>
    </MainWrapper>
  );
}
