"use client";

import MainWrapper from "@/components/MainWrapper";
import NegativeWrapper from "@/components/NegativeWrapper";
import NodataLines from "@/components/NodataLines";
import SectionTitle from "@/components/SectionTitle";
import { StyledLink, XContainer } from "@/components/StyledUI";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { resetSparams } from "@/lib/searchSlice";
import { up } from "@/misc/tools";
import { LS } from "@/misc/tv";
import { Box, Stack } from "@mui/material";
import axios from "axios";
import { useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

interface ISearchResult {
  id: number;
  title: string;
  source?: string | null;
  sector?: string | null;
}

export default function SearchPage() {
  // SIC!
  const router = useRouter();
  const { path, word } = useAppSelector((state: ReduxState) => state.search);
  const dispatch = useAppDispatch();
  const [data, setData] = useState<ISearchResult[] | null>(null);

  useEffect(() => {
    // window.scrollTo(0, 0);
    up();
  }, []);

  useEffect(() => {
    if (path?.length && word?.length) {
      axios
        .get(`${HOST}/api/search/${path}`, {
          params: { word: word },
        })
        .then((res) => {
          setData(res.data);
        })
        .catch((err) => console.log(err));
    } else router.push("/");
    return () => {
      dispatch(resetSparams());
    };
  }, [path, word]);

  if (data) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <SectionTitle first="Результаты" second="поиска" decorated />
            <NegativeWrapper>
              <Stack spacing={3}>
                {data.map((x) => {
                  if (path == "goods" && x.sector && x.source) {
                    return (
                      <StyledLink
                        href={`/goods/${x.source}/${x.sector}/${x.id}`}
                      >
                        {x.title}
                      </StyledLink>
                    );
                  } else {
                    return (
                      <StyledLink href={`/news/${x.id}`}>{x.title}</StyledLink>
                    );
                  }
                })}
              </Stack>
            </NegativeWrapper>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else {
    return <NodataLines />;
  }
}
