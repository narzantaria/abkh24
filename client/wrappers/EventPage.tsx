"use client";

import MainSider from "@/components/MainSider";
import MainWrapper from "@/components/MainWrapper";
import NegativeWrapper from "@/components/NegativeWrapper";
import NodataSingle from "@/components/NodataSingle";
import SectionTitle from "@/components/SectionTitle";
import { XContainer } from "@/components/StyledUI";
import { LS } from "@/misc/tv";
import { IPlaybill } from "@/misc/types";
import { Box, Grid, Stack, Typography } from "@mui/material";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

export default function EventPage({ data }: { data: IPlaybill }) {
  return (
    <MainWrapper>
      <Box sx={{ paddingBlock: LS }}>
        <XContainer>
          <Grid container spacing={3}>
            <Grid item xs={12} md={8}>
              <NegativeWrapper>
                <Stack spacing={4}>
                  {data.title && (
                    <SectionTitle first={data.title} decorated mb={0} />
                  )}
                  {data.photo && data.dir && (
                    <Box
                      component="img"
                      sx={{ width: 555, maxWidth: "100%" }}
                      src={`${HOST}/dist/${data.dir}/${data.photo}`}
                    />
                  )}
                  {data.premiere && (
                    <Box component="div" sx={{ fontStyle: "italic" }}>
                      {new Date(data.premiere).toISOString().slice(0, 10)}
                    </Box>
                  )}
                  {data.text && (
                    <Box
                      component="div"
                      dangerouslySetInnerHTML={{ __html: data.text }}
                    />
                  )}
                  {data.source && (
                    <Stack spacing={2} direction="row" flexWrap="wrap">
                      <Typography variant="h6">Ссылка на источник:</Typography>
                      <Typography variant="h6" sx={{ fontStyle: "italic" }}>
                        {data.source}
                      </Typography>
                    </Stack>
                  )}
                </Stack>
              </NegativeWrapper>
            </Grid>
            <Grid item xs={12} md={4}>
              <MainSider />
            </Grid>
          </Grid>
        </XContainer>
      </Box>
    </MainWrapper>
  );
}
