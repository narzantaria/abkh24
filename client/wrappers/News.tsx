"use client";

import MainWrapper from "@/components/MainWrapper";
import Newsletter from "@/components/Newsletter";
import NodataGrid from "@/components/NodataGrid";
import PrevFull from "@/components/PrevFull";
import SectionTitle from "@/components/SectionTitle";
import { ColTitle, XButton4, XContainer } from "@/components/StyledUI";
import ThemeWrapper from "@/components/ThemeWrapper";
import XAccodrion from "@/components/XAccodrion";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { useGetTagsDataQuery } from "@/lib/tagsApi";
import { resetTags, setTags } from "@/lib/tagsSlice";
import {
  modifyArr,
  tagColor,
  up,
  useIsMount,
  useScrollLock,
} from "@/misc/tools";
import { LS, XSS } from "@/misc/tv";
import { IPost } from "@/misc/types";
import {
  Accordion,
  AccordionDetails,
  AccordionProps,
  AccordionSummary,
  AccordionSummaryProps,
  Box,
  Chip,
  Grid,
  Stack,
  Typography,
  styled,
  useMediaQuery,
} from "@mui/material";
import axios from "axios";
import React, { Fragment, ReactNode, useEffect, useRef, useState } from "react";
import { AiOutlineRight } from "react-icons/ai";

const HOST = process.env.NEXT_PUBLIC_HOST;
const LIMIT = 5;
const OFFSET = 5;

export default function News() {
  const { data, error, isLoading } = useGetTagsDataQuery(null);
  const tags = useAppSelector((state: ReduxState) => state?.tags);
  const dispatch = useAppDispatch();
  const isMount = useIsMount();
  const [posts, setPosts] = useState<IPost[]>([]);
  const [increment, setIncrement] = useState<number>(0);
  const [loading, setLoading] = useState(true);
  const [finished, setFinished] = useState(false);
  const hiddenRef = useRef<HTMLDivElement>(null);
  const [page, setPage] = useState<number>(1);
  const { lockScroll, unlockScroll } = useScrollLock();
  const matches = useMediaQuery("(max-width: 900px)");  

  useEffect(() => {
    // window.scrollTo(0, 0);
    up();
  }, []);

  const loadPosts = async (pageNumber: number) => {
    try {
      const response = await axios.get(`${HOST}/api/pub/news`, {
        params: {
          tags: tags,
          offset: (pageNumber - 1) * OFFSET,
          limit: LIMIT,
        },
      });

      const newPosts = response.data;
      if (!newPosts.length) {
        setFinished(true);
        window.removeEventListener("scroll", handleScroll);
      } else if (pageNumber === 1) {
        setPosts(newPosts);
      } else {
        setPosts((prevPosts) => [...prevPosts, ...newPosts]);
      }
    } catch (error) {
      console.error(error);
    }
    unlockScroll();
    setLoading(false);
  };

  useEffect(() => {
    loadPosts(page);
  }, [page]);

  const handleScroll = () => {
    if (
      hiddenRef.current &&
      window.scrollY + window.innerHeight >= hiddenRef.current.offsetTop + 300
    ) {
      lockScroll();
      setLoading(true);
      setTimeout(() => {
        const pageNumber = Math.ceil(posts.length / LIMIT) + 1;
        setPage(pageNumber);
      }, 500);
    }
  };

  useEffect(() => {
    if (!finished) window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [posts, loading]);

  // СМЫСЛ ЭТОЙ ШТУКИ В СБРОСЕ???
  useEffect(() => {
    if (!isMount) loadPosts(1);
  }, [increment]);

  if (data) {
    return (
      <Fragment>
        <MainWrapper>
          <Box sx={{ paddingBlock: LS }}>
            <XContainer>
              <SectionTitle first="Новости" decorated />
              <Grid container spacing={4}>
                {matches && (
                  <Grid item sm={12}>
                    <XAccodrion title="Теги">
                      <Stack spacing={2}>
                        <ThemeWrapper>
                          <TagsContainer
                            callBack={() => {
                              setIncrement(increment + 1);
                              lockScroll();
                              setLoading(true);
                              setFinished(false);
                            }}
                          />
                        </ThemeWrapper>
                        <XButton4
                          onClick={() => {
                            dispatch(resetTags());
                            setIncrement(increment + 1);
                            lockScroll();
                            setLoading(true);
                            setFinished(false);
                          }}
                        >
                          Сбросить
                        </XButton4>
                      </Stack>
                    </XAccodrion>
                  </Grid>
                )}
                <Grid item sm={12} md={8} order={{ md: 1, sm: 2 }}>
                  <ThemeWrapper>
                    <Stack spacing={3}>
                      {posts?.map((x) => {
                        return (
                          <PrevFull
                            key={x.id}
                            {...x}
                            tagClr={tagColor(x.tags[0])}
                          />
                        );
                      })}
                    </Stack>
                  </ThemeWrapper>
                </Grid>
                {!matches && (
                  <Grid item sm={0} md={4} order={{ md: 2, sm: 1 }}>
                    <Stack spacing={3}>
                      <ColTitle>Теги</ColTitle>
                      <ThemeWrapper>
                        <TagsContainer
                          callBack={() => {
                            setIncrement(increment + 1);
                            lockScroll();
                            setLoading(true);
                            setFinished(false);
                          }}
                        />
                      </ThemeWrapper>
                      <XButton4
                        onClick={() => {
                          dispatch(resetTags());
                          setIncrement(increment + 1);
                          lockScroll();
                          setLoading(true);
                          setFinished(false);
                        }}
                      >
                        Сбросить
                      </XButton4>
                      <Newsletter />
                    </Stack>
                  </Grid>
                )}
              </Grid>
            </XContainer>
          </Box>
        </MainWrapper>
        <div ref={hiddenRef}></div>
        {loading && <NodataGrid />}
      </Fragment>
    );
  } else return <NodataGrid />;
}

function TagsContainer({ callBack }: { callBack: () => void }) {
  const { data, error, isLoading } = useGetTagsDataQuery(null);
  const tags = useAppSelector((state: ReduxState) => state?.tags);
  const dispatch = useAppDispatch();

  return (
    <Box
      sx={{
        display: "flex",
        columnGap: XSS,
        rowGap: XSS,
        flexWrap: "wrap",
      }}
    >
      {data?.map((x) => (
        <Chip
          key={x}
          sx={{
            cursor: "pointer",
            backgroundColor: tags.some((y) => y == x) ? "#ddd" : "initial",
          }}
          label={x}
          variant="outlined"
          onClick={() => {
            dispatch(setTags(modifyArr(tags, x)));
            callBack();
          }}
        />
      ))}
    </Box>
  );
}
