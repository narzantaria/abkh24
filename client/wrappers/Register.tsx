"use client";

import { Title2, XContainer } from "@/components/StyledUI";
import MainForm from "@/form/MainForm";
import { isEmail, isPhone, makeId, parsePhone } from "@/misc/tools";
import { XLS } from "@/misc/tv";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import {
  Box,
  FormControlLabel,
  Radio,
  RadioGroup,
  Snackbar,
  Stack,
  Typography,
} from "@mui/material";
import axios from "axios";
import React, { ChangeEvent, Fragment, useEffect, useState } from "react";
import { setForm } from "@/lib/formSlice";
import { useRouter } from "next/navigation";

const HOST = process.env.NEXT_PUBLIC_HOST;

const emailMethod = [
  {
    name: "email",
    label: "Email",
    type: "email",
  },
];

const phoneMethod = [
  {
    name: "phone",
    required: true,
    label: "Телефон",
    type: "phone",
    extra: [
      { name: "Абхазия", code: "+7", flag: "abkh.png" },
      { name: "Россия", code: "+7", flag: "rus.png" },
    ],
  },
];

export default function Register() {
  // SIC!
  const router = useRouter();
  const [method, setMethod] = useState<string>("email");
  const [message, setMessage] = useState<string>("");
  const { email, gender, phone, religion, requestId, root } = useAppSelector(
    (state: ReduxState) => state?.form
  );
  const dispatch = useAppDispatch();
  const newRequestId = makeId(10);

  useEffect(() => {
    if (requestId && root && gender && religion) router.push("/finish");
  }, [requestId, root, gender, religion]);

  return (
    <Fragment>
      <main>
        <Box sx={{ paddingBlock: XLS }}>
          <XContainer>
            <Box sx={{ width: 555, maxWidth: "100%" }}>
              <Stack spacing={3}>
                <Title2>Регистрация</Title2>
                <Typography variant="body1">
                  Введите Email для регистрации. На него придет код подтверждения.
                </Typography>
                {/* <Typography variant="body1">
                  Выберите метод регистрации. В зависимости от выбранного метода
                  вам придет код верификации на указанный номер телефона или
                  Email. При регистрации через телефон смс приходит только на
                  номера российских операторов.
                </Typography>
                <RadioGroup
                  row
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  name="row-radio-buttons-group"
                  value={method}
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    setMethod(e.target.value)
                  }
                >
                  <FormControlLabel
                    key="email"
                    value="email"
                    control={<Radio />}
                    label="Email"
                  />
                  <FormControlLabel
                    key="phone"
                    value="phone"
                    control={<Radio />}
                    label="Телефон"
                  />
                </RadioGroup> */}
                <MainForm
                  fields={method === "email" ? emailMethod : phoneMethod}
                  btntext="Далее"
                  callBack={() => {
                    if (method === "phone") {
                      const parsedPhone = parsePhone(phone);
                      if (!parsedPhone) {
                        setMessage("Введите правильный номер телефона");
                        return;
                      }
                      const phoneNumber = parsedPhone.code + parsedPhone.value;
                      const isPhoneNumber = isPhone(phoneNumber);
                      if (isPhoneNumber) {
                        axios
                          .post(
                            `${HOST}/api/register/phone`,
                            {
                              requestId: newRequestId,
                              phone: `7${parsedPhone.value}`,
                            },
                            {
                              headers: {
                                "Content-Type": "application/json",
                              },
                            }
                          )
                          .then((res) => {
                            setMessage("Document created successfully");
                            dispatch(
                              setForm({
                                requestId: newRequestId,
                                root: newRequestId,
                                gender: "не указывать",
                                religion: "не указывать",
                                status: true,
                              })
                            );
                          })
                          .catch((err) => {
                            const resStatus: number = err.response.status;
                            const errMsg: string = err.response.data.message;
                            setMessage(errMsg);
                          });
                      } else {
                        setMessage("Пожалуйста, введите правильный номер");
                      }
                    } else if (method === "email") {
                      const isEmailResult = isEmail(email);
                      if (isEmailResult) {
                        axios
                          .post(
                            `${HOST}/api/register/email`,
                            { requestId: newRequestId, email: email },
                            {
                              headers: {
                                "Content-Type": "application/json",
                              },
                            }
                          )
                          .then((res) => {
                            setMessage("Document created successfully");
                            dispatch(
                              setForm({
                                requestId: newRequestId,
                                root: newRequestId,
                                gender: "не указывать",
                                religion: "не указывать",
                                status: true,
                              })
                            );
                          })
                          .catch((err) => {
                            console.log(err);
                            const resStatus: number = err.response.status;
                            const errMsg: string = err.response.data.message;
                            setMessage(errMsg);
                          });
                      } else {
                        setMessage("Пожалуйста, введите правильный Email");
                      }
                    }
                  }}
                />
              </Stack>
            </Box>
          </XContainer>
        </Box>
      </main>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
