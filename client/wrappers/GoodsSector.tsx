"use client";

import Crumbs from "@/components/Crumbs";
import MainWrapper from "@/components/MainWrapper";
import NegativeWrapper from "@/components/NegativeWrapper";
import NodataLines from "@/components/NodataLines";
import { StyledLink, Title3, XContainer } from "@/components/StyledUI";
import { LS } from "@/misc/tv";
import { IFolder } from "@/misc/types";
import { Box, Stack } from "@mui/material";
import React from "react";

export default function GoodsSector({
  data,
  model,
}: {
  data: IFolder[];
  model: string;
}) {
  return (
    <MainWrapper>
      <Box sx={{ paddingBlock: LS }}>
        <NegativeWrapper>
          <XContainer>
            <Stack spacing={4}>
              <Crumbs
                data={[
                  {
                    link: "/",
                    title: "Главная",
                  },
                  {
                    link: "goods",
                    title: "Объявления",
                  },
                  {
                    title: data.filter((x) => x.name == model)[0]?.title,
                  },
                ]}
              />
              <Stack spacing={2}>
                {data
                  .filter((x) => x.name == model)[0]
                  ?.files.map((y) => {
                    return (
                      <Title3 variant="h3" key={y.name}>
                        <StyledLink href={`/goods/${model}/${y.name}`}>
                          {y.data.filter((z) => z.name === "title")[0].label}
                        </StyledLink>
                      </Title3>
                    );
                  })}
              </Stack>
            </Stack>
          </XContainer>
        </NegativeWrapper>
      </Box>
    </MainWrapper>
  );
}
