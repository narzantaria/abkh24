"use client";

import MainWrapper from "@/components/MainWrapper";
import NegativeWrapper from "@/components/NegativeWrapper";
import NodataSingle from "@/components/NodataSingle";
import { XContainer } from "@/components/StyledUI";
import MainForm from "@/form/MainForm";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { reset, setForm } from "@/lib/formSlice";
import { useIsMount } from "@/misc/tools";
import { LS } from "@/misc/tv";
import { IComment, IField } from "@/misc/types";
import { Box, Snackbar } from "@mui/material";
import axios from "axios";
import { useRouter } from "next/navigation";
import React, { Fragment, useEffect, useState } from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

const fields: IField[] = [
  {
    name: "title",
    label: "Title",
    type: "string",
  },
  {
    name: "text",
    label: "Text",
    type: "text",
  },
  {
    name: "img",
    label: "Image",
    type: "img",
  },
];

export default function CommentPage({ data, slug }: { data: IComment; slug: string }) {
  const router = useRouter();
  const { author, ...form } = useAppSelector((state: ReduxState) => state.form);
  const { category, id, token } = useAppSelector(
    (state: ReduxState) => state.auth
  );
  const dispatch = useAppDispatch();

  const [message, setMessage] = useState<string>("");
  const [ready, setReady] = useState<boolean>(false);

  useEffect(() => {
    // axios.get(`${HOST}/api/comments/${slug}`).then(async (res) => {
    //   await dispatch(reset());
    //   await dispatch(setForm(res.data));
    //   setReady(true);
    // });
    (async ()=>{
      await dispatch(reset());
      await dispatch(setForm(data));
      setReady(true);
    })()
  }, [data]);

  useEffect(() => {
    if (author?.id && author?.id != id) router.push("/");
  }, [author?.id, id]);

  if (ready) {
    return (
      <Fragment>
        <MainWrapper>
          <Box sx={{ paddingBlock: LS }}>
            <XContainer>
              <NegativeWrapper>
                <MainForm
                  fields={fields}
                  callBack={() => {
                    if (author?.id == id) {
                      const { clearTemp, category, ...rest } = form;
                      axios
                        .patch(
                          `${HOST}/api/pub/comments/${slug}`,
                          { ...rest, editorId: id },
                          {
                            headers: {
                              "Content-Type": "application/json",
                              "auth-token": token,
                            },
                          }
                        )
                        .then(async (res) => {
                          await setForm(res.data);
                          setMessage("Комментарий обновлен");
                        })
                        .catch((err) => {
                          const resStatus: number = err.response.status;
                          const errMsg: string = err.response.data.message;
                          // dispatch(logout());
                          setMessage(errMsg);
                        });
                    }
                  }}
                />
              </NegativeWrapper>
            </XContainer>
          </Box>
        </MainWrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else return <NodataSingle />;
}
