"use client";

import MainSider from "@/components/MainSider";
import MainWrapper from "@/components/MainWrapper";
import NodataGrid from "@/components/NodataGrid";
import PlaybillPrev from "@/components/PlaybillPrev";
import SectionTitle from "@/components/SectionTitle";
import { XContainer } from "@/components/StyledUI";
import ThemeWrapper from "@/components/ThemeWrapper";
import { LS } from "@/misc/tv";
import { IPlaybill } from "@/misc/types";
import { Box, Grid } from "@mui/material";
import React from "react";

export default function Events({ data }: { data: IPlaybill[] }) {
  return (
    <MainWrapper>
      <Box sx={{ paddingBlock: LS }}>
        <XContainer>
          <SectionTitle first="Новости ТВ" decorated />
          <Grid container spacing={4}>
            <Grid item sm={12} md={8}>
              <ThemeWrapper>
                <Grid container spacing={4}>
                  {data?.map((x) => {
                    return (
                      <Grid item xs={12} md={6}>
                        <PlaybillPrev key={x.id} {...x} />
                      </Grid>
                    );
                  })}
                </Grid>
              </ThemeWrapper>
            </Grid>
            <Grid item sm={12} md={4}>
              <MainSider />
            </Grid>
          </Grid>
        </XContainer>
      </Box>
    </MainWrapper>
  );
}
