"use client";

import { Title2, XButton3, XButton4, XButton5, XContainer } from "@/components/StyledUI";
import { XLS } from "@/misc/tv";
import { Box, Stack } from "@mui/material";
import Link from "next/link";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

interface Props {
  title: string;
  img: string;
}

export default function ErrWrapper({ img, title }: Props) {
  return (
    <main>
      <Box sx={{ paddingBlock: XLS }}>
        <XContainer>
          <Box sx={{ width: 555, maxWidth: "100%" }}>
            <Stack spacing={3}>
              <Title2>{title}</Title2>
              <img src={`${HOST}/dist/common/${img}`} alt="" />
              <Link href="/">
                <XButton5>Вернуться на главную</XButton5>
              </Link>
            </Stack>
          </Box>
        </XContainer>
      </Box>
    </main>
  );
}
