"use client";

import MainWrapper from "@/components/MainWrapper";
import NegativeWrapper from "@/components/NegativeWrapper";
import NodataSingle from "@/components/NodataSingle";
import PhotoSlider from "@/components/PhotoSlider";
import SectionTitle from "@/components/SectionTitle";
import { XContainer } from "@/components/StyledUI";
import { LS } from "@/misc/tv";
import { IPage } from "@/misc/types";
import { Box, Card, Stack } from "@mui/material";
import React from "react";
import ReactPlayer from "react-player";

const HOST = process.env.NEXT_PUBLIC_HOST;
const SITE = process.env.NEXT_PUBLIC_SITE;

export default function Misc({ data }: { data: IPage }) {
  return (
    <MainWrapper>
      <Box sx={{ paddingBlock: LS }}>
        <XContainer>
          <NegativeWrapper>
            <Stack spacing={4}>
              <SectionTitle first={data.title} decorated mb={0} />
              {data.photos?.length === 1 && data.dir && (
                <Box
                  component="img"
                  sx={{ width: 555, maxWidth: "100%" }}
                  src={`${HOST}/dist/${data.dir}/${data.photos[0]}`}
                />
              )}
              {data.photos?.length && data.photos?.length > 1 && data.dir && (
                <PhotoSlider
                  images={data.photos.map(
                    (x) => `${HOST}/dist/${data.dir}/${x}`
                  )}
                />
              )}
              {data.video && (
                <Card sx={{ width: "min-content", maxWidth: "100%" }}>
                  <ReactPlayer
                    style={{ maxWidth: "100%" }}
                    controls
                    url={`${HOST}/dist/${data.dir}/${data.video}`}
                  />
                </Card>
              )}
              {data.text && (
                <Box
                  component="div"
                  dangerouslySetInnerHTML={{ __html: data.text }}
                />
              )}
              {/* <XButton5 onClick={() => console.log(data)}>12345</XButton5> */}
            </Stack>
          </NegativeWrapper>
        </XContainer>
      </Box>
    </MainWrapper>
  );
}
