"use client";

import {
  StyledLink,
  Title2,
  Title5,
  XButton2,
  XButton3,
  XButton4,
  XButton5,
} from "@/components/StyledUI";
import { Avatar, Box, Stack, Typography } from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import Wrapper from "@/components/Wrapper";
import { up, useIsMount } from "@/misc/tools";
import Link from "next/link";
import { useGetUserContentDataQuery } from "@/lib/userContentApi";
import NodataForm from "@/components/NodataForm";
import MainWrapper from "@/components/MainWrapper";
import { useRouter } from "next/navigation";
import { AiOutlineLogout } from "react-icons/ai";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { reset } from "@/lib/formSlice";
import { logout } from "@/lib/authSlice";

const HOST = process.env.NEXT_PUBLIC_HOST;

function categorys(arg: string): string | null {
  switch (arg) {
    case "user":
      return "Пользователь";
    case "google":
      return "Пользователь Google";
    case "moderator":
      return "Модератор";
    case "editor":
      return "Редактор";
    default:
      return null;
  }
}

const religions: { [field: string]: string | null } = {
  христианство: "christianity",
  ислам: "islam",
  иудаизм: "judaism",
  буддизм: "buddhism",
  конфуцианство: "confucianism",
  даосизм: "taosism",
  атеизм: "data",
  "не указывать": null,
};

export default function Cabinet() {
  const router = useRouter();
  const [login, setLogin] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const isMount = useIsMount();
  const dispatch = useAppDispatch();
  const { id, name, avatar, category, religion, subcription, token } =
    useAppSelector((state: ReduxState) => state.auth);
  const form = useAppSelector((state: ReduxState) => state.form);
  const { data, error, isLoading, refetch } = useGetUserContentDataQuery(
    String(id)
  );

  useEffect(() => {
    if ((category !== "google" && !token) || (category === "google" && !id))
      router.push("/login");
  }, [avatar]);

  useEffect(() => {
    up();
    refetch();
  }, []);

  if (name && avatar && category === "google") {
    return (
      <MainWrapper>
        <Wrapper>
          <Box sx={{ width: 555, maxWidth: "100%" }}>
            <Stack spacing={3}>
              <Title2>{`Личный кабинет (${name})`}</Title2>
              <Avatar
                src={avatar}
                sx={{ width: "200px", height: "200px", maxWidth: "100%" }}
              />
              <Typography variant="h5">{categorys(category)}</Typography>
              <Link href="addgoods">
                <XButton4>Дать объявление</XButton4>
              </Link>
            </Stack>
          </Box>
        </Wrapper>
      </MainWrapper>
    );
  }

  if (data && name && avatar && category && token) {
    return (
      <Wrapper>
        <Box sx={{ width: 555, maxWidth: "100%" }}>
          <Stack spacing={3}>
            <Title2>{`Личный кабинет (${name})`}</Title2>
            {subcription && (
              <Typography variant="caption" sx={{ fontStyle: "italic" }}>
                {subcription}
              </Typography>
            )}
            <Avatar
              src={avatar}
              sx={{ width: "200px", height: "200px", maxWidth: "100%" }}
            />
            <Stack direction="row" spacing={2} sx={{ alignItems: "center" }}>
              <Typography variant="h5">{categorys(category)}</Typography>
              {religions[religion] && (
                <Avatar
                  sx={{ height: "25px", width: "25px" }}
                  src={`icons/${religions[religion]}.png`}
                />
              )}
            </Stack>
            {(category === "user" ||
              category === "moderator" ||
              category === "editor") && (
              <XButton2>
                <StyledLink href="/cabinet/edit">
                  Редактировать данные
                </StyledLink>
              </XButton2>
            )}
            {data?.goods?.length ? (
              <Fragment>
                <Typography variant="h5">Объявления пользователя</Typography>
                {data.goods.map((item: { [data: string]: any }) => {
                  return (
                    <Title5 variant="h5" key={item.id}>
                      <StyledLink
                        href={`cabinet/goods/${item.source}/${item.id}`}
                      >
                        {item.title}
                      </StyledLink>
                    </Title5>
                  );
                })}
              </Fragment>
            ) : null}
            {data?.posts?.length ? (
              <Fragment>
                <Typography variant="h5">Публикации пользователя</Typography>
                {data.posts.map((item: { [data: string]: any }) => {
                  return (
                    <Title5 variant="h5" key={"_" + item.id}>
                      <StyledLink href={`cabinet/posts/${item.id}`}>
                        {item.title}
                      </StyledLink>
                    </Title5>
                  );
                })}
              </Fragment>
            ) : null}
            <Stack direction="row" spacing={2}>
              <Link href="addgoods">
                <XButton4>Дать объявление</XButton4>
              </Link>
              {category !== "user" ? (
                <Link href="addpost">
                  <XButton5>Добавить новость</XButton5>
                </Link>
              ) : null}
              <XButton3
                sx={{ color: "#777", borderColor: "#777" }}
                startIcon={<AiOutlineLogout />}
                variant="outlined"
                onClick={async () => {
                  await dispatch(reset());
                  await dispatch(logout());
                }}
              >
                Выйти
              </XButton3>
            </Stack>
          </Stack>
        </Box>
      </Wrapper>
    );
  } else {
    return <NodataForm />;
  }
}
