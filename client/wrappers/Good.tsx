"use client";

import MainSider from "@/components/MainSider";
import MainWrapper from "@/components/MainWrapper";
import NegativeWrapper from "@/components/NegativeWrapper";
import NodataSingle from "@/components/NodataSingle";
import PhotoSlider from "@/components/PhotoSlider";
import SectionTitle from "@/components/SectionTitle";
import { Title4, XContainer } from "@/components/StyledUI";
import { renderDate } from "@/misc/tools";
import { LS } from "@/misc/tv";
import { IField, Item } from "@/misc/types";
import { Box, Grid, Stack, Typography } from "@mui/material";
import React, { Fragment } from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

interface IGallery {
  dir: string;
  photos: string[];
}

interface IParams {
  title: string;
  createdAt?: string | null;
  text?: string | null;
  gallery?: IGallery | null;
  phone?: string | null;
  price: string;
}

interface Props {
  model: string;
  sector: string;
  slug: string;
  fields: IField[];
  data: Item;
  specialParams: IParams;
}

export default function Good({ data, fields, specialParams }: Props) {
  return (
    <MainWrapper>
      <Box sx={{ paddingBlock: LS }}>
        <XContainer>
          <Grid container spacing={3}>
            <Grid item xs={12} md={8}>
              <Stack spacing={2}>
                <SectionTitle first={specialParams.title} decorated mb={0} />
                {specialParams.gallery?.photos?.length === 1 &&
                  specialParams.gallery.dir && (
                    <Box
                      component="img"
                      sx={{ width: 555, maxWidth: "100%" }}
                      src={`${HOST}/dist/${specialParams.gallery.dir}/${specialParams.gallery.photos[0]}`}
                    />
                  )}
                {specialParams.gallery?.photos &&
                  specialParams.gallery?.photos?.length > 1 &&
                  specialParams.gallery.dir && (
                    <PhotoSlider
                      images={specialParams.gallery.photos.map(
                        (x) => `${HOST}/dist/${specialParams.gallery?.dir}/${x}`
                      )}
                    />
                  )}
                {specialParams.createdAt && (
                  <NegativeWrapper>
                    <Stack spacing={2} direction="row">
                      <Typography variant="body1">Дата размещения:</Typography>
                      <Box component="div" sx={{ fontStyle: "italic" }}>
                        {renderDate(new Date(specialParams.createdAt))}
                      </Box>
                    </Stack>
                  </NegativeWrapper>
                )}

                {specialParams.text ? (
                  <NegativeWrapper>
                    <Box
                      component="div"
                      dangerouslySetInnerHTML={{
                        __html: specialParams.text,
                      }}
                    />
                  </NegativeWrapper>
                ) : null}

                {specialParams.phone ? (
                  <NegativeWrapper>
                    <ItemField
                      first="Номер телефона"
                      second={specialParams.phone}
                    />
                  </NegativeWrapper>
                ) : null}

                {specialParams.price ? (
                  <NegativeWrapper>
                    <ItemField
                      first="Цена"
                      second={specialParams.price + " р."}
                    />
                  </NegativeWrapper>
                ) : null}

                {Object.keys(data).map((x) => (
                  <Fragment>
                    {data[x] ? (
                      <ItemField
                        first={
                          fields.filter((elem) => elem.name === x)[0].label ||
                          ""
                        }
                        second={String(data[x])}
                      />
                    ) : null}
                  </Fragment>
                ))}
              </Stack>
            </Grid>
            <Grid item xs={12} md={4}>
              <MainSider />
            </Grid>
          </Grid>
        </XContainer>
      </Box>
    </MainWrapper>
  );
}

function ItemField({ first, second }: { first: string; second: string }) {
  if (first?.length && second?.length) {
    return (
      <Stack spacing={3} direction="row">
        <Title4 variant="h4">{`${first}:`}</Title4>
        <Typography variant="body1">{second}</Typography>
      </Stack>
    );
  } else return null;
}
