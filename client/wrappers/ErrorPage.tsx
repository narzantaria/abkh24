"use client";

import { Title2, XContainer } from "@/components/StyledUI";
import { useAppDispatch } from "@/lib";
import { logout } from "@/lib/authSlice";
import { XLS } from "@/misc/tv";
import { Box, Stack, Typography } from "@mui/material";
import React, { useEffect } from "react";

export default function ErrorPage() {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(logout());
  }, []);

  return (
    <main>
      <Box sx={{ paddingBlock: XLS }}>
        <XContainer>
          <Box sx={{ width: 555, maxWidth: "100%" }}>
            <Stack spacing={3}>
              <Title2>Бамбарбия киргуду!</Title2>
              <img src="/error.jpg" alt="" />
              <Typography variant="body1">
                Произошла какая-то ошибка. Возможно, вы заблокированы.
                Попробуйте еще раз либо свяжитесь с администрацией.
              </Typography>
            </Stack>
          </Box>
        </XContainer>
      </Box>
    </main>
  );
}
