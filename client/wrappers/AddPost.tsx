"use client";

import NodataSingle from "@/components/NodataSingle";
import SectionTitle from "@/components/SectionTitle";
import Wrapper from "@/components/Wrapper";
import MainForm from "@/form/MainForm";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { reset, setField } from "@/lib/formSlice";
import { useGetMainDataQuery } from "@/lib/mainApi";
import { useAddPostMutation } from "@/lib/userContentApi";
import { IField } from "@/misc/types";
import { Snackbar } from "@mui/material";
import axios from "axios";
import { useRouter } from "next/navigation";
import React, { Fragment, useEffect, useState } from "react";

export default function AddPost({ formProps }: { formProps: IField[] }) {
  const { refetch: refetchMainData } = useGetMainDataQuery(null);
  const form = useAppSelector((state: ReduxState) => state.form);
  const { avatar, category, id, token } = useAppSelector(
    (state: ReduxState) => state.auth
  );
  const [addPost] = useAddPostMutation();
  const dispatch = useAppDispatch();
  const router = useRouter();
  const [ready, setReady] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");

  useEffect(() => {
    (async () => {
      await dispatch(reset());
      // await dispatch(setField({ field: "root", value: `userid${id}` }));
      setReady(true);
    })();
  }, [id]);

  useEffect(() => {
    if ((category !== "google" && !token) || (category === "google" && !id))
      router.push("/login");
  }, [token]);

  if (ready) {
    return (
      <Fragment>
        <Wrapper>
          <SectionTitle first="Добавить новость" decorated />
          <MainForm
            fields={formProps}
            /**
             * А ЗДЕСЬ МЫ ОСТАВИЛИ "author" так как здесь вроде TypeORM
             * А он читает так, а сохраняет по-другому. Такой вот он...
             **/
            callBack={async () => {
              if (!form.tags?.length) {
                setMessage("Введите пожалуйста все необходимые поля!");
                return;
              }
              const { clearTemp, ...rest } = form;
              await addPost({ ...rest, author: id });
              setMessage("Новость успешно добавлена");
              await dispatch(reset());
              setTimeout(() => {
                refetchMainData();
                router.push("/cabinet");
              }, 3000);
            }}
          />
        </Wrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else return <NodataSingle />;
}
