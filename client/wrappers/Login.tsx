"use client";

import { Title2, XButton2, XContainer } from "@/components/StyledUI";
import { XLS } from "@/misc/tv";
import {
  Box,
  Button,
  IconButton,
  Snackbar,
  Stack,
  TextField,
} from "@mui/material";
import { blue } from "@mui/material/colors";
import axios from "axios";
import React, { ChangeEvent, Fragment, useEffect, useState } from "react";
import { FaFacebookF } from "react-icons/fa";
import { useRouter } from "next/navigation";
import { up } from "@/misc/tools";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { setAuth } from "@/lib/authSlice";
import { authorizeFapi } from "@/lib/miscSlice";
import { AiFillGoogleCircle } from "react-icons/ai";
import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { auth } from "@/utils/fb";
import { setField } from "@/lib/formSlice";

const HOST = process.env.NEXT_PUBLIC_HOST;

export default function Login() {
  const router = useRouter();
  const [login, setLogin] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [message, setMessage] = useState<string>("");
  const { category, id, token } = useAppSelector((state: ReduxState) => state.auth);
  const dispatch = useAppDispatch();

  const googleProvider = new GoogleAuthProvider();
  const GoogleLogin = async () => {
    try {
      const result = await signInWithPopup(auth, googleProvider);
      console.log(result);
      await dispatch(
        setAuth({
          avatar: result.user.photoURL,
          category: "google",
          name: result.user.displayName,
          id: result.user.uid,
        })
      );
      // А ЭТО ПРАВИЛЬНО???
      await dispatch(
        setField({ field: "root", value: `userid${result.user.uid}` }),
      );
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    up();
  }, []);

  useEffect(() => {
    if (category || token) router.push("/cabinet");
  }, [category, token]);

  return (
    <Fragment>
      <main>
        <Box sx={{ paddingBlock: XLS }}>
          <XContainer>
            <Box sx={{ width: 555, maxWidth: "100%" }}>
              <Stack spacing={3}>
                <Title2>Добро пожаловать!</Title2>
                <XButton2
                  startIcon={<AiFillGoogleCircle />}
                  onClick={GoogleLogin}
                >
                  Google
                </XButton2>
                <TextField
                  required
                  id="outlined-required"
                  label="Телефон или Email"
                  value={login}
                  onChange={(event: ChangeEvent<HTMLInputElement>) => {
                    setLogin(event.target.value);
                  }}
                />
                <TextField
                  required
                  id="outlined-required"
                  type="password"
                  label="Пароль"
                  value={password}
                  onChange={(event: ChangeEvent<HTMLInputElement>) => {
                    setPassword(event.target.value);
                  }}
                />
                <XButton2
                  onClick={() => {
                    axios
                      .post(`${HOST}/api/pub/login`, { login, password })
                      .then(async (res) => {
                        await dispatch(
                          setAuth({
                            ...res.data,
                            avatar: res.data.photos
                              ? `${HOST}/dist/${res.data.dir}/${res.data.photos[0]}`
                              : `${HOST}/dist/common/avatar.png`,
                            name: res.data.title,
                            token: res.data.token,
                            category: res.data.category,
                          })
                        );
                        await dispatch(authorizeFapi());
                        router.push("/cabinet");
                      })
                      .catch((err) => {
                        console.log(err);
                        const resStatus: number = err.response.status;
                        const errMsg: string = err.response.data.message;
                        switch (resStatus) {
                          case 400:
                            setMessage("Неверный пароль! Попробуйте еще раз.");
                            break;

                          case 403:
                            setMessage("Пользователь временно заблокирован!");
                            break;

                          case 404:
                            setMessage("Пользователь не обнаружен!");
                            break;

                          default:
                            setMessage(errMsg);
                            break;
                        }
                      });
                  }}
                >
                  Войти
                </XButton2>
              </Stack>
            </Box>
          </XContainer>
        </Box>
      </main>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
