"use client";

import Banners from "@/components/Banners";
import BigBanner from "@/components/BigBanner";
import DarkNews from "@/components/DarkNews";
import MainItems from "@/components/MainItems";
import MainTV from "@/components/MainTV";
import MainWrapper from "@/components/MainWrapper";
import MiscSection from "@/components/MiscSection";
import NodataGrid from "@/components/NodataGrid";
import Playbill from "@/components/Playbill";
import PopNews from "@/components/PopNews";
import SimpleNews from "@/components/SimpleNews";
import { XContainer } from "@/components/StyledUI";
import UpperNewsAlt from "@/components/UpperNewsAlt";
import WorldNews from "@/components/WorldNews";
import { useGetMainDataQuery } from "@/lib/mainApi";
import { useGetSettingsDataQuery } from "@/lib/settingsApi";
import { up } from "@/misc/tools";
import { ITV } from "@/misc/types";
import React, { Fragment, useEffect } from "react";

export default function Index() {
  const { data, error, isLoading } = useGetMainDataQuery(null);
  const { data: settings } = useGetSettingsDataQuery(null);

  useEffect(() => {
    up();
  }, []);

  return (
    <MainWrapper>
      {data && data[0].banners ? (
        <Fragment>
          <UpperNewsAlt data={data[0].news.slice(0, 3)} />
          <SimpleNews data={data[0].news.slice(3)} />
          <XContainer>
            <BigBanner {...data[0].banners[0]} />
          </XContainer>
          {/* <WorldNews data={data[0].world} /> */}
          <MiscSection data={data[0].banners.slice(3, 9)} />
          <MainTV
            data={data[0].videos.map((x: ITV) => {
              return {
                ...x,
                source:
                  x.source === "https://www.youtube.com/@abaza-tv"
                    ? "Абаза-ТВ"
                    : "АГТРК",
              };
            })}
          />
          <XContainer>
            <BigBanner {...data[0].banners[1]} />
          </XContainer>
          <PopNews data={data[0].popular} />
          <DarkNews data={data[0].movies} />
          {data[0].playbill && <Playbill data={data[0].playbill} />}
          <XContainer>
            <BigBanner {...data[0].banners[3]} />
          </XContainer>
          <Banners data={data[0].banners.slice(7, 10)} />
          {data[0].goods && <MainItems data={data[0].goods} />}
        </Fragment>
      ) : (
        <NodataGrid />
      )}
    </MainWrapper>
  );
}
