"use client";

import Comment from "@/components/Comment";
import ConditionalWrapper from "@/components/ConditionalWrapper";
import MainSider from "@/components/MainSider";
import MainWrapper from "@/components/MainWrapper";
import NegativeWrapper from "@/components/NegativeWrapper";
import NodataSingle from "@/components/NodataSingle";
import PhotoSlider from "@/components/PhotoSlider";
import PhotoSlider2 from "@/components/PhotoSlider2";
import PlayerFixed from "@/components/PlayerFixed";
import RutubePlayer from "@/components/RutubePlayer";
import SectionTitle from "@/components/SectionTitle";
import { Title3, Title4, Title5, XContainer } from "@/components/StyledUI";
import XAccodrion from "@/components/XAccodrion";
import YoutubePlayer from "@/components/YoutubePlayer";
import MainForm from "@/form/MainForm";
import TextEditorFixed from "@/form/TextEditorFixed";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { logout } from "@/lib/authSlice";
import { reset, setField, setForm } from "@/lib/formSlice";
import { hasTextContent, renderDate, useIsMount } from "@/misc/tools";
import { LS } from "@/misc/tv";
import { IComment, IField, IPost } from "@/misc/types";
import {
  Box,
  Card,
  Drawer,
  Grid,
  Snackbar,
  Stack,
  TextField,
  Typography,
  styled,
  useMediaQuery,
} from "@mui/material";
import axios from "axios";
import React, {
  ChangeEvent,
  Fragment,
  ReactNode,
  useEffect,
  useRef,
  useState,
} from "react";
import {
  FaFacebookF,
  FaOdnoklassniki,
  FaTelegram,
  FaTwitter,
  FaVk,
  FaWhatsapp,
} from "react-icons/fa";
import ReactPlayer from "react-player";
import {
  FacebookIcon,
  FacebookShareButton,
  MailruShareButton,
  OKIcon,
  OKShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  VKIcon,
  VKShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from "react-share";

const HOST = process.env.NEXT_PUBLIC_HOST;

const IconWrapper = styled("div")(({ color }: { color?: string | null }) => {
  return `
  background: ${color || "#000"};
  color: #fff !important;
  width: 32px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  `;
});

export default function Post({ data, slug }: { data: IPost; slug: string }) {
  const ref = useRef<HTMLDivElement | null>(null);
  const form = useAppSelector((state: ReduxState) => state.form);
  const { category, id, token, ...auth } = useAppSelector(
    (state: ReduxState) => state.auth
  );
  const dispatch = useAppDispatch();

  const [message, setMessage] = useState<string>("");
  const [comments, setComments] = useState<IComment[]>([]);
  const matches = useMediaQuery("(max-width: 555px)");

  const handleClick = () => {
    ref.current?.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    setComments(data?.comment);
  }, [data]);

  return (
    <Fragment>
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <Grid container spacing={3}>
              <Grid item xs={12} md={8}>
                <NegativeWrapper>
                  <Stack spacing={3}>
                    <SectionTitle first={data.title} decorated mb={0} />
                    {data.photos?.length === 1 && data.dir && (
                      <Box
                        component="img"
                        sx={{ width: 555, maxWidth: "100%" }}
                        src={`${HOST}/dist/${data.dir}/${data.photos[0]}`}
                      />
                    )}
                    {data.photos?.length && data.photos?.length > 1
                      ? data.dir && (
                          <PhotoSlider2
                            images={data.photos.map(
                              (x) => `${HOST}/dist/${data.dir}/${x}`
                            )}
                          />
                        )
                      : null}
                    {data.video && (
                      <Card sx={{ width: "min-content", maxWidth: "100%" }}>
                        <PlayerFixed
                          url={`${HOST}/dist/${data.dir}/${data.video}`}
                        />
                      </Card>
                    )}
                    {data.youtube && (
                      <Card>
                        <YoutubePlayer videoId={data.youtube} />
                      </Card>
                    )}
                    {data.rutube && (
                      <Card>
                        <RutubePlayer url={data.rutube} />
                      </Card>
                    )}
                    <Stack spacing={2} direction="row">
                      <Typography variant="body1" sx={{ fontStyle: "italic" }}>
                        Автор:
                      </Typography>
                      <Typography variant="body1" sx={{ fontWeight: 500 }}>
                        {data.authorname}
                      </Typography>
                    </Stack>
                    <Box component="div" sx={{ fontStyle: "italic" }}>
                      {renderDate(data.createdAt)}
                    </Box>
                    {data.text && (
                      <Box
                        component="div"
                        dangerouslySetInnerHTML={{ __html: data.text }}
                      />
                    )}
                    <Stack spacing={1}>
                      <Title5 variant="h5">Поделиться:</Title5>
                      <Stack direction="row" spacing={1}>
                        <FacebookShareButton
                          url={`${HOST}/news/${data.id}`}
                          quote={data.title}
                          className="Demo__some-network__share-button"
                        >
                          <IconWrapper color="#1450A3">
                            <FaFacebookF />
                          </IconWrapper>
                        </FacebookShareButton>
                        <VKShareButton
                          url={`${HOST}/news/${data.id}`}
                          className="Demo__some-network__share-button"
                        >
                          <IconWrapper color="#26577C">
                            <FaVk />
                          </IconWrapper>
                        </VKShareButton>
                        <OKShareButton
                          url={`${HOST}/news/${data.id}`}
                          className="Demo__some-network__share-button"
                        >
                          <IconWrapper color="#F79327">
                            <FaOdnoklassniki />
                          </IconWrapper>
                        </OKShareButton>
                        <WhatsappShareButton
                          url={`${HOST}/news/${data.id}`}
                          className="Demo__some-network__share-button"
                        >
                          <IconWrapper color="#61B15A">
                            <FaWhatsapp />
                          </IconWrapper>
                        </WhatsappShareButton>
                        <TelegramShareButton
                          url={`${HOST}/news/${data.id}`}
                          className="Demo__some-network__share-button"
                        >
                          <IconWrapper color="#7FBCD2">
                            <FaTelegram />
                          </IconWrapper>
                        </TelegramShareButton>
                        <MailruShareButton
                          url={`${HOST}/news/${data.id}`}
                          className="Demo__some-network__share-button"
                        >
                          <img
                            width={32}
                            height={32}
                            src={`${HOST}/dist/common/mailru.png`}
                            alt="Mail.ru"
                            style={{ borderRadius: "50%" }}
                          />
                        </MailruShareButton>
                        <TwitterShareButton
                          url={`${HOST}/news/${data.id}`}
                          className="Demo__some-network__share-button"
                        >
                          <IconWrapper color="#5BC0F8">
                            <FaTwitter />
                          </IconWrapper>
                        </TwitterShareButton>
                      </Stack>
                    </Stack>
                    {data.sourceurl && (
                      <Typography variant="body1">
                        <Box
                          component="span"
                          sx={{ fontWeight: 600, marginRight: "10px" }}
                        >
                          Ссылка на источник:
                        </Box>
                        <Box component="span" sx={{ fontStyle: "italic" }}>
                          <a
                            href={data.sourceurl}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            {data.sourceurl}
                          </a>
                        </Box>
                      </Typography>
                    )}
                    {comments.length ? (
                      <Stack spacing={3}>
                        <Title3 variant="h3">Комментарии</Title3>
                        {comments.map((x) => (
                          <Comment
                            {...x}
                            client={
                              category === "editor" ||
                              category === "moderator" ||
                              category === "user"
                                ? id
                                : null
                            }
                            // Здесь у нас только редактирование
                            remove={() => {
                              axios
                                .delete(`${HOST}/api/pub/comments/${x.id}`, {
                                  headers: {
                                    "Content-Type": "application/json",
                                    "auth-token": token,
                                  },
                                  data: { authorId: id },
                                })
                                .then(() => {
                                  setComments(
                                    comments.filter((elem) => elem.id != x.id)
                                  );
                                  setMessage("Document deleted");
                                })
                                .catch((err) => {
                                  console.log(err);
                                  const resStatus: number = err.response.status;
                                  const errMsg: string =
                                    err.response.data.message;
                                  if (resStatus === 401) {
                                    // dispatch(logout());
                                  } else {
                                    setMessage(errMsg);
                                  }
                                });
                            }}
                            answer={() => {
                              handleClick();
                            }}
                          />
                        ))}
                      </Stack>
                    ) : null}
                    {id && token && (
                      <ConditionalWrapper
                        condition={matches}
                        render={(props) => (
                          <XAccodrion title="Добавить комментарий">
                            {props}
                          </XAccodrion>
                        )}
                      >
                        <Stack spacing={2}>
                          <Title4 variant="h4">Добавить комментарий</Title4>
                          <Typography
                            variant="body1"
                            sx={{ fontStyle: "italic" }}
                          >
                            Для создания комментария необходим текст или
                            изображение, либо и то, и другое.
                          </Typography>
                          <div ref={ref}>
                            <MainForm
                              fields={[
                                {
                                  name: "text",
                                  label: "Добавить комментарий",
                                  type: "text",
                                },
                                {
                                  name: "img",
                                  label: "Изображение",
                                  type: "img",
                                },
                              ]}
                              callBack={() => {
                                const { clearTemp, ...rest } = form;
                                if (rest.text && !hasTextContent(rest.text))
                                  rest.text = null;
                                axios
                                  .post(
                                    `${HOST}/api/pub/comments`,
                                    { ...rest, author: id, post: data.id },
                                    {
                                      headers: {
                                        "Content-Type": "application/json",
                                        "auth-token": token,
                                      },
                                    }
                                  )
                                  .then(async (res) => {
                                    await dispatch(reset());
                                    setComments(
                                      comments.concat({
                                        ...res.data,
                                        author: {
                                          id: id,
                                          title: auth.name,
                                          photos: auth.photos,
                                          dir: auth.dir,
                                        },
                                      })
                                    );
                                  })
                                  .catch((err) => {
                                    const resStatus: number =
                                      err.response.status;
                                    const errMsg: string =
                                      err.response.data.message;
                                    if (resStatus === 401) {
                                      // dispatch(logout());
                                    } else {
                                      setMessage(errMsg);
                                    }
                                  });
                              }}
                            />
                          </div>
                        </Stack>
                      </ConditionalWrapper>
                    )}
                  </Stack>
                </NegativeWrapper>
              </Grid>
              <Grid item xs={12} md={4}>
                <MainSider />
              </Grid>
            </Grid>
          </XContainer>
        </Box>
      </MainWrapper>
      <Snackbar
        open={Boolean(message?.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
