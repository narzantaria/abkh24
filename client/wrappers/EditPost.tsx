"use client";

import Wrapper from "@/components/Wrapper";
import { Box, Snackbar, Stack } from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import MainForm from "@/form/MainForm";
import NodataLines from "@/components/NodataLines";
import { IField, IPost } from "@/misc/types";
import { useRouter } from "next/navigation";
import { reset, setField, setForm } from "@/lib/formSlice";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";

const HOST = process.env.NEXT_PUBLIC_HOST;

interface Field {
  name: string;
  type: string;
  required?: boolean;
}

interface IProps {
  fields: Field[] | null;
  data: IPost | null;
}

export default function EditPost({ data, fields }: IProps) {
  const router = useRouter();
  const { id, token } = useAppSelector((state: ReduxState) => state.auth);
  const form = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();
  const [message, setMessage] = useState<string>("");
  const [ready, setReady] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      if (data && fields && !ready) {
        await dispatch(reset());
        await dispatch(setForm(data));
        setReady(true);
      }
    })();
  }, [data, fields]);

  useEffect(() => {
    if (!token) router.push("/cabinet");
  }, [token]);

  useEffect(() => {
    (async () => {
      await dispatch(reset());
      await dispatch(
        setField({ field: "root", value: data?.dir || `userid${id}` })
      );
      setReady(true);
    })();
  }, [id]);

  if (ready && fields) {
    return (
      <Fragment>
        <Wrapper>
          <Box sx={{ width: 555, maxWidth: "100%" }}>
            <Stack spacing={3}>
              <MainForm
                fields={fields}
                callBack={() => {
                  const { id, clearTemp, category, ...rest } = form;
                  axios
                    .patch(`${HOST}/api/pub/posts/${id}`, rest, {
                      headers: {
                        "Content-Type": "application/json",
                        "auth-token": token,
                      },
                    })
                    .then((res) => {
                      console.log(res.data);
                    })
                    .catch((err) => {
                      const resStatus: number = err.response.status;
                      const errMsg: string = err.response.data.message;
                      // dispatch(logout());
                      setMessage(errMsg);
                    });
                }}
              />
            </Stack>
          </Box>
        </Wrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else {
    return <NodataLines />;
  }
}
