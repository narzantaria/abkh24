"use client";

import GoodsPrev from "@/components/GoodsPrev";
import NodataSingle from "@/components/NodataSingle";
import SectionTitle from "@/components/SectionTitle";
import { StyledLink, XContainer } from "@/components/StyledUI";
import { ReduxState, useAppSelector } from "@/lib";
import { LS } from "@/misc/tv";
import { IFolder } from "@/misc/types";
import { Box, Grid } from "@mui/material";
import { useRouter } from "next/navigation";
import React, { useEffect } from "react";

export default function AddGoods({ data }: { data: IFolder[] | null }) {
  const router = useRouter();

  const { avatar, category, id, token } = useAppSelector(
    (state: ReduxState) => state.auth
  );

  useEffect(() => {
    if ((category !== "google" && !token) || (category === "google" && !id))
      router.push("/login");
  }, [token]);

  if (data) {
    return (
      <main>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <SectionTitle first="Объявления" decorated />
            <Grid container spacing={4}>
              {data.map((x) => (
                <Grid item xs={6} md={3} key={x.title}>
                  <StyledLink href={`/addgoods/${x.name}`}>
                    <GoodsPrev {...x} />
                  </StyledLink>
                </Grid>
              ))}
            </Grid>
          </XContainer>
        </Box>
      </main>
    );
  } else return <NodataSingle />;
}
