"use client";

import Wrapper from "@/components/Wrapper";
import { Box, Snackbar, Stack } from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import MainForm from "@/form/MainForm";
import NodataLines from "@/components/NodataLines";
import { useRouter } from "next/navigation";
import { reset, setField, setForm } from "@/lib/formSlice";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { IField } from "@/misc/types";
import { useGetMainDataQuery } from "@/lib/mainApi";

const HOST = process.env.NEXT_PUBLIC_HOST;

export default function EditGood({
  model,
  slug,
  data,
  fields
}: {
  model: string;
  slug: string;
  data: { [key: string]: any } | null;
  fields: IField[];
}) {
  const router = useRouter();
  const [ready, setReady] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const { refetch: refetchMainData } = useGetMainDataQuery(null);
  const { id, token } = useAppSelector((state: ReduxState) => state.auth);
  const form = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();
  useEffect(() => {
    (async () => {
      await dispatch(reset());
      await dispatch(setForm(data));
      setReady(true);
    })();
    return () => {
      dispatch(reset());
    };
  }, [data]);

  useEffect(() => {
    if (!token) router.push("/cabinet");
  }, [token]);

  if (ready) {
    return (
      <Fragment>
        <Wrapper>
          <Box sx={{ width: 555, maxWidth: "100%" }}>
            <Stack spacing={3}>
              <MainForm
                fields={fields}
                callBack={() => {
                  //
                  const { id, clearTemp, createdAt, sector, ...rest } = form;
                  axios
                    .patch(`${HOST}/api/pub/goods/${model}/${slug}`, rest, {
                      headers: {
                        "Content-Type": "application/json",
                        "auth-token": token,
                      },
                    })
                    .then((res) => {
                      setMessage("Объявление успешно добавлено");
                      refetchMainData();
                    })
                    .catch((err) => {
                      const resStatus: number = err.response.status;
                      const errMsg: string = err.response.data.message;
                      // dispatch(logout());
                      setMessage(errMsg);
                    });
                }}
              />
            </Stack>
          </Box>
        </Wrapper>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else {
    return <NodataLines />;
  }
}
