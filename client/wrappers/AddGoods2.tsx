"use client";

import Crumbs from "@/components/Crumbs";
import NodataSingle from "@/components/NodataSingle";
import { StyledLink, Title3, XContainer } from "@/components/StyledUI";
import { ReduxState, useAppSelector } from "@/lib";
import { LS } from "@/misc/tv";
import { IFolder } from "@/misc/types";
import { Box, Stack } from "@mui/material";
import { useRouter } from "next/navigation";
import React, { useEffect } from "react";

interface IProps {
  data: IFolder[] | null;
  model: string | null;
}

export default function AddGoods2({ data, model }: IProps) {
  const router = useRouter();
  const { avatar, category, id, token } = useAppSelector(
    (state: ReduxState) => state.auth
  );

  useEffect(() => {
    if ((category !== "google" && !token) || (category === "google" && !id))
      router.push("/login");
  }, [token]);

  if (data && model) {
    return (
      <main>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <Stack spacing={4}>
              <Crumbs
                data={[
                  {
                    link: "/",
                    title: "Главная",
                  },
                  {
                    link: "addgoods", // addgoods
                    title: "Объявления",
                  },
                  {
                    title: data.filter((x) => x.name == model)[0].title, // animal
                  },
                ]}
              />
              <Stack spacing={2}>
                {data
                  .filter((x) => x.name == model)[0]
                  .files.map((y) => (
                    <Title3 variant="h3" key={y.name}>
                      <StyledLink href={`/addgoods/${model}/${y.name}`}>
                        {y.data.filter((z) => z.name === "title")[0].label}
                      </StyledLink>
                    </Title3>
                  ))}
              </Stack>
            </Stack>
          </XContainer>
        </Box>
      </main>
    );
  } else return <NodataSingle />;
}
