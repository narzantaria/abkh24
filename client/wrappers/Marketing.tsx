"use client";

import BannerPrev from "@/components/BannerPrev";
import BigBanner from "@/components/BigBanner";
import MainWrapper from "@/components/MainWrapper";
import NodataGrid from "@/components/NodataGrid";
import SectionTitle from "@/components/SectionTitle";
import { XContainer } from "@/components/StyledUI";
import { LS } from "@/misc/tv";
import { IBanner } from "@/misc/types";
import { Box, Grid } from "@mui/material";
import React from "react";

export default function Marketing({data}: { data: IBanner[] | null }) {
  if (data) {
    return (
      <MainWrapper>
        <Box sx={{ paddingBlock: LS }}>
          <XContainer>
            <SectionTitle first="Реклама" decorated />
            {data.slice(0, 3).map((x) => (
              <BigBanner key={x.id} {...x} />
            ))}
            <Grid container spacing={3}>
              {data.slice(3).map((x) => (
                <Grid item xs={12} sm={4} key={x.id}>
                  <BannerPrev {...x} />
                </Grid>
              ))}
            </Grid>
          </XContainer>
        </Box>
      </MainWrapper>
    );
  } else return <NodataGrid />;
}
