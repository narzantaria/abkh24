"use client";

import MainWrapper from '@/components/MainWrapper';
import NodataGrid from '@/components/NodataGrid';
import PrevFuneral from '@/components/PrevFuneral';
import SectionTitle from '@/components/SectionTitle';
import { XContainer } from '@/components/StyledUI';
import ThemeWrapper from '@/components/ThemeWrapper';
import { useScrollLock } from '@/misc/tools';
import { LS } from '@/misc/tv';
import { IFuneral } from '@/misc/types';
import { Box, Stack } from '@mui/material';
import axios from 'axios';
import React, { Fragment, useEffect, useRef, useState } from 'react'

const HOST = process.env.NEXT_PUBLIC_HOST;
const LIMIT = 5;
const OFFSET = 5;

export default function Funerals() {
  const [posts, setPosts] = useState<IFuneral[]>([]);
  const [loading, setLoading] = useState(true);
  const [finished, setFinished] = useState(false);
  const hiddenRef = useRef<HTMLDivElement>(null);
  const [page, setPage] = useState<number>(1);
  const { lockScroll, unlockScroll } = useScrollLock();

  const loadPosts = async (pageNumber: number) => {
    try {
      const response = await axios.get(`${HOST}/api/funerals`, {
        params: {
          offset: (pageNumber - 1) * OFFSET,
          limit: LIMIT,
        },
      });

      const newPosts = response.data;
      if (!newPosts.length) {
        setFinished(true);
        window.removeEventListener("scroll", handleScroll);
      } else if (pageNumber === 1) {
        setPosts(newPosts);
      } else {
        setPosts((prevPosts) => [...prevPosts, ...newPosts]);
      }
    } catch (error) {
      console.error(error);
    }
    unlockScroll();
    setLoading(false);    
  };

  useEffect(() => {
    loadPosts(page);
  }, [page]);

  const handleScroll = () => {
    if(hiddenRef.current && window.scrollY + window.innerHeight >= hiddenRef.current.offsetTop + 300) {
      lockScroll();
      setLoading(true);
      setTimeout(() => {
        const pageNumber = Math.ceil(posts.length / LIMIT) + 1;
        setPage(pageNumber);
      }, 500);
    }
  };

  useEffect(() => {
    if (!finished) window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [posts, loading]);

  if (posts.length) {
    return (
      <Fragment>
        <MainWrapper>
          <Box sx={{ paddingBlock: LS }}>
            <XContainer>
              <SectionTitle first="Некролог" decorated />
              <ThemeWrapper>
                <Stack spacing={4}>
                  {posts.map((x) => {
                    return <PrevFuneral {...x} />;
                  })}
                </Stack>
              </ThemeWrapper>
            </XContainer>
          </Box>
        </MainWrapper>
        <div ref={hiddenRef}></div>
        {loading && <NodataGrid />}
      </Fragment>
    );
  } else return <NodataGrid />;
}
