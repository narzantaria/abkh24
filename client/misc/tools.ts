import { useRef, useEffect, useCallback } from "react";
import { BLUE_CLR, GREEN_CLR } from "./tv";
import { IField } from "./types";
import dayjs from "dayjs";
import { hparse } from "./stools";

const useIsMount = () => {
  const isMountRef = useRef(true);
  useEffect(() => {
    isMountRef.current = false;
  }, []);
  return isMountRef.current;
};

// random symbols string
const makeId = (length: number) => {
  let result = "";
  const characters = "abcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const capitalized = (word: string) => {
  return word.charAt(0).toUpperCase() + word.slice(1);
};

/**
 * Валидация некоторых полей формы. Функция специфична к данному проекту,
 * например обрабатывает formProps модели...
 **/
function validator(
  formProps: any[],
  data: any,
  checkRequired?: boolean
): string {
  for (let x = 0; x < formProps.length; x++) {
    if (
      formProps[x].required == true &&
      !data[formProps[x].name] &&
      checkRequired
    ) {
      return "Please fill all required inputs!";
    }
    if (formProps[x].type == "phone") {
      const phone = hparse(data[formProps[x].name]).value;
      const isPhone = phone.match(/^[0-9]+$/); // null - неправильный номер
      if (phone.length < 10 || phone.length > 11 || !isPhone) {
        return "Please type a valid phone number!";
      }
    }
    if (formProps[x].type == "email") {
      const isEmail = String(data[formProps[x].name])
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
      if (!isEmail) {
        return "Please type a valid email address!";
      }
    }
  }
  return "";
}

const isEmail = (arg: string): boolean => {
  const isMatching = String(arg)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
  if (!isMatching) return false;
  return true;
};

const isPhone = (arg: string): boolean => {
  const isMatching = arg.match(
    /^\+?[(]?[0-9]{3}[)]?[-\s.?]?[0-9]{3}[-\s.?]?[0-9]{4,6}$/
  ); // null - неправильный номер
  if (!isMatching) return false;
  if (arg.length < 12 || arg.length > 13 || arg.slice(0, 1) !== "+")
    return false;
  return true;
};

// function hstr(arg: object | any[]): string | null {
//   return arg ? JSON.stringify(arg).replaceAll('"', "'") : null;
// }

// // function h_parse(arg: string): { code: string; value: string } {
// //   return arg ? JSON.parse(arg.replaceAll("'", '"')) : { code: "", value: "" };
// // }

// function hparse(arg: string) {
//   return JSON.parse(arg.replaceAll("'", '"'));
// }

function parsePhone(arg: string): { code: string; value: string } {
  return arg ? JSON.parse(arg.replaceAll("'", '"')) : { code: "", value: "" };
}

function removeRelations(body: { [field: string]: any }, model: IField[]) {
  const filteredBody: { [field: string]: any } = {};
  const keys = Object.keys(body);
  const relations = model
    .filter((x) => x.type === "relation")
    .map((x) => x.name);
  const filteredKeys = keys.filter((x) => !relations.includes(x));
  for (let x = 0; x < filteredKeys.length; x++) {
    filteredBody[filteredKeys[x]] = body[filteredKeys[x]];
  }
  return filteredBody;
}

interface IColors {
  [key: string]: {
    background: string;
    color: string;
    bordercolor: string;
    name?: string;
  };
}

function tagColor(arg?: string) {
  const colors: IColors = {
    "новости мира": {
      name: "orange",
      background: "#F86F03",
      color: "#FFF",
      bordercolor: "#F86F03",
    },
    "Народный фронт": {
      name: "basic",
      background: "transparent",
      color: "#FFF",
      bordercolor: "#FFF",
    },
    Республика: {
      name: "yellow",
      background: "#FFC436",
      color: "#000",
      bordercolor: "#FFC436",
    },
    "Абхазия 24": {
      name: "green",
      background: GREEN_CLR,
      color: "#FFF",
      bordercolor: GREEN_CLR,
    },
    "ИБ Пятнашка": {
      name: "red",
      background: "#ED2B2A",
      color: "#FFF",
      bordercolor: "#ED2B2A",
    },
    "ЧП Абхазии": {
      name: "black",
      background: "#000",
      color: "#FFF",
      bordercolor: "#000",
    },
    происшествия: {
      name: "black",
      background: "#000",
      color: "#FFF",
      bordercolor: "#000",
    },
    политика: {
      name: "brown",
      background: "#A4907C",
      color: "#FFF",
      bordercolor: "#A4907C",
    },
    АГТРК: {
      name: "brown",
      background: GREEN_CLR,
      color: "#FFF",
      bordercolor: GREEN_CLR,
    },
    "Абаза-ТВ": {
      name: "blue",
      background: BLUE_CLR,
      color: "#FFF",
      bordercolor: BLUE_CLR,
    },
    "МВД РА": {
      name: "violet",
      background: "#9400FF",
      color: "#FFF",
      bordercolor: "#9400FF",
    },
    "МВД Абхазии": {
      name: "violet",
      background: "#9400FF",
      color: "#FFF",
      bordercolor: "#9400FF",
    },
    "АМРА-life": {
      name: "brown",
      background: "#65451F",
      color: "#FFF",
      bordercolor: "#65451F",
    },
    спорт: {
      name: "green",
      background: GREEN_CLR,
      color: "#FFF",
      bordercolor: GREEN_CLR,
    },
    "ЧП Сочи": {
      name: "blue",
      background: "#0E21A0",
      color: "#FFF",
      bordercolor: "#0E21A0",
    },
  };
  return arg
    ? colors[arg]
    : {
        name: "basic",
        background: "transparent",
        color: "#FFF",
        bordercolor: "#FFF",
      };
}

function renderDate(arg: Date): string {
  return dayjs(arg).format("YYYY.MM.DD");
}

function hasTextContent(html: string): boolean {
  const doc = new DOMParser().parseFromString(html, "text/html");
  const element = doc.body.firstChild;

  if (!element || element.textContent === null) {
    return false;
  }

  const textContent = element.textContent.trim();
  return textContent.length > 0;
}

function modifyArr(arr: any[], elem: any): any[] {
  console.log(arr);
  const index = arr.indexOf(elem);
  if (index !== -1) {
    // Если элемент найден в массиве, удаляем его
    return arr.filter((x) => x !== elem);
  } else {
    // Если элемент не найден в массиве, добавляем его
    return arr.concat([elem]);
  }
}

const useScrollLock = () => {
  const lockScroll = useCallback(() => {
    document.body.style.overflow = "hidden";
  }, []);

  const unlockScroll = useCallback(() => {
    document.body.style.overflow = "";
  }, []);

  return {
    lockScroll,
    unlockScroll,
  };
};

function up() {
  // window.scrollTo(0, 0)
  window.scrollTo({
    top: 0,
    behavior: "smooth", // for smoothly scrolling
  });
}

export {
  capitalized,
  isPhone,
  isEmail,
  makeId,
  useIsMount,
  validator,
  parsePhone,
  removeRelations,
  tagColor,
  renderDate,
  hasTextContent,
  modifyArr,
  useScrollLock,
  up,
};
