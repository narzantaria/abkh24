// server tools

export const imgProps = (alt: string) => {
  return {
    width: 800,
    height: 600,
    alt: alt,
  };
};

export const goodsHeader: {
  [key: string]: { title: string; description: string };
} = {
  agro: {
    title: "Cельское хозяйство",
    description:
      "Объявления сельское хозяйство, удобрения, химикаты, саженцы, семена, рассада, сельскохозяйственная техника",
  },
  animal: {
    title: "Животные",
    description:
      "Объявления животные, собаки, ценки, кошки, котята, попугаи, птицы, рептилии, домашние питомцы, товары для животных",
  },
  auto: {
    title: "Транспорт",
    description:
      "Объявления транспорт, автомобили, легковые, грузовые, мотоциклы, автобусы, минивэны",
  },
  autopart: {
    title: "Автозапчасти",
    description:
      "Объявления автозапчасти, для иномарок, отечественного транспорта, экипировка, аксессуары, тюнинг, шины, диски, масло, фильтры",
  },
  beauty: {
    title: "Красота и здоровье",
    description:
      "Объявления красота и здоровье, косметика, парфюмерия, средства гигиены, уход за кожей, маникюр, педикюр, приборы и аксессуары",
  },
  building: {
    title: "Строительство и ремонт",
    description:
      "Объявления строительство и ремонт, окна, двери, сантехника, электрика, плитка, обои, стройматериалы",
  },
  children: {
    title: "Товары для детей",
    description:
      "Объявления товары для детей, игрушки, питание, коляски, подгузники, товары для мам, для учебы, обустройство детской, развивающие игры",
  },
  electro: {
    title: "Электротовары",
    description:
      "Объявления электротовары, телефоны, смартфоны, планшеты, компьютеры, ноутбуки, бытовая техника",
  },
  estate: {
    title: "Недвидимость",
    description:
      "Объявления недвидимость в Абхазии, Краснодарском крае, Северном Кавказе, купля и продажа, снять жилье, складские помещения, ангары, офисы",
  },
  fashion: {
    title: "Одежда и обувь",
    description:
      "Объявления одежда, обувь, мужская, женская, детская, мировые бренды, пошив, ткани",
  },
  hobby: {
    title: "Хобби и отдых",
    description:
      "Объявления хобби, отдых, спорт, музыка, книги, журналы, игры для приставок, ПК, охота, рыбалка",
  },
  home: {
    title: "Товары для дома",
    description:
      "Объявления товары для дома, мебель, домашняя утварь, комнатные растения, охрана и сигнализация, инструменты",
  },
  product: {
    title: "Продукты",
    description:
      "Объявления продукты питания, оптом, в розницу, фрукты, овощи, бытовая химия, косметика и туалет",
  },
  service: {
    title: "Услуги",
    description:
      "Объявления услуги, автоуслуги, ремонт, IT-услуги, фото видео съемка, изготовление на заказ, реклама, обучение, перевозки, другие услуги",
  },
  tourism: {
    title: "Отдых и туризм",
    description:
      "Объявления отдых в Абхазии, туризм, путевки, трансфер, гостиницы, экскурсии, частный сектор, другое",
  },
  work: {
    title: "Работа и бизнес",
    description:
      "Объявления работа в Абхазии, вакансии, бизнес, ищу работу, другое",
  },
};

export function hstr(arg: object | any[]): string | null {
  return arg ? JSON.stringify(arg).replaceAll('"', "'") : null;
}

// function h_parse(arg: string): { code: string; value: string } {
//   return arg ? JSON.parse(arg.replaceAll("'", '"')) : { code: "", value: "" };
// }

export function hparse(arg: string) {
  return JSON.parse(arg.replaceAll("'", '"'));
}

// Чистка
export function processText(text: string) {
  // Убираем лишние переводы строки
  text = text.replace(/\n+/g, "\n");

  // Убираем смайлики
  text = text.replace(
    /([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g,
    ""
  );

  return text;
}

// Create title (max 70-80 primerno)
// Description max 200
export function trimText(text: string, len: number) {
  text = text.replace("\n", " ");
  const phrase1 = text.split(".")[0] + ".";
  const phrase2 = text.split("!")[0] + "!";
  const phrase3 = text.split("?")[0] + "?";
  const phrase = [phrase1, phrase2, phrase3].reduce((a, b) => (a > b ? b : a));
  if (phrase.length <= len) return phrase;
  const subPhrase = phrase.substring(0, len);
  const splittedPhrase = subPhrase.split(" ");
  const lastWord = splittedPhrase[splittedPhrase.length - 1];
  if (
    lastWord.slice(-1) === " " ||
    lastWord.slice(-1) === "." ||
    lastWord.slice(-1) === "?" ||
    lastWord.slice(-1) === "!"
  ) {
    return subPhrase.slice(0, -1) + lastWord.slice(-1);
  } else {
    return subPhrase.slice(0, -lastWord.length - 1);
  }
}