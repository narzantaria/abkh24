import { Inter, Open_Sans } from "next/font/google";

export const primaryFont = Open_Sans({
  subsets: ["cyrillic"],
  variable: "--primary-font",
});
export const headingFont = Inter({
  subsets: ["cyrillic"],
  variable: "--heading-font",
});

/*
font-family: 'Fira Code', monospace;
font-family: 'Inconsolata', monospace;
font-family: 'Inter', sans-serif;
font-family: 'Manrope', sans-serif;
font-family: 'Merriweather', serif;
font-family: 'Montserrat', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
font-family: 'Ubuntu', sans-serif;
*/
