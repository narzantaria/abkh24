"use client";

import React from "react";
import { styled } from "@mui/material";
import { FaFacebookF, FaVk, FaInstagram } from "react-icons/fa";
import { GREEN_CLR, TRANS_EASE_IN_OUT } from "@/misc/tv";

interface IProps {
  facebook: string | null;
  instagram: string | null;
  vk: string | null;
}

const SocialWrapper = styled("div")`
  display: flex;
  align-items: center;
  column-gap: 7px;
`;

const SocialItem2 = styled("div")`
  cursor: pointer;
  border-radius: 50%;
  background-color: #1f1f1f;
  border: 1px solid rgba(0, 0, 0, 0.09);
  width: 38px;
  height: 38px;
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    font-size: 20px;
    transition: ${TRANS_EASE_IN_OUT};
    color: #fff;
    &:hover {
      color: ${GREEN_CLR};
    }
  }
`;

export default function Social({ facebook, instagram, vk }: IProps) {
  if (facebook || instagram || vk) {
    return (
      <SocialWrapper>
        {facebook && (
          <SocialItem2>
            <FaFacebookF />
          </SocialItem2>
        )}
        {vk && (
          <SocialItem2>
            <FaVk />
          </SocialItem2>
        )}
        {instagram && (
          <SocialItem2>
            <FaInstagram />
          </SocialItem2>
        )}
      </SocialWrapper>
    );
  } else return <></>;
}
