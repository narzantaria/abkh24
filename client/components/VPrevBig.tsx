"use client";

import React, { useState } from "react";
import { styled, Box, Stack } from "@mui/material";
import {
  BackgroundImage2,
  Border,
  Overlay,
  StyledLink,
  TagButton,
  Title1,
  VideoIcon,
} from "./StyledUI";
import { MS } from "@/misc/tv";

interface IVideo {
  id: string;
  title: string;
  videoId: string;
  source: string;
  createdAt: Date;
  tagClr?: { background: string; color: string; bordercolor: string } | null;
}

const StyledBorder = styled(Border)`
  height: 550px;
`;

const Caption = styled(Stack)`
  position: absolute;
  color: #fff;
  bottom: 0;
  padding: ${MS};
`;

export default function VPrevBig({
  createdAt,
  id,
  title,
  source,
  videoId,
  tagClr,
}: IVideo) {
  const [hover, setHover] = useState<boolean>(false);

  return (
    <StyledLink href={`/videos/${id}`}>
      <StyledBorder
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        <VideoIcon fontSize={115} />
        <BackgroundImage2
          url={`https://i.ytimg.com/vi/${videoId}/hqdefault.jpg`}
          hover={hover}
        />
        <Overlay hover={hover} />
        <Caption spacing={2}>
          <TagButton background={tagClr?.background} size="small">
            {source}
          </TagButton>
          <Title1 variant="h5" sx={{ color: "#fff !important" }}>
            {title}
          </Title1>
          <Box sx={{ color: "#efefef", fontSize: "13px" }}>
            <span>{new Date(createdAt).toISOString().slice(0, 10)}</span>
          </Box>
        </Caption>
      </StyledBorder>
    </StyledLink>
  );
}
