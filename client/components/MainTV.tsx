"use client";

import React, { Fragment } from "react";
import { XContainer } from "./StyledUI";
import { Box, Grid, useMediaQuery } from "@mui/material";
import { XLS } from "@/misc/tv";
import SectionTitleMid from "./SectionTitleMid";
import VPrevBig from "./VPrevBig";
import { tagColor } from "@/misc/tools";
import VPrev from "./VPrev";
import ThemeWrapper from "./ThemeWrapper";

interface ITV {
  id: string;
  title: string;
  videoId: string;
  source: string;
  createdAt: Date;
}

export default function MainTV({ data }: { data: ITV[] }) {
  const matches = useMediaQuery("(max-width:576px)");

  if (data) {
    return (
      <Box sx={{ pb: XLS }}>
        <XContainer>
          <SectionTitleMid first="Новости" second="ТВ" />
          <ThemeWrapper>
            <Grid container spacing="4px">
              {data.slice(0, 2).map((x) => (
                <Fragment>
                  {matches ? (
                    <Grid item xs={12} md={6}>
                      <VPrev tagClr={tagColor(x.source)} {...x} key={x.id} />
                    </Grid>
                  ) : (
                    <Grid item xs={12} md={6}>
                      <VPrevBig tagClr={tagColor(x.source)} {...x} key={x.id} />
                    </Grid>
                  )}
                </Fragment>
              ))}
              {data.slice(2, 8).map((x) => (
                <Grid item xs={12} md={3}>
                  <VPrev tagClr={tagColor(x.source)} {...x} key={x.id} />
                </Grid>
              ))}
            </Grid>
          </ThemeWrapper>
        </XContainer>
      </Box>
    );
  } else return null;
}
