"use client";

import { styled } from "@mui/material";
import React from "react";
import { tagColor } from "@/misc/tools";
import { IPost } from "@/misc/types";
import PrevBigAlt from "./PrevBigAlt";

const NewsWrapper = styled("section")`
  display: grid;
  grid-template-columns: 7fr 5fr 5fr;
  @media (max-width: 900px) {
    display: block;
  }
`;

interface IData {
  [key: string]: IPost[];
}

export default function UpperNewsAlt({ data }: { data: IPost[] }) {
  if (data) {
    return (
      <NewsWrapper>
        {/* <PrevBigAlt
          params={["ЧП Абхазии"]}
          tagClr={tagColor("ЧП Абхазии")}
          {...data.chp[0]}
        />  
        <PrevBigAlt
          params={["ЧП Сочи"]}
          tagClr={tagColor("ЧП Сочи")}
          {...data.sochi[0]}
        />        
        <PrevBigAlt
          params={["спорт"]}
          tagClr={tagColor("спорт")}
          {...data.sport[0]}
        /> */}
        {data.map(x=>(<PrevBigAlt
          params={[]}
          tagClr={tagColor(x.tags[0])}
          {...x}
        />))}
      </NewsWrapper>
    );
  } else return null;
}
