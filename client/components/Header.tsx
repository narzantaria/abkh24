"use client";

import React, { Fragment, useEffect, useState } from "react";
import { Box, styled, useMediaQuery } from "@mui/material";
import { HeaderButtons, XContainer, XDarkIconButton, XDarkIconButton2 } from "./StyledUI";
import { FaRegMoon, FaRegSun } from "react-icons/fa";
import { BiSearch } from "react-icons/bi";
import SideNav from "./SideNav";
import TopNav from "./TopNav";
import { motion } from "framer-motion";
import SearchBar from "./SearchBar";
import { useAppDispatch, useAppSelector } from "@/lib";
import { ReduxState } from "@/lib";
import { setTheme } from "@/lib/themeSlice";
import SwitchTheme from "./SwitchTheme";

const NavWrapper = styled("div")`
  background-color: #151515;
  padding-block: 10px;
  border-bottom: 1px solid rgba(255, 255, 255, 0.1);
  border-top: 1px solid rgba(255, 255, 255, 0.1);
`;

const InnerContent = styled("div")`
  display: grid;
  grid-template-columns: 1fr 7fr 1fr;
`;

export default function Header() {
  const { path, word } = useAppSelector((state: ReduxState) => state.search);
  const { color } = useAppSelector((state: ReduxState) => state?.theme);
  const dispatch = useAppDispatch();
  const matches = useMediaQuery("(min-width:992px)");

  const [isVisible, setIsVisible] = useState(false);

  const handleButtonClick = () => {
    setIsVisible(!isVisible);
  };

  const containerVariants = {
    hidden: { height: 0 },
    visible: { height: "auto", transition: { duration: 0.3 } },
  };

  useEffect(() => {
    if (path === "posts" && !word.length) setIsVisible(false);
  }, [path, word]);

  if (matches) {
    return (
      <Fragment>
        <NavWrapper>
          <XContainer>
            <InnerContent>
              <Box sx={{ display: "flex", alignItems: "center" }}>
                <SideNav />
              </Box>
              <TopNav />
              <HeaderButtons>
                <SwitchTheme />
                <XDarkIconButton
                  sx={{ transform: "translateY(3px)" }}
                  aria-label="search"
                  onClick={handleButtonClick}
                >
                  <BiSearch />
                </XDarkIconButton>
              </HeaderButtons>
            </InnerContent>
          </XContainer>
        </NavWrapper>
        <motion.div
          style={{ width: "100%", overflow: "hidden" }}
          variants={containerVariants}
          initial={false}
          animate={isVisible ? "visible" : "hidden"}
        >
          <div
            style={{
              paddingBlock: "50px",
              width: "100%",
              borderBottom: "1px solid #ddd",
              backgroundColor: "#f0f0f0",
            }}
          >
            <SearchBar />
          </div>
        </motion.div>
      </Fragment>
    );
  } else return null;
}
