"use client";

import React from "react";
import { Box, styled } from "@mui/material";
import { LS } from "@/misc/tv";
import { IBanner } from "@/misc/types";

const HOST = process.env.NEXT_PUBLIC_HOST;

const StyledImg = styled("img")`
  max-width: 100%;
`;

export default function BigBanner({ url, img, dir, title }: IBanner) {
  return (
    <Box sx={{ paddingBlock: LS }}>
      <a href={url} target="_blank" rel="noopener noreferrer">
        <StyledImg src={`${HOST}/dist/${dir}/${img}`} alt={title} />
      </a>
    </Box>
  );
}
