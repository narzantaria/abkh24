"use client";

import React from "react";
import { Box, styled, Typography } from "@mui/material";
import { Decoration } from "./StyledUI";
import { useAppSelector } from "@/lib";
import { ReduxState } from "@/lib";

const MainWrapper = styled(Typography)(
  ({ decorated }: { decorated?: any | null }) => `
align-items: center;
border-bottom: ${decorated ? "1px solid #e2e2e2" : "none"};
display: inline-flex;
justify-content: space-between;
padding-bottom: ${decorated ? "13px" : 0};
position: relative;
width: 100%;
font-size: 17px;
font-weight: 700;
text-transform: uppercase;
`
);

export default function SectionTitle({
  first,
  second,
  decorated,
  mb,
}: {
  first: string;
  second?: string;
  decorated?: any | null;
  mb?: number;
}) {
  const { color } = useAppSelector((state: ReduxState) => state?.theme);

  return (
    <Box
      component="div"
      sx={{
        color: color === "dark" ? "#fff" : "initial",
        position: "relative",
        marginBottom: mb || '25px'
      }}
    >
      <MainWrapper variant="h2" decorated={decorated?.toString() || null} mb={mb}>
        {`${first}${second ? " " + second : ""}`}
      </MainWrapper>
      {decorated && <Decoration />}
    </Box>
  );
}
