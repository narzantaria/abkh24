"use client";

import { Stack } from '@mui/material';
import Image from 'next/image';
import React from 'react'

const ICONS_WIDTH: number = 45;

export default function AppIcons() {
  return (
    <a href="https://app.abkhazia24.com" target="_blank" rel="noopener noreferrer">
      <Stack direction="row" spacing={2}>
        <Image width={ICONS_WIDTH} height={ICONS_WIDTH} src="/android.png" alt="Android" />
        <Image width={ICONS_WIDTH} height={ICONS_WIDTH} src="/apple.png" alt="IOS" />
      </Stack>
    </a>
  )
}