"use client";

import React from "react";
import SectionTitle from "./SectionTitle";
import { StyledLink, XContainer } from "./StyledUI";
import { Box, Grid } from "@mui/material";
import { LS } from "@/misc/tv";
import { Item } from "@/misc/types";
import ItemPrev from "./ItemPrev";

export default function MainItems({ data }: { data: Item[] }) {
  return (
    <Box sx={{ paddingBlock: LS }}>
      <XContainer>
        <SectionTitle first="Последние" second="объявления" decorated />
        <Grid container spacing={3}>
          {data.map((x) => (
            <Grid item xs={12} md={3}>
              <StyledLink href={`/goods/${x.source}/${x.sector}/${x.id}`}>
                <ItemPrev {...x} key={x.id} />
              </StyledLink>
            </Grid>
          ))}
        </Grid>
      </XContainer>
    </Box>
  );
}
