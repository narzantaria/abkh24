"use client";

import { Stack, styled } from "@mui/material";
import { capitalize } from "lodash";
import React from "react";
import ConditionalWrapper from "./ConditionalWrapper";
import { Decoration2, StyledLink } from "./StyledUI";

const MainWrapper = styled("nav")`
  display: flex;
  flex-wrap: wrap;
`;

const CrumbItem = styled("div")`
  a {
    color: initial;
  }
  &:after {
    content: "»";
    padding-inline: 5px;
  }
  &:last-child:after {
    content: "";
  }
`;

interface Crumb {
  title: string;
  link?: string;
}

export default function Crumbs({ data }: { data: Crumb[] }) {
  return (
    <Stack spacing={2} sx={{ borderBottom: "1px solid #e2e2e2" }}>
      <MainWrapper>
        {data.map((x) => (
          <CrumbItem>
            <ConditionalWrapper
              condition={Boolean(x.link)}
              render={(props) => (
                <StyledLink href={`/${x.link}`}>{props}</StyledLink>
              )}
            >
              {capitalize(x.title)}
            </ConditionalWrapper>
          </CrumbItem>
        ))}
      </MainWrapper>
      <Decoration2 />
    </Stack>
  );
}
