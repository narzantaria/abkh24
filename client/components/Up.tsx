"use client";

import React, { Fragment, useEffect, useState } from "react";
import { AiOutlineUpSquare } from "react-icons/ai";
import { styled } from "@mui/material";
import { GREEN_CLR, TRANS_EASE_IN_OUT } from "../misc/tv";

const RawData = styled("div")`
  position: fixed;
  bottom: 25px;
  right: 25px;
  z-index: 9999;
  font-size: 35px;
  cursor: pointer;
  height: 45px;
  width: 45px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #000;
  border: 1px solid rgba(221, 221, 221, 0.3);
  svg {
    transition: ${TRANS_EASE_IN_OUT};
    color: #fff;
  }
  &:hover svg {
    color: ${GREEN_CLR};
  }
`;

export default function Up() {
  const [showButton, setShowButton] = useState<boolean>(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        setShowButton(true);
      } else {
        setShowButton(false);
      }
    });
  }, []);

  // This function will scroll the window to the top
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth", // for smoothly scrolling
    });
  };
  return (
    <Fragment>
      {showButton ? (
        <RawData onClick={(_) => scrollToTop()}>
          <AiOutlineUpSquare />
        </RawData>
      ) : null}
    </Fragment>
  );
}
