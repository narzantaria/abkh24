"use client";

import { XLS } from "@/misc/tv";
import { Box } from "@mui/material";
import React, { ReactNode } from "react";
import { XContainer } from "./StyledUI";

export default function Wrapper({ children }: { children: ReactNode }) {
  return (
    <main>
      <Box sx={{ paddingBlock: XLS }}>
        <XContainer>{children}</XContainer>
      </Box>
    </main>
  );
}
