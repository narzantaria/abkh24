"use client";

import React from "react";
import { VideoContainer, YoutubeWrapper } from "./YoutubePlayer";

export default function RutubePlayer({ url }: { url: string }) {
  return (
    <YoutubeWrapper>
      <VideoContainer>
        <iframe
          width="720"
          height="405"
          src={url}
          allow="clipboard-write; autoplay"
        />
      </VideoContainer>
    </YoutubeWrapper>
  );
}
