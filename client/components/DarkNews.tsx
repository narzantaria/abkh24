"use client";

import React from "react";
import { XContainer } from "./StyledUI";
import { Box, Grid } from "@mui/material";
import { LS } from "@/misc/tv";
import DarkSectionTitle from "./DarkSectionTitle";
import MoviePrev from "./MoviePrev";
import { IMovie } from "@/misc/types";

export default function DarkNews({ data }: { data: IMovie[] }) {
  return (
    <Box sx={{ paddingBlock: LS, backgroundColor: "#0a0a0a" }}>
      <XContainer>
        <DarkSectionTitle second="Новинки кино" />
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <MoviePrev {...data[4]} pb={147} />
          </Grid>
          <Grid item xs={12} md={6}>
            <Grid container spacing={3}>
              {data?.slice(0, 4).map((x: IMovie, i: number) => (
                <Grid item xs={12} md={6}>
                  <MoviePrev {...x} key={x.id} />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </XContainer>
    </Box>
  );
}
