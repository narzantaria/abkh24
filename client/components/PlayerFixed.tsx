"use client";

import dynamic from "next/dynamic";
const PlayerFixed = dynamic(() => import("./Player"), { ssr: false });

export default PlayerFixed;