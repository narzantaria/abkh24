"use client";

import Script from "next/script";
import React, { Fragment } from "react";

export default function GoogleAnalytics() {
  return (
    <Fragment>
      <Script
        async
        src="https://www.googletagmanager.com/gtag/js?id=G-C2DECDPLSP"
      />
      <Script
        dangerouslySetInnerHTML={{
          __html: `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-C2DECDPLSP');
        `,
        }}
      />
    </Fragment>
  );
}
