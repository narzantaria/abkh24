"use client";

// В зависимости от условия оборачивает компонент. Рендер-пропс.
import React, { Fragment, ReactNode } from "react";

interface IProps {
  condition: boolean;
  render: (arg0: ReactNode) => ReactNode;
  children: ReactNode;
}

export default function ConditionalWrapper({
  condition,
  render,
  children,
}: IProps) {
  return <Fragment>{condition ? render(children) : children}</Fragment>;
}
