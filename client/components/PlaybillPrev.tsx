"use client";

import React, { useState } from "react";
import {
  BackgroundImage,
  Border,
  Caption,
  Overlay,
  StyledLink,
  TagButton,
  Title3,
} from "./StyledUI";
import { Box, styled } from "@mui/material";
import { IPlaybill } from "@/misc/types";
import { GREEN_CLR } from "@/misc/tv";

const StyledBorder = styled(Border)`
  padding-bottom: 65%;
`;

export default function PlaybillPrev({
  id,
  dir,
  photo,
  title,
  source,
  premiere,
  textclr,
  tagClr = {
    background: GREEN_CLR,
    color: "#FFF",
    bordercolor: GREEN_CLR,
  },
}: IPlaybill) {
  const [hover, setHover] = useState<boolean>(false);

  return (
    <StyledLink href={`/playbill/${id}`}>
      <StyledBorder
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        <BackgroundImage url={`${dir}/${photo}`} hover={hover} />
        <Overlay hover={hover} />
        <Caption spacing={1}>
          <TagButton background={tagClr?.background} size="small">
            {/* {source} */}
            Русдрам
          </TagButton>
          <Title3 variant="h5" sx={{ color: "#fff !important" }}>
            {title}
          </Title3>
          <Box sx={{ color: "#efefef", fontSize: "13px" }}>
            {premiere && (
              <span>{new Date(premiere).toISOString().slice(0, 10)}</span>
            )}
          </Box>
        </Caption>
      </StyledBorder>
    </StyledLink>
  );
}
