"use client";

import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "@/lib";
import { ReduxState } from "@/lib";
import { useIsMount } from "@/misc/tools";
import { logout, setAuth } from "@/lib/authSlice";
import axios from "axios";
import { setField } from "@/lib/formSlice";
import { setTheme } from "@/lib/themeSlice";
import { authorizeFapi } from "@/lib/miscSlice";

const HOST = process.env.NEXT_PUBLIC_HOST;
const ACCESS_TOKEN = process.env.NEXT_PUBLIC_ACCESS_TOKEN;

export default function ThinkTank() {
  const { name, avatar, category } = useAppSelector(
    (state: ReduxState) => state.auth
  );
  const dispatch = useAppDispatch();
  const isMount = useIsMount();

  useEffect(() => {
    const clientToken = localStorage.getItem("token");
    const clientAvatar = localStorage.getItem("avatar");
    const clientName = localStorage.getItem("name");
    const clientRange = localStorage.getItem("category");
    const clientId = localStorage.getItem("id");
    const clientTheme = localStorage.getItem("theme");

    if (clientTheme?.length) dispatch(setTheme(clientTheme));

    if (clientRange === "google" && clientId?.length) {
      // Проверка, не забанен ли пользователь (или вообще удален):
      axios
        .post(
          `${HOST}/api/pub/scheck/${clientId}`,
          {},
          {
            headers: {
              "Content-Type": "application/json",
              // "auth-token": clientToken,
            },
          }
        )
        .then(async (res) => {
          const response = await fetch(
            `https://graph.facebook.com/${clientId}?fields=name,picture&access_token=${ACCESS_TOKEN}`
          );
          const data = await response.json();
          await dispatch(
            setAuth({
              avatar: data.picture.data.url,
              category: "google",
              name: data.name,
              id: clientId,
            })
          );
          // А ЭТО ПРАВИЛЬНО???
          await dispatch(
            setField({ field: "root", value: `userid${clientId}` })
          );
        })
        .catch((err) => {
          console.log(err);
          // alert("Incorrect login or password");
          dispatch(logout());
        });
    } else if (clientRange?.length && clientId?.length && clientToken?.length) {
      axios
        .post(
          `${HOST}/api/pub/check/${clientId}`,
          {},
          {
            headers: {
              "Content-Type": "application/json",
              "auth-token": clientToken,
            },
          }
        )
        .then(async (res) => {
          await dispatch(
            setAuth({
              ...res.data,
              // avatar: `${HOST}/dist/${res.data.dir}/${res.data.photos[0]}`,
              avatar: res.data.photos
                ? `${HOST}/dist/${res.data.dir}/${res.data.photos[0]}`
                : `${HOST}/dist/common/avatar.png`,
              name: res.data.title,
              token: clientToken,
              category: res.data.category,
            })
          );
          await dispatch(
            setField({ field: "root", value: `userid${clientId}` })
          );
          await dispatch(authorizeFapi());
        })
        .catch((err) => {
          console.log(err);
          // alert("Incorrect login or password");
          dispatch(logout());
        });
    }
  }, []);

  return <></>;
}
