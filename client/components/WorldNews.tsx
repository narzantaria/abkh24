"use client";

import React from "react";
import SectionTitle from "./SectionTitle";
import { XContainer } from "./StyledUI";
import { Box, Grid } from "@mui/material";
import { XLS } from "@/misc/tv";
import Prev from "./Prev";
import { IPost } from "@/misc/types";
import MainSider from "./MainSider";
import ThemeWrapper from "./ThemeWrapper";
import { tagColor } from "@/misc/tools";

const HOST = process.env.NEXT_PUBLIC_HOST;

export default function WorldNews({ data }: { data: IPost[] }) {
  if (data) {
    return (
      <Box sx={{ pb: XLS }}>
        <XContainer>
          <Grid container spacing={3}>
            <Grid item xs={12} md={8}>
              <SectionTitle first="Новости" second="мира" />
              <ThemeWrapper>
                <Grid container spacing={2}>
                  {data.map((x) => (
                    <Grid item xs={12} md={6}>
                      <Prev {...x} key={x.id} tagClr={tagColor("новости мира")} />
                    </Grid>
                  ))}
                </Grid>
              </ThemeWrapper>
            </Grid>
            <Grid item xs={12} md={4}>
              <MainSider />
            </Grid>
          </Grid>
        </XContainer>
      </Box>
    );
  } else return null;
}
