"use client";

import { GREEN_CLR, TRANS_EASE_IN_OUT } from "@/misc/tv";
import { styled } from "@mui/material";
import React from "react";

const RawData = styled("div")`
  cursor: pointer;
  width: fit-content;
  height: 26px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  &:hover * {
    background-color: ${GREEN_CLR};
  }
  & > * {
    transition: ${TRANS_EASE_IN_OUT};
    width: 26px;
    height: 4px;
    margin-bottom: 5px;
    background-color: #888;
  }
  & > *:first-of-type {
  }
  & > *:last-child {
    margin: 0;
    width: 20px;
  }
`;

export default function Hamburger() {
  return (
    <RawData>
      <div></div>
      <div></div>
      <div></div>
    </RawData>
  );
}
