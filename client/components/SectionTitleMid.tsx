"use client";

import React from "react";
import { Box, Typography, styled } from "@mui/material";
import { MS } from "@/misc/tv";
import { ReduxState, useAppSelector } from "@/lib";

const MainWrapper = styled(Typography)`
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: center;
  margin-bottom: 25px;
  position: relative;
  width: 100%;
  font-size: 17px;
  font-weight: 700;
  text-transform: uppercase;
  &:before,
  &:after {
    content: "";
    margin-top: 1px;
    height: 1px;
    background-color: #e2e2e2;
    flex: 1 1 auto;
  }
  &:before {
    margin-left: 0;
    margin-right: ${MS};
  }
  &:after {
  }
`;

export default function SectionTitleMid({
  first,
  second,
}: {
  first: string;
  second: string;
}) {
  const { color } = useAppSelector((state: ReduxState) => state?.theme);

  return (
    <Box component="span" sx={{ color: color === "dark" ? "#fff" : "initial" }}>
      <MainWrapper variant="h2">{`${first}${second ? " " + second : ""}`}</MainWrapper>
    </Box>
  );
}
