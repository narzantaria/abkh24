"use client"

import { LS } from "@/misc/tv";
import { Box, Skeleton, Stack } from "@mui/material";
import React from "react";
import { v4 as uuidv4 } from "uuid";
import { XContainer } from "./StyledUI";

const data = Array.from({ length: 5 }).map(() => uuidv4());

export default function NodataLines() {
  return (
    <Box sx={{ paddingBlock: LS }}>
      <XContainer>
        <Stack spacing={3}>
          {data.map((x) => (
            <Skeleton
              variant="rectangular"
              height={20}
              sx={{ backgroundColor: "#777" }}
              key={x}
            />
          ))}
        </Stack>
      </XContainer>
    </Box>
  );
}
