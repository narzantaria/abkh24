import { GREEN_CLR } from "@/misc/tv";
import { styled } from "@mui/material";
import React, { useState } from "react";
import Carousel from "react-material-ui-carousel";
import { FcNext, FcPrevious } from "react-icons/fc";

const settings: {[key: string]: any} = {
  autoPlay: false,
  animation: "slide",
  duration: 500,
  slidesToScroll: 1,
};

const SliderWrapper = styled('div')`
polygon {fill: #fff !important;}
`

export default function PhotoSlider2({ images }: { images: string[] }) {
  const [hover, setHover] = useState<boolean>(false);
  return (
    <SliderWrapper
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <Carousel
        {...settings}
        navButtonsAlwaysVisible={true}
        navButtonsProps={{
          // Change the colors and radius of the actual buttons. THIS STYLES BOTH BUTTONS
          style: {
            backgroundColor: GREEN_CLR,
            transition: ".3s",
            visibility: hover ? "visible" : "hidden",
            opacity: hover ? 1 : 0,
            color: '#fff',
            top: 'calc(50% - 46px) !important'
          },
        }}
        PrevIcon={
          <FcPrevious />
        }
        NextIcon={
          <FcNext />
        }
      >
        {images.map((x) => (
          <img src={x} alt="" style={{ maxWidth: "100%" }} />
        ))}
      </Carousel>
    </SliderWrapper>
  );
}
