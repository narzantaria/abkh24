"use client";

import React, { Fragment, useState } from "react";
import { styled, Box, Typography, useMediaQuery } from "@mui/material";
import {
  BackgroundImage,
  Border,
  Caption,
  Overlay,
  StyledLink,
  TagButton,
  Title1,
  Title4,
} from "./StyledUI";
import { IPost } from "@/misc/types";
import LocalVideoPrev from "./LocalVideoPrev";
import { useRouter } from "next/navigation";
import { useAppDispatch } from "@/lib";
import { setTags } from "@/lib/tagsSlice";
import Link from "next/link";

const StyledBorder = styled(Border)(({matches}:{matches: boolean})=>`
height: ${matches ? "333px" : "550px"};
`)

const PrevTitle = styled(Typography)`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 3;
`;

interface IProps extends IPost {
  params: string[];
}

export default function PrevBigAlt({
  author,
  tags,
  createdAt,
  dir,
  id,
  photos,
  title,
  tagClr,
  video,
  params,
}: IProps) {
  const [hover, setHover] = useState<boolean>(false);
  const matches = useMediaQuery("(max-width:576px)");
  const router = useRouter();
  const dispatch = useAppDispatch();

  return (
    <StyledLink href={`/news/${id}`}>
      <StyledBorder
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        matches={matches}
      >
        {photos?.length ? (
          <BackgroundImage url={`${dir}/${photos[0]}`} hover={hover} />
        ) : video ? (
          <LocalVideoPrev dir={dir} video={video} />
        ) : (
          <BackgroundImage url="common/main.jpg" hover={hover} />
        )}
        {/* {video ? <LocalVideoPrev dir={dir} video={video} /> : null} */}
        <Overlay hover={hover} />
        <Caption spacing={2}>
          <TagButton
            background={tagClr?.background}
            bordercolor={tagClr?.bordercolor}
            txtcolor={tagClr?.color}
            size="small"
          >
            {tags[0]}
          </TagButton>
          {matches ? (
            <Title4 variant="h5">{title}</Title4>
          ) : (
            <Title1 variant="h5">{title}</Title1>
          )}
          <Box sx={{ color: "#efefef", fontSize: "13px" }}>
            {author && (
              <Fragment>
                <span>{author[0].name}</span> -{" "}
              </Fragment>
            )}
            <span>{new Date(createdAt).toISOString().slice(0, 10)}</span>
          </Box>
        </Caption>
      </StyledBorder>
    </StyledLink>
  );
}
