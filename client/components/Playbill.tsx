"use client";

import React from "react";
import SectionTitle from "./SectionTitle";
import { XContainer } from "./StyledUI";
import { Box, Grid } from "@mui/material";
import { LS } from "@/misc/tv";
import { IPlaybill } from "@/misc/types";
import PlaybillPrev from "./PlaybillPrev";

export default function Playbill({ data }: { data: IPlaybill[] }) {
  return (
    <Box sx={{ paddingBlock: LS }}>
      <XContainer>
        <SectionTitle first="Афиша" decorated />
        <Grid container spacing={3}>
          {data.map((x) => (
            <Grid item xs={12} md={4}>
              <PlaybillPrev key={x.id} {...x} />
            </Grid>
          ))}
        </Grid>
      </XContainer>
    </Box>
  );
}
