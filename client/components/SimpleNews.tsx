"use client";

import React, { useEffect } from "react";
import SectionTitle from "./SectionTitle";
import { XContainer } from "./StyledUI";
import { Box, Grid, useMediaQuery } from "@mui/material";
import { LS } from "@/misc/tv";
import { tagColor } from "@/misc/tools";
import { IPost } from "@/misc/types";
import ThemeWrapper from "./ThemeWrapper";
import PrevAlt from "./PrevAlt";

interface IData {
  [key: string]: IPost[];
}

export default function SimpleNews({ data }: { data: IPost[] }) {
  const matches = useMediaQuery("(max-width:900px)");

  if (data) {
    return (
      <Box sx={{ paddingBlock: LS }}>
        <XContainer>
          <SectionTitle first="Свежие" second="новости" />
          <ThemeWrapper>
            <Grid container spacing={{ xs: 0, md: 2 }}>
              {/* <Grid item xs={12} md={4}>
                <PrevAlt
                  params={["новости мира"]}
                  tagClr={tagColor("новости мира")}
                  {...data.world[0]}
                />
              </Grid>              
              <Grid item xs={12} md={4}>
                <PrevAlt
                  params={["АМРА-life"]}
                  tagClr={tagColor("АМРА-life")}
                  {...data.amra[0]}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <PrevAlt
                  params={["Республика"]}
                  tagClr={tagColor("Республика")}
                  {...data.resp[0]}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <PrevAlt
                  params={["Народный фронт"]}
                  tagClr={tagColor("Народный фронт")}
                  {...data.nf[0]}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <PrevAlt
                  params={["МВД Абхазии"]}
                  tagClr={tagColor("МВД Абхазии")}
                  {...data.mvd[0]}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <PrevAlt
                  params={["ИБ Пятнашка"]}
                  tagClr={tagColor("ИБ Пятнашка")}
                  {...data.ibp[0]}
                />
              </Grid> */}
              {data
                .filter((x, i) => {
                  if ((matches && i <= 5) || !matches) return x;
                })
                .map((x) => (
                  <Grid item xs={12} md={4}>
                    <PrevAlt params={[]} tagClr={tagColor(x.tags[0])} {...x} />
                  </Grid>
                ))}
            </Grid>
          </ThemeWrapper>
        </XContainer>
      </Box>
    );
  } else return null;
}
