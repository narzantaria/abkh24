"use client";

import { Box, Stack, styled } from "@mui/material";
import React from "react";
import { Title4 } from "./StyledUI";

const HOST = process.env.NEXT_PUBLIC_HOST;

const StyledStack = styled(Stack)`
  text-align: center;
`;

const StyledImg = styled("img")`
  max-width: 100%;
  width: 175px;
  filter: invert(60%) sepia(31%) saturate(936%) hue-rotate(115deg)
    brightness(93%) contrast(81%);
`;

interface IProps {
  name: string;
  title: string;
}

export default function GoodsPrev({ name, title }: IProps) {
  return (
    <StyledStack spacing={2}>
      <Box sx={{ textAlign: "center" }}>
        <StyledImg src={`${HOST}/dist/common/${name}.png`} />
      </Box>
      <Title4 variant="h4">{title}</Title4>
    </StyledStack>
  );
}
