"use client";

import React from "react";
import SectionTitle from "./SectionTitle";
import { XContainer } from "./StyledUI";
import { Box, Grid, useMediaQuery } from "@mui/material";
import { XLS } from "@/misc/tv";
import Prev from "./Prev";
import { IBanner, IPost } from "@/misc/types";
import MainSider from "./MainSider";
import ThemeWrapper from "./ThemeWrapper";
import { tagColor } from "@/misc/tools";
import BannerPrev from "./BannerPrev";

const HOST = process.env.NEXT_PUBLIC_HOST;

export default function MiscSection({ data }: { data: IBanner[] }) {
  const matches = useMediaQuery("(max-width:900px)");

  if (data) {
    return (
      <Box sx={{ pb: XLS }}>
        <XContainer>
          <Grid container spacing={3}>
            <Grid item xs={12} md={8}>
              <SectionTitle first="Реклама" />
              <ThemeWrapper>
                <Grid container spacing={2}>
                  {data
                    .filter((x, i) => {
                      if ((matches && i <= 2) || !matches) return x;
                    })
                    .map((x) => (
                      <Grid item xs={12} md={6}>
                        {/* <Prev {...x} key={x.id} tagClr={tagColor("новости мира")} /> */}
                        <BannerPrev {...x} key={x.id} />
                      </Grid>
                    ))}
                </Grid>
              </ThemeWrapper>
            </Grid>
            <Grid item xs={12} md={4}>
              <MainSider />
            </Grid>
          </Grid>
        </XContainer>
      </Box>
    );
  } else return null;
}
