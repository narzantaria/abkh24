"use client";

import { styled } from "@mui/material";
import React from "react";

export const YoutubeWrapper = styled("div")`
  width: 100%;
  margin: 0 auto;
  position: relative;
`;

export const VideoContainer = styled("div")`
  position: relative;
  padding-bottom: 56.25%;
  padding-top: 30px;
  height: 0;
  overflow: hidden;
  iframe,
  object,
  embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
`;

export default function YoutubePlayer({ videoId }: { videoId: string }) {
  return (
    <YoutubeWrapper>
      <VideoContainer>
        <iframe
          width="1250"
          height="703"
          allowFullScreen
          src={`https://www.youtube.com/embed/${videoId}`}
          title="YouTube video player"
          allow="accelerometer; autoplay; clipboard-write;encrypted-media; gyroscope; picture-in-picture"
        />
      </VideoContainer>
    </YoutubeWrapper>
  );
}
