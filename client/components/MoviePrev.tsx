"use client";

import React, { useState } from "react";
import {
  BackgroundImage,
  Border,
  Caption,
  TagButton,
  Turkish,
} from "./StyledUI";
import { styled } from "@mui/material";
import { IMovie } from "@/misc/types";

const StyledBorder = styled(Border)(
  ({ pb }: { pb?: number | null }) => `
  padding-bottom: ${pb || 150}%;
  `,
);

export default function MoviePrev({
  dir,
  id,
  pb,
  photo,
  premiere,
  rating,
  title,
  textclr,
  tagClr = {
    background: "#F86F03",
    color: "#FFF",
    bordercolor: "#F86F03",
  },
  url,
}: IMovie) {
  const [hover, setHover] = useState<boolean>(false);

  return (
    <div
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <a href={url} target="_blank" rel="noopener noreferrer">
        <StyledBorder pb={pb}>
          <BackgroundImage url={`${dir}/${photo}`} hover={hover} />
          <Turkish hover={hover} />
          <Caption>
            <TagButton background={tagClr?.background} size="small">
              {`IMDB: ${rating}`}
            </TagButton>
          </Caption>
        </StyledBorder>
      </a>
    </div>
  );
}
