"use client";

import { IComment, IField } from "@/misc/types";
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Drawer,
  Paper,
  Snackbar,
  Stack,
  Typography,
  styled,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { ActionButton, StyledLink, Title5 } from "./StyledUI";
import { renderDate, useIsMount } from "@/misc/tools";
import { BsChatLeftQuote } from "react-icons/bs";
import { AiOutlineCloseCircle, AiOutlineEdit } from "react-icons/ai";
import { motion } from "framer-motion";
import MainForm from "@/form/MainForm";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { setForm } from "@/lib/formSlice";
import axios from "axios";
import DeleteModal from "./DeleteModal";

const HOST = process.env.NEXT_PUBLIC_HOST;

const fields: IField[] = [
  {
    name: "title",
    label: "Title",
    type: "string",
  },
  {
    name: "text",
    label: "Text",
    type: "text",
  },
  {
    name: "img",
    label: "Image",
    type: "img",
  },
];

const MainWrapper = styled("div")`
  display: grid;
  grid-column-gap: 20px;
  grid-template-columns: 1fr 7fr;
`;

const StyledStack = styled(Stack)`
  * > {
    margin-bottom: initial;
  }
  p {
    margin: 0;
  }
`;

const Remove = styled("div")`
  position: absolute;
  top: 15px;
  right: 15px;
  svg {
    cursor: pointer;
    color: #d83f31;
  }
`;

interface IProps extends IComment {
  client?: string | null;
  callBack?: () => void;
  remove?: () => void;
  answer: () => void;
}

const containerVariants = {
  hidden: { height: 0 },
  visible: { height: "auto", transition: { duration: 0.3 } },
};

export default function Comment({
  callBack,
  client,
  createdAt,
  dir,
  id,
  img,
  text,
  title,
  author,
  remove,
  answer,
}: IProps) {
  const [edit, setEdit] = useState<boolean>(false);
  const { token } = useAppSelector((state: ReduxState) => state.auth);
  const form = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();
  const [message, setMessage] = useState<string>("");
  const [increment, setIncrement] = useState<number>(0);
  const isMount = useIsMount();

  return (
    <Fragment>
      <Card variant="outlined" className="__comment">
        <CardContent sx={{ position: "relative" }}>
          <Stack spacing={1}>
            <MainWrapper>
              <Avatar
                src={`${HOST}/dist/${
                  author?.photos?.length && author.dir
                    ? `${author.dir}/${author.photos[0]}`
                    : "common/avatar.png"
                }`}
                sx={{ width: 64, height: 64 }}
              />
              <StyledStack spacing={1}>
                {author?.title && <Title5 variant="h5">{author.title}</Title5>}
                <Box
                  component="div"
                  sx={{
                    fontStyle: "italic",
                    fontSize: 13,
                    color: "#7D7C7C !important",
                  }}
                >
                  {renderDate(createdAt)}
                </Box>
                {img && (
                  <Box
                    component="img"
                    sx={{ width: 555, maxWidth: "100%" }}
                    src={`${HOST}/dist/${dir}/${img}`}
                  />
                )}
                {text && (
                  <Box
                    component="div"
                    sx={{ fontSize: 13 }}
                    dangerouslySetInnerHTML={{ __html: text }}
                  />
                )}
              </StyledStack>
            </MainWrapper>
          </Stack>
          {author?.id && author.id === client && (
            <Remove>
              <DeleteModal callBack={remove} />
            </Remove>
          )}
        </CardContent>
        <CardActions>
          {client && (
            <ActionButton
              size="small"
              startIcon={<BsChatLeftQuote />}
              onClick={() => answer()}
            >
              Ответить
            </ActionButton>
          )}
          {author?.id && author.id === client && (
            <ActionButton size="small" startIcon={<AiOutlineEdit />}>
              <StyledLink href={`/cabinet/comments/${id}`}>
                Редактировать
              </StyledLink>
            </ActionButton>
          )}
        </CardActions>
      </Card>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
