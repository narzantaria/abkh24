"use client";

import { styled } from "@mui/material";
import React from "react";
import UpperFooter from "./UpperFooter";
import LowerFooter from "./LowerFooter";

const StyledFooter = styled("footer")`
  color: #fff;
  a {
    text-decoration: none;
  }
`;

export default function Basement() {
  return (
    <StyledFooter>
      <UpperFooter />
      <LowerFooter />
    </StyledFooter>
  );
}
