"use client";

import React, { Fragment, useState } from "react";
import {
  BackgroundImage,
  Border,
  StyledLink,
  TagButtonSharp,
  Title3,
  Turkish,
} from "./StyledUI";
import { Box, Grid, Stack, styled } from "@mui/material";
import { IPost } from "@/misc/types";
import LocalVideoPrev from "./LocalVideoPrev";

const StyledBorder = styled(Border)`
  padding-bottom: 65%;
`;

export default function PrevFull({
  author,
  createdAt,
  dir,
  id,
  photos,
  tags,
  title,
  textclr,
  tagClr,
  video,
}: IPost) {
  const [hover, setHover] = useState<boolean>(false);

  return (
    <div>
      <Grid container spacing={1}>
        <Grid item xs={12} md={5}>
          <StyledLink href={`/news/${id}`}>
            <StyledBorder>
              {/* {photos?.length ? (
                <BackgroundImage url={`${dir}/${photos[0]}`} hover={hover} />
              ) : null}
              {video ? <LocalVideoPrev dir={dir} video={video} /> : null} */}
              {photos?.length ? (
                <BackgroundImage url={`${dir}/${photos[0]}`} hover={hover} />
              ) : video ? (
                <LocalVideoPrev dir={dir} video={video} />
              ) : (
                <BackgroundImage url="common/main.jpg" hover={hover} />
              )}
              <Turkish hover={hover} />
              <TagButtonSharp
                background={tagClr?.background}
                bordercolor={tagClr?.bordercolor}
                txtcolor={tagClr?.color}
                size="small"
              >
                {tags[0]}
              </TagButtonSharp>
            </StyledBorder>
          </StyledLink>
        </Grid>
        <Grid item xs={12} md={7}>
          <Stack
            spacing={2}
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
          >
            <Title3 variant="h5">
              <StyledLink href={`/news/${id}`}>{title}</StyledLink>
            </Title3>
            <Box sx={{ fontSize: "13px" }}>
              {author && (
                <Fragment>
                  <span className="__author">{author[0].name}</span> -{" "}
                </Fragment>
              )}
              <time>{new Date(createdAt).toISOString().slice(0, 10)}</time>
            </Box>
          </Stack>
        </Grid>
      </Grid>
    </div>
  );
}
