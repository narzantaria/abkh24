"use client";

import { ReduxState } from "@/lib";
import { useAppSelector } from "@/lib";
import { Box, styled } from "@mui/material";
import React, { ReactNode } from "react";

const StyledBox = styled(Box)(({ thx }: { thx: "light" | "dark" }) => {
  return `
  background-color: ${thx === "light" ? "#fff" : "#151516"};
  `;
});

interface IProps {
  children: ReactNode;
}

export default function MainWrapper({ children }: IProps) {
  const { color } = useAppSelector((state: ReduxState) => state?.theme);

  return (
    <StyledBox component="main" thx={color}>
      {children}
    </StyledBox>
  );
}
