"use client";

import { LS } from "@/misc/tv";
import { Box, Grid, Skeleton, Stack } from "@mui/material";
import React from "react";
import { v4 as uuidv4 } from "uuid";
import { XContainer } from "./StyledUI";

const data = Array.from({ length: 9 }).map(() => uuidv4());

export default function NodataGrid() {
  return (
    <Box sx={{ paddingBlock: LS }}>
      <XContainer>
        <Grid container spacing={{ xs: 0, md: 3 }}>
          {data.map((x) => (
            <Grid item xs={12} md={4} key={x}>
              <Stack spacing={2}>
                <Skeleton
                  variant="rectangular"
                  height={150}
                  sx={{ backgroundColor: "#777" }}
                />
                <Skeleton
                  variant="rectangular"
                  height={20}
                  sx={{ backgroundColor: "#777" }}
                />
              </Stack>
            </Grid>
          ))}
        </Grid>
      </XContainer>
    </Box>
  );
}
