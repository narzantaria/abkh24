"use client";

import React from "react";
import { XDarkIconButton2 } from "./StyledUI";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { setTheme } from "@/lib/themeSlice";
import { FaRegMoon, FaRegSun } from "react-icons/fa";

export default function SwitchTheme() {
  const { color } = useAppSelector((state: ReduxState) => state?.theme);
  const dispatch = useAppDispatch();

  return (
    <XDarkIconButton2
      aria-label="login"
      onClick={() => {
        if (color === "light") {
          dispatch(setTheme("dark"));
        } else {
          dispatch(setTheme("light"));
        }
      }}
    >
      {color === "light" ? <FaRegSun /> : <FaRegMoon />}
    </XDarkIconButton2>
  );
}
