"use client";

import React from "react";
import { Coloured, StyledLink, Title5 } from "./StyledUI";
import { Typography, styled } from "@mui/material";
import { IPost } from "@/misc/types";

const StyledLi = styled("li")`
  position: relative;
  margin-left: 23px;
  padding: 10px 0px;
  color: #555;
  font-size: 12px;
  font-style: italic;
  list-style-type: none;
  &:before {
    position: absolute;
    left: -20px;
    margin-top: 3px;
    font-size: 20px;
    font-weight: normal;
    font-style: normal;
    vertical-align: middle;
    content: "○";
    opacity: 0.7;
  }
`;

const StyledTitle = styled(Typography)`
  font-style: italic;
`;

const StyledTitle2 = styled(Title5)`
  color: #f0f0f0;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
`;

export default function PrevLi({ author, title, id }: IPost) {
  return (
    <StyledLi>
      <StyledLink href={`/news/${id}`}>
        <Coloured>
          {author && <StyledTitle variant="h6">{author[0].name}</StyledTitle>}
        </Coloured>
        <StyledTitle2 variant="h6">{title}</StyledTitle2>
      </StyledLink>
    </StyledLi>
  );
}
