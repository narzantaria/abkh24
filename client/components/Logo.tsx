"use client";

import React from "react";
import { styled } from "@mui/material";

const LogoWrapper = styled("div")`
  cursor: pointer;
  width: 300px;
  img {
    max-width: 100%;
  }
  @media (max-width: 992px) {
    width: 175px;
  }
  @media (max-width: 444px) {
    width: 115px;
  }
`;

export default function Logo() {
  return (
    <LogoWrapper>
      <img src="/logo.png" alt="" />
    </LogoWrapper>
  );
}
