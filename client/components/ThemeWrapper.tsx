"use client";

import { ReduxState, useAppSelector } from "@/lib";
import { Box, styled } from "@mui/material";
import React, { ReactNode } from "react";

interface IProps {
  children: ReactNode;
}

const StyledBox = styled(Box)(({ thx }: { thx: "light" | "dark" }) => {
  return `
  h1, h2, h3, h4, h5, h6, time, .__author, .MuiTypography-body1, .MuiTypography-caption, .MuiChip-label { color: ${
    thx === "light" ? "initial" : "#fff"
  }; }
  `;
});

export default function ThemeWrapper({ children }: IProps) {
  const { color } = useAppSelector((state: ReduxState) => state?.theme);

  return <StyledBox thx={color}>{children}</StyledBox>;
}
