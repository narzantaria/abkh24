"use client"

import { useGetMainDataQuery } from "@/lib/mainApi";
import React from "react";
import NodataLines from "./NodataLines";
import { Box, Stack } from "@mui/material";
import { ColTitle, Title4 } from "./StyledUI";
import { GREEN_CLR } from "@/misc/tv";
import ThemeWrapper from "./ThemeWrapper";

interface IApiItem {
  first: string;
  second: string;
}

const rounded = function (arg: number): number {
  return +arg.toFixed(2);
};

export default function MiscApi() {
  const { data, error, isLoading } = useGetMainDataQuery(null);

  if (data) {
    const weather: IApiItem[] = [
      {
        first: "Температура:",
        second:
          String(rounded(Number(data[0].weather["temp"]) - 273.15)) + " °C",
      },
      {
        first: "Ощущается:",
        second:
          String(rounded(Number(data[0].weather["feels_like"]) - 273.15)) +
          " °C",
      },
      {
        first: "Давление:",
        second:
          String(rounded(Number(data[0].weather["pressure"]) * 0.75)) +
          " мм. рт.ст.",
      },
      {
        first: "Влажность:",
        second: String(rounded(Number(data[0].weather["humidity"]))) + " %",
      },
    ];
    const rawRates = data[0].rates;
    const rates: IApiItem[] = [
      {
        first: "USD/RUB:",
        second: String(
          rounded(Number(data[0].rates["RUB"] / data[0].rates["USD"])),
        ),
      },
      {
        first: "EUR/RUB:",
        second: String(rounded(Number(data[0].rates["RUB"]))),
      },
      {
        first: "BTC/RUB:",
        second: String(
          rounded(Number(data[0].rates["RUB"] / data[0].rates["BTC"])),
        ),
      },
      {
        first: "CNY/RUB:",
        second: String(
          rounded(Number(data[0].rates["RUB"] / data[0].rates["CNY"])),
        ),
      },
      {
        first: "JPY/RUB:",
        second: String(
          rounded(Number(data[0].rates["RUB"] / data[0].rates["JPY"])),
        ),
      },
      {
        first: "TRY/RUB:",
        second: String(
          rounded(Number(data[0].rates["RUB"] / data[0].rates["TRY"])),
        ),
      },
      {
        first: "ILS/RUB:",
        second: String(
          rounded(Number(data[0].rates["RUB"] / data[0].rates["ILS"])),
        ),
      },
    ];

    return (
      <ThemeWrapper>
        <Stack spacing={2}>
          <ColTitle onClick={() => console.log(data[0].rates["RUB"])}>
            Погода в Сухуме
          </ColTitle>
          {weather.map((x) => (
            <ApiItem key={x.first} {...x} />
          ))}
          <ColTitle onClick={() => console.log(data[0].rates["RUB"])}>
            Курсы валют
          </ColTitle>
          {rates.map((x) => (
            <ApiItem key={x.first} {...x} />
          ))}
        </Stack>
      </ThemeWrapper>
    );
  } else return <NodataLines />;
}

function ApiItem({ first, second }: IApiItem) {
  return (
    <Stack spacing={2} direction="row">
      <Box sx={{ fontWeight: 500, color: GREEN_CLR, fontSize: "17px" }}>
        {first}
      </Box>
      <Title4
        variant="body1"
        sx={{ fontWeight: 500, fontStyle: "italic", paddingRight: "5px" }}
      >
        {second}
      </Title4>
    </Stack>
  );
}
