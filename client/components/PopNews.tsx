"use client";

import React from "react";
import SectionTitle from "./SectionTitle";
import { ColTitle, XContainer } from "./StyledUI";
import { Box, Grid, Stack } from "@mui/material";
import { XLS } from "@/misc/tv";
import Prev from "./Prev";
import PrevFull from "./PrevFull";
import { tagColor } from "@/misc/tools";
import PrevMinFull from "./PrevMinFull";
import { IPost } from "@/misc/types";
import ThemeWrapper from "./ThemeWrapper";

const HOST = process.env.NEXT_PUBLIC_HOST;

export default function PopNews({ data }: { data: IPost[] }) {
  if (data) {
    return (
      <Box sx={{ pb: XLS }}>
        <XContainer>
          <ThemeWrapper>
            <Grid container spacing={2}>
              <Grid item xs={12} md={8}>
                <SectionTitle first="Популярные" second="новости" />
                <Stack spacing={2}>
                  {data.slice(1, 5).map((x) => (
                    <PrevFull tagClr={tagColor(x.tags[0])} {...x} key={x.id} />
                  ))}
                </Stack>
              </Grid>
              <Grid item xs={12} md={4}>
                <Stack spacing={3}>
                  <ColTitle>Топ-новость</ColTitle>
                  <Prev tagClr={tagColor(data[0].tags[0])} {...data[0]} />
                  <Stack spacing={3}>
                    {data.slice(5, 9).map((x) => (
                      <PrevMinFull {...x} key={x.id} />
                    ))}
                  </Stack>
                </Stack>
              </Grid>
            </Grid>
          </ThemeWrapper>
        </XContainer>
      </Box>
    );
  } else return null;
}
