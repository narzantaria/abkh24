import * as React from "react";
import Box from "@mui/material/Box";
import Slider from "@mui/material/Slider";
import { useIsMount } from "@/misc/tools";

function valuetext(value: number) {
  return `${value}°C`;
}

interface IProps {
  min: number;
  max: number;
  data: number[];
  callBack: (arg: number[]) => void;
}

export default function RangeSlider({ callBack, data, min, max }: IProps) {
  const isMount = useIsMount();
  const [value, setValue] = React.useState<number[]>(data);

  const handleChange = (event: Event, newValue: number | number[]) => {
    setValue(newValue as number[]);
  };

  React.useEffect(() => {
    if (!isMount) callBack(value);
  }, [value]);

  return (
    <Slider
      getAriaLabel={() => "Temperature range"}
      min={min}
      max={max}
      value={value}
      onChange={handleChange}
      valueLabelDisplay="auto"
      getAriaValueText={valuetext}
    />
  );
}
