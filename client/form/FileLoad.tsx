"use client";

import React, {
  useState,
  Fragment,
  useEffect,
  useRef,
  ReactNode,
  ChangeEvent,
} from "react";
import { AiOutlineCloseCircle, AiOutlineProfile } from "react-icons/ai";
import axios from "axios";
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Checkbox,
  FormControlLabel,
  IconButton,
  Snackbar,
  styled,
  Typography,
} from "@mui/material";
import { Stack } from "@mui/system";
import { useDispatch, useSelector } from "react-redux";
import DeleteModal from "@/components/DeleteModal";
import { ReduxState } from "@/lib";
import { logout } from "@/lib/authSlice";

const NEXT_PUBLIC_HOST = process.env.NEXT_PUBLIC_HOST;

const PhotoIcon = styled(AiOutlineProfile)(() => ({
  visibility: "visible",
  opacity: "1",
  color: "#888",
}));

const StyledCard = styled(Card)(() => ({
  borderRadius: 0,
  boxShadow: "none",
  border: "1px solid #ddd",
  cursor: "pointer",
  svg: {
    color: "brown",
    transition: ".3s ease-in-out",
    visibility: "hidden",
    opacity: 0,
  },
  "&:hover svg": {
    visibility: "visible",
    opacity: 1,
  },
}));

interface IProps {
  callBack?: any;
  children?: ReactNode;
  clearTemp?: boolean;
  root?: string;
  title?: string;
  close?: () => void;
}

const fileExt = (name: string) => {
  const rExp = /\.[0-9a-z]+$/i;
  const result: any = name.match(rExp);
  return result[0];
};

export default function FileLoad({
  callBack,
  children,
  clearTemp = false,
  root = "common",
  title = "Common files",
  close,
}: IProps) {
  const [keep, setKeep] = useState(false);
  const [data, setData] = useState<string[]>([]);
  const [message, setMessage] = useState("");
  const loadRef = useRef<HTMLInputElement>(null);

  const { avatar, token } = useSelector((state: ReduxState) => state.auth);
  const { fapi } = useSelector((state: ReduxState) => state.misc);
  const dispatch = useDispatch();

  useEffect(() => {
    axios
      .get(`${NEXT_PUBLIC_HOST}/api/files`, {
        headers: {
          "Content-Type": "application/json",
          "docimages-root": root,
          "clear-temp": clearTemp,
        },
      })
      .then((res) => setData(res.data.filter((x: string) => x.length < 29))) // скрыть файлы thumb
      .catch((err) => console.log(err));
  }, []);

  function load(e: ChangeEvent<HTMLInputElement>) {
    const formData = new FormData();
    const { files } = e.target;
    if (files) {
      if (files.length > 10) {
        setMessage("Maximum files limit (10) exceeded!");
        return;
      }
      // formData.append("file", files[0]);
      for (let i = 0; i < files.length; i++) {
        formData.append("files", files[i]);
      }
      axios
        .post(`${NEXT_PUBLIC_HOST}/api/files`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            "auth-token": token,
            "docimages-root": root,
            "keep-original": keep,
          },
        })
        .then((res) => {
          setMessage("Document loaded successfully");
          setData((prev) => [...prev, ...res.data.names]);
          setKeep(false);
        })
        .catch((err) => console.log(err));
    }
  }

  return (
    <Fragment>
      <Card>
        <CardHeader
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography>{title || "Files list"}</Typography>
              <IconButton
                aria-label="close"
                onClick={(e) => {
                  e.stopPropagation();
                  close && close();
                }}
              >
                <AiOutlineCloseCircle />
              </IconButton>
            </Box>
          }
          sx={{ borderBottom: "1px solid #ddd" }}
        />
        <CardContent>
          {children}
          <Box
            sx={{
              display: "grid",
              gridTemplateColumns: "repeat(auto-fit, minmax(100px, 200px))",
              gridColumnGap: "10px !important",
              gridRowGap: "10px",
            }}
          >
            {data.map((x: string) => {
              const isImage =
                fileExt(x) == ".jpg" ||
                fileExt(x) == ".jpeg" ||
                fileExt(x) == ".png" ||
                fileExt(x) == ".bmp" ||
                fileExt(x) == ".gif" ||
                fileExt(x) == ".tif";
              return (
                <StyledCard
                  key={x}
                  onClick={() => {
                    if (isImage) {
                      callBack(x);
                    } else {
                      setMessage("The file is not an image");
                    }
                  }}
                >
                  <Box
                    sx={{
                      paddingBottom: "100%",
                      position: "relative",
                    }}
                  >
                    <DeleteModal
                      callBack={() => {
                        axios
                          .delete(
                            `${NEXT_PUBLIC_HOST}/api/${fapi || "files"}`,
                            {
                              headers: {
                                "Content-Type": "application/json",
                                "auth-token": token,
                                filename: x,
                                "docimages-root": root,
                              },
                            }
                          )
                          .then((res) => setData(data.filter((y) => y != x)))
                          .catch((err) => {
                            console.log(err);
                            // logout
                            dispatch(logout());
                          });
                      }}
                      style={{
                        position: "absolute",
                        top: "7px",
                        right: "7px",
                        zIndex: 2,
                      }}
                    />
                    <Box
                      sx={{
                        position: "absolute",
                        height: "86%",
                        width: "86%",
                        left: 0,
                        top: 0,
                        padding: "7%",
                        background: "#ddd",
                      }}
                    >
                      {isImage ? (
                        <Box
                          sx={{
                            height: "100%",
                            transition: "all 0.4s ease-in-out 0s",
                            width: "100%",
                            background: `url(${NEXT_PUBLIC_HOST}/dist${
                              root ? "/" + root + "/" : "/"
                            }${x}) center center / contain no-repeat`,
                          }}
                        />
                      ) : (
                        <Box
                          sx={{
                            color: "#888",
                            height: "100%",
                            textAlign: "center",
                            display: "flex",
                            alignItems: "center",
                            flexDirection: "column",
                            width: "100%",
                            justifyContent: "center",
                            rowGap: "10px",
                            fontSize: "45px",
                          }}
                        >
                          <PhotoIcon />
                          <Typography variant="h5">
                            Not an image type
                          </Typography>
                        </Box>
                      )}
                    </Box>
                  </Box>
                  <CardContent sx={{ padding: "15px !important" }}>
                    <Typography
                      sx={{
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        display: "-webkit-box",
                        WebkitLineClamp: 1,
                        WebkitBoxOrient: "vertical",
                      }}
                    >
                      {x}
                    </Typography>
                  </CardContent>
                </StyledCard>
              );
            })}
          </Box>
        </CardContent>
        <CardActions sx={{ padding: "16px" }}>
          <Stack direction="row" spacing={1}>
            <Button
              variant="contained"
              onClick={() => loadRef.current?.click()}
            >
              Upload
            </Button>
            <FormControlLabel
              control={<Checkbox onChange={(e) => setKeep(!keep)} />}
              label="Keep original"
            />
            <Button
              variant="outlined"
              sx={{ color: "#CC3636", bordercolor: "#CC3636" }}
              onClick={close}
            >
              Close
            </Button>
          </Stack>
        </CardActions>
      </Card>
      <Box sx={{ visibility: "hidden" }}>
        <input
          type="file"
          id="file"
          name="file"
          onChange={load}
          ref={loadRef}
          multiple
        />
      </Box>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}
