"use client";

import {
  Chip,
  FilledInput,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  Paper,
  Stack,
} from "@mui/material";
import React, { useState, ChangeEvent, useEffect } from "react";
import { useIsMount } from "../misc/tools";
import { AiFillPlusCircle } from "react-icons/ai";

interface Iprops {
  callBack: any;
  data?: string[];
}

export default function TagsOld({ callBack, data }: Iprops) {
  const [inputValue, setInputValue] = useState("");
  const [tags, setTags] = useState(data || []);
  const mount = useIsMount();
  useEffect(() => {
    setInputValue("");
    if (!mount) callBack(tags);
  }, [tags]);

  return (
    <Paper variant="outlined" elevation={0} sx={{ p: 2 }}>
      <Stack spacing={2}>
        <FormControl sx={{ width: "45ch" }} variant="filled">
          <InputLabel htmlFor="new tag input">New tag input</InputLabel>
          <FilledInput
            id="new tag"
            value={inputValue}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setInputValue(e.target.value)
            }
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="add new tag"
                  onClick={() => setTags((prev) => [...prev, inputValue])}
                  edge="end"
                >
                  <AiFillPlusCircle />
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        {tags.length && (
          <Stack direction="row" spacing={2}>
            {tags.map((item, index) => {
              // const itemKey: string = uuidv4();
              return (
                <Chip
                  key={item}
                  label={item}
                  onDelete={() =>
                    setTags((prev) => prev.filter((x) => x != item))
                  }
                />
              );
            })}
          </Stack>
        )}
      </Stack>
    </Paper>
  );
}
