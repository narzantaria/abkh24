"use client";

import dynamic from "next/dynamic";
const TextEditorFixed = dynamic(() => import("./TextEditor"), { ssr: false });

export default TextEditorFixed;
