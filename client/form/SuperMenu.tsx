"use client";

import React, { ChangeEvent, Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Slider,
  Snackbar,
  Stack,
  TextField,
} from "@mui/material";
import { useIsMount } from "../misc/tools";
import { capitalize } from "lodash";
import Tree3 from "./Tree3";
import { IField } from "@/misc/types";
import { v4 as uuidv4 } from "uuid";
import TagsOld from "./TagsOld";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { setField } from "@/lib/formSlice";
import { hparse, hstr } from "@/misc/stools";
import RangeSlider from "./RangeSlider";

// Пропс MainForm
interface IProps {
  fields: IField[] | [];
  callBack?: () => void;
}

/**
 * Этот компонент рендерится в цикле. В качестве пропса
 * принимает элемент массива-схемы, загруженной из файла.
 * Если есть данные из сервера, они в сторе formSlice.
 * Оттуда их значения подставляются по имени в RenderItem.
 **/
function RenderItem({ extra, label, name, required, type, value }: IField) {
  const form = useSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();

  return (
    <Fragment>
      {(() => {
        switch (type) {
          case "string":
            return (
              <TextField
                required={required}
                label={label}
                variant="outlined"
                value={value}
                onChange={async (e: ChangeEvent<HTMLInputElement>) => {
                  const detector: string = uuidv4();
                  await dispatch(
                    setField({ field: name, value: e.target.value })
                  );
                  await dispatch(
                    setField({ field: "detector", value: detector })
                  );
                }}
              />
            );

          case "tags":
            return (
              <TagsOld
                data={value}
                callBack={async (arg: string[]) => {
                  const detector: string = uuidv4();
                  await dispatch(setField({ field: name, value: arg }));
                  await dispatch(
                    setField({ field: "detector", value: detector })
                  );
                }}
              />
            );

          case "slider":
            if (extra?.length && form[name]?.length) {
              return (
                <FormControl>
                  <FormLabel>{label}</FormLabel>
                  <Slider
                    min={extra[0]}
                    max={extra[1]}
                    getAriaLabel={() => "Prices range"}
                    value={form[name]}
                    marks={[
                      {
                        value: extra[0],
                        label: (
                          <span
                            style={{
                              left: "-8px",
                              transform: "none",
                              position: "absolute",
                            }}
                          >
                            {extra[0]}
                          </span>
                        ),
                      },
                      {
                        value: extra[1],
                        label: (
                          <span
                            style={{
                              right: "-8px",
                              transform: "none",
                              position: "absolute",
                            }}
                          >
                            {extra[1]}
                          </span>
                        ),
                      },
                    ]}
                    onChange={async (e: Event, newValue: number | number[]) => {
                      const detector: string = uuidv4();
                      await dispatch(
                        setField({ field: name, value: newValue })
                      );
                      await dispatch(
                        setField({ field: "detector", value: detector })
                      );
                    }}
                  />
                </FormControl>
              );
            } else return null;

          case "checkbox":
            return (
              <CheckboxField
                name={name}
                label={label}
                value={value ? hparse(value) : []}
                extra={extra}
                callBack={async (arg: any) => {
                  const detector: string = uuidv4();
                  await dispatch(setField({ field: name, value: hstr(arg) }));
                  await dispatch(
                    setField({ field: "detector", value: detector })
                  );
                }}
              />
            );

          default:
            return <></>;
        }
      })()}
    </Fragment>
  );
}

function CheckboxField({
  label,
  name,
  extra,
  value = [],
  callBack,
}: {
  label?: string;
  name: string;
  extra?: string[];
  value: string[];
  callBack: (arg: any) => void;
}) {
  const [state, setState] = useState<string[]>(value);
  const mount = useIsMount();
  useEffect(() => {
    if (!mount) callBack(state);
  }, [state]);

  return (
    <FormControl>
      <FormLabel
        sx={{
          fontSize: "18px",
          fontWeight: 600,
          marginBottom: "7px",
        }}
      >
        {label}
      </FormLabel>
      <FormGroup row>
        {extra &&
          extra.map((x: string) => (
            <FormControlLabel
              key={x}
              control={
                <Checkbox
                  checked={state.some((y) => y == x)}
                  onChange={(e: ChangeEvent<HTMLInputElement>) => {
                    setState(
                      state.some((y) => y == x)
                        ? state.filter((y) => y != x)
                        : [...state, x]
                    );
                  }}
                />
              }
              label={capitalize(x)}
            />
          ))}
      </FormGroup>
    </FormControl>
  );
}

export default function SuperMenu({ callBack, fields }: IProps) {
  const [message, setMessage] = useState<string>("");

  const state = useAppSelector((state: ReduxState) => state.form);

  if (fields && fields.length) {
    return (
      <Fragment>
        <form style={{ paddingBottom: "45px" }}>
          <Stack spacing={3}>
            {fields
              .filter((x: IField) => x.label && x.type !== "tree")
              .map((field, i: number) => (
                <RenderItem key={i} {...field} value={state[field.name]} />
              ))}
            {fields
              .filter((field) => field.type === "tree")
              .map((field: any) => (
                <Tree3 key={field.name} tree={field.data} fields={fields} />
              ))}
          </Stack>
        </form>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else return <>Loading...</>;
}
