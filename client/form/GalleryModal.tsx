"use client";

import React, { Fragment, useEffect, useState } from "react";
import FileLoad from "./FileLoad";
import { Box, Modal, Snackbar, styled } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { TfiGallery } from "react-icons/tfi";
import { ReduxState } from "@/lib";
import { setField } from "@/lib/formSlice";
import DeleteModal from "@/components/DeleteModal";
import DragDropGrid from "./DragDropGrid";

const NEXT_PUBLIC_HOST = process.env.NEXT_PUBLIC_HOST;

const GalleryWrapper = styled("div")(() => ({
  display: "grid",
  gridTemplateColumns: "repeat(5, 1fr)",
  gridColumnGap: "10px",
  gridRowGap: "10px",
  marginTop: "20px",
}));

const GalleryItem = styled("div")(() => ({
  position: "relative",
  paddingBottom: "100%",
}));

const ImgWrapper = styled("div")(() => ({
  position: "absolute",
  height: "100%",
  width: "100%",
  left: "0px",
  top: "0px",
  border: "1px solid #ddd",
}));

interface ImgProps {
  name: string;
  root: string;
}

const ImgX = styled("div")(({ name, root }: ImgProps) => ({
  height: "100%",
  transition: "all 0.4s ease-in-out 0s",
  width: "100%",
  background: `url(${
    NEXT_PUBLIC_HOST + "/dist/" + root + "/" + name
  }) center center / contain no-repeat`,
}));

const ModalWrapper = styled("div")(() => ({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "100%",
  maxWidth: "1111px",
}));

interface IProps {
  clearTemp?: boolean;
  root?: string;
  callBack?: any;
  name: string;
}

function processArray(arr1: any[], arr2: number[]): any[] | null {
  if (arr2.length > arr1.length) return null;
  if (arr2.reduce((a, b) => (b > a ? b : a)) >= arr2.length) return null;
  const result = [];
  for (let i = 0; i < arr2.length; i++) {
    const index = arr2[i];
    if (index >= 0 && index < arr1.length) {
      result.push(arr1[index]);
    }
  }
  return result;
}

function GalleryModal({
  clearTemp = false,
  root = "common",
  callBack,
  name,
}: IProps) {
  const [open, setOpen] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const state = useSelector((state: ReduxState) => state.form);
  const dispatch = useDispatch();

  const data: string[] = state[name] || [];

  useEffect(() => {
    if (data.length) dispatch(setField({ field: "clearTemp", value: false }));
  }, [data]);

  return (
    <Fragment>
      <Box
        sx={{
          fontSize: "100px",
          lineHeight: 0,
          cursor: "pointer",
          width: "max-content",
          border: "1px solid #ddd",
          borderRadius: "4px",
          overflow: "hidden",
        }}
        onClick={handleOpen}
      >
        <TfiGallery />
      </Box>
      {/* {data && data.length ? (
        <GalleryWrapper>
          {data.map((x: string) => {
            return (
              <GalleryItem key={x}>
                <ImgWrapper>
                  <ImgX name={x} root={state.dir || state.root || "common"} />
                  <Box
                    sx={{
                      position: "absolute",
                      top: "8px",
                      right: "8px",
                      zIndex: 1,
                    }}
                  >
                    <DeleteModal
                      callBack={(_: any) =>
                        dispatch(
                          setField({
                            field: [name],
                            value: data.filter((y: string) => y != x),
                          }),
                        )
                      }
                    />
                  </Box>
                </ImgWrapper>
              </GalleryItem>
            );
          })}
        </GalleryWrapper>
      ) : (
        ""
      )} */}
      {data?.length ? (
        <DragDropGrid
          data={data.map(
            (x: string) =>
              `${NEXT_PUBLIC_HOST}/dist/${
                state.dir || state.root || "common"
              }/${x}`
          )}
          cb={(arg) => {
            const newData = processArray(data, arg);
            if (newData) {
              dispatch(
                setField({
                  field: [name],
                  value: newData,
                })
              );
            }
          }}
          remove={(arg) => {
            // console.log(arg);
            // console.log(data);
            let dataProxy = [...data];
            // console.log(dataProxy);
            dataProxy.splice(arg, 1);
            // console.log(dataProxy);
            dispatch(
              setField({
                field: [name],
                // value: data.filter((y: string, index) => index != arg),
                value: dataProxy,
              })
            );
          }}
        />
      ) : null}
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <ModalWrapper>
          <FileLoad
            callBack={(arg: string) => {
              if (data.some((elem: string) => elem == arg)) {
                setMessage("Изображение уже добавлено");
              } else {
                dispatch(setField({ field: [name], value: [...data, arg] }));
              }
            }}
            root={state.root}
            clearTemp={state.clearTemp}
            title="Upload files"
            close={handleClose}
          />
        </ModalWrapper>
      </Modal>
      <Snackbar
        open={Boolean(message.length)}
        autoHideDuration={3000}
        onClose={() => setMessage("")}
        message={message}
      />
    </Fragment>
  );
}

export default GalleryModal;
