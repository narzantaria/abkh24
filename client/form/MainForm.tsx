"use client";

/**
 * Мне не удалось решить одну проблему в этом компоненте. Когда RenderItem и Tree
 * получают параметр key, то весь компонент начинает просто гнать. Неохота сейчас
 * вникать в эту философию, и я попросту убрал этот параметр
 **/
import React, {
  ChangeEvent,
  FormEvent,
  Fragment,
  useEffect,
  useState,
} from "react";
import { useSelector } from "react-redux";
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  SelectChangeEvent,
  Snackbar,
  Stack,
  Switch,
  TextField,
} from "@mui/material";
import ImageModal from "./ImageModal";
import DatePicker from "./DatePicker";
import GalleryModal from "./GalleryModal";
import Vendor from "./Vendor";
import { useIsMount } from "../misc/tools";
import { capitalize } from "lodash";
import { XButton5 } from "@/components/StyledUI";
import Tree3 from "./Tree3";
import VideoModal from "./VideoModal";
import Tags from "./Tags";
import TextEditorFixed from "./TextEditorFixed";
import { IField, IForm } from "@/misc/types";
import { ReduxState, useAppDispatch, useAppSelector } from "@/lib";
import { setField, updateForm } from "@/lib/formSlice";
import { hparse, hstr } from "@/misc/stools";
import Colors from "./Colors";

const NEXT_PUBLIC_HOST = process.env.NEXT_PUBLIC_HOST;

// Поле схемы
interface IFormItem {
  label?: string;
  name: string;
  type: string;
  value: any;
  required?: boolean;
  unique?: boolean;
  extra?: any[];
  data?: any[];
}

// Пропс MainForm
interface IProps {
  fields: IField[];
  callBack?: () => void;
  nopad?: boolean;
  btntext?: string;
  nobutton?: boolean;
}

// Ветвь дерева
interface IBranch {
  name: string;
  branch: string;
  children: any[];
}

// Создает случайный пароль
function newPassword(): string {
  const chars =
    "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const passwordLength = 10;
  let result = "";
  for (let i = 0; i <= passwordLength; i++) {
    const randomNumber = Math.floor(Math.random() * chars.length);
    result += chars.substring(randomNumber, randomNumber + 1);
  }
  return result;
}

/**
 * Этот компонент рендерится в цикле. В качестве пропса
 * принимает элемент массива-схемы, загруженной из файла.
 * Если есть данные из сервера, они в сторе formSlice.
 * Оттуда их значения подставляются по имени в RenderItem.
 **/
function RenderItem({ extra, label, name, required, type, value }: IFormItem) {
  const form = useSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();

  return (
    <Fragment>
      {(() => {
        switch (type) {
          case "string":
          case "email":
          case "youtube":
          case "rutube":
            return (
              <TextField
                required={required}
                label={label}
                variant="outlined"
                value={value}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  dispatch(setField({ field: name, value: e.target.value }))
                }
              />
            );

          case "switch":
            return (
              <FormControlLabel
                control={
                  <Switch
                    checked={value}
                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                      dispatch(
                        setField({ field: name, value: e.target.checked })
                      )
                    }
                  />
                }
                label={label}
              />
            );

          case "int":
            return (
              <TextField
                label={label}
                variant="outlined"
                type="number"
                inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }}
                value={value}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  dispatch(setField({ field: name, value: e.target.value }))
                }
              />
            );

          case "double":
            return (
              <TextField
                label={label}
                variant="outlined"
                type="number"
                inputProps={{ inputMode: "decimal", pattern: "d*" }}
                value={value}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  dispatch(setField({ field: name, value: e.target.value }))
                }
              />
            );

          case "description":
            return (
              <Box sx={{ borderRadius: "4px", overflow: "hidden" }}>
                <TextField
                  label={label}
                  multiline
                  rows={4}
                  value={value}
                  variant="filled"
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    dispatch(setField({ field: name, value: e.target.value }))
                  }
                  sx={{ width: "100%" }}
                />
              </Box>
            );

          case "text":
            return (
              <TextEditorFixed
                data={value}
                callBack={(arg: any) => {
                  dispatch(setField({ field: name, value: arg }));
                }}
              />
            );

          case "tags":
            if (extra?.length) {
              return (
                <FormControl>
                  <FormLabel>{label}</FormLabel>
                  <Tags
                    name={name}
                    data={value}
                    extra={extra}
                    callBack={(arg: string[]) => {
                      dispatch(setField({ field: name, value: arg }));
                    }}
                  />
                </FormControl>
              );
            } else return null;

          case "img":
            return (
              <FormControl>
                <FormLabel>{label}</FormLabel>
                <ImageModal name={name} />
              </FormControl>
            );

          case "gallery":
            return (
              <FormControl>
                <FormLabel>{label}</FormLabel>
                <GalleryModal name={name} />
              </FormControl>
            );

          case "video":
            return (
              <FormControl>
                <FormLabel>{label}</FormLabel>
                <VideoModal name={name} />
              </FormControl>
            );

          case "color":
            if (extra?.length && label) {
              return (
                <FormControl fullWidth>
                  <InputLabel>{label}</InputLabel>
                  <Colors
                    value={value}
                    extra={extra}
                    label={label}
                    callBack={(arg) => {
                      dispatch(
                        setField({ field: name, value: arg })
                      );
                    }}
                  />
                </FormControl>
              );
            } else return null;

          case "select":
            return (
              <FormControl fullWidth>
                <InputLabel>{label}</InputLabel>
                <Select
                  value={value}
                  label={label}
                  onChange={(e: SelectChangeEvent) => {
                    dispatch(setField({ field: name, value: e.target.value }));
                  }}
                >
                  {extra?.length &&
                    extra.map((item: string, index: number) => {
                      // const itemKey: string = uuidv4();
                      return (
                        <MenuItem value={item} key={index}>
                          {capitalize(item)}
                        </MenuItem>
                      );
                    })}
                </Select>
              </FormControl>
            );

          case "radio":
            return (
              <FormControl>
                <FormLabel id="demo-row-radio-buttons-group-label">
                  {label}
                </FormLabel>
                <RadioGroup
                  row
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  name="row-radio-buttons-group"
                  value={value}
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    dispatch(setField({ field: name, value: e.target.value }))
                  }
                >
                  {extra?.length &&
                    extra.map((item: string, index: number) => {
                      // const itemKey: string = uuidv4();
                      return (
                        <FormControlLabel
                          key={index}
                          value={item}
                          control={<Radio />}
                          label={item}
                        />
                      );
                    })}
                </RadioGroup>
              </FormControl>
            );

          case "date":
            return (
              <DatePicker
                date={value}
                callBack={(arg: any) => {
                  dispatch(setField({ field: name, value: arg }));
                }}
              />
            );

          case "phone":
            return (
              <Phone
                name={name}
                label={label}
                value={value ? hparse(value) : null}
                extra={extra}
                callBack={(arg: any) =>
                  dispatch(setField({ field: name, value: hstr(arg) }))
                }
              />
            );

          case "vendor":
            return (
              <Vendor
                label={label}
                value={value}
                callBack={(arg: any) => {
                  dispatch(setField({ field: name, value: arg }));
                }}
              />
            );

          case "password":
            return (
              <TextField
                label={label}
                variant="outlined"
                type="password"
                value={value}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  dispatch(setField({ field: name, value: e.target.value }))
                }
              />
            );

          case "checkbox":
            return (
              <CheckboxField
                name={name}
                label={label}
                value={value ? hparse(value) : []}
                extra={extra}
                callBack={(arg: any) =>
                  dispatch(setField({ field: name, value: hstr(arg) }))
                }
              />
            );

          default:
            return <></>;
        }
      })()}
    </Fragment>
  );
}

function CheckboxField({
  label,
  name,
  extra,
  value = [],
  callBack,
}: {
  label?: string;
  name: string;
  extra?: string[];
  value: string[];
  callBack: (arg: any) => void;
}) {
  const [state, setState] = useState<string[]>(value);
  const mount = useIsMount();
  useEffect(() => {
    if (!mount) callBack(state);
  }, [state]);

  return (
    <FormControl>
      <FormLabel>{label}</FormLabel>
      <FormGroup row>
        {extra &&
          extra.map((x: string) => (
            <FormControlLabel
              key={x}
              control={
                <Checkbox
                  checked={state.some((y) => y == x)}
                  onChange={(e: ChangeEvent<HTMLInputElement>) => {
                    setState(
                      state.some((y) => y == x)
                        ? state.filter((y) => y != x)
                        : [...state, x]
                    );
                  }}
                />
              }
              label={capitalize(x)}
            />
          ))}
      </FormGroup>
    </FormControl>
  );
}

interface Country {
  name: string;
  code: string;
  flag: string;
  value: string;
}

function Phone({
  label,
  name,
  extra,
  value,
  callBack,
}: {
  label?: string;
  name: string;
  extra?: Country[];
  value: any; //.........
  callBack: (arg: any) => void;
}) {
  const [state, setState] = useState<Country>(
    value
      ? value
      : {
          name: extra![0].name, // ЗАПРЕЩЕННЫЙ ПРИЕМ!!!
          code: extra![0].code,
          flag: extra![0].flag,
          value: "",
        }
  );
  const mount = useIsMount();
  useEffect(() => {
    if (!mount && state.code && state.value) callBack(state);
  }, [state]);

  return (
    <FormControl>
      <FormLabel>{label}</FormLabel>
      {extra && (
        <Box
          sx={{
            display: "grid",
            gridColumnGap: "10px",
            gridTemplateColumns: "115px auto",
          }}
        >
          <Select
            name={name}
            label="Country"
            value={extra.findIndex((z) => z.name == state.name).toString()}
            onChange={(e: SelectChangeEvent) => {
              const itemValue: Country = extra[Number(e.target.value)];
              setState({
                ...state,
                name: itemValue.name,
                code: itemValue.code,
                flag: itemValue.flag,
              });
            }}
          >
            {extra.map((y: Country, i: number) => (
              <MenuItem value={i} key={i}>
                <Box
                  sx={{
                    display: "flex",
                    columnGap: "8px",
                    alignItems: "center",
                  }}
                >
                  <img
                    src={`${NEXT_PUBLIC_HOST}/dist/common/${y.flag}`}
                    alt={y.name}
                    width={35}
                  />
                  {y.code}
                </Box>
              </MenuItem>
            ))}
          </Select>
          <TextField
            label={label}
            variant="outlined"
            value={state.value}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setState({ ...state, value: e.target.value })
            }
          />
        </Box>
      )}
    </FormControl>
  );
}

export default function MainForm({
  callBack,
  fields,
  nopad,
  btntext = "Отправить",
  nobutton,
}: IProps) {
  const isMount = useIsMount();
  const state = useAppSelector((state: ReduxState) => state.form);
  const dispatch = useAppDispatch();
  const [message, setMessage] = useState<string>("");
  const [increment, setIncrement] = useState<number>(0);
  async function submit(e: FormEvent) {
    e.preventDefault();
    if (callBack) {
      const formProxy: IForm = {
        clearTemp: true,
        root: "",
      };
      const keys: string[] = Object.keys(state);
      for (let x = 0; x < keys.length; x++) {
        const element = keys[x];
        if (state[element] != null) formProxy[element] = state[element];
      }
      await dispatch(updateForm(formProxy));
      setIncrement(increment + 1);
    }
  }

  useEffect(() => {
    if (!isMount && callBack) callBack();
  }, [increment]);

  const basicFields = fields.filter((x) => x.label && x.type !== "tree");
  const treeFields = fields.filter(
    (field) => field.type === "tree" && field.data
  );

  if (fields?.length) {
    return (
      <Fragment>
        <form onSubmit={submit} style={nopad ? {} : { paddingBottom: "45px" }}>
          <Stack spacing={3}>
            {basicFields.map((field, i: number) => (
              <RenderItem
                key={field.name}
                {...field}
                value={state[field.name]}
              />
            ))}
            {treeFields.map((field) => (
              // <Tree3 key={field.name} tree={field.data!} fields={fields} /> // ЗАПРЕЩЕННЫЙ ПРИЕМ!!!
              <Fragment>
                {field?.data ? (
                  <Tree3 key={field.name} tree={field.data} fields={fields} />
                ) : null}
              </Fragment>
            ))}
            {!nobutton && (
              <XButton5 type="submit" sx={{ width: "max-content" }}>
                {btntext}
              </XButton5>
            )}
          </Stack>
        </form>
        <Snackbar
          open={Boolean(message.length)}
          autoHideDuration={3000}
          onClose={() => setMessage("")}
          message={message}
        />
      </Fragment>
    );
  } else return <>Loading...</>;
}
