"use client";

import React, { useEffect, useState } from "react";
import dayjs, { Dayjs } from "dayjs";
import {
  DesktopDatePicker,
  LocalizationProvider,
  MobileDatePicker,
} from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { useIsMount } from "../misc/tools";
import { useMediaQuery } from "@mui/material";

const dateFormat = "DD.MM.YYYY";

interface Iprops {
  callBack: any;
  date?: string;
}

export default function DatePicker({ callBack, date }: Iprops) {
  const isMount = useIsMount();
  const [value, setValue] = useState<Dayjs | null>(
    // dayjs(date ? date : new Date().toISOString())
    date ? dayjs(date) : null
  );

  const matches = useMediaQuery("(min-width:768px)");

  //   const handleChange = (newValue: string) => {
  //     setValue(newValue);
  //   };

  useEffect(() => {
    if (!isMount) {
      callBack(dayjs(value).toISOString());
    }
  }, [value]);

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      {matches ? (
        <DesktopDatePicker
          label="Дата"
          value={value}
          onChange={(newValue) => setValue(newValue)}
          // renderInput={(params) => <TextField {...params} />}
        />
      ) : (
        <MobileDatePicker
          label="Дата"
          value={value}
          onChange={(newValue) => setValue(newValue)}
          slotProps={{
            actionBar: {
              // The actions will be the same between desktop and mobile
              actions: ["cancel", "accept"],
            },
          }}
        />
      )}
    </LocalizationProvider>
  );
}
