const path = require("path");

/** @type {import('next').NextConfig} */
const nextConfig = {
  sassOptions: {
    includePaths: [path.join(__dirname, "app")],
  },
  // experimental: {
  //   serverActions: true,
  // },
};

module.exports = nextConfig;