import { imgProps } from '@/misc/stools';
import Videos from '@/wrappers/Videos'
import { Metadata } from 'next';
import React from 'react'

const HOST = process.env.NEXT_PUBLIC_HOST;

export const metadata: Metadata = {
  metadataBase: new URL(String(HOST)),
  title: "Новости ТВ",
  description: "Новости Абхазии, телевидение, АГТРК, Абаза-ТВ, видео",
  alternates: {
    canonical: `${HOST}/videos`,
  },
  openGraph: {
    title: "Новости ТВ",
    description: "Новости Абхазии, телевидение, АГТРК, Абаза-ТВ, видео",
    images: [
      {
        // url: `${HOST}/main.jpg`,
        url: "/dist/common/main.jpg",
        ...imgProps("Новости ТВ"),
      },
    ],
  },
};

export default function Page() {
  return <Videos />
}
