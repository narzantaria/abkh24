import NodataSingle from "@/components/NodataSingle";
import { imgProps } from "@/misc/stools";
import { ITV } from "@/misc/types";
import VideoPage from "@/wrappers/VideoPage";
import { Metadata, ResolvingMetadata } from "next";
import { notFound } from "next/navigation";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { slug: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

export async function generateMetadata(
  { params, searchParams }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const slug = params.slug;

  // fetch data
  // const data: ITV = await fetch(`${HOST}/api/videos/${String(params.slug)}`, {
  //   cache: "no-store",
  // })
  //   .then((res) => res.json())
  //   .catch((err) => {
  //     throw new Error("Failed to fetch data");
  //   });

  const res = await fetch(`${HOST}/api/pub/news/${String(slug)}`, {
    cache: "no-store",
  });

  if (!res.ok || res.status == 404) {
    notFound();
  }

  const data = await res.json();

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  // const content = data[0];

  return {
    metadataBase: new URL(String(HOST)),
    title: data.title ? data.title.slice(0, 50) : "" + "...",
    description: data.title,
    alternates: {
      canonical: `${HOST}/videos/${slug}`,
    },
    openGraph: {
      url: `${HOST}/videos/${slug}`,
      title: data.title ? data.title.slice(0, 50) : "" + "...",
      description: data.title,
      images: [
        {
          // url: `https://i.ytimg.com/vi/${data.videoId}/hqdefault.jpg`,
          url: "/film.png",
          ...imgProps(data.title),
        },
      ],
    },
  };
}

async function getData(slug: string) {
  const res = await fetch(`${HOST}/api/videos/${String(slug)}`, {
    cache: "no-store",
  });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // if (!res.ok) {
  //   // This will activate the closest `error.js` Error Boundary
  //   throw new Error("Failed to fetch data");
  // }

  if (!res.ok || res.status == 404) {
    notFound();
  }

  return res.json();
}

export default async function Page({ params, searchParams }: Props) {
  const { slug } = params;
  const data = await getData(slug);

  if (data) {
    return <VideoPage data={data} />;
  } else return <NodataSingle />;
}
