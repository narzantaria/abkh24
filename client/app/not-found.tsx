"use client"; // Error components must be Client Components

import ErrWrapper from "@/wrappers/ErrWrapper";
import { useEffect } from "react";

export default function Error({
  error,
  reset,
}: {
  error: Error;
  reset: () => void;
}) {
  useEffect(() => {
    // Log the error to an error reporting service
    console.error(error);
  }, [error]);

  return <ErrWrapper title="404 - Страница не найдена" img="obama.gif" />;
}
