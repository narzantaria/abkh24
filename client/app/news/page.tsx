import { imgProps } from "@/misc/stools";
import News from "@/wrappers/News";
import { Metadata } from "next";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

export const metadata: Metadata = {
  metadataBase: new URL(String(HOST)),
  title: "Новости Абхазии, последние новости, новости мира",
  description: "Новости Абхазии, последние новости Абхазии, новости мира, фото и видео",
  alternates: {
    canonical: `${HOST}/news`,
  },
  openGraph: {
    title: "Новости Абхазии, последние новости, новости мира",
    description: "Новости Абхазии, последние новости Абхазии, новости мира, фото и видео",
    images: [
      {
        // url: `${HOST}/main.jpg`,
        url: "/dist/common/main.jpg",
        ...imgProps("Новости Абхазии"),
      },
    ],
  },
};

export default async function Page() {
  return <News />;
}
