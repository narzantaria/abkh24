import NodataSingle from "@/components/NodataSingle";
import { imgProps, processText, trimText } from "@/misc/stools";
import { IPost } from "@/misc/types";
import Post from "@/wrappers/Post";
import { Metadata, ResolvingMetadata } from "next";
import { notFound, redirect } from "next/navigation";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { slug: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

export async function generateMetadata(
  { params, searchParams }: Props,
  parent: ResolvingMetadata
): Promise<Metadata | undefined> {
  // read route params
  const slug = params.slug;

  // fetch data
  const res = await fetch(`${HOST}/api/pub/news/${String(slug)}`, {
    cache: "no-store",
  });

  // if (!res.ok) {
  //   return;
  // }

  if (!res.ok || res.status == 404) {
    notFound();
  }

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  const data = await res.json();
  const content = data[0];

  return {
    metadataBase: new URL(String(HOST)),
    title: content.title,
    description: content.description
      ? content.description
      : content.text
      ? trimText(processText(content.text), 200)
      : content.title,
    alternates: {
      canonical: `${HOST}/news/${slug}`,
    },
    openGraph: {
      url: `${HOST}/news/${slug}`,
      title: content.title,
      description: content.description
        ? content.description
        : content.text
        ? trimText(processText(content.text), 200)
        : content.title,
      images:
        content.photos && content.dir
          ? content.photos.map((x: string) => {
              return {
                url: `/dist/${content.dir}/${x}`,
                ...imgProps(content.title),
              };
            })
          : [
              {
                url: "/dist/common/main.jpg",
                ...imgProps(content.title),
              },
            ],
    },
  };
}

async function getData(slug: string) {
  const res = await fetch(`${HOST}/api/pub/news/${slug}`, {
    cache: "no-store",
  });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // if (!res.ok) {
  //   return;
  // }

  if (!res.ok || res.status == 404) {
    notFound();
  }

  return res.json();
}

export default async function Page({ params, searchParams }: Props) {
  const { slug } = params;
  const data = await getData(slug);

  if (!data) {
    return redirect("/404");
  }

  return <Post data={data[0]} slug={slug} />;
}
