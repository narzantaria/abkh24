import NodataSingle from '@/components/NodataSingle';
import { imgProps } from '@/misc/stools';
import { IPage } from '@/misc/types';
import Misc from '@/wrappers/Misc';
import { Metadata, ResolvingMetadata } from 'next';
import React from 'react'

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { slug: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

async function getData(arg: string) {
  const res = await fetch(`${HOST}/api/sdocs/${String(arg)}`, { cache: "no-store" })
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.
 
  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error('Failed to fetch data')
  }
 
  return res.json()
}

export async function generateMetadata(
  { params, searchParams }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const { slug } = params;

  // fetch data
  const data = await fetch(`${HOST}/api/sdocs/${slug}`, { cache: "no-store" })
    .then((res) => res.json())
    .catch((err) => {
      throw new Error("Failed to fetch data");
    });

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  return {
    metadataBase: new URL(String(HOST)),
    title: data.title,
    description: data.text?.slice(0, 50) + "...",
    alternates: {
      canonical: `${HOST}/misc/${slug}`,
    },
    openGraph: {
      url: `${HOST}/misc/${slug}`,
      title: data.title,
      description: data.text?.slice(0, 50) + "...",
      images: [
        {
          // url: `${HOST}/main.jpg`,
          url: "/dist/common/main.jpg",
          ...imgProps(data.title),
        },
      ],
    },
  };
}

export default async function Page({ params }: { params: { slug: string } }) {
  const data = await getData(params.slug)

  if(data) {
    return (
      <Misc data={data} />
    )
  } else return <NodataSingle />;
}
