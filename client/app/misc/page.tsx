import React from 'react'
import { redirect } from 'next/navigation'

function delay(ms: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

async function waitThreeSeconds() {
  await delay(3000);
  redirect('/');
}

export default async function Page() {
  await waitThreeSeconds();

  return (
    <div id="items" style={{ height: "400px", backgroundColor: "#f0f0f0" }}></div>
  )
}
