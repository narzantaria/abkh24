import { imgProps } from "@/misc/stools";
import Contacts from "@/wrappers/Contacts";
import Register from "@/wrappers/Register";
import { Metadata } from "next";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

export const metadata: Metadata = {
  metadataBase: new URL(String(`${HOST}/contacts`)),
  title: "Контакты",
  description: "Наши контакты, форма обратной связи",
  openGraph: {
    title: "Контакты",
    description: "Наши контакты, форма обратной связи",
    images: [
      {
        url: "/dist/common/main.jpg",
        ...imgProps("Контакты"),
      },
    ],
  },
};

export default function Page() {
  return <Contacts />;
}