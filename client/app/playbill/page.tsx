import NodataGrid from "@/components/NodataGrid";
import { imgProps } from "@/misc/stools";
import Events from "@/wrappers/Events";
import { Metadata } from "next";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

export const metadata: Metadata = {
  metadataBase: new URL(String(HOST)),
  title: "Афиша",
  description:
    "Ближайшие мероприятия в Абхазии, концерты, театр, спектакли, мероприятия",
  alternates: {
    canonical: `${HOST}/playbill`,
  },
  openGraph: {
    title: "Афиша",
    description:
      "Ближайшие мероприятия в Абхазии, концерты, театр, спектакли, мероприятия",
    images: [
      {
        // url: `${HOST}/main.jpg`,
        url: "/dist/common/main.jpg",
        ...imgProps("Афиша"),
      },
    ],
  },
};

async function getData() {
  const res = await fetch(`${HOST}/api/pub/playbills`, { cache: "no-store" });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export default async function Page() {
  const data = await getData();

  if (data) {
    return <Events data={data} />;
  } else return <NodataGrid />;
}
