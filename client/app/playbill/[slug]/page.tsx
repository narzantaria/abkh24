import NodataSingle from "@/components/NodataSingle";
import { imgProps } from "@/misc/stools";
import { IPlaybill } from "@/misc/types";
import EventPage from "@/wrappers/EventPage";
import { Metadata, ResolvingMetadata } from "next";
import { notFound } from "next/navigation";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { slug: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

export async function generateMetadata(
  { params, searchParams }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const slug = params.slug;

  // fetch data
  // const data = await fetch(`${HOST}/api/playbills/${slug}`, {
  //   cache: "no-store",
  // })
  //   .then((res) => res.json())
  //   .catch((err) => {
  //     throw new Error("Failed to fetch data");
  //   });

  // fetch data
  const res = await fetch(`${HOST}/api/pub/news/${String(slug)}`, {
    cache: "no-store",
  });

  if (!res.ok || res.status == 404) {
    notFound();
  }

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  const data = await res.json();
  const content = data;

  return {
    metadataBase: new URL(String(HOST)),
    title: content.title,
    description: content.text?.slice(0, 50) + "...",
    openGraph: {
      url: `${HOST}/news/${slug}`,
      title: content.title,
      description: content.text?.slice(0, 50) + "...",
      images:
        content.photo && content.dir
          ? [
              {
                // url: `${HOST}/dist/${content.dir}/${content.photo}`,
                url: `/dist/${content.dir}/${content.photo}`,
                ...imgProps(content.title || "Афиша"),
              },
            ]
          : [
              {
                // url: `${HOST}/main.jpg`,
                url: "/dist/common/main.jpg",
                ...imgProps(content.title || "Афиша"),
              },
            ],
    },
  };
}

async function getData(slug: string) {
  const res = await fetch(`${HOST}/api/playbills/${String(slug)}`, {
    cache: "no-store",
  });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // if (!res.ok) {
  //   // This will activate the closest `error.js` Error Boundary
  //   throw new Error("Failed to fetch data");
  // }

  if (!res.ok || res.status == 404) {
    notFound();
  }

  return res.json();
}

export default async function Page({ params, searchParams }: Props) {
  const { slug } = params;
  const data = await getData(slug);

  if (data) {
    return <EventPage data={data} />;
  } else return <NodataSingle />;
}
