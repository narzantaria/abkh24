import Index from "@/wrappers/Index";
import Image from "next/image";

export default function Home() {
  return (
    <main>
      <Index />
    </main>
  );
}
