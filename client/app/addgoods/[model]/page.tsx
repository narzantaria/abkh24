import { IFolder } from "@/misc/types";
import AddGoods2 from "@/wrappers/AddGoods2";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

async function getData() {
  const res = await fetch(`${HOST}/api/goods/router`, { cache: "no-store" });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export default async function Page({ params }: { params: { model: string } }) {
  const data: IFolder[] | null = await getData();

  return <AddGoods2 data={data} model={params.model} />;
}
