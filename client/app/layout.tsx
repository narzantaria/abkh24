import { Providers } from "@/lib/providers";
import "./main.scss";
import type { Metadata, ResolvingMetadata } from "next";
import { Inter } from "next/font/google";
import Basement from "@/components/Basement";
import Link from "next/link";
import ThinkTank from "@/components/ThinkTank";
import SuperHeader from "@/components/SuperHeader";
import Header from "@/components/Header";
import { ISettings } from "@/misc/types";
import { imgProps } from "@/misc/stools";
import Up from "@/components/Up";
import GoogleAnalytics from "@/components/GoogleAnalytics";
import YM from "@/components/YM";

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { slug: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

const inter = Inter({ subsets: ["latin"] });

export async function generateMetadata(
  { params, searchParams }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const slug = params.slug;

  // fetch data
  const data: ISettings = await fetch(`${HOST}/api/settings`)
    .then((res) => res.json())
    .catch((err) => {
      throw new Error("Failed to fetch data");
    });

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  return {
    metadataBase: new URL(String(HOST)),
    title: data.title,
    description: data.description,
    openGraph: {
      url: `${HOST}/videos/${slug}`,
      title: data.title,
      description: data.description,
      images: [
        {
          // url: `${HOST}/main.jpg`,
          url: "/dist/common/main.jpg",
          ...imgProps(data.title),
        },
      ],
    },
  };
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <Providers>
      <html lang="en">
        <GoogleAnalytics />        
        <body>
          <YM />
          <ThinkTank />
          <SuperHeader />
          <Header />
          {children}
          <Basement />
          <Up />
        </body>
      </html>
    </Providers>
  );
}
