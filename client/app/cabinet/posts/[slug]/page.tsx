import { IField, IPost } from '@/misc/types';
import EditPost from '@/wrappers/EditPost';
import React from 'react'

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { slug: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

async function getData(slug: string) {
  const res = await fetch(`${HOST}/api/pub/posts/${slug}`, { cache: "no-store" });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch posts data");
  }
  
  const res2 = await fetch(`${HOST}/api/builder/post`, { cache: "no-store" });
  if (!res2.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch builder data");
  }

  const data: IPost[] = await res.json();
  const fields: IField[] = await res2.json();
  const { looks, ...restRes } = data[0];

  return {
    data: restRes,
    fields: fields.filter(
      (x: IField) =>
        x.type !== "relation" && x.type !== "youtube" && x.name !== "looks",
    ),
  }
}

export default async function Page({ params, searchParams }: Props) {
  const data = await getData(params.slug);

  return <EditPost {...data} />
}
