import EditSelf from '@/wrappers/EditSelf'
import React from 'react'

export default function Page() {
  return <EditSelf />
}
