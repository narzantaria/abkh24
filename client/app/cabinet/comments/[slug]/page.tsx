import NodataSingle from "@/components/NodataSingle";
import CommentPage from "@/wrappers/CommentPage";
import axios from "axios";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { slug: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

async function getData(slug: string) {
  const res = await fetch(`${HOST}/api/comments/${String(slug)}`, {
    cache: "no-store",
  });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

// export default async function Page({ params, searchParams }: Props) {
//   const { slug } = params;

//   return <CommentPage slug={slug} />;
// }

export default async function Page({ params, searchParams }: Props) {
  const { slug } = params;
  const data = await getData(slug);

  if (data) {
    return <CommentPage data={data} slug={slug} />;
  } else return <NodataSingle />;
}
