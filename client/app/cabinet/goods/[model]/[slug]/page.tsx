import NodataLines from "@/components/NodataLines";
import { IField, IFolder } from "@/misc/types";
import EditGood from "@/wrappers/EditGood";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { slug: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

const primaryFields: IField[] = [
  {
    name: "title",
    required: true,
    label: "Заголовок",
    type: "string",
  },
  {
    name: "text",
    required: true,
    label: "Текст",
    type: "text",
  },
  {
    name: "name",
    required: true,
    label: "Имя автора",
    type: "string",
  },
  {
    name: "phone",
    required: true,
    label: "Телефон",
    type: "phone",
    extra: [
      { name: "Абхазия", code: "+7", flag: "abkh.png" },
      { name: "Россия", code: "+7", flag: "rus.png" },
    ],
  },
  {
    name: "email",
    label: "Email",
    type: "email",
  },
  {
    name: "photos",
    label: "Фотографии",
    type: "gallery",
  },
  {
    name: "sold",
    label: "Продано",
    type: "switch",
  },
];

async function getData(model: string, slug: string) {
  const res1 = await fetch(`${HOST}/api/pub/goods/${model}/${slug}`, {
    cache: "no-store",
  });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res1.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  const data = await res1.json();

  const res2 = await fetch(`${HOST}/api/goods/router`, { cache: "no-store" });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res2.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  const rawMenu: IFolder[] | null = await res2.json();
  const fields: IField[] | null =
    rawMenu
      ?.filter((x) => x.name == model)[0]
      ?.files.filter((x) => x.name == data.sector)[0]
      .data // .filter((x) => x.name !== "title")
      .map((x) => {
        if (x.type === "checkbox") return { ...x, type: "select" };
        if (x.type === "slider") return { ...x, type: "int" };
        if (x.type === "dslider") return { ...x, type: "double" };
        return x;
      }) || null;

  return { data, fields };
}

export default async function Page({
  params,
}: {
  params: { model: string; slug: string };
}) {
  const data = await getData(params.model, params.slug);

  if (data.data && data.fields) {
    return (
      <EditGood
        model={params.model}
        slug={params.slug}
        data={data.data}
        fields={primaryFields.concat(data.fields || [])}
      />
    );
  } else return <NodataLines />;
}
