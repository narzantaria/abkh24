import ErrWrapper from "@/wrappers/ErrWrapper";

export default function page() {
  return <ErrWrapper title="404 - Страница не найдена" img="obama.gif" />;
}