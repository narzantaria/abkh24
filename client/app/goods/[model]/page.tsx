import NodataLines from "@/components/NodataLines";
import { goodsHeader, imgProps } from "@/misc/stools";
import { IFolder } from "@/misc/types";
import GoodsSector from "@/wrappers/GoodsSector";
import type { Metadata, ResolvingMetadata } from "next";
import { notFound } from "next/navigation";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { model: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

async function getData() {
  const res = await fetch(`${HOST}/api/goods/router`);
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // if (!res.ok) {
  //   // This will activate the closest `error.js` Error Boundary
  //   throw new Error("Failed to fetch data");
  // }

  if (!res.ok || res.status == 404) {
    notFound();
  }

  return res.json();
}

export async function generateMetadata(
  { params, searchParams }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const model = params.model;

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  return {
    metadataBase: new URL(String(HOST)),
    title: goodsHeader[model].title,
    description: goodsHeader[model].description,
    alternates: {
      canonical: `${HOST}/api/pub/news/${model}`,
    },
    openGraph: {
      url: `${HOST}/api/pub/news/${model}`,
      title: goodsHeader[model].title,
      description: goodsHeader[model].description,
      images: [
        { 
          // url: `${HOST}/main.jpg`,
          url: "/dist/common/main.jpg",
          ...imgProps(goodsHeader[model].title) 
        },
      ],
    },
  };
}

export default async function Page({ params }: { params: { model: string } }) {
  const data: IFolder[] | null = await getData();

  if(data) {
    return <GoodsSector data={data} model={params.model} />;
  } else return <NodataLines />;
}
