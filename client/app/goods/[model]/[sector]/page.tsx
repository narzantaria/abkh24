import NodataGrid from "@/components/NodataGrid";
import { imgProps } from "@/misc/stools";
import { IField } from "@/misc/types";
import GoodsPage from "@/wrappers/GoodsPage";
import { Metadata, ResolvingMetadata } from "next";
import { notFound } from "next/navigation";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { model: string; sector: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

async function getData(model: string, sector: string) {
  const res = await fetch(`${HOST}/api/goods/router`, { cache: "no-store" });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // if (!res.ok) {
  //   // This will activate the closest `error.js` Error Boundary
  //   throw new Error("Failed to fetch data");
  // }

  if (!res.ok || res.status == 404) {
    notFound();
  }

  const res2 = await fetch(`${HOST}/api/goods/xmenu/${model}/${sector}`, {
    cache: "no-store",
  });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // if (!res2.ok) {
  //   // This will activate the closest `error.js` Error Boundary
  //   throw new Error("Failed to fetch data");
  // }

  if (!res2.ok || res.status == 404) {
    notFound();
  }

  const data = await res.json();
  const xmenu = await res2.json();

  return { data, xmenu };
}

export async function generateMetadata(
  { params, searchParams }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const { model, sector } = params;

  // fetch data
  // const data: IField[] = await fetch(
  //   `${HOST}/api/goods/xmodel/${model}/${sector}`,
  //   {
  //     cache: "no-store",
  //   }
  // )
  //   .then((res) => res.json())
  //   .catch((err) => {
  //     throw new Error("Failed to fetch data");
  //   });

  const res = await fetch(`${HOST}/api/goods/xmodel/${model}/${sector}`, {
    cache: "no-store",
  });

  if (!res.ok || res.status == 404) {
    notFound();
  }

  const data: IField[] = await res.json();

  // console.log(data);

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  return {
    metadataBase: new URL(String(HOST)),
    title: data.filter((x) => x.type === "title")[0].label,
    description: data.filter((x) => x.type === "description")[0].label,
    alternates: {
      canonical: `${HOST}/api/pub/news/${model}/${sector}`,
    },
    openGraph: {
      url: `${HOST}/api/pub/news/${model}/${sector}`,
      title: data.filter((x) => x.type === "title")[0].label,
      description: data.filter((x) => x.type === "description")[0].label,
      images: [
        {
          // url: `${HOST}/main.jpg`,
          url: "/dist/common/main.jpg",
          ...imgProps(data.filter((x) => x.type === "title")[0].label || ""),
        },
      ],
    },
  };
}

export default async function Page({ params }: Props) {
  const data = await getData(params.model, params.sector);

  if (data.data && data.xmenu) {
    return <GoodsPage {...params} {...data} />;
  } else return <NodataGrid />;
}
