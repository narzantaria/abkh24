import NodataSingle from "@/components/NodataSingle";
import { hparse, imgProps } from "@/misc/stools";
import { IField, Item } from "@/misc/types";
import Good from "@/wrappers/Good";
import { Metadata, ResolvingMetadata } from "next";
import { notFound } from "next/navigation";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

type Props = {
  params: { model: string; sector: string; slug: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

async function getData(model: string, slug: string) {
  const res = await fetch(`${HOST}/api/${model}s/${slug}`, { cache: "no-store" });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // if (!res.ok) {
  //   // This will activate the closest `error.js` Error Boundary
  //   throw new Error("Failed to fetch data");
  // }

  if (!res.ok || res.status == 404) {
    notFound();
  }

  const rawData = await res.json();
  const {
    id,
    sector,
    sold,
    dir,
    phone,
    photos,
    price,
    text,
    title,
    createdAt,
    ...data
  } = rawData;
  const parsedPhone = hparse(phone);
  const specialParams = {
    title: title,
    createdAt: createdAt,
    gallery: { dir: dir, photos: photos },
    text: text,
    phone: parsedPhone.code + parsedPhone.value,
    price: price,
  };

  const res2 = await fetch(`${HOST}/api/builder/${model}`, { cache: "no-store" });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // if (!res2.ok) {
  //   // This will activate the closest `error.js` Error Boundary
  //   throw new Error("Failed to fetch data");
  // }

  if (!res2.ok || res.status == 404) {
    notFound();
  }

  const rawFields = await res2.json();
  const fields = rawFields.filter((x: IField) => x.type !== "tree");

  return { fields, data, specialParams };
}

export async function generateMetadata(
  { params, searchParams }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const { model, sector, slug } = params;

  // fetch data
  // const data: Item = await fetch(`${HOST}/api/${model}s/${slug}`, { cache: "no-store" })
  //   .then((res) => res.json())
  //   .catch((err) => {
  //     throw new Error("Failed to fetch data");
  //   });

  const res = await fetch(`${HOST}/api/${model}s/${slug}`);

  if (!res.ok || res.status == 404) {
    notFound();
  }

  const data = await res.json();

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  return {
    metadataBase: new URL(String(HOST)),
    title: data.title,
    description: data.text?.slice(0, 50) + "...",
    alternates: {
      canonical: `${HOST}/api/pub/news/${model}/${sector}/${slug}`,
    },
    openGraph: {
      url: `${HOST}/goods/${model}/${sector}/${slug}`,
      title: data.title,
      description: data.text?.slice(0, 50) + "...",
      images:
        data.photos && data.dir
          ? data.photos.map((x: string) => {
              return {
                url: `${HOST}/dist/${data.dir}/${x}`,
                ...imgProps(data.title),
              };
            })
          : [
              {
                // url: `${HOST}/main.jpg`,
                url: "/dist/common/main.jpg",
                ...imgProps(data.title),
              },
            ],
    },
  };
}

export default async function Page({ params }: Props) {
  const data = await getData(params.model, params.slug);

  if (data.data && data.fields && data.specialParams) {
    return <Good {...params} {...data} />;
  } else return <NodataSingle />;
}
