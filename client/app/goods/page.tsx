import NodataGrid from "@/components/NodataGrid";
import { imgProps } from "@/misc/stools";
import { IFolder, Item } from "@/misc/types";
import GoodsMain from "@/wrappers/GoodsMain";
import type { Metadata } from "next";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

export const metadata: Metadata = {
  title: "Объявления в Абхазии и не только",
  description: "Здесь вы можете разместить бесплатные объявления",
  alternates: {
    canonical: `${HOST}/goods`,
  },
  openGraph: {
    title: "Объявления в Абхазии и не только",
    description: "Здесь вы можете разместить бесплатные объявления",
    images: [
      {
        url: `${HOST}/main.jpg`,
        ...imgProps("Объявления в Абхазии и не только"),
      },
    ],
  },
};

async function getData() {
  const res = await fetch(`${HOST}/api/goods/router`);
  const res2 = await fetch(`${HOST}/api/pub/goods`);
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok || !res2.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  const data = await res.json()
  const goods = await res2.json()

  return {data,goods}
}

export default async function Page() {
  const rawData: {data: IFolder[] | null, goods: Item[] | null} = await getData();
  const {data,goods}=rawData

  if(data && goods) {
    return <GoodsMain data={data} goods={goods} />;
  } else return <NodataGrid />;
}
