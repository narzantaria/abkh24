import NodataSingle from "@/components/NodataSingle";
import { IField } from "@/misc/types";
import AddPost from "@/wrappers/AddPost";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

async function getData() {
  const res = await fetch(`${HOST}/api/builder/post`, {
    cache: "no-store",
  });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  const data = await res.json();

  return data.filter(
    (x: IField) =>
      x.type !== "relation" && x.type !== "youtube" && x.name !== "looks"
  );
}

export default async function Page() {
  const data = await getData();

  if (data) {
    return <AddPost formProps={data} />;
  } else return <NodataSingle />;
}
