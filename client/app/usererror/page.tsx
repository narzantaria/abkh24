import ErrorPage from '@/wrappers/ErrorPage'
import React from 'react'

export default function page() {
  return <ErrorPage />
}
