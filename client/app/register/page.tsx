import { imgProps } from "@/misc/stools";
import Register from "@/wrappers/Register";
import { Metadata } from "next";
import React from "react";

const HOST = process.env.NEXT_PUBLIC_HOST;

export const metadata: Metadata = {
  metadataBase: new URL(String(`${HOST}/register`)),
  title: "Регистрация",
  description: "Регистрация на сайте, создание аккаунта через смс, email",
  alternates: {
    canonical: `${HOST}/register`,
  },
  openGraph: {
    title: "Регистрация",
    description: "Регистрация на сайте, создание аккаунта через смс, email",
    images: [
      {
        // url: `${HOST}/main.jpg`,
        url: "/dist/common/main.jpg",
        ...imgProps("Регистрация"),
      },
    ],
  },
};

export default function Page() {
  return <Register />;
}
