import Funerals from "@/wrappers/Funerals";
import { Metadata } from "next";
import React from "react";

export const metadata: Metadata = {
  title: "Некролог",
  description: "Некролог",
  openGraph: {
    title: "Некролог",
    description: "Некролог"
  },
};

export default function Page() {
  return <Funerals />;
}
