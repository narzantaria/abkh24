import { imgProps } from '@/misc/stools';
import Marketing from '@/wrappers/Marketing';
import { Metadata } from 'next';
import React from 'react'

const HOST = process.env.NEXT_PUBLIC_HOST;

export const metadata: Metadata = {
  metadataBase: new URL(String(HOST)),
  title: "Реклама",
  description: "Реклама в Абхазии, для вашего бизнеса, VIP-объявления, разместить рекламу, баннеры, сотрудничество",
  alternates: {
    canonical: `${HOST}/marketing`,
  },
  openGraph: {
    title: "Реклама",
    description: "Реклама в Абхазии, для вашего бизнеса, VIP-объявления, разместить рекламу, баннеры, сотрудничество",
    images: [
      {
        // url: `${HOST}/main.jpg`,
        url: "/dist/common/main.jpg",
        ...imgProps("Реклама"),
      },
    ],
  },
};

async function getData() {
  const res = await fetch(`${HOST}/api/banners`)
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.
 
  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error('Failed to fetch data')
  }
 
  return res.json()
}

export default async function Page() {
  const data = await getData()

  return <Marketing data={data} />
}
