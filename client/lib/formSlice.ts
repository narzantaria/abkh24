import { IForm } from "@/misc/types";
import { createSlice } from "@reduxjs/toolkit";

const initialState: IForm = {
  clearTemp: true,
  root: "",
  // fapi: "pub/files",
};

export const formSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    setField: (state, action) => {
      state[action.payload.field] = action.payload.value;
    },
    // Не очень мне нравится эта функция, но ничего другого придумать не удалось... с этим реактом...
    removeField: (state, action) => {
      const stateProxy: IForm = {
        // fapi: "pub/files",
        clearTemp: state.clearTemp,
        root: state.root,
      };
      const keys = Object.keys(state);
      for (let y = 0; y < keys.length; y++) {
        if (
          keys[y] != action.payload &&
          keys[y] != "clearTemp" &&
          keys[y] != "root"
        )
          stateProxy[keys[y]] = state[keys[y]];
      }
      return stateProxy;
    },
    setForm: (state, action) => {
      const fields = { ...state, ...action.payload };
      if (action.payload.dir) fields.root = action.payload.dir;
      return fields;
    },
    updateForm: (state, action) => {
      return action.payload;
    },
    // reset: () => initialState,
    reset: () => {
      const clientId = localStorage.getItem("id");
      return {
        ...initialState,
        root: clientId?.length ? `userid${clientId}` : "",
      };
    },
  },
});

// Action creators are generated for each case reducer function
export const { setField, setForm, updateForm, reset, removeField } =
  formSlice.actions;

export default formSlice.reducer;
