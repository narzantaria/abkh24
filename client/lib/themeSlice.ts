import { createSlice } from "@reduxjs/toolkit";

export interface ITheme {
  color: "light" | "dark";
}

const initialState: ITheme = {
  color: "light",
};

export const themeSlice = createSlice({
  name: "theme",
  initialState,
  reducers: {
    setTheme: (state, action) => {
      localStorage.setItem("theme", action.payload);
      state.color = action.payload;
    },
    resetTheme: (state) => {
      localStorage.setItem("theme", "light");
      return initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setTheme, resetTheme } = themeSlice.actions;

export default themeSlice.reducer;
