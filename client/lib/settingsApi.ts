// Общие данные
import { ISettings } from "@/misc/types";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const settingsApi = createApi({
  reducerPath: "settingsApi",
  baseQuery: fetchBaseQuery({ baseUrl: `${process.env.NEXT_PUBLIC_HOST}/api` }),
  endpoints: (builder) => ({
    getSettingsData: builder.query<ISettings, null>({
      query: () => "settings",
    }),
  }),
});

export const { useGetSettingsDataQuery } = settingsApi;
