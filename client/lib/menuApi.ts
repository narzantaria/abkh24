import { IMenuElement } from "@/misc/types";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const menuApi = createApi({
  reducerPath: "menuApi",
  baseQuery: fetchBaseQuery({ baseUrl: `${process.env.NEXT_PUBLIC_HOST}/api` }),
  endpoints: (builder) => ({
    getMenuData: builder.query<IMenuElement[], null>({
      query: () => "menu",
    }),
  }),
});

export const { useGetMenuDataQuery } = menuApi;
