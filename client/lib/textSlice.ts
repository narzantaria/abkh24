import { createSlice } from "@reduxjs/toolkit";

const initialState: { data: string | null } = {
  data: null,
};

export const textSlice = createSlice({
  name: "text",
  initialState,
  reducers: {
    setText: (state, action) => {
      state.data = action.payload;
    },
    clearText: (state) => {
      return initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setText, clearText } = textSlice.actions;

export default textSlice.reducer;
