/**
 * Общий слайс для хранения самого разного.
 * Например загадочный параматр "fapi" - это роут для файловых операций
 * зарегистрированного пользователя, не админа. Возможно - излишнее,
 * надо пересмотреть в будущем.
 **/
import { createSlice } from "@reduxjs/toolkit";

interface IMisc {
  fapi?: string | null;
  [field: string]: any;
}

const initialState: IMisc = {
  fapi: null,
};

export const miscSlice = createSlice({
  name: "misc",
  initialState,
  reducers: {
    setMiscField: (state, action) => {
      state[action.payload.field] = action.payload.value;
    },
    // Не очень мне нравится эта функция, но ничего другого придумать не удалось... с этим реактом...
    removeMiscField: (state, action) => {
      const stateProxy: IMisc = {};
      const keys = Object.keys(state);
      for (let y = 0; y < keys.length; y++) {
        if (keys[y] != action.payload) stateProxy[keys[y]] = state[keys[y]];
      }
      return stateProxy;
    },
    setData: (state, action) => {
      const fields = { ...state, ...action.payload };
      return fields;
    },
    updateData: (state, action) => {
      return action.payload;
    },
    // reset: () => initialState,
    resetData: () => {
      return {};
    },
    authorizeFapi: () => {
      return {
        fapi: "pub/files",
      };
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  removeMiscField,
  resetData,
  setData,
  setMiscField,
  updateData,
  authorizeFapi,
} = miscSlice.actions;

export default miscSlice.reducer;
