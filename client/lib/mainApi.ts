// Общие данные
import { IMain } from "@/misc/types";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const mainApi = createApi({
  reducerPath: "mainApi",
  baseQuery: fetchBaseQuery({ baseUrl: `${process.env.NEXT_PUBLIC_HOST}/api` }),
  endpoints: (builder) => ({
    getMainData: builder.query<IMain[], null>({
      query: () => "pub",
    }),
  }),
});

export const { useGetMainDataQuery } = mainApi;
