// Общие данные
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const tagsApi = createApi({
  reducerPath: "tagsApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.NEXT_PUBLIC_HOST}/api/pub`,
  }),
  endpoints: (builder) => ({
    getTagsData: builder.query<string[], null>({
      query: () => "tags",
      // Pick out data and prevent nested properties in a hook or selector
      transformResponse: (response: [{ unique_tags: string[] }], meta, arg) =>
        response[0].unique_tags,
    }),
  }),
});

export const { useGetTagsDataQuery } = tagsApi;
