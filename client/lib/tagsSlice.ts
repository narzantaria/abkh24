import { createSlice } from "@reduxjs/toolkit";

export interface ITags {
  path: string;
  word: string;
}

const initialState: string[] = [];

export const tagsSlice = createSlice({
  name: "tags",
  initialState,
  reducers: {
    setTags: (state, action) => {
      return action.payload;
    },
    resetTags: (state) => {
      return initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setTags, resetTags } = tagsSlice.actions;

export default tagsSlice.reducer;
